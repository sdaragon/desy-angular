module.exports = {  
  presets: [require('desy-html/config/tailwind.config.js')],  
  content: [    
    "./projects/demo/src/**/*.{html,css,ts}",  
    "./projects/desy-angular/src/**/*.{html,css,ts}"  ],
};
