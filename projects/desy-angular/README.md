# README #

Desy Angular contains the code you need to start building your Angular app for Gobierno de Aragón. You can see the documentation into [https://paega2.atlassian.net/wiki/spaces/AreaUsuariosIntegradores/pages/2996797605/3.-+DESY+-+Sistema+de+dise+o+del+Gobierno+de+Arag+n](https://paega2.atlassian.net/wiki/spaces/AreaUsuariosIntegradores/pages/2996797605/3.-+DESY+-+Sistema+de+dise+o+del+Gobierno+de+Arag+n)

See live examples of Desy Frontend components: [https://desy.aragon.es/desy-angular](https://desy.aragon.es/desy-angular)

This project is built upon the Desy Frontend project: [https://desy.aragon.es/](https://desy.aragon.es/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18


### How to set up? ###
This guide explains how to setup your Angular project to begin using Desy Angular.
* Run `npm install desy-angular` 
* Dependencies: Node.js v18.20.0, Angular 18

### Contact the team ###

* Desy Angular is maintained by a team at Servicios Digitales de Aragón (Spain).

### License ###
Unless stated otherwise, the codebase is released under the [EUPL-1.2 License](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12). This covers both the codebase and any sample code in the documentation.