import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DesyCommonsModule } from '../desy-commons/desy-commons.module';
import { SharedModule } from '../shared/shared.module';

import { DesyButtonsModule } from '../desy-buttons/desy-buttons.module';
import { BreadcrumbsItemComponent } from './components/breadcrumbs/breadcrumbs-item/breadcrumbs-item.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { ErrorSummaryItemComponent } from './components/error-summary/error-summary-item/error-summary-item.component';
import { ErrorSummaryComponent } from './components/error-summary/error-summary.component';
import { FooterMetaItemComponent } from './components/footer/footer-meta/footer-meta-item/footer-meta-item.component';
import { FooterMetaComponent } from './components/footer/footer-meta/footer-meta.component';
import { FooterNavigationItemComponent } from './components/footer/footer-navigation/footer-navigation-item/footer-navigation-item.component';
import { FooterNavigationComponent } from './components/footer/footer-navigation/footer-navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderAdvancedCustomNavigationComponent } from './components/header-advanced/header-advanced-custom-navigation/header-custom-navigation.component';
import { HeaderAdvancedDropdownComponent } from './components/header-advanced/header-advanced-dropdown/header-advanced-dropdown.component';
import { HeaderAdvancedLogoComponent } from './components/header-advanced/header-advanced-logo/header-advanced-logo.component';
import { HeaderAdvancedSubComponent } from './components/header-advanced/header-advanced-sub/header-advanced-sub.component';
import { HeaderAdvancedSubtitleComponent } from './components/header-advanced/header-advanced-subtitle/header-advanced-subtitle.component';
import { HeaderAdvancedSuperComponent } from './components/header-advanced/header-advanced-super/header-advanced-super.component';
import { HeaderAdvancedTitleContainerComponent } from './components/header-advanced/header-advanced-title-container/header-advanced-title-container.component';
import { HeaderAdvancedTitleComponent } from './components/header-advanced/header-advanced-title/header-advanced-title.component';
import { HeaderAdvancedComponent } from './components/header-advanced/header-advanced.component';
import { HeaderMiniComponent } from './components/header-mini/header-mini.component';
import { HeaderCustomNavigationComponent } from './components/header/header-custom-navigation/header-custom-navigation.component';
import { HeaderDropdownComponent } from './components/header/header-dropdown/header-dropdown.component';
import { HeaderMobileTextComponent } from './components/header/header-mobileText/header-mobile-text.component';
import { HeaderNavigationItemComponent } from './components/header/header-navigation/header-navigation-item/header-navigation-item.component';
import { HeaderNavigationComponent } from './components/header/header-navigation/header-navigation.component';
import { HeaderOffcanvasButtonComponent } from './components/header/header-offcanvas/header-offcanvas-button/header-offcanvas-button.component';
import { HeaderOffcanvasCloseButtonComponent } from './components/header/header-offcanvas/header-offcanvas-close-button/header-offcanvas-close-button.component';
import { HeaderOffcanvasComponent } from './components/header/header-offcanvas/header-offcanvas.component';
import { HeaderSubnavComponent } from './components/header/header-subnav/header-subnav.component';
import { HeaderComponent } from './components/header/header.component';
import { LinksListIconRightComponent } from './components/links-list/links-list-icon-right/links-list-icon-right.component';
import { LinksListItemSubComponent } from './components/links-list/links-list-item-sub/links-list-item-sub.component';
import { LinksListItemComponent } from './components/links-list/links-list-item/links-list-item.component';
import { LinksListComponent } from './components/links-list/links-list.component';
import { MenuHorizontalItemComponent } from './components/menu-horizontal/menu-horizontal-item/menu-horizontal-item.component';
import { MenuHorizontalComponent } from './components/menu-horizontal/menu-horizontal.component';
import { MenuNavigationItemComponent } from './components/menu-navigation/components/menu-navigation-item/menu-navigation-item.component';
import { MenuNavigationSubitemComponent } from './components/menu-navigation/components/menu-navigation-subitem/menu-navigation-subitem.component';
import { MenuNavigationComponent } from './components/menu-navigation/menu-navigation.component';
import { MenuVerticalItemSubItemComponent } from './components/menu-vertical/menu-vertical-item-sub-item/menu-vertical-item-sub-item.component';
import { MenuVerticalItemSubComponent } from './components/menu-vertical/menu-vertical-item-sub/menu-vertical-item-sub.component';
import { MenuVerticalItemComponent } from './components/menu-vertical/menu-vertical-item/menu-vertical-item.component';
import { MenuVerticalComponent } from './components/menu-vertical/menu-vertical.component';
import { MenubarItemComponent } from './components/menubar/components/menubar-item/menubar-item.component';
import { MenubarLabelComponent } from './components/menubar/components/menubar-label/menubar-label.component';
import { MenubarSubitemComponent } from './components/menubar/components/menubar-subitem/menubar-subitem.component';
import { MenubarSubsubitemComponent } from './components/menubar/components/menubar-subsubitem/menubar-subsubitem.component';
import { MenubaritemDirective } from './components/menubar/directives/menubaritem.directive';
import { MenubarComponent } from './components/menubar/menubar.component';
import { NavItemComponent } from './components/nav/nav-item/nav-item.component';
import { NavComponent } from './components/nav/nav.component';
import { NotificationItemComponent } from './components/notification/notification-item/notification-item.component';
import { NotificationComponent } from './components/notification/notification.component';
import { SkipLinkComponent } from './components/skip-link/skip-link.component';
import { DynamicHeadingDirective } from './directives/dynamic-heading.directive';


@NgModule({
  declarations: [
    BreadcrumbsComponent,
    BreadcrumbsItemComponent,
    ErrorSummaryComponent,
    ErrorSummaryItemComponent,
    FooterComponent,
    FooterMetaComponent,
    FooterMetaItemComponent,
    FooterNavigationComponent,
    FooterNavigationItemComponent,
    HeaderComponent,
    HeaderSubnavComponent,
    HeaderNavigationComponent,
    HeaderNavigationItemComponent,
    HeaderDropdownComponent,
    HeaderOffcanvasButtonComponent,
    HeaderOffcanvasComponent,
    HeaderOffcanvasCloseButtonComponent,
    HeaderMobileTextComponent,
    HeaderMiniComponent,
    HeaderAdvancedComponent,
    HeaderAdvancedSuperComponent,
    HeaderAdvancedLogoComponent,
    HeaderAdvancedCustomNavigationComponent,
    HeaderAdvancedTitleComponent,
    HeaderAdvancedSubtitleComponent,
    HeaderAdvancedTitleContainerComponent,
    HeaderAdvancedDropdownComponent,
    HeaderAdvancedSubComponent,
    LinksListComponent,
    LinksListItemComponent,
    LinksListIconRightComponent,
    LinksListItemSubComponent,
    MenuHorizontalComponent,
    MenuHorizontalItemComponent,
    MenuVerticalComponent,
    MenuVerticalItemComponent,
    MenuVerticalItemSubComponent,
    MenuVerticalItemSubItemComponent,
    MenubarComponent,
    MenubarLabelComponent,
    MenubarItemComponent,
    MenubarSubitemComponent,
    MenubarSubsubitemComponent,
    NavComponent,
    NotificationComponent,
    NotificationItemComponent,
    SkipLinkComponent,
    NavItemComponent,
    MenuNavigationComponent,

    MenubaritemDirective,

    MenuNavigationItemComponent,
    MenuNavigationSubitemComponent,
    HeaderCustomNavigationComponent,
    
    DynamicHeadingDirective,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DesyCommonsModule,
    DesyButtonsModule
  ],
  exports: [
    BreadcrumbsComponent,
    BreadcrumbsItemComponent,
    ErrorSummaryComponent,
    ErrorSummaryItemComponent,
    FooterComponent,
    FooterMetaComponent,
    FooterMetaItemComponent,
    FooterNavigationComponent,
    FooterNavigationItemComponent,
    HeaderComponent,
    HeaderSubnavComponent,
    HeaderNavigationComponent,
    HeaderNavigationItemComponent,
    HeaderDropdownComponent,
    HeaderOffcanvasComponent,
    HeaderOffcanvasButtonComponent,
    HeaderOffcanvasCloseButtonComponent,
    HeaderMobileTextComponent,
    LinksListComponent,
    LinksListItemComponent,
    LinksListIconRightComponent,
    LinksListItemSubComponent,
    MenuHorizontalComponent,
    MenuHorizontalItemComponent,
    MenuVerticalComponent,
    MenuVerticalItemComponent,
    MenuVerticalItemSubComponent,
    MenuVerticalItemSubItemComponent,
    MenubarComponent,
    MenubarLabelComponent,
    MenubarItemComponent,
    MenubarSubitemComponent,
    MenubarSubsubitemComponent,
    NavComponent,
    NotificationComponent,
    NotificationItemComponent,
    NavItemComponent,
    SkipLinkComponent,
    MenuNavigationComponent,
    MenuNavigationSubitemComponent,
    MenuNavigationItemComponent,
    MenuNavigationSubitemComponent,
    HeaderCustomNavigationComponent,
    HeaderMiniComponent,
    HeaderAdvancedComponent,
    HeaderAdvancedSuperComponent,
    HeaderAdvancedLogoComponent,
    HeaderAdvancedCustomNavigationComponent,
    HeaderAdvancedTitleComponent,
    HeaderAdvancedSubtitleComponent,
    HeaderAdvancedTitleContainerComponent,
    HeaderAdvancedDropdownComponent,
    HeaderAdvancedSubComponent,
    
    DynamicHeadingDirective,
  ]
})
export class DesyNavModule { }
