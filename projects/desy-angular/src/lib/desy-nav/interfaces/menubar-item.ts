import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { MenubarItemSub } from './menubar-item-sub';

export interface MenubarItem extends AccessibilityData {
  href?: string;
  target?: string;
  routerLink?: string|any[];
  routerLinkActiveClasses?: string;
  fragment?: string;
  text?: string;
  html?: string;
  id?: string;
  ariaLabel?: string;
  sub?: MenubarItemSub;
  classes?: string;

  title?: string;
  disabled?: boolean;
  active?: boolean;
}
