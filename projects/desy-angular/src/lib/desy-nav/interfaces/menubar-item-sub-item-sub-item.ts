export interface MenubarItemSubItemSubItem {
  role?: string;
  html?: string;
  text?: string;
  checked?: boolean;
}
