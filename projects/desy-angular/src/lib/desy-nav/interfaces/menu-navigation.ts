
import { DividerData } from '../../desy-commons/interfaces';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { MenuNavigationItemSubItem } from './menu-navigation-item-sub-item';

export interface MenuNavigationItem extends AccessibilityData {
    href?: string;
    routerLink?: string;
    fragment?: string;
    routerLinkActiveClasses?: string | string[];
    target?: string;
    text: string;
    html: string;
    id?: string;
    expanded?: boolean;
    divider?: DividerData;
    disabled?: boolean;
    sub?: MenuNavigationItemSubItem;
    classes?: string;

    title?: string;
    active?: boolean;
}