import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface BreadcrumbsData extends AccessibilityData {
    text?: string;
    html?: string;
    routerLink?: string;
    id?: string;
}
