export interface HeaderSkipLinkData {
    text?: string;
    html?: string;
    classes?: string;
    fragment?: string;
    id?: string;
  }