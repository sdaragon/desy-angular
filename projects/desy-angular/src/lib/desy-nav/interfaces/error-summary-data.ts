import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface ErrorSummaryData extends AccessibilityData {
    fragment?: string;
    text?: string;
    html?: string;
    id?: string;
}
