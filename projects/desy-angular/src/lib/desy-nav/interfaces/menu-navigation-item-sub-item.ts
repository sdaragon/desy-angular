import { DividerData } from '../../desy-commons/interfaces/divider-data';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { MenuNavigationItemSubItemSubItem } from './menu-navigation-item-sub-item-sub-item';

export interface MenuNavigationItemSubItem extends AccessibilityData {
  classes?: string;
  items?: MenuNavigationItemSubItemSubItem[];
  html?: string;
  disabled?: boolean;
  active?: boolean;
  href?: string;
  routerLink?: string;
  fragment?: string;
  routerLinkActiveClasses?: string | string[];
  text?: string;
  target?: string;
  divider?: DividerData;
}