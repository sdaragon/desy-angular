import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface NotificationItemsData extends AccessibilityData{
  href?: string;
  target?: string;
  routerLink?: string;
  fragment?: string;
  text?: string;
  html?: string;
  id?: string;
}
