import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { MenubarItemSubItem } from './menubar-item-sub-item';

export interface MenubarItemSub extends AccessibilityData {
  classes?: string;
  items?: MenubarItemSubItem[];
}
