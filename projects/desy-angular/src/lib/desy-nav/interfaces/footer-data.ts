import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface MetaData {
  visuallyHiddenTitle?: string;
  html?: string;
  text?: string;
  items?: MetaItemData[];
}

export interface MetaItemData extends AccessibilityData {
  text?: string;
  href?: string;
  routerLink?: string;
  fragment?: string;
  routerLinkActiveClasses?: string | string[];
  target?: string;
}

export interface NavigationData {
  title: string;
  columns: number;
  items?: NavigationItemData[];
}

export interface NavigationItemData extends AccessibilityData {
  text?: string;
  href?: string;
  routerLink?: string;
  fragment?: string;
  routerLinkActiveClasses?: string | string[];
  target?: string;
}


