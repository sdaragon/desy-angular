import {NavItemData} from "./nav-item.data";

export interface NavItemEventData {
  item: NavItemData;
  event: any;
}
