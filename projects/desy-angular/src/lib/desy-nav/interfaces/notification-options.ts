
import { ContentData, DescriptionData, IconData, TitleData } from '../../desy-commons/interfaces';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { NotificationItemsData } from './notification-items-data';

export interface NotificationOptions extends AccessibilityData {
  id?: string;
  classes?: string;
  isOpen?: boolean;

  titleNotification?: TitleData;
  description?: DescriptionData;
  content?: ContentData;
  items?: Array<NotificationItemsData>;

  icon?: IconData;
  type?: string;
  isDismissible?: boolean;
}

