import { AccessibilityData } from "../../shared/interfaces/accessibility-data";

export interface HeaderMobileTextData extends AccessibilityData {
  html?: string;
  text?: string;
  classes?: string;
}

