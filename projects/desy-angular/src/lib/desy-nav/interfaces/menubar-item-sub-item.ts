import {MenubarItemSubItemSubItem} from './menubar-item-sub-item-sub-item';

export interface MenubarItemSubItem {
  role?: string;
  ariaLabel?: string;
  text?: string;
  html?: string;
  checked?: boolean;
  items?: MenubarItemSubItemSubItem[];
}
