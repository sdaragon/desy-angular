import { HeaderNavigationItemData } from './header-navigation-item-data';

export interface HeaderNavigationData {
  classes?: string;
  items: HeaderNavigationItemData[];
}
