import { DividerData } from "../../desy-commons/interfaces";
import { AccessibilityData } from "../../shared/interfaces/accessibility-data";

export interface MenuNavigationItemSubItemSubItem extends AccessibilityData {
    href?: string;
    routerLink?: string;
    fragment?: string;
    routerLinkActiveClasses?: string | string[];
    target?: string;
    text: string;
    html: string;
    id?: string;
    expanded?: boolean;
    divider?: DividerData;
    disabled?: boolean;
    classes?: string;
    active?: boolean;
}
