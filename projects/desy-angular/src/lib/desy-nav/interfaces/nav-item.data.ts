import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface NavItemData extends AccessibilityData {
  href?: string;
  routerLink?: string|any[];
  routerLinkActiveClasses?: string | string[];
  fragment?: string;
  target?: string;
  text?: string;
  html?: string;
  id?: string;
  active?: boolean;
  disabled?: boolean;
  divider?: boolean;
  classes?: string;
  title?: string;
}
