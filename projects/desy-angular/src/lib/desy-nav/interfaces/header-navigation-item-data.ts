import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface HeaderNavigationItemData extends AccessibilityData {
  text?: string;
  html?: string;
  href?: string;
  routerLink?: string;
  routerLinkActiveClasses?: string | string[];
  fragment?: string;
  id?: string;
  active?: boolean;
  disabled?: boolean;
  target?: string;
}
