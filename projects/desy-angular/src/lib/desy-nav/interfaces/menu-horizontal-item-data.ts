import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface MenuHorizontalItemData extends AccessibilityData {
  href?: string;
  routerLink?: string|any[];
  routerLinkActiveClasses?: string | string[];
  fragment?: string;
  target?: string;
  text?: string;
  html?: string;
  id?: string;
  active?: boolean;
  disabled?: boolean;
  classes?: string;
  title?: string;
}
