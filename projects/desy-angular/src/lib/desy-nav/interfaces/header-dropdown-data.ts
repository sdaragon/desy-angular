import { AccessibilityData } from "../../shared/interfaces/accessibility-data";
import { NavItemData } from './nav-item.data';

export interface HeaderDropdownData extends AccessibilityData {
  text?: string;
  html?: string;
  classes?: string;
  classesContainer?: string;
  classesTooltip?: string;
  hiddenText?: string;
  items?: NavItemData[];
}
