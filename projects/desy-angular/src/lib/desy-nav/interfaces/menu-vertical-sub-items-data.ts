import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface MenuVerticalSubItemsData extends AccessibilityData {
  id?: string;
  text?: string;
  html?: string;
  classes?: string;
  href?: string;
  routerLink?: string;
  routerLinkActiveClasses?: string;
  fragment?: string;
  target?: string;
  disabled?: boolean;
  active?: boolean;
  divider?: boolean;
  title?: string;
}
