import { TemplateRef } from '@angular/core';

export interface HeaderOffcanvasData {
  // Parámetros del button
  text?: string;
  html?: string;
  classes?: string;

  // Parámetros del offcanvas
  textClose: string;
  contentHtml: TemplateRef<any>;
}
