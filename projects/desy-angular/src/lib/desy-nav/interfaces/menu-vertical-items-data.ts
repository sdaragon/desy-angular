import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { MenuVerticalSubData } from './menu-vertical-sub-data';

export interface MenuVerticalItemsData extends AccessibilityData {
  id?: string;
  text?: string;
  html?: string;
  classes?: string;
  href?: string;
  routerLink?: string;
  routerLinkActiveClasses?: string;
  fragment?: string;
  target?: string;
  disabled?: boolean;
  active?: boolean;
  divider?: boolean;
  title?: string;

  sub?: MenuVerticalSubData;
}
