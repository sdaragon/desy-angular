import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { MenuVerticalSubItemsData } from './menu-vertical-sub-items-data';

export interface MenuVerticalSubData extends AccessibilityData {
    html?: string;
    classes?: string;
    items?: Array<MenuVerticalSubItemsData>;
}
