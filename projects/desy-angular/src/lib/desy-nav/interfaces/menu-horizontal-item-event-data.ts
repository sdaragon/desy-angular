import { MenuHorizontalItemData } from './menu-horizontal-item-data';

export interface MenuHorizontalItemEventData {
  item: MenuHorizontalItemData;
  event: any;
  href?: any;
}
