import { AccessibilityData } from "../../shared/interfaces/accessibility-data";
import { NavItemData } from './nav-item.data';

export interface HeaderSubnavData extends AccessibilityData {
  html?: string;
  text?: string;
  classes?: string;
  classesContainer?: string;
  classesTooltip?: string;
  hiddenText?: string;
  items?: NavItemData[];
}

