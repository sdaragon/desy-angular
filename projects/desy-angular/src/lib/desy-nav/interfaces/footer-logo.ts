export enum Type {
  UE = 'UE',
  FEDER = 'FEDER',
  FEADER = 'FEADER',
  FSE = 'FSE+',
  Plurifondo = 'Plurifondo',
  Custom = 'Custom'
}
