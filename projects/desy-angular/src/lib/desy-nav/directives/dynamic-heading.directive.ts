import { Directive, ElementRef, Input, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[desyDynamicHeading]'
})
export class DynamicHeadingDirective {
  @Input() classes: string;
  private currentView = null;
  private currentElement = null;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private renderer: Renderer2,
    private el: ElementRef
  ) { }

  @Input() set desyDynamicHeading(headingLevel: number) {
    if (this.currentView) {
      this.currentView.destroy();
      this.renderer.removeChild(this.el.nativeElement.parentElement, this.currentElement);
    }

    this.currentElement = this.renderer.createElement(`h${headingLevel ? headingLevel : '2'}`);
    this.renderer.setAttribute(this.currentElement, 'class', this.classes ? this.classes : 'text-2xl lg:text-3xl font-bold');
    this.currentView = this.viewContainer.createEmbeddedView(this.templateRef);
    this.currentView.rootNodes.forEach(node => {
      this.renderer.appendChild(this.currentElement, node);
    });
    if (this.el.nativeElement.parentElement instanceof HTMLElement) {
      this.renderer.appendChild(this.el.nativeElement.parentElement, this.currentElement);
    }
  }
}
