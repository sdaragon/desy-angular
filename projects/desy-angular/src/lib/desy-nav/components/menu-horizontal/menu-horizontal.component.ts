import {Component, ContentChildren, EventEmitter, Input, OnChanges, Output, QueryList, SimpleChanges} from '@angular/core';
import { MenuHorizontalItemData, MenuHorizontalItemEventData } from '../../interfaces';
import { AccessibilityComponent } from '../../../shared/components';
import {MenuHorizontalItemComponent} from './menu-horizontal-item/menu-horizontal-item.component';

@Component({
  selector: 'desy-menu-horizontal',
  templateUrl: './menu-horizontal.component.html'
})
export class MenuHorizontalComponent extends AccessibilityComponent {

  @Input() id: string;
  @Input() idPrefix: string;
  @Input() classes: string;
  @Input() items: MenuHorizontalItemData[];

  @ContentChildren(MenuHorizontalItemComponent) itemComponents: QueryList<MenuHorizontalItemComponent>;

  @Output() clickEvent = new EventEmitter<MenuHorizontalItemEventData>();

  onClick(event: any, item: MenuHorizontalItemData): void {
    const itemsActiveChange = [];
    if (!item.active) {
      itemsActiveChange.push(item);
    }

    const itemList = this.getItemList();
    itemList.forEach(i => {
      if (i.active && i !== item) {
        itemsActiveChange.push(i);
      }
      i.active = false;
    });
    item.active = true;

    // Se emite el evento sólo para los items cuyo valor de active ha sido modificado
    itemsActiveChange.forEach(i => {
      if (i instanceof  MenuHorizontalItemComponent) {
        i.activeChange.emit(i.active);
      }
    });

    this.clickEvent.emit({ item, event });
    if (item instanceof MenuHorizontalItemComponent) {
      item.clickEvent.emit({ item, event });
    }
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : 'menu-item';
  }

  getItemList(): MenuHorizontalItemData[] {
    const itemList =  this.itemComponents && this.itemComponents.length > 0 ? this.itemComponents.toArray() : this.items; 
    return itemList;
  }


  getItemId(item: MenuHorizontalItemData, index: number): string {
    let id;
    if (item.id) {
      id = item.id;
    } else {
      id = this.getIdPrefix() + '-' + ++index;
    }
    return id;
  }
}
