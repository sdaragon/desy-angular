import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';
import { MenuHorizontalItemData, MenuHorizontalItemEventData } from '../../../interfaces';

@Component({
  selector: 'desy-menu-horizontal-item',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class MenuHorizontalItemComponent extends ContentBaseComponent implements MenuHorizontalItemData {

  @Input() href?: string;
  @Input() routerLink?: string|any[];
  @Input() routerLinkActiveClasses: string|string[];
  @Input() fragment?: string;
  @Input() target?: string;
  @Input() id?: string;
  @Input() active?: boolean;
  @Input() disabled?: boolean;
  @Input() classes?: string;

  @Output() clickEvent = new EventEmitter<MenuHorizontalItemEventData>();
  @Output() activeChange = new EventEmitter<boolean>();

}
