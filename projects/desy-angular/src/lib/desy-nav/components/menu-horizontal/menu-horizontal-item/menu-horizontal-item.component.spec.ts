import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuHorizontalItemComponent } from './menu-horizontal-item.component';

xdescribe('MenuHorizontalItemComponent', () => {
  let component: MenuHorizontalItemComponent;
  let fixture: ComponentFixture<MenuHorizontalItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuHorizontalItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuHorizontalItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
