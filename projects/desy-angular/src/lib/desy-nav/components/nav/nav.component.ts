import {
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  Output,
  QueryList
} from '@angular/core';
import {
  AccessibilityComponent
} from '../../../shared/components';
import { NavItemData, NavItemEventData } from '../../interfaces';
import { NavItemComponent } from './nav-item/nav-item.component';


@Component({
  selector: 'desy-nav',
  templateUrl: './nav.component.html'
})
export class NavComponent extends AccessibilityComponent {

  @Input() hasNav = true;
  @Input() id: string;
  @Input() idPrefix: string;
  @Input() items: NavItemData[];
  @Input() classes: string;

  @Output() clickEvent = new EventEmitter<NavItemEventData>();
  @ContentChildren(NavItemComponent) itemComponentList: QueryList<NavItemComponent>;

  onClick(event: any, item: NavItemData): void {
    const itemsActiveChange = [];
    if (!item.active) {
      itemsActiveChange.push(item);
    }

    const itemList = this.getItemList();
    itemList.forEach(i => {
      if (i.active && i !== item) {
        itemsActiveChange.push(i);
      }
      i.active = false;
    });
    item.active = true;

    // Se emite el evento sólo para los items cuyo valor de active ha sido modificado
    itemsActiveChange.forEach(i => {
      if (i instanceof NavItemComponent) {
        i.activeChange.emit(i.active);
      }
    });

    this.clickEvent.emit({ item, event });
    if (item instanceof NavItemComponent) {
      item.clickEvent.emit({ item, event });
    }
  }

  getItemList(): NavItemData[] {
    const itemList = this.itemComponentList && this.itemComponentList.length > 0 ? this.itemComponentList.toArray() : this.items;

    return itemList;
  }

  getItemId(item: NavItemData, index: number): string {
    let id;
    if (item.id) {
      id = item.id;
    } else {
      id = this.getIdPrefix() + '-' + ++index;
    }
    return id;
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : 'nav-item';
  }

  movePrevious(event: Event, currentIndex: number): void {
    event.preventDefault();
    const items = this.getItemList();
    let nextIndex = currentIndex - 1;
    while (nextIndex >= 0 && !this.isFocusableItem(items[nextIndex])) {
      nextIndex--;
    }

    if (nextIndex < items.length) {
      const itemElem = document.getElementById(this.getItemId(items[nextIndex], nextIndex));
      if (itemElem) {
        itemElem.focus();
      }
    }
  }

  moveNext(event: Event, currentIndex: number): void {
    event.preventDefault();
    const items = this.getItemList();
    let nextIndex = currentIndex + 1;
    while (nextIndex < items.length && !this.isFocusableItem(items[nextIndex])) {
      nextIndex++;
    }

    if (nextIndex < items.length) {
      const itemElem = document.getElementById(this.getItemId(items[nextIndex], nextIndex));
      if (itemElem) {
        itemElem.focus();
      }
    }
  }

  simulateClick(event: Event, item: NavItemData, index: number): void {
    event.preventDefault();
    const element = document.getElementById(this.getItemId(item, index));
    element?.click();
  }


  private isFocusableItem(item: NavItemData): boolean {
    return !item.active && !item.disabled;
  }

}
