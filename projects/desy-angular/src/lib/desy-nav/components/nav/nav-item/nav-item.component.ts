import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';
import { NavItemData, NavItemEventData } from '../../../interfaces';

@Component({
  selector: 'desy-nav-item',
  templateUrl: './nav-item.component.html'
})
export class NavItemComponent extends ContentBaseComponent implements NavItemData {

  @Input() id?: string;
  @Input() active?: boolean;
  @Input() classes?: string;
  @Input() titleModal?: string;
  @Input() href?: string;
  @Input() routerLink?: string|any[];
  @Input() routerLinkActiveClasses: string|string[];
  @Input() fragment?: string;
  @Input() target?: string;
  @Input() disabled?: boolean;
  @Input() divider?: boolean;

  @Output() clickEvent = new EventEmitter<NavItemEventData>();
  @Output() activeChange = new EventEmitter<boolean>();
}
