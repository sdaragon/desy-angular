import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {ErrorSummaryData} from '../../../interfaces';

@Component({
  selector: 'desy-error-summary-item',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ErrorSummaryItemComponent extends ContentBaseComponent implements ErrorSummaryData {

  @Input() fragment: string;
  @Input() id: string;
}
