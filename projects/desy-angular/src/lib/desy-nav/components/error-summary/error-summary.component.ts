import {
  Component,
  ContentChildren,
  Input,
  QueryList
} from '@angular/core';
import { DescriptionComponent } from '../../../desy-commons/components/description/description.component';
import { TitleComponent } from '../../../desy-commons/components/title/title.component';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { ErrorSummaryData } from '../../interfaces';
import { ErrorSummaryItemComponent } from './error-summary-item/error-summary-item.component';

@Component({
  selector: 'desy-error-summary',
  templateUrl: './error-summary.component.html'
})
export class ErrorSummaryComponent extends AccessibilityComponent {

  @Input() titleText: string;
  @Input() titleHtml: string;
  @Input() descriptionText: string;
  @Input() descriptionHtml: string;
  @Input() classes: string;
  @Input() id: string;
  @Input() errorList: Array<ErrorSummaryData>;
  @Input() headingLevel: number

  @DesyContentChild()
  @ContentChildren(TitleComponent) titleComponent: TitleComponent;

  @DesyContentChild()
  @ContentChildren(DescriptionComponent) descriptionComponent: DescriptionComponent;

  @ContentChildren(ErrorSummaryItemComponent) errorComponentList: QueryList<ErrorSummaryItemComponent>;

  lastErrorItemCount: number;
  
  getErrorItemList(): ErrorSummaryData[] {
    let errorItemList: ErrorSummaryData[];
    if (this.errorComponentList && this.errorComponentList.length > 0) {
      errorItemList = this.errorComponentList.toArray();
    } else {
      errorItemList = this.errorList ? this.errorList : null;
    }

    const lastLength = this.lastErrorItemCount;
    this.lastErrorItemCount = errorItemList ? errorItemList.length : null;

    return errorItemList;
  }
}
