import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {NotificationItemsData} from '../../../interfaces';

@Component({
  selector: 'desy-notification-item',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class NotificationItemComponent extends ContentBaseComponent implements NotificationItemsData {
  @Input() href: string;
  @Input() target?: string;
  @Input() routerLink?: string;
  @Input() fragment?: string;
  @Input() id: string;
}
