import {
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  Output,
  QueryList
} from '@angular/core';

import { animate, state, style, transition, trigger } from '@angular/animations';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { DescriptionComponent } from '../../../desy-commons/components/description/description.component';
import { IconComponent } from '../../../desy-commons/components/icon/icon.component';
import { TitleComponent } from '../../../desy-commons/components/title/title.component';
import { ContentData, DescriptionData, IconData, TitleData } from '../../../desy-commons/interfaces';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { NotificationItemsData } from '../../interfaces';
import { NotificationItemComponent } from './notification-item/notification-item.component';

@Component({
  selector: 'desy-notification',
  templateUrl: './notification.component.html',
  animations: [
    trigger('displayNotification', [
      state('void', style({
        opacity: '0.0',
      })),
      state('*', style({
        opacity: '1.0',
      })),
      transition(':enter', [
        animate('150ms ease-out')
      ]),
      transition(':leave', [
        animate('150ms ease-in')
      ])
    ])
  ]
})
export class NotificationComponent extends AccessibilityComponent {

  @Input() id: string;
  @Input() classes: string;
  @Input() isOpen = true;
  @Output() isOpenChange = new EventEmitter<boolean>();

  @Input() titleNotification: TitleData;
  @Input() description: DescriptionData;
  @Input() content: ContentData;
  @Input() items: NotificationItemsData[];

  @Input() icon: IconData;
  @Input() type: string;
  @Input() isDismissible: boolean;
  @Input() headingLevel: number;

  @DesyContentChild()
  @ContentChildren(TitleComponent) titleComponent: TitleComponent;

  @DesyContentChild()
  @ContentChildren(DescriptionComponent) descriptionComponent: DescriptionComponent;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  @DesyContentChild()
  @ContentChildren(IconComponent) iconComponent: IconComponent;

  @ContentChildren(NotificationItemComponent) itemCompontents: QueryList<NotificationItemComponent>;

  isDismissing = false;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  dismiss(): void {
    // Se emite 75ms más tarde para poder visualizar la animación en cualquier caso
    this.isDismissing = true;
    setTimeout(() => {
      this.isDismissing = false;
      this.isOpenChange.emit(false);
    }, 75);
  }

  getType(): string {
    let type: string;
    if (this.type){
      type = this.type.toLocaleLowerCase();
    }
    return type;
  }

  getItems(): NotificationItemsData[] {
    return this.itemCompontents && this.itemCompontents.length > 0 ? this.itemCompontents.toArray() : this.items;
  }

}
