import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MenubarItemSubItemSubItem} from '../../../../interfaces/menubar-item-sub-item-sub-item';
import {ContentBaseComponent} from '../../../../../shared/components';

@Component({
  selector: 'desy-menubar-subsubitem',
  templateUrl: './menubar-subsubitem.component.html'
})
export class MenubarSubsubitemComponent extends ContentBaseComponent implements MenubarItemSubItemSubItem {

  @Input() html?: string;
  @Input() text?: string;
  @Input() checked?: boolean;

  @Output() selected = new EventEmitter<any>();
  @Output() checkedChange = new EventEmitter<boolean>();

}
