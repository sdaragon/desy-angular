import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../../shared/components';

@Component({
  selector: 'desy-menubar-label',
  templateUrl: './menubar-label.component.html'
})
export class MenubarLabelComponent extends ContentBaseComponent {

  @Input() classes: string;

}
