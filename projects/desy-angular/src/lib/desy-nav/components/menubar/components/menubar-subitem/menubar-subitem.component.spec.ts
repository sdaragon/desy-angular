import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenubarSubitemComponent } from './menubar-subitem.component';

xdescribe('MenubarSubitemComponent', () => {
  let component: MenubarSubitemComponent;
  let fixture: ComponentFixture<MenubarSubitemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenubarSubitemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenubarSubitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
