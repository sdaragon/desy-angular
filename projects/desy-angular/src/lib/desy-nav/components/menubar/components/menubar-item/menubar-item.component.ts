import {
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  Output,
  QueryList,
} from '@angular/core';
import { ContentBaseComponent } from '../../../../../shared/components';
import { MenubarItem } from '../../../../interfaces/menubar-item';
import { MenubarItemSub } from '../../../../interfaces/menubar-item-sub';
import { MenubarSubitemComponent } from '../menubar-subitem/menubar-subitem.component';

@Component({
  selector: 'desy-menubar-item',
  templateUrl: './menubar-item.component.html'
})
export class MenubarItemComponent extends ContentBaseComponent implements MenubarItem {

  @Input() href?: string;
  @Input() target?: string;
  @Input() routerLink?: string|any[];
  @Input() routerLinkActiveClasses?: string;
  @Input() fragment?: string;
  @Input() text?: string;
  @Input() html?: string;
  @Input() id?: string;
  @Input() sub?: MenubarItemSub;
  @Input() classes?: string;

  @Input() titleModal?: string;
  @Input() disabled?: boolean;
  @Input() active?: boolean;

  @Output() selected = new EventEmitter<any>();

  @ContentChildren(MenubarSubitemComponent) subItems: QueryList<MenubarSubitemComponent>;

  public getSubItems(): MenubarSubitemComponent[] {
    return this.subItems.toArray();
  }

}
