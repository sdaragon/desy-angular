import {
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  Output,
  QueryList,
} from '@angular/core';
import {MenubarItemSubItem} from '../../../../interfaces/menubar-item-sub-item';
import {MenubarItemSubItemSubItem} from '../../../../interfaces/menubar-item-sub-item-sub-item';
import {MenubarSubsubitemComponent} from '../menubar-subsubitem/menubar-subsubitem.component';
import {ContentBaseComponent} from '../../../../../shared/components';

@Component({
  selector: 'desy-menubar-subitem',
  templateUrl: './menubar-subitem.component.html'
})
export class MenubarSubitemComponent extends ContentBaseComponent implements MenubarItemSubItem {

  @Input() text?: string;
  @Input() html?: string;
  @Input() checked?: boolean;
  @Input() items?: MenubarItemSubItemSubItem[];

  @Output() selected = new EventEmitter<any>();
  @Output() checkedChange = new EventEmitter<boolean>();

  @ContentChildren(MenubarSubsubitemComponent) subItems: QueryList<MenubarSubsubitemComponent>;

  public getSubItems(): MenubarSubsubitemComponent[] {
    return this.subItems.toArray();
  }

}
