import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenubarSubsubitemComponent } from './menubar-subsubitem.component';

xdescribe('MenubarSubsubitemComponent', () => {
  let component: MenubarSubsubitemComponent;
  let fixture: ComponentFixture<MenubarSubsubitemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenubarSubsubitemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenubarSubsubitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
