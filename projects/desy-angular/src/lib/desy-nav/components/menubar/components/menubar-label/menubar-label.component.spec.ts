import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenubarLabelComponent } from './menubar-label.component';

xdescribe('MenubarLabelComponent', () => {
  let component: MenubarLabelComponent;
  let fixture: ComponentFixture<MenubarLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenubarLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenubarLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
