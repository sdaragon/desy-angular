import {
  ContentChild,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  Output,
  QueryList,
  AfterContentChecked
} from '@angular/core';

@Directive({
  selector: '[desyMenubaritem]'
})
export class MenubaritemDirective implements AfterContentChecked {
  @ContentChild('link', { read: ElementRef }) link: ElementRef;
  @ContentChild('itemContentWrapper', { read: ElementRef }) itemContentWrapper: ElementRef;
  @ContentChild('popupMenu', { read: ElementRef }) popupMenu: ElementRef;
  @ContentChildren('popupMenuItem', { read: ElementRef, descendants: true}) popupMenuItems: QueryList<ElementRef>;

  @Output() contentChanged: EventEmitter<any> = new EventEmitter();

  private lastPopupMenu: ElementRef;
  private lastPopupMenuItems: QueryList<ElementRef>;
  private lastItemContentWrapper: ElementRef;
  private lastLink: ElementRef;

  contentHasBeenChecked = false;

  constructor(public elementRef: ElementRef) { }

  ngAfterContentChecked(): void {
    if (this.popupMenu !== this.lastPopupMenu || this.popupMenuItems !== this.lastPopupMenuItems
        || this.link !== this.lastLink || this.itemContentWrapper !== this.lastItemContentWrapper) {
      setTimeout(() => this.contentChanged.emit());
      this.lastPopupMenu = this.popupMenu;
      this.lastPopupMenuItems = this.popupMenuItems;
      this.lastLink = this.link;
      this.lastItemContentWrapper = this.itemContentWrapper;
    }

    if (!this.contentHasBeenChecked) {
      setTimeout(() => this.contentHasBeenChecked = true);
    }
  }

}
