import {
  AfterViewInit, ChangeDetectorRef,
  Component, ContentChildren,
  ElementRef, EventEmitter,
  HostBinding,
  Input,
  OnChanges, Output,
  QueryList,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { LabelComponent } from '../../../desy-forms/components/label/label.component';
import { LabelData } from '../../../desy-forms/interfaces';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { StringUtils } from '../../../shared/utils/string-utils';
import { MenubarItem } from '../../interfaces/menubar-item';
import { MenubarItemSubItem } from '../../interfaces/menubar-item-sub-item';
import { MenubarItemSubItemSubItem } from '../../interfaces/menubar-item-sub-item-sub-item';
import { MenubarItemComponent } from './components/menubar-item/menubar-item.component';
import { MenubarLabelComponent } from './components/menubar-label/menubar-label.component';
import { MenubarSubitemComponent } from './components/menubar-subitem/menubar-subitem.component';
import { MenubarSubsubitemComponent } from './components/menubar-subsubitem/menubar-subsubitem.component';
import { MenubaritemDirective } from './directives/menubaritem.directive';

@Component({
  selector: 'desy-menubar',
  templateUrl: './menubar.component.html'
})
export class MenubarComponent extends AccessibilityComponent implements OnChanges, AfterViewInit {

  @Input() idPrefix: string;
  @Input() items: MenubarItem[];
  @Output() itemsChange = new EventEmitter<MenubarItem[]>();
  @Output() activeItemChange = new EventEmitter<MenubarItem>();
  @Output() activeSubItemChange = new EventEmitter<MenubarItemSubItem|MenubarItemSubItemSubItem>();

  /**
   * label, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir componente menubar-label anidado
   * 2) incluir template
   * 3) incluir objeto label (interfaz expuesta)
   * 4) incluir legendText - incluir un label con solo un texto
   */
  @Input() labelRef: TemplateRef<LabelComponent>;
  @Input() labelData: LabelData;
  @Input() labelText: string;

  @Input() @HostBinding('class') classes: any;
  @HostBinding('class.c-menubar') cfg = true
  @Input() @HostBinding('attr.id') id: any;
  
  @HostBinding('attr.role') role: string = null;
  @HostBinding('attr.aria-label') ariaLabel: string = null;
  @HostBinding('attr.aria-describedby') ariaDescribedBy: string = null;
  @HostBinding('attr.aria-labelledby') ariaLabelledBy: string = null;
  @HostBinding('attr.aria-hidden') ariaHidden: string = null;
  @HostBinding('attr.aria-disabled') ariaDisabled: string = null;
  @HostBinding('attr.aria-controls') ariaControls: string = null;
  @HostBinding('attr.aria-current') ariaCurrent: string = null;
  @HostBinding('attr.aria-live') ariaLive: string = null;
  @HostBinding('attr.aria-expanded') ariaExpanded: string = null;
  @HostBinding('attr.aria-errormessage') ariaErrorMessage: string = null;
  @HostBinding('attr.aria-haspopup') ariaHasPopup: string = null;
  @HostBinding('attr.aria-modal') ariaModal: string = null;
  @HostBinding('attr.aria-checked') ariaChecked: string = null;
  @HostBinding('attr.aria-pressed') ariaPressed: string = null;
  @HostBinding('attr.aria-readonly') ariaReadonly: string = null;
  @HostBinding('attr.aria-required') ariaRequired: string = null;
  @HostBinding('attr.aria-selected') ariaSelected: string = null;
  @HostBinding('attr.aria-valuemin') ariaValuemin: string = null;
  @HostBinding('attr.aria-valuemax') ariaValuemax: string = null;
  @HostBinding('attr.aria-valuenow') ariaValuenow: string = null;
  @HostBinding('attr.aria-valuetext') ariaValuetext: string = null;
  @HostBinding('attr.aria-orientation') ariaOrientation: string = null;
  @HostBinding('attr.aria-level') ariaLevel: string = null;
  @HostBinding('attr.aria-multiselectable') ariaMultiselectable: string = null;
  @HostBinding('attr.aria-placeholder') ariaPlaceholder: string = null;
  @HostBinding('attr.aria-posinset') ariaPosinset: string = null;
  @HostBinding('attr.aria-setsize') ariaSetsize: string = null;
  @HostBinding('attr.aria-sort') ariaSort: string = null;
  @HostBinding('attr.aria-busy') ariaBusy: string = null;
  @HostBinding('attr.aria-dropeffect') ariaDropeffect: string = null;
  @HostBinding('attr.aria-grabbed') ariaGrabbed: string = null;
  @HostBinding('attr.aria-activedescendant') ariaActivedescendant: string = null;
  @HostBinding('attr.aria-atomic') ariaAtomic: string = null;
  @HostBinding('attr.aria-autocomplete') ariaAutocomplete: string = null;
  @HostBinding('attr.aria-braillelabel') ariaBraillelabel: string = null;
  @HostBinding('attr.aria-brailleroledescription') ariaBrailleroledescription: string = null;
  @HostBinding('attr.aria-colcount') ariaColcount: string = null;
  @HostBinding('attr.aria-colindex') ariaColindex: string = null;
  @HostBinding('attr.aria-colindextext') ariaColindextext: string = null;
  @HostBinding('attr.aria-colspan') ariaColspan: string = null;
  @HostBinding('attr.aria-description') ariaDescription: string = null;
  @HostBinding('attr.aria-details') ariaDetails: string = null;
  @HostBinding('attr.aria-flowto') ariaFlowto: string = null;
  @HostBinding('attr.aria-invalid') ariaInvalid: string = null;
  @HostBinding('attr.aria-keyshortcuts') ariaKeyshortcuts: string = null;
  @HostBinding('attr.aria-owns') ariaOwns: string = null;
  @HostBinding('attr.aria-relevant') ariaRelevant: string = null;
  @HostBinding('attr.aria-roledescription') ariaRoledescription: string = null;
  @HostBinding('attr.aria-rowcount') ariaRowcount: string = null;
  @HostBinding('attr.aria-rowindex') ariaRowindex: string = null;
  @HostBinding('attr.aria-rowindextext') ariaRowindextext: string = null;
  @HostBinding('attr.aria-rowspan') ariaRowspan: string = null;
  @HostBinding('attr.tabindex') tabindex: string = null;
  @HostBinding('attr.title') title: string = null;
  @HostBinding('attr.alt') alt: string = null;
  @HostBinding('attr.lang') lang: string = null;
  @HostBinding('attr.accesskey') accesskey: string = null;
  @HostBinding('attr.autocomplete') autocomplete: string = null;
  @HostBinding('attr.autofocus') autofocus: string = null;
  @HostBinding('attr.contenteditable') contenteditable: string = null;
  @HostBinding('attr.dir') dir: string = null;
  @HostBinding('attr.draggable') draggable: string = null;
  @HostBinding('attr.enterkeyhint') enterkeyhint: string = null;
  @HostBinding('attr.hidden') hidden: boolean = null;
  @HostBinding('attr.inputmode') inputmode: string = null;
  @HostBinding('attr.spellcheck') spellcheck: string = null;
  @HostBinding('attr.translate') translate: string = null;
  @HostBinding('attr.aria-multiline') ariaMultiline: string = null;
  @HostBinding('attr.for') for: string = null;
  @HostBinding('attr.form') form: string = null;
  @HostBinding('attr.headers') headers: string = null;
  @HostBinding('attr.placeholder') placeholder: string = null;
  @HostBinding('attr.readonly') readonly: string = null;
  @HostBinding('attr.required') required: string = null;

  @ViewChild('menubar', { read: ElementRef }) menubar: ElementRef;
  @ViewChildren(MenubaritemDirective) menuItems: QueryList<MenubaritemDirective>;
  @DesyContentChild() @ContentChildren(MenubarLabelComponent) labelComponent: MenubarLabelComponent;
  @ContentChildren(MenubarItemComponent) itemComponentList: QueryList<MenubarItemComponent>;

  menuData: any[];
  viewInit = false;
  isFocused: boolean;
  currentFocusItemIndex = 0;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnChanges(changes?: SimpleChanges): void {
    if (this.viewInit) {
      this.checkChanges();
    }
  }

  ngAfterViewInit(): void {
    this.viewInit = true;
    this.checkChanges();
  }

  checkChanges(): void {
    if (!this.menuItems) {
      return;
    }
    
    this.menuData = [];
    this.getItems().forEach((_, index) => {
      const itemData = {
        open: false,
        currentFocusSubItemIndex: 0,
        currentFocusSubSubItemIndex: 0,
        menuItem: this.menuItems.toArray()[index]
      };
      this.menuData.push(itemData);
    });

    if (this.viewInit) {
      this.checkRequired();
    }


    const lastCurrentFocusItemIndex = this.currentFocusItemIndex;
    const items = this.getItems();
    if (items) {
      while (!this.isItemFocusable(this.currentFocusItemIndex % items.length)) {
        this.currentFocusItemIndex = (this.currentFocusItemIndex + items.length + 1) % items.length;
        if (this.currentFocusItemIndex === lastCurrentFocusItemIndex) {
          break;
        }
      }
    }
  }


  /*
   * Eventos
   */

  handleMenuFocusIn(): void {
    this.isFocused = true;
    this.changeDetectorRef.detectChanges();
  }

  handleMenuFocusOut(event): void {
    if (!this.menubar.nativeElement.contains(event.relatedTarget)) {
      this.closeMenu();
    }
    this.isFocused = false;
    this.changeDetectorRef.detectChanges();
  }

  handleMenuItemClick(event, itemIndex: number): void {
    if (this.hasPopupMenu(itemIndex)) {
      event.preventDefault();
      const isOpen = this.menuData[itemIndex].open;
      this.closeMenu();
      if (!isOpen) {
        this.openMenu(itemIndex);
      }
    } else {
      this.activateMenuItem(itemIndex);
    }
  }

  handleMenuItemKeydown(event: KeyboardEvent, itemIndex: number): void {
    switch (event.key) {
      case 'Enter':
      case ' ':
      case 'ArrowDown':
        if (this.hasPopupMenu(itemIndex)) {
          this.openMenu(itemIndex);
          setTimeout(() => this.focusFirstSubItem(itemIndex));
          event.stopPropagation();
          event.preventDefault();
        }
        break;

      case 'Escape':
        if (this.hasPopupMenu(itemIndex)) {
          this.closeMenu(itemIndex);
        }
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowLeft':
        this.focusNextAvailableItem(itemIndex, -1);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowRight':
        this.focusNextAvailableItem(itemIndex, +1);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowUp':
        if (this.hasPopupMenu(itemIndex)) {
          this.openMenu(itemIndex);
          setTimeout(() => this.focusLastSubItem(itemIndex));
        }
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Home':
      case 'PageUp':
        if (this.hasPopupMenu(itemIndex)) {
          this.closeMenu();
        }
        this.focusFirstItem();
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'End':
      case 'PageDown':
        if (this.hasPopupMenu(itemIndex)) {
          this.closeMenu();
        }
        this.focusLastItem();
        event.stopPropagation();
        event.preventDefault();
        break;

      default:
        if (this.isPrintableChar(event.key)) {
          this.focusItemByFirstChar(event.key);
          event.stopPropagation();
          event.preventDefault();
        }
        break;
    }
  }

  handleMenuItemMouseOver(itemIndex: number): void {
    this.focusItem(itemIndex, true);
  }

  handlePopupMenuItemClick(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    this.activatePopupMenuItem(itemIndex, subItemIndex, subSubItemIndex);
    this.focusItem(itemIndex);
    this.closeMenu(itemIndex);
  }

  handlePopupMenuItemKeydown(event: KeyboardEvent, itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    const currentSubIndex = this.menuData[itemIndex].currentFocusSubItemIndex;
    const currentSubSubIndex = this.menuData[itemIndex].currentFocusSubSubItemIndex;
    const items = this.getItems();
    switch (event.key) {
      case ' ':
        this.activatePopupMenuItem(itemIndex, subItemIndex, subSubItemIndex);
        if (this.mustCloseAfterSelectPopupItem(itemIndex, subItemIndex, subSubItemIndex)) {
          this.closeMenu(itemIndex);
          this.focusItem(itemIndex);
        }
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Enter':
        this.activatePopupMenuItem(itemIndex, subItemIndex, subSubItemIndex);
        this.closeMenu(itemIndex);
        this.focusItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Escape':
        this.closeMenu(itemIndex);
        this.focusItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowUp':
        this.focusNextAvailableSubItem(itemIndex, currentSubIndex, currentSubSubIndex, -1);
        // this.focusSubItem(itemIndex, (currentSubIndex + listLength - 1) % listLength);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowDown':
        this.focusNextAvailableSubItem(itemIndex, currentSubIndex, currentSubSubIndex, +1);
        // this.focusSubItem(itemIndex, (currentSubIndex + listLength + 1) % listLength);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowLeft':
        this.closeMenu(itemIndex);
        this.focusItem((itemIndex + items.length - 1) % items.length);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowRight':
        this.closeMenu(itemIndex);
        this.focusItem((itemIndex + items.length + 1) % items.length);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Home':
      case 'PageUp':
        this.focusFirstSubItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'End':
      case 'PageDown':
        this.focusLastSubItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Tab':
        this.closeMenu(itemIndex);
        this.focusItem(itemIndex);
        break;

      default:
        if (this.isPrintableChar(event.key)) {
          this.focusSubItemByFirstChar(itemIndex, event.key);
          event.stopPropagation();
          event.preventDefault();
        }
        break;
    }
  }

  handlePopupMenuItemMouseOver(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    this.focusSubItem(itemIndex, subItemIndex, subSubItemIndex);
  }

  /*
   * Comunes
   */

  checkRequired(): void {


    const items = this.getItems();

    // No se ha puesto label required porque en algunos ejemplos de desy-html no aparece
  }


  activateMenuItem(itemIndex: number): void {
    const items = this.getItems();
    this.activeItemChange.emit(items[itemIndex] as MenubarItem);
    if (items[itemIndex] instanceof MenubarItemComponent) {
      (items[itemIndex] as MenubarItemComponent).selected.emit();
    }
  }

  activatePopupMenuItem(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    let item: MenubarItemSubItem|MenubarItemSubItemSubItem = subItems[subItemIndex];
    let subSubItems = [];
    if (subSubItemIndex !== null && subSubItemIndex !== undefined) {
      subSubItems = this.getSubItemSubitems(item);
      item = subSubItems[subSubItemIndex];
    }

    let checkedChange = false;
    if (item.role === 'menuitemcheckbox') {
      item.checked = !item.checked;
      checkedChange = true;
    } else if (item.role === 'menuitemradio') {
      checkedChange = !item.checked;
      subSubItems.forEach(i => i.checked = false);
      item.checked = true;
    }

    this.itemsChange.emit(items);
    this.activeSubItemChange.emit(item as MenubarItem);
    if (item instanceof MenubarSubitemComponent || item instanceof MenubarSubsubitemComponent) {
      item.selected.emit();

      if (checkedChange) {
        item.checkedChange.emit(item.checked);
      }
    }
  }

  mustCloseAfterSelectPopupItem(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): boolean {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    let item: MenubarItemSubItem|MenubarItemSubItemSubItem = subItems[subItemIndex];
    if (subSubItemIndex !== null && subSubItemIndex !== undefined) {
      const subSubItems = this.getSubItemSubitems(item);
      item = subSubItems[subSubItemIndex];
    }

    return item.role !== 'menuitemcheckbox' && item.role !== 'menuitemradio';
  }

  openMenu(itemIndex: number): void {
    const wasOpened = this.menuData[itemIndex].open;
    this.menuData[itemIndex].open = true;
    if (wasOpened) {
      this.activateMenuItem(itemIndex);
    }
  }

  closeMenu(itemIndex?: number): void {
    if (itemIndex !== null && itemIndex !== undefined) {
      this.menuData[itemIndex].open = false;
    } else {
      this.menuData.forEach(item => item.open = false);
    }
  }

  hasPopupMenu(itemIndex: number): boolean {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    return items[itemIndex].sub && subItems && subItems.length > 0;
  }

  private isPrintableChar(str: string): boolean {
    return str.length === 1 && !!str.match(/\S/);
  }

  private getMenuDataIndex(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): number {
    let menuDataIndex = 0;
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    for (let i = 0; i < subItemIndex; i++) {
      const subItem = subItems[i];
      const subSubItems = this.getSubItemSubitems(subItem);
      menuDataIndex += subSubItems && subSubItems.length > 0 ? subSubItems.length : 1;
    }

    if (subSubItemIndex !== null && subSubItemIndex !== undefined) {
      menuDataIndex += subSubItemIndex;
    }

    return menuDataIndex;
  }

  /*
   * Comunes - focus
   */

  focusItem(itemIndex: number, hover?: boolean): void {
    const hasFocus = this.menubar.nativeElement.contains(document.activeElement);
    const isOpen = this.menuData[this.currentFocusItemIndex].open;

    if (!hover || hasFocus) {
      this.menuData[itemIndex].menuItem.link.nativeElement.focus();
    }

    this.closeMenu();
    if (isOpen && this.hasPopupMenu(itemIndex)) {
      this.openMenu(itemIndex);
    }

    this.currentFocusItemIndex = itemIndex;
  }

  focusNextAvailableItem(fromItemIndex: number, step: number): void {
    let nextItem = fromItemIndex;
    const items = this.getItems();
    do {
      nextItem = (nextItem + items.length + step) % items.length;
    } while (!this.isItemFocusable(nextItem) && nextItem !== fromItemIndex);

    if (nextItem !== fromItemIndex) {
      this.focusItem(nextItem);
    }
  }

  focusFirstItem(): void {
    if (this.isItemFocusable(0)) {
      this.focusItem(0);
    } else {
      this.focusNextAvailableItem(0, +1);
    }
  }

  focusLastItem(): void {
    const items = this.getItems();
    if (this.isItemFocusable(items.length - 1)) {
      this.focusItem(items.length - 1);
    } else {
      this.focusNextAvailableItem(items.length - 1, -1);
    }
  }

  isItemFocusable(itemIndex: number): boolean {
    return !this.getItems()[itemIndex].disabled;
  }

  focusItemByFirstChar(char: string): void {
    const itemContents: string[] = this.menuItems.map(menuItem => menuItem.itemContentWrapper.nativeElement.textContent);
    let index = this.currentFocusItemIndex;
    let foundIndex = -1;
    do {
      index = (index + itemContents.length + 1) % itemContents.length;
      if (itemContents[index] && itemContents[index].trim().substr(0, 1).toLowerCase() === char.toLowerCase()
          && this.isItemFocusable(index)) {
        foundIndex = index;
      }
    } while (foundIndex === -1 && index !== this.currentFocusItemIndex);

    if (foundIndex >= 0) {
      this.focusItem(foundIndex);
    }
  }

  focusSubItem(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    if (this.menuData[itemIndex].menuItem.popupMenuItems.length > 0) {
      const menuDataIndex = this.getMenuDataIndex(itemIndex, subItemIndex, subSubItemIndex);

      this.menuData[itemIndex].currentFocusSubItemIndex = subItemIndex;
      this.menuData[itemIndex].currentFocusSubSubItemIndex = subSubItemIndex;
      const elem = this.menuData[itemIndex].menuItem.popupMenuItems.toArray()[menuDataIndex].nativeElement;
      if (elem) {
        elem.focus();
      } else {
        console.error('No subitem to focus');
      }
    } else {
      console.error('No subitems');
    }
  }

  focusNextAvailableSubItem(itemIndex: number, fromItemSubIndex: number, fromItemSubSubIndex: number, step: number): void {
    let nextSubIndex = fromItemSubIndex;
    let nextSubSubIndex = fromItemSubSubIndex;
    let allSubItemsChecked;
    const items = this.getItems();
    do {
      const nextIndexes = this.getNextSubItemIndexes(items, itemIndex, nextSubIndex, nextSubSubIndex, step);
      nextSubIndex = nextIndexes.subIndex;
      nextSubSubIndex = nextIndexes.subSubIndex;
      allSubItemsChecked = (fromItemSubIndex === nextSubIndex && fromItemSubSubIndex === nextSubSubIndex);
    } while (!this.isSubItemFocusable(itemIndex, nextSubIndex, nextSubSubIndex) && !allSubItemsChecked);

    if (!allSubItemsChecked) {
      this.focusSubItem(itemIndex, nextSubIndex, nextSubSubIndex);
    }
  }

  focusFirstSubItem(itemIndex: number): void {
    let firstSubSubItem = null;
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    const subSubItems = this.getSubItemSubitems(subItems[0]);
    if (subSubItems && subSubItems.length > 0) {
      firstSubSubItem = 0;
    }

    if (this.isSubItemFocusable(itemIndex, 0, firstSubSubItem)) {
      this.focusSubItem(itemIndex, 0, firstSubSubItem);
    } else {
      this.focusNextAvailableSubItem(itemIndex, 0, firstSubSubItem, +1);
    }
  }

  focusLastSubItem(itemIndex: number): void {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    const lastItemIndex = subItems.length - 1;
    const lastItem = subItems[lastItemIndex];
    const subSubItems = this.getSubItemSubitems(lastItem);
    let lastSubSubItem = null;
    if (subSubItems && subSubItems.length > 0) {
      lastSubSubItem = subSubItems.length - 1;
    }

    if (this.isSubItemFocusable(itemIndex, lastItemIndex, lastSubSubItem)) {
      this.focusSubItem(itemIndex, lastItemIndex, lastSubSubItem);
    } else {
      this.focusNextAvailableSubItem(itemIndex, lastItemIndex, lastSubSubItem, -1);
    }
  }

  isSubItemFocusable(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): boolean {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    let item: MenubarItemSubItem|MenubarItemSubItemSubItem = subItems[subItemIndex];
    if (subSubItemIndex !== null && subSubItemIndex !== undefined) {
      const subSubItems = this.getSubItemSubitems(item);
      item = subSubItems[subSubItemIndex];
    }

    let isFocusable = false;
    if (item.role !== 'separator' && item.role !== 'none') {
      isFocusable = true;
    }

    return isFocusable;
  }

  focusSubItemByFirstChar(itemIndex: number, char: string): void {
    const itemContents: string[] = this.menuItems.toArray()[itemIndex].popupMenuItems.map(menuItem => menuItem.nativeElement.textContent);
    let subItemIndex = this.menuData[itemIndex].currentFocusSubItemIndex;
    let subSubItemIndex = this.menuData[itemIndex].currentFocusSubSubItemIndex;
    let foundSubIndex = -1;
    let foundSubSubIndex = null;
    let allSubItemsChecked = false;
    const items = this.getItems();
    do {
      const nextIndexes = this.getNextSubItemIndexes(items, itemIndex, subItemIndex, subSubItemIndex, +1);
      subItemIndex = nextIndexes.subIndex;
      subSubItemIndex = nextIndexes.subSubIndex;
      const index = this.getMenuDataIndex(itemIndex, subItemIndex, subSubItemIndex);
      if (itemContents[index] && itemContents[index].trim().substr(0, 1).toLowerCase() === char.toLowerCase()
          && this.isSubItemFocusable(itemIndex, subItemIndex, subSubItemIndex)) {
        foundSubIndex = subItemIndex;
        foundSubSubIndex = subSubItemIndex;
      }
      allSubItemsChecked = (this.menuData[itemIndex].currentFocusSubItemIndex === subItemIndex
        && this.menuData[itemIndex].currentFocusSubSubItemIndex === subSubItemIndex);
    } while (foundSubIndex === -1 && !allSubItemsChecked);

    if (foundSubIndex >= 0) {
      this.focusSubItem(itemIndex, foundSubIndex, foundSubSubIndex);
    }
  }

  /*
   * Metodos para facilitar contenido
   */

  getItems(): MenubarItem[]|MenubarItemComponent[] {
    let items;
    if (this.itemComponentList && this.itemComponentList.length > 0) {
      items = this.itemComponentList.toArray();
    } else {
      items = this.items;
    }
    return items;
  }

  getItemSubitems(item: MenubarItem|MenubarItemComponent): MenubarItemSubItem[]|MenubarSubitemComponent[] {
    let subItems = [];
    if (item instanceof MenubarItemComponent){
      subItems = item.getSubItems();
    }

    if (!subItems || subItems.length === 0) {
      subItems = item.sub ? item.sub.items : [];
    }

    return subItems;
  }

  getSubItemSubitems(item: MenubarItemSubItem|MenubarSubitemComponent): MenubarItemSubItemSubItem[]|MenubarSubsubitemComponent[] {
    let subItems = [];

    if (item.role === 'group') {
      if (item instanceof MenubarSubitemComponent) {
        subItems = item.getSubItems();
      }

      if (!subItems || subItems.length === 0) {
        subItems = item.items ? item.items : [];
      }
    }

    return subItems;
  }

  getNextSubItemIndexes(items, itemIndex, nextSubIndex, nextSubSubIndex, step): { subIndex: number, subSubIndex?: number} {
    const subItems = this.getItemSubitems(items[itemIndex]);
    const subSubItems = this.getSubItemSubitems(subItems[nextSubIndex]);

    let checkNextSubItem = true;
    if (subSubItems && subSubItems.length > 0) {
      if (nextSubSubIndex + step >= 0 && nextSubSubIndex + step < subSubItems.length) {
        nextSubSubIndex += step;
        checkNextSubItem = false;
      }
    }

    if (checkNextSubItem) {
      nextSubIndex = (nextSubIndex + step + subItems.length) % subItems.length;
      const nextSubItemItems = this.getSubItemSubitems(subItems[nextSubIndex]);
      if (nextSubItemItems && nextSubItemItems.length > 0) {
        nextSubSubIndex = ((step > 0 ? -1 : 0) + step + nextSubItemItems.length) % nextSubItemItems.length;
      } else {
        nextSubSubIndex = null;
      }
    }

    return {subIndex: nextSubIndex, subSubIndex: nextSubSubIndex};
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : this.id + 'menubar-item';
  }

  getLabelRef(): TemplateRef<any> {
    return this.labelComponent ? this.labelComponent.getContent() : this.labelRef;
  }

  getLabelContent(): string {
    let content: string;
    if (this.labelData) {
      content = this.labelData.html ? this.labelData.html : `<p>${ StringUtils.escapeHtml(this.labelData.text) }</p>`;
    } else if (this.labelText) {
      content = `<p>${ StringUtils.escapeHtml(this.labelText) }</p>`;
    }
    return content;
  }

  getItemId(item, index: number): string {
    let id: string;
    if (item) {
      if (item.id) {
        id = item.id;
      } else {
        id = `${this.getIdPrefix()}-${++index}`;
      }
    }
    return id;
  }

  getPopupStyle(itemIndex: number): object {
    let style = {};
    if (this.menuData && itemIndex < this.menuData.length) {
      const menuItem = this.menuData[itemIndex].menuItem;
      const rect = menuItem.link.nativeElement.getBoundingClientRect();
      if (this.menuData[itemIndex].open) {
        style = {
          position: 'absolute',
          top: (rect.height - 1) + 'px',
          left: '0px',
          zIndex: 100,
          display: 'block'
        };
      } else {
        style = {
          zIndex: 0,
          display: 'none'
        };
      }
    }

    return style;
  }

  getSubItemId(item, index: number, parentId: string): string {
    let id: string;
    if (item) {
      if (item.id) {
        id = item.id;
      } else {
        id = `sub-${parentId}-${++index}`;
      }
    }
    return id;
  }

  getSubSubItemAriaChecked(subsubitem): string {
    let ariaChecked = null;
    if (subsubitem.role === 'menuitemcheckbox' || subsubitem.role === 'menuitemradio') {
      ariaChecked = !!subsubitem.checked;
    }
    return ariaChecked;
  }
}
