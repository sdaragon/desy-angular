import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';

@Component({
  selector: 'desy-links-icon-right',
  templateUrl: './links-list-icon-right.component.html',
})
export class LinksListIconRightComponent extends ContentBaseComponent {

  @Input() type?: string;
  @Input() containerClasses?: string;

}
