import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';

@Component({
  selector: 'desy-links-list-item-sub',
  templateUrl: './links-list-item-sub.component.html',
})
export class LinksListItemSubComponent extends ContentBaseComponent {

  @Input() classes?: string;
}
