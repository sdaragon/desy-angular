import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { LinksListItemComponent } from './links-list-item/links-list-item.component';

@Component({
  selector: 'desy-links-list',
  templateUrl: './links-list.component.html',
})
export class LinksListComponent extends AccessibilityComponent {

  @Input() hasNav = true;
  @Input() classes?: string;
  @Input() listClasses?: string;
  @Input() idPrefix?: string;

  @ContentChildren(LinksListItemComponent) items: QueryList<LinksListItemComponent>;


  getItemId(item: LinksListItemComponent, index: number): string {
    let id;
    if (item.id) {
      id = item.id;
    } else {
      id = this.idPrefix || 'links-list-item';
      id += '-' + ++index;
    }
    return id;
  }
}
