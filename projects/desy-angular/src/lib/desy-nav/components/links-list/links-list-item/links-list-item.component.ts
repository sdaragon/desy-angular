import { Component, ContentChild, Input } from '@angular/core';
import { IconComponent } from '../../../../desy-commons/components/icon/icon.component';
import { ContentBaseComponent } from '../../../../shared/components';
import { LinksListIconRightComponent } from '../links-list-icon-right/links-list-icon-right.component';
import { LinksListItemSubComponent } from '../links-list-item-sub/links-list-item-sub.component';

@Component({
  selector: 'desy-links-list-item',
  templateUrl: './links-list-item.component.html',
})
export class LinksListItemComponent extends ContentBaseComponent {

  @Input() containerClasses?: string;
  @Input() classes?: string | null;
  @Input() titleModal?: string;
  @Input() id?: string;
  @Input() href?: string | null;
  @Input() routerLink?: string | any[] | null;
  @Input() routerLinkActiveClasses: string|string[];
  @Input() target?: string | null;
  @Input() fragment?: string | null;
  @Input() active?: boolean;
  @Input() disabled?: boolean;
  @ContentChild(IconComponent) icon: IconComponent;
  @ContentChild(LinksListIconRightComponent) iconRight: LinksListIconRightComponent;
  @ContentChild(LinksListItemSubComponent) sub: LinksListItemSubComponent;

}
