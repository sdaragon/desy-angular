import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';
import { MenuVerticalSubItemsData } from '../../../interfaces';

@Component({
  selector: 'desy-menu-vertical-item-sub-item',
  templateUrl: './menu-vertical-item-sub-item.component.html'
})
export class MenuVerticalItemSubItemComponent extends ContentBaseComponent implements MenuVerticalSubItemsData {

  @Input() id?: string;
  @Input() classes?: string;
  @Input() href?: string;
  @Input() routerLink?: string;
  @Input() routerLinkActiveClasses?: string;
  @Input() fragment?: string;
  @Input() target?: string;
  @Input() disabled?: boolean;
  @Input() active?: boolean;
  @Input() divider?: boolean;
  @Input() titleModal?: string;
}
