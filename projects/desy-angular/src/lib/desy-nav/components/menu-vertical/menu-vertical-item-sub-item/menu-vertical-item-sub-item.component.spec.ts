import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuVerticalItemSubItemComponent } from './menu-vertical-item-sub-item.component';

xdescribe('MenuVerticalItemSubItemComponent', () => {
  let component: MenuVerticalItemSubItemComponent;
  let fixture: ComponentFixture<MenuVerticalItemSubItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuVerticalItemSubItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuVerticalItemSubItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
