import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { StringUtils } from '../../../shared/utils/string-utils';
import { MenuVerticalItemsData, MenuVerticalSubData, MenuVerticalSubItemsData } from '../../interfaces';
import { MenuVerticalItemSubComponent } from './menu-vertical-item-sub/menu-vertical-item-sub.component';
import { MenuVerticalItemComponent } from './menu-vertical-item/menu-vertical-item.component';

@Component({
  selector: 'desy-menu-vertical',
  templateUrl: './menu-vertical.component.html',
  animations: [
    trigger('displayMenuVertical', [
      state('void', style({
        opacity: '0.0',
        transform: 'scale(0.95)'
      })),
      state('*', style({
        opacity: '1.0',
        transform: 'scale(1.0)'
      })),
      transition(':enter', [
        animate('100ms ease-out')
      ])
    ])
  ]
})
export class MenuVerticalComponent extends AccessibilityComponent {

  @Input() id: string;
  @Input() idPrefix: string;
  @Input() items: MenuVerticalItemsData[];
  @Input() hasUnderline: boolean;
  @Input() classes: string;

  @ContentChildren(MenuVerticalItemComponent) itemComponents: QueryList<MenuVerticalItemComponent>;

  getItemHtml(item: MenuVerticalItemsData): string {
    return item.html ? item.html : StringUtils.escapeHtml(item.text);
  }

  getId(item: MenuVerticalItemsData, i: number): string {
    if (item.id) {
      return item.id;
    } else {
      const idPrefix = this.idPrefix ? this.idPrefix : 'nav-item';
      return idPrefix + '-' + ++i;
    }
  }

  getSubItemId(item: MenuVerticalItemsData, itemIndex: number, subItemIndex: number): string {
    let subItemId: string;
    const sub = this.getItemSub(item);
    const subItems = this.getSubItems(sub);
    const subItem = subItems[subItemIndex];
    if (subItem) {
      if (subItem.id) {
        subItemId = subItem.id;
      } else {
        const itemId = this.getId(item, itemIndex);
        subItemId = `sub-${itemId}-${++subItemIndex}`;
      }
    }

    return subItemId;
  }

  getItems(): MenuVerticalItemsData[] {
    const itemList = this.itemComponents && this.itemComponents.length > 0 ? this.itemComponents.toArray() : this.items;
    return itemList;
  }

  getItemSub(item: MenuVerticalItemsData): MenuVerticalSubData {
    return item instanceof MenuVerticalItemComponent ? item.subComponent : item.sub;
  }

  getSubItems(sub: MenuVerticalSubData): MenuVerticalSubItemsData[] {
    const subItems = sub instanceof MenuVerticalItemSubComponent ? sub.itemComponents.toArray() : sub.items;
    return subItems && subItems.length > 0 ? subItems : null;
  }
}
