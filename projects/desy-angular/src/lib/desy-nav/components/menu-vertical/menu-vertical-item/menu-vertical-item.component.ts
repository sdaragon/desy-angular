import { Component, ContentChildren, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';
import { DesyContentChild } from '../../../../shared/decorators/desy-content-child.decorator';
import { MenuVerticalItemsData } from '../../../interfaces';
import { MenuVerticalItemSubComponent } from '../menu-vertical-item-sub/menu-vertical-item-sub.component';

@Component({
  selector: 'desy-menu-vertical-item',
  templateUrl: './menu-vertical-item.component.html'
})
export class MenuVerticalItemComponent extends ContentBaseComponent implements MenuVerticalItemsData {

  @Input() id?: string;
  @Input() classes?: string;
  @Input() href?: string;
  @Input() routerLink?: string;
  @Input() routerLinkActiveClasses?: string;
  @Input() fragment?: string;
  @Input() target?: string;
  @Input() disabled?: boolean;
  @Input() active?: boolean;
  @Input() divider?: boolean;
  @Input() titleModal?: string;

  @DesyContentChild()
  @ContentChildren(MenuVerticalItemSubComponent) subComponent: MenuVerticalItemSubComponent;

}
