import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuVerticalItemComponent } from './menu-vertical-item.component';

xdescribe('MenuVerticalItemComponent', () => {
  let component: MenuVerticalItemComponent;
  let fixture: ComponentFixture<MenuVerticalItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuVerticalItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuVerticalItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
