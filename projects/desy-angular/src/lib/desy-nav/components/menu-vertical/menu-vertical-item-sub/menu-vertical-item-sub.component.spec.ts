import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuVerticalItemSubComponent } from './menu-vertical-item-sub.component';

xdescribe('MenuVerticalItemSubComponent', () => {
  let component: MenuVerticalItemSubComponent;
  let fixture: ComponentFixture<MenuVerticalItemSubComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuVerticalItemSubComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuVerticalItemSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
