import {Component, ContentChildren, Input, QueryList} from '@angular/core';
import {MenuVerticalSubData} from '../../../interfaces';
import {MenuVerticalItemSubItemComponent} from '../menu-vertical-item-sub-item/menu-vertical-item-sub-item.component';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-menu-vertical-item-sub',
  templateUrl: './menu-vertical-item-sub.component.html'
})
export class MenuVerticalItemSubComponent extends ContentBaseComponent implements MenuVerticalSubData {
  @Input() classes?: string;

  @ContentChildren(MenuVerticalItemSubItemComponent) itemComponents: QueryList<MenuVerticalItemSubItemComponent>;
}
