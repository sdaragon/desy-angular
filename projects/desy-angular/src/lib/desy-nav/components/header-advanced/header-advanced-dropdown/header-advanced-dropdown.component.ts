import { Component, ContentChildren, Input } from '@angular/core';
import { ContentComponent } from "../../../../desy-commons/components/content/content.component";
import { AccessibilityComponent } from "../../../../shared/components";
import { DesyContentChild } from '../../../../shared/decorators/desy-content-child.decorator';
import { NavItemData } from '../../../interfaces/nav-item.data';

@Component({
  selector: 'desy-header-advanced-dropdown',
  templateUrl: './header-advanced-dropdown.component.html',
})
export class HeaderAdvancedDropdownComponent extends AccessibilityComponent {

  @Input() hiddenText: string;
  @Input() classesContainer: string;
  @Input() classesTooltip: string;
  @Input() classes: string;
  @Input() items: NavItemData[];

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

}
