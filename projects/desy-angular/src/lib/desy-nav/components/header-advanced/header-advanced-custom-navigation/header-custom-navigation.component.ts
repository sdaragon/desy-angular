import { Component } from '@angular/core';

@Component({
  selector: 'desy-header-advanced-custom-navigation',
  template: '<ng-content></ng-content>'
})
export class HeaderAdvancedCustomNavigationComponent {
}
