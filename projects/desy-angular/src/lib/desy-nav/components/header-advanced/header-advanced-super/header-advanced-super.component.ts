import { Component, Input } from '@angular/core';
import { Type } from '../../../interfaces/header-advanced-logo';

@Component({
  selector: 'desy-header-advanced-super, desy-header-mini-super',
  templateUrl: './header-advanced-super.component.html',
})
export class HeaderAdvancedSuperComponent {
  @Input() classes: string;
  @Input() backgroundFullColor: string;
  @Input() backgroundFullUrl: string;
  @Input() backgroundContainerUrl: string;
  @Input() logoUrl: string;
  @Input() logoAlt: string;
  @Input() logoHref: string;
  @Input() logoRouterLink: string;
  @Input() logoRouterLinkActiveClasses: string;
  @Input() logoTarget: string;
  @Input() logoFragment: string;
  @Input() logoClasses: string;
  super = Type.Super;
}
