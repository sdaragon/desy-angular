import { Component, Input } from '@angular/core';

@Component({
  selector: 'desy-header-advanced-title',
  templateUrl: './header-advanced-title.component.html',
})
export class HeaderAdvancedTitleComponent {
  @Input() classes: string;
  @Input() headingLevel: number;
  @Input() homepageUrl: string;
}
