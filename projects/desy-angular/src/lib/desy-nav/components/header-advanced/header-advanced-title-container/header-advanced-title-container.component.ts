import { Component, Input } from '@angular/core';
import { Type } from '../../../interfaces/header-advanced-logo';

@Component({
  selector: 'desy-header-advanced-title-container',
  templateUrl: './header-advanced-title-container.component.html',
})
export class HeaderAdvancedTitleContainerComponent {
  @Input() classes: string;
  @Input() backgroundColor: string;
  @Input() logoUrl: string;
  @Input() logoAlt: string;
  @Input() logoHref: string;
  @Input() logoRouterLink: string;
  @Input() logoRouterLinkActiveClasses: string;
  @Input() logoTarget: string;
  @Input() logoFragment: string;
  @Input() logoClasses: string;
  title = Type.Title;
}
