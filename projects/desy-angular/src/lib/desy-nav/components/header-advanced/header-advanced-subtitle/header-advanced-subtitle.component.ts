import { Component, Input } from '@angular/core';

@Component({
  selector: 'desy-header-advanced-subtitle',
  templateUrl: './header-advanced-subtitle.component.html'
})
export class HeaderAdvancedSubtitleComponent {
  @Input() classes: string;
}
