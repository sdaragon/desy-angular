import { Component, ContentChildren, Input } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { HeaderMiniComponent } from '../header-mini/header-mini.component';
import { HeaderNavigationComponent } from '../header/header-navigation/header-navigation.component';
import { SkipLinkComponent } from '../skip-link/skip-link.component';

@Component({
  selector: 'desy-header-advanced',
  templateUrl: './header-advanced.component.html'
})
export class HeaderAdvancedComponent extends AccessibilityComponent {
  @Input() classes: string;
  @Input() containerClasses: string;

  @DesyContentChild({ onSetCallbackName: 'overrideSkiplinkParams' })
  @ContentChildren(SkipLinkComponent) skiplinkComponent: SkipLinkComponent;

  @DesyContentChild()
  @ContentChildren(HeaderMiniComponent) headerMiniComponent: HeaderMiniComponent;

  @DesyContentChild({ onSetCallbackName: 'overrideNavigationParams' })
  @ContentChildren(HeaderNavigationComponent) navigationComponent: HeaderNavigationComponent;

  overrideNavigationParams(navigationComponent: HeaderNavigationComponent): void {
    navigationComponent.idPrefix = 'header-nav-item';
    navigationComponent.ariaLabel = 'Menú principal';
  }
  overrideSkiplinkParams(skiplinkComponent: SkipLinkComponent): void {
    if (!skiplinkComponent.id) skiplinkComponent.id = 'skip-link';
  }
}
