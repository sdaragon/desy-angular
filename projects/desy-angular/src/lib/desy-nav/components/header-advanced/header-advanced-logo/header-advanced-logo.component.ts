import { Component, Input } from '@angular/core';
import { Type } from '../../../interfaces/header-advanced-logo';

@Component({
  selector: 'desy-header-advanced-logo',
  templateUrl: './header-advanced-logo.component.html',
})
export class HeaderAdvancedLogoComponent {
  @Input() url: string;
  @Input() alt: string;
  @Input() href: string;
  @Input() fragment: string;
  @Input() routerLink: string;
  @Input() routerLinkActiveClasses: string|string[];
  @Input() target: string;
  @Input() classes: string;
  @Input({ required: true }) type: Type;
  typeEnum = Type;
}
