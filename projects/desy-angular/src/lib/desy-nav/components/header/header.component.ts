import {Component, ContentChildren, Input} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { HeaderSubnavData } from '../../interfaces';
import { HeaderNavigationData } from '../../interfaces';
import { HeaderDropdownData } from '../../interfaces';
import { HeaderOffcanvasData } from '../../interfaces';
import {DesyContentChild} from '../../../shared/decorators/desy-content-child.decorator';
import {HeaderOffcanvasComponent} from './header-offcanvas/header-offcanvas.component';
import {HeaderNavigationComponent} from './header-navigation/header-navigation.component';
import {HeaderSubnavComponent} from './header-subnav/header-subnav.component';
import {HeaderDropdownComponent} from './header-dropdown/header-dropdown.component';
import {SkipLinkComponent} from '../skip-link/skip-link.component';
import { HeaderCustomNavigationComponent } from './header-custom-navigation/header-custom-navigation.component';
import { HeaderSkipLinkData } from '../../interfaces/header-skiplink-data';
import { HeaderMobileTextComponent } from './header-mobileText/header-mobile-text.component';
import { HeaderMobileTextData } from '../../interfaces/header-mobile-text-data';

@Component({
  selector: 'desy-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent extends AccessibilityComponent {


  @Input() classes: string;
  @Input() containerClasses: string;
  @Input() homepageUrl: string;
  @Input() homepageRouterLink: string;
  @Input() homepageFragment: string;
  @Input() expandedLogo: boolean;
  @Input() noLogo: boolean;
  @Input() customLogoHtml: string;
  @Input() customNavigationHtml: string;

  @Input() subnavData: HeaderSubnavData;
  @Input() navigationData: HeaderNavigationData;
  @Input() dropdownData: HeaderDropdownData;
  @Input() offcanvasData: HeaderOffcanvasData;
  @Input() skipLinkData: HeaderSkipLinkData;
  @Input() mobileTextData: HeaderMobileTextData;

  @DesyContentChild()
  @ContentChildren(SkipLinkComponent) skiplinkComponent: SkipLinkComponent;

  @DesyContentChild()
  @ContentChildren(HeaderSubnavComponent) subnavComponent: HeaderSubnavComponent;

  @DesyContentChild()
  @ContentChildren(HeaderMobileTextComponent) mobileTextComponent: HeaderMobileTextComponent;

  @DesyContentChild()
  @ContentChildren(HeaderCustomNavigationComponent) customNavigationComponent: HeaderCustomNavigationComponent;

  @DesyContentChild({ onSetCallbackName: 'overrideNavigationParams'})
  @ContentChildren(HeaderNavigationComponent) navigationComponent: HeaderNavigationComponent;

  @DesyContentChild()
  @ContentChildren(HeaderDropdownComponent) dropdownComponent: HeaderDropdownComponent;

  @DesyContentChild()
  @ContentChildren(HeaderOffcanvasComponent) offcanvasComponent: HeaderOffcanvasComponent;

  overrideNavigationParams(navigationComponent: HeaderNavigationComponent): void {
    navigationComponent.idPrefix = 'header-nav-item';
    navigationComponent.ariaLabel = 'Menú principal';
  }
}
