import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderCustomNavigationComponent } from './header-custom-navigation.component';

xdescribe('HeaderCustomNavigationComponent', () => {
  let component: HeaderCustomNavigationComponent;
  let fixture: ComponentFixture<HeaderCustomNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderCustomNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderCustomNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
