import { Component, OnInit } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';

@Component({
  selector: 'desy-header-custom-navigation',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class HeaderCustomNavigationComponent extends ContentBaseComponent{


}
