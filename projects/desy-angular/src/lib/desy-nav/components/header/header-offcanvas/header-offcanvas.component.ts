import {
  Component,
  ComponentRef,
  ContentChildren,
  HostBinding,
  Input,
  OnDestroy,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { ContentComponent } from '../../../../desy-commons/components/content/content.component';
import { DialogComponent } from '../../../../desy-modals/components/dialog/dialog.component';
import { DialogOptions } from '../../../../desy-modals/interfaces';
import { DialogService } from '../../../../desy-modals/services/dialog.service';
import { DesyContentChild } from '../../../../shared/decorators/desy-content-child.decorator';
import { DesyOnInputChange } from '../../../../shared/decorators/desy-on-input-change.decorator';
import { HeaderOffcanvasButtonComponent } from './header-offcanvas-button/header-offcanvas-button.component';
import { HeaderOffcanvasCloseButtonComponent } from './header-offcanvas-close-button/header-offcanvas-close-button.component';

@Component({
  selector: 'desy-header-offcanvas, desy-header-advanced-offcanvas',
  templateUrl: './header-offcanvas.component.html'
})
export class HeaderOffcanvasComponent implements OnDestroy {

  private static dialogOptions: DialogOptions = {
    id: 'header-offcanvas-dialog',
    focusOnClose: 'header-offcanvas-button',
    ariaModal: 'true',
    ariaLabelledBy: 'header-offcanvas-button-text',
    role: 'dialog'
  };

  @DesyOnInputChange('setHostClass')
  @Input() classes: string;

  @HostBinding('class') hostClass = '-mr-2 flex lg:hidden';

  @ViewChild('offcanvasContent') offcanvasContent: TemplateRef<any>;

  @DesyContentChild({ onSetCallbackName: 'bindButton' })
  @ContentChildren(HeaderOffcanvasButtonComponent) button: HeaderOffcanvasButtonComponent;

  @DesyContentChild()
  @ContentChildren(ContentComponent) content: ContentComponent;

  @DesyContentChild()
  @ContentChildren(HeaderOffcanvasCloseButtonComponent) closeButton: HeaderOffcanvasCloseButtonComponent;

  private dialog: ComponentRef<DialogComponent>;

  constructor(private dialogService: DialogService) {
  }

  ngOnDestroy(): void {
    if (this.isOpen()) {
      this.close();
    }
  }

  async open(): Promise<void> {
    if (!this.isOpen()) {
      this.dialog = (await this.dialogService.openDialog(this.offcanvasContent, HeaderOffcanvasComponent.dialogOptions)).dialog;
      this.dialogService.onCloseDialog(this.dialog).then(() => this.dialog = null);
    }
  }

  close(): void {
    if (this.isOpen()) {
      this.dialogService.closeDialog(this.dialog);
      this.dialog = null;
      const elementToFocus = document.getElementById('header-offcanvas-button');
      elementToFocus?.focus()
    }
  }

  isOpen(): boolean {
    return !!this.dialog;
  }

  /**
   * Estable las clases del elemento html del componente
   */
  setHostClass(): void {
    this.hostClass = this.classes ? this.classes : '-mr-2 flex lg:hidden';
  }

  /**
   * Enlaza el botón del componente hijo con la acción de abrir de este componente
   * @param button componente botón pasado como contenido
   */
  bindButton(button: HeaderOffcanvasButtonComponent): void {
    button.openOffcanvas = this.open.bind(this);
  }
}
