import { Component } from '@angular/core';
import { ContentBaseComponent } from '../../../../../shared/components';

@Component({
  selector: 'desy-header-offcanvas-close-button, desy-header-advanced-offcanvas-close-button',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class HeaderOffcanvasCloseButtonComponent extends ContentBaseComponent {

}
