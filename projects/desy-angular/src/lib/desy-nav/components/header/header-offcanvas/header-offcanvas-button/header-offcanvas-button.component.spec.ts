import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderOffcanvasButtonComponent } from './header-offcanvas-button.component';

xdescribe('HeaderOffcanvasButtonComponent', () => {
  let component: HeaderOffcanvasButtonComponent;
  let fixture: ComponentFixture<HeaderOffcanvasButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderOffcanvasButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderOffcanvasButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
