import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderOffcanvasCloseButtonComponent } from './header-offcanvas-close-button.component';

xdescribe('HeaderOffcanvasCloseButtonComponent', () => {
  let component: HeaderOffcanvasCloseButtonComponent;
  let fixture: ComponentFixture<HeaderOffcanvasCloseButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderOffcanvasCloseButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderOffcanvasCloseButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
