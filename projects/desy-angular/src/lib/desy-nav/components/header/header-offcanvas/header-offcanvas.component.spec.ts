import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderOffcanvasComponent } from './header-offcanvas.component';

xdescribe('HeaderOffcanvasComponent', () => {
  let component: HeaderOffcanvasComponent;
  let fixture: ComponentFixture<HeaderOffcanvasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderOffcanvasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderOffcanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
