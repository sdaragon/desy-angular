import { Component } from '@angular/core';
import { ContentBaseComponent } from '../../../../../shared/components';

@Component({
  selector: 'desy-header-offcanvas-button, desy-header-advanced-offcanvas-button',
  templateUrl: './header-offcanvas-button.component.html'
})
export class HeaderOffcanvasButtonComponent extends ContentBaseComponent {

  openOffcanvas = async () => {};

  async handleClick(event: Event): Promise<void> {
    event.stopPropagation();
    await this.openOffcanvas();
  }


}
