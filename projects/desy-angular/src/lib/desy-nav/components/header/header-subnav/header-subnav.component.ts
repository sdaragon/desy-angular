import {Component, ContentChildren, Input} from '@angular/core';
import {NavComponent} from '../../nav/nav.component';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {AccessibilityComponent} from "../../../../shared/components";

@Component({
  selector: 'desy-header-subnav',
  templateUrl: './header-subnav.component.html'
})
export class HeaderSubnavComponent extends AccessibilityComponent {

  @Input() hiddenText: string;
  @Input() classesContainer: string;
  @Input() classesTooltip: string;
  @Input() classes: string;

  @DesyContentChild({onSetCallbackName: 'overrideNavParams'})
  @ContentChildren(NavComponent) nav: NavComponent;

  hasSubnavItems(): boolean {
    return this.nav && this.nav.getItemList().length > 0;
  }

  overrideNavParams(nav: NavComponent): void {
    nav.hasNav = true;
    nav.idPrefix = 'header-subnav-nav-item';
    nav.ariaLabel = 'Aplicaciones';
    nav.classes = 'w-max max-w-64';
    // nav.id = 'id-subnav-nav'; // No existe como param de navComponent
  }


}
