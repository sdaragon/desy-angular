import {Component, ContentChildren, Input} from '@angular/core';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {NavComponent} from '../../nav/nav.component';
import {AccessibilityComponent} from "../../../../shared/components";
import {ContentComponent} from "../../../../desy-commons/components/content/content.component";

@Component({
  selector: 'desy-header-dropdown',
  templateUrl: './header-dropdown.component.html',
})
export class HeaderDropdownComponent extends AccessibilityComponent {

  @Input() hiddenText: string;
  @Input() classesContainer: string;
  @Input() classesTooltip: string;
  @Input() classes: string;

  @DesyContentChild({onSetCallbackName: 'overrideNavParams'})
  @ContentChildren(NavComponent) nav: NavComponent;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  overrideNavParams(nav: NavComponent): void {
    nav.hasNav = true;
    nav.idPrefix = 'header-dropdown-nav-item';
    nav.ariaLabel = 'Menú de usuario';
    nav.classes = 'w-max max-w-64';
    // nav.id = 'id-dropdown-nav'; // No existe como param de navComponent
  }

}
