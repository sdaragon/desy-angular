import { AfterContentInit, Component, ContentChildren, Input, QueryList } from '@angular/core';
import { AccessibilityComponent } from '../../../../shared/components';
import { HeaderNavigationItemData } from '../../../interfaces/header-navigation-item-data';
import { HeaderNavigationItemComponent } from './header-navigation-item/header-navigation-item.component';

@Component({
  selector: 'desy-header-navigation, desy-header-advanced-navigation',
  templateUrl: './header-navigation.component.html'
})
export class HeaderNavigationComponent extends AccessibilityComponent implements AfterContentInit {

  @Input() idPrefix: string;
  @Input() classes: string;
  @Input() items: HeaderNavigationItemData[];

  @ContentChildren(HeaderNavigationItemComponent) itemComponents: QueryList<HeaderNavigationItemComponent>;

  ngAfterContentInit(): void {
    const items = this.getItems();
  }

  getItemId(item: HeaderNavigationItemData, index: number): string {
    const prefix = this.idPrefix ? this.idPrefix : 'header-nav-item';
    return item.id ? item.id : prefix +  '-' + ++index;
  }

  getItems(): HeaderNavigationItemData[] {
    return this.itemComponents && this.itemComponents.length > 0 ? this.itemComponents.toArray() : this.items;
  }
}
