import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNavigationItemComponent } from './header-navigation-item.component';

xdescribe('HeaderNavigationItemComponent', () => {
  let component: HeaderNavigationItemComponent;
  let fixture: ComponentFixture<HeaderNavigationItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderNavigationItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderNavigationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
