import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../../shared/components';
import { HeaderNavigationItemData } from '../../../../interfaces/header-navigation-item-data';

@Component({
  selector: 'desy-header-navigation-item, desy-header-advanced-navigation-item',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class HeaderNavigationItemComponent extends ContentBaseComponent implements HeaderNavigationItemData {
  @Input() href: string;
  @Input() routerLink: string;
  @Input() routerLinkActiveClasses: string|string[];
  @Input() fragment: string;
  @Input() target: string;
  @Input() id: string;
  @Input() active: boolean;
  @Input() disabled: boolean;
}
