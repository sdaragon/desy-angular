import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../../shared/components';
import { MetaItemData } from '../../../../interfaces';

@Component({
  selector: 'desy-footer-meta-item',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class FooterMetaItemComponent extends ContentBaseComponent implements MetaItemData {
  @Input() href?: string;
  @Input() routerLink?: string;
  @Input() routerLinkActiveClasses: string|string[];
  @Input() fragment?: string;
  @Input() target: string;
}
