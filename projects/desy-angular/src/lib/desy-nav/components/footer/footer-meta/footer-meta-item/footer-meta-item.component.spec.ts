import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterMetaItemComponent } from './footer-meta-item.component';

xdescribe('FooterMetaItemComponent', () => {
  let component: FooterMetaItemComponent;
  let fixture: ComponentFixture<FooterMetaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterMetaItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterMetaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
