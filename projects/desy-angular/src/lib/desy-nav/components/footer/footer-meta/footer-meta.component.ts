import {Component, ContentChildren, Input, QueryList} from '@angular/core';
import {MetaData} from '../../../interfaces';
import {FooterMetaItemComponent} from './footer-meta-item/footer-meta-item.component';
import {ContentComponent} from '../../../../desy-commons/components/content/content.component';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-footer-meta',
  template: ''
})
export class FooterMetaComponent extends ContentBaseComponent implements MetaData {

  @Input() visuallyHiddenTitle: string;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;
  @ContentChildren(FooterMetaItemComponent) itemComponents: QueryList<FooterMetaItemComponent>;
}
