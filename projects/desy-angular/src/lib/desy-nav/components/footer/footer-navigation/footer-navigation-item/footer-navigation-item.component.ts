import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../../shared/components';
import { NavigationItemData } from '../../../../interfaces';

@Component({
  selector: 'desy-footer-navigation-item',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class FooterNavigationItemComponent extends ContentBaseComponent implements NavigationItemData {
  @Input() href?: string;
  @Input() routerLink?: string;
  @Input() routerLinkActiveClasses: string|string[];
  @Input() fragment?: string;
  @Input() target: string;
}
