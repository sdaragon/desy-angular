import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { NavigationData } from '../../../interfaces';
import { FooterNavigationItemComponent } from './footer-navigation-item/footer-navigation-item.component';

@Component({
  selector: 'desy-footer-navigation',
  templateUrl: './footer-navigation.component.html'
})
export class FooterNavigationComponent implements NavigationData {
  @Input() title: string;
  @Input() columns: number;
  @Input() classes: number;

  @ContentChildren(FooterNavigationItemComponent) itemComponents: QueryList<FooterNavigationItemComponent>;
}
