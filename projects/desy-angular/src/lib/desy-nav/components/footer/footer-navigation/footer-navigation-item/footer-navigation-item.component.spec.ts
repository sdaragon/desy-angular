import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterNavigationItemComponent } from './footer-navigation-item.component';

xdescribe('FooterNavigationItemComponent', () => {
  let component: FooterNavigationItemComponent;
  let fixture: ComponentFixture<FooterNavigationItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterNavigationItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterNavigationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
