import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { IconComponent } from '../../../desy-commons/components/icon/icon.component';
import { IconData } from '../../../desy-commons/interfaces';
import { DescriptionData } from '../../../desy-commons/interfaces/';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { MetaData, MetaItemData, NavigationData, NavigationItemData } from '../../interfaces';
import { Type } from '../../interfaces/footer-logo';
import { DescriptionComponent } from './../../../desy-commons/components/description/description.component';
import { FooterMetaComponent } from './footer-meta/footer-meta.component';
import { FooterNavigationComponent } from './footer-navigation/footer-navigation.component';


@Component({
  selector: 'desy-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent extends AccessibilityComponent {

  @Input() meta: MetaData;
  @Input() navigation: NavigationData[];
  @Input() icon: IconData;
  @Input() containerClasses: string;
  @Input() classes: string;
  @Input() description: DescriptionData;
  @Input() noLogo: boolean;
  @Input() urlFeder: string;

  @Input() url: string;
  @Input() logoContainerClasses: string;
  @Input() type: Type | string = Type.FEDER;
  typeEnum = Type;

  @DesyContentChild()
  @ContentChildren(IconComponent) iconComponent: IconComponent;

  @DesyContentChild()
  @ContentChildren(FooterMetaComponent) metaComponent: FooterMetaComponent;

  @DesyContentChild()
  @ContentChildren(DescriptionComponent) descriptionComponent: DescriptionComponent

  @ContentChildren(FooterNavigationComponent) navigationComponent: QueryList<FooterNavigationComponent>;


  getNavigationItems(): NavigationData[] {
    const navItems = this.navigationComponent && this.navigationComponent.length > 0 ? this.navigationComponent.toArray() : this.navigation;
    return navItems && navItems.length > 0 ? navItems : null;
  }

  getNavigationItemItems(navigation: NavigationData): NavigationItemData[] {
    return navigation instanceof FooterNavigationComponent ? navigation.itemComponents.toArray() : navigation.items;
  }

  getMetaItems(meta: MetaData): MetaItemData[] {
    return meta instanceof FooterMetaComponent ? meta.itemComponents.toArray() : meta.items;
  }

  getMetaContent(meta: MetaData): ContentComponent {
    return meta instanceof FooterMetaComponent ? meta.contentComponent : null;
  }
}
