import { Location } from '@angular/common';
import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { BreadcrumbsData } from '../../interfaces';
import { BreadcrumbsItemComponent } from './breadcrumbs-item/breadcrumbs-item.component';

@Component({
  selector: 'desy-breadcrumbs',
  templateUrl: './breadcrumbs.component.html'
})
export class BreadcrumbsComponent extends AccessibilityComponent {

  @Input() items: Array<BreadcrumbsData>;
  @Input() classes: string;
  @Input() id: string;

  @Input() collapseOnMobile: boolean;
  @Input() inlineOnMobile: boolean;
  @Input() inlineOnDesktop: boolean;
  @Input() hasBackButton: boolean;
  @ContentChildren(BreadcrumbsItemComponent) itemComponentList: QueryList<BreadcrumbsItemComponent>;

  constructor(private location: Location){
    super();
  }

  getLength(): number {
    let itemsLength: number = this.items ? this.items.length : this.itemComponentList.length;

    if (this.hasBackButton) {
      itemsLength += 1;
    }
    return itemsLength;
  }

  goBack(): void {
    this.location.back();
  }

  getItemList(): BreadcrumbsData[] {
    const itemList = this.itemComponentList && this.itemComponentList.length > 0 ? this.itemComponentList.toArray() : this.items;

    return itemList;
  }

}
