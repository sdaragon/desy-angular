import {Component, Input} from '@angular/core';
import { BreadcrumbsData } from '../../../interfaces';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-breadcrumbs-item',
  templateUrl: './breadcrumbs-item.component.html'
})
export class BreadcrumbsItemComponent extends ContentBaseComponent implements BreadcrumbsData {

  @Input() id?: string;
  @Input() routerLink?: string;

}
