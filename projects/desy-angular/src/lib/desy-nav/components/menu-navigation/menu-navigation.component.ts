import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  Output,
  QueryList,
  SimpleChanges,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { MenuNavigationItemSubItem, MenuNavigationItemSubItemSubItem } from '../../interfaces';
import { MenuNavigationItem } from '../../interfaces/menu-navigation';
import { MenubaritemDirective } from '../menubar/directives/menubaritem.directive';
import { MenuNavigationItemComponent } from './components/menu-navigation-item/menu-navigation-item.component';
import { MenuNavigationSubitemComponent } from './components/menu-navigation-subitem/menu-navigation-subitem.component';

@Component({
  selector: 'desy-menu-navigation',
  templateUrl: './menu-navigation.component.html'
})
export class MenuNavigationComponent extends AccessibilityComponent implements OnChanges, AfterViewInit {

  @Input() idPrefix: string;
  _items: MenuNavigationItem[];
  @Input()
  set items(items: MenuNavigationItem[]) {
    this._items = items;
    this.activeItemIfSubItemIsActive(items)
  }
  get items() {
    return this._items
  }
  @Output() itemsChange = new EventEmitter<MenuNavigationItem[]>();
  @Output() activeItemChange = new EventEmitter<MenuNavigationItem>();
  @Output() activeSubItemChange = new EventEmitter<MenuNavigationItemSubItem | MenuNavigationItemSubItemSubItem>();

  @ViewChild('menunavigation', { read: ElementRef }) menunavigation: ElementRef;
  @ViewChildren(MenubaritemDirective) menuItems: QueryList<MenubaritemDirective>;
  @ContentChildren(MenuNavigationItemComponent) itemComponentList: QueryList<MenuNavigationItemComponent>;

  @Input() @HostBinding('class') classes: any;
  @HostBinding('class.c-menu-navigation') cfg = true
  @Input() @HostBinding('attr.id') id: any;

  @HostBinding('attr.role') role: string = null;
  @HostBinding('attr.aria-label') ariaLabel: string = null;
  @HostBinding('attr.aria-describedby') ariaDescribedBy: string = null;
  @HostBinding('attr.aria-labelledby') ariaLabelledBy: string = null;
  @HostBinding('attr.aria-hidden') ariaHidden: string = null;
  @HostBinding('attr.aria-disabled') ariaDisabled: string = null;
  @HostBinding('attr.aria-controls') ariaControls: string = null;
  @HostBinding('attr.aria-current') ariaCurrent: string = null;
  @HostBinding('attr.aria-live') ariaLive: string = null;
  @HostBinding('attr.aria-expanded') ariaExpanded: string = null;
  @HostBinding('attr.aria-errormessage') ariaErrorMessage: string = null;
  @HostBinding('attr.aria-haspopup') ariaHasPopup: string = null;
  @HostBinding('attr.aria-modal') ariaModal: string = null;
  @HostBinding('attr.aria-checked') ariaChecked: string = null;
  @HostBinding('attr.aria-pressed') ariaPressed: string = null;
  @HostBinding('attr.aria-readonly') ariaReadonly: string = null;
  @HostBinding('attr.aria-required') ariaRequired: string = null;
  @HostBinding('attr.aria-selected') ariaSelected: string = null;
  @HostBinding('attr.aria-valuemin') ariaValuemin: string = null;
  @HostBinding('attr.aria-valuemax') ariaValuemax: string = null;
  @HostBinding('attr.aria-valuenow') ariaValuenow: string = null;
  @HostBinding('attr.aria-valuetext') ariaValuetext: string = null;
  @HostBinding('attr.aria-orientation') ariaOrientation: string = null;
  @HostBinding('attr.aria-level') ariaLevel: string = null;
  @HostBinding('attr.aria-multiselectable') ariaMultiselectable: string = null;
  @HostBinding('attr.aria-placeholder') ariaPlaceholder: string = null;
  @HostBinding('attr.aria-posinset') ariaPosinset: string = null;
  @HostBinding('attr.aria-setsize') ariaSetsize: string = null;
  @HostBinding('attr.aria-sort') ariaSort: string = null;
  @HostBinding('attr.aria-busy') ariaBusy: string = null;
  @HostBinding('attr.aria-dropeffect') ariaDropeffect: string = null;
  @HostBinding('attr.aria-grabbed') ariaGrabbed: string = null;
  @HostBinding('attr.aria-activedescendant') ariaActivedescendant: string = null;
  @HostBinding('attr.aria-atomic') ariaAtomic: string = null;
  @HostBinding('attr.aria-autocomplete') ariaAutocomplete: string = null;
  @HostBinding('attr.aria-braillelabel') ariaBraillelabel: string = null;
  @HostBinding('attr.aria-brailleroledescription') ariaBrailleroledescription: string = null;
  @HostBinding('attr.aria-colcount') ariaColcount: string = null;
  @HostBinding('attr.aria-colindex') ariaColindex: string = null;
  @HostBinding('attr.aria-colindextext') ariaColindextext: string = null;
  @HostBinding('attr.aria-colspan') ariaColspan: string = null;
  @HostBinding('attr.aria-description') ariaDescription: string = null;
  @HostBinding('attr.aria-details') ariaDetails: string = null;
  @HostBinding('attr.aria-flowto') ariaFlowto: string = null;
  @HostBinding('attr.aria-invalid') ariaInvalid: string = null;
  @HostBinding('attr.aria-keyshortcuts') ariaKeyshortcuts: string = null;
  @HostBinding('attr.aria-owns') ariaOwns: string = null;
  @HostBinding('attr.aria-relevant') ariaRelevant: string = null;
  @HostBinding('attr.aria-roledescription') ariaRoledescription: string = null;
  @HostBinding('attr.aria-rowcount') ariaRowcount: string = null;
  @HostBinding('attr.aria-rowindex') ariaRowindex: string = null;
  @HostBinding('attr.aria-rowindextext') ariaRowindextext: string = null;
  @HostBinding('attr.aria-rowspan') ariaRowspan: string = null;
  @HostBinding('attr.tabindex') tabindex: string = null;
  @HostBinding('attr.title') title: string = null;
  @HostBinding('attr.alt') alt: string = null;
  @HostBinding('attr.lang') lang: string = null;
  @HostBinding('attr.accesskey') accesskey: string = null;
  @HostBinding('attr.autocomplete') autocomplete: string = null;
  @HostBinding('attr.autofocus') autofocus: string = null;
  @HostBinding('attr.contenteditable') contenteditable: string = null;
  @HostBinding('attr.dir') dir: string = null;
  @HostBinding('attr.draggable') draggable: string = null;
  @HostBinding('attr.enterkeyhint') enterkeyhint: string = null;
  @HostBinding('attr.hidden') hidden: boolean = null;
  @HostBinding('attr.inputmode') inputmode: string = null;
  @HostBinding('attr.spellcheck') spellcheck: string = null;
  @HostBinding('attr.translate') translate: string = null;
  @HostBinding('attr.aria-multiline') ariaMultiline: string = null;
  @HostBinding('attr.for') for: string = null;
  @HostBinding('attr.form') form: string = null;
  @HostBinding('attr.headers') headers: string = null;
  @HostBinding('attr.placeholder') placeholder: string = null;
  @HostBinding('attr.readonly') readonly: string = null;
  @HostBinding('attr.required') required: string = null;

  menuData: any[];
  viewInit = false;
  isFocused: boolean;
  currentFocusItemIndex = 0;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnChanges(changes?: SimpleChanges): void {
    if (this.viewInit) {
      this.checkChanges();
    }
  }

  ngAfterViewInit(): void {
    this.viewInit = true;
    this.checkChanges();
    this.itemComponentList.changes.subscribe((items) => this.activeItemIfSubItemIsActive(items.toArray()))
  }

  checkChanges(): void {
    if (!this.menuItems) {
      return;
    }

    this.menuData = [];
    this.getItems().forEach((_, index) => {
      const itemData = {
        open: false,
        currentFocusSubItemIndex: 0,
        currentFocusSubSubItemIndex: 0,
        menuItem: this.menuItems.toArray()[index]
      };
      this.menuData.push(itemData);
    });

    if (this.viewInit) {
      this.checkRequired();
    }


    const lastCurrentFocusItemIndex = this.currentFocusItemIndex;
    const items = this.getItems();
    if (items) {
      while (!this.isItemFocusable(this.currentFocusItemIndex % items.length)) {
        this.currentFocusItemIndex = (this.currentFocusItemIndex + items.length + 1) % items.length;
        if (this.currentFocusItemIndex === lastCurrentFocusItemIndex) {
          break;
        }
      }
    }
  }

  activeItemIfSubItemIsActive(items: MenuNavigationItem[] | MenuNavigationItemComponent[]) {
    items.forEach((item) => {
      let subItems = this.getItemSubitems(item)
      if (subItems instanceof MenuNavigationItemComponent) {
        item.active = subItems.sub.items.filter(subItem => subItem.active).length > 0;
      } else if (item?.sub?.items) {
        item.active = item.sub.items.filter(subItem => subItem.active).length > 0;
      }
    })
  }

  /*
   * Eventos
   */

  handleMenuFocusIn(): void {
    this.isFocused = true;
    this.changeDetectorRef.detectChanges();
  }

  handleMenuFocusOut(event): void {
    if (!this.menunavigation.nativeElement.contains(event.relatedTarget)) {
      this.closeMenu();
    }
    this.isFocused = false;
    this.changeDetectorRef.detectChanges();
  }

  handleMenuItemClick(event, itemIndex: number): void {
    if (this.hasPopupMenu(itemIndex)) {
      event.preventDefault();
      const isOpen = this.menuData[itemIndex].open;
      this.closeMenu();
      if (!isOpen) {
        this.openMenu(itemIndex);
      }
    } else {
      this.activateMenuItem(itemIndex);
    }
  }

  handleMenuItemKeydown(event: KeyboardEvent, itemIndex: number): void {
    switch (event.key) {
      case 'Enter':
      case ' ':
      case 'ArrowDown':
        if (this.hasPopupMenu(itemIndex)) {
          this.openMenu(itemIndex);
          setTimeout(() => this.focusFirstSubItem(itemIndex));
          event.stopPropagation();
          event.preventDefault();
        }
        break;

      case 'Escape':
        if (this.hasPopupMenu(itemIndex)) {
          this.closeMenu(itemIndex);
        }
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowLeft':
        this.focusNextAvailableItem(itemIndex, -1);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowRight':
        this.focusNextAvailableItem(itemIndex, +1);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowUp':
        if (this.hasPopupMenu(itemIndex)) {
          this.openMenu(itemIndex);
          setTimeout(() => this.focusLastSubItem(itemIndex));
        }
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Home':
      case 'PageUp':
        if (this.hasPopupMenu(itemIndex)) {
          this.closeMenu();
        }
        this.focusFirstItem();
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'End':
      case 'PageDown':
        if (this.hasPopupMenu(itemIndex)) {
          this.closeMenu();
        }
        this.focusLastItem();
        event.stopPropagation();
        event.preventDefault();
        break;

      default:
        if (this.isPrintableChar(event.key)) {
          this.focusItemByFirstChar(event.key);
          event.stopPropagation();
          event.preventDefault();
        }
        break;
    }
  }

  handleMenuItemMouseOver(itemIndex: number): void {
    this.focusItem(itemIndex, true);
  }

  handlePopupMenuItemClick(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    this.activatePopupMenuItem(itemIndex, subItemIndex, subSubItemIndex);
    this.focusItem(itemIndex);
    this.closeMenu(itemIndex);
  }

  handlePopupMenuItemKeydown(event: KeyboardEvent, itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    const currentSubIndex = this.menuData[itemIndex].currentFocusSubItemIndex;
    const currentSubSubIndex = this.menuData[itemIndex].currentFocusSubSubItemIndex;
    const items = this.getItems();
    switch (event.key) {
      case ' ':
        this.activatePopupMenuItem(itemIndex, subItemIndex, subSubItemIndex);
        if (this.mustCloseAfterSelectPopupItem(itemIndex, subItemIndex, subSubItemIndex)) {
          this.closeMenu(itemIndex);
          this.focusItem(itemIndex);
        }
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Enter':
        this.activatePopupMenuItem(itemIndex, subItemIndex, subSubItemIndex);
        this.closeMenu(itemIndex);
        this.focusItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Escape':
        this.closeMenu(itemIndex);
        this.focusItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowUp':
        this.focusNextAvailableSubItem(itemIndex, currentSubIndex, currentSubSubIndex, -1);
        // this.focusSubItem(itemIndex, (currentSubIndex + listLength - 1) % listLength);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowDown':
        this.focusNextAvailableSubItem(itemIndex, currentSubIndex, currentSubSubIndex, +1);
        // this.focusSubItem(itemIndex, (currentSubIndex + listLength + 1) % listLength);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowLeft':
        this.closeMenu(itemIndex);
        this.focusItem((itemIndex + items.length - 1) % items.length);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'ArrowRight':
        this.closeMenu(itemIndex);
        this.focusItem((itemIndex + items.length + 1) % items.length);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Home':
      case 'PageUp':
        this.focusFirstSubItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'End':
      case 'PageDown':
        this.focusLastSubItem(itemIndex);
        event.stopPropagation();
        event.preventDefault();
        break;

      case 'Tab':
        this.closeMenu(itemIndex);
        this.focusItem(itemIndex);
        break;

      default:
        if (this.isPrintableChar(event.key)) {
          this.focusSubItemByFirstChar(itemIndex, event.key);
          event.stopPropagation();
          event.preventDefault();
        }
        break;
    }
  }

  handlePopupMenuItemMouseOver(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    this.focusSubItem(itemIndex, subItemIndex, subSubItemIndex);
  }

  /*
   * Comunes
   */

  checkRequired(): void {
    const items = this.getItems();
  }


  activateMenuItem(itemIndex: number): void {
    const items = this.getItems();
    this.activeItemChange.emit(items[itemIndex] as MenuNavigationItem);
    if (items[itemIndex] instanceof MenuNavigationItemComponent) {
      (items[itemIndex] as MenuNavigationItemComponent).selected.emit();
    }
  }

  activatePopupMenuItem(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    let item: MenuNavigationItemSubItem | MenuNavigationItemSubItemSubItem = subItems[subItemIndex];

    this.itemsChange.emit(items);
    this.activeSubItemChange.emit(item as MenuNavigationItem);
    if (item instanceof MenuNavigationSubitemComponent) {
      item.selected.emit();
    }
  }

  mustCloseAfterSelectPopupItem(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): boolean {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    let item: MenuNavigationItemSubItem | MenuNavigationItemSubItemSubItem = subItems[subItemIndex];

    return item.role !== 'menuitemcheckbox' && item.role !== 'menuitemradio';
  }

  openMenu(itemIndex: number): void {
    const wasOpened = this.menuData[itemIndex].open;
    this.menuData[itemIndex].open = true;
    if (wasOpened) {
      this.activateMenuItem(itemIndex);
    }
  }

  closeMenu(itemIndex?: number): void {
    if (itemIndex !== null && itemIndex !== undefined) {
      this.menuData[itemIndex].open = false;
    } else {
      this.menuData.forEach(item => item.open = false);
    }
  }

  hasPopupMenu(itemIndex: number): boolean {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    return items[itemIndex].sub && subItems && subItems.length > 0;
  }

  private isPrintableChar(str: string): boolean {
    return str.length === 1 && !!str.match(/\S/);
  }



  /*
   * Comunes - focus
   */

  focusItem(itemIndex: number, hover?: boolean): void {
    const hasFocus = this.menunavigation.nativeElement.contains(document.activeElement);
    const isOpen = this.menuData[this.currentFocusItemIndex].open;

    if (!hover || hasFocus) {
      this.menuData[itemIndex].menuItem.link.nativeElement.focus();
    }

    this.closeMenu();
    if (isOpen && this.hasPopupMenu(itemIndex)) {
      this.openMenu(itemIndex);
    }

    this.currentFocusItemIndex = itemIndex;
  }

  focusNextAvailableItem(fromItemIndex: number, step: number): void {
    let nextItem = fromItemIndex;
    const items = this.getItems();
    do {
      nextItem = (nextItem + items.length + step) % items.length;
    } while (!this.isItemFocusable(nextItem) && nextItem !== fromItemIndex);

    if (nextItem !== fromItemIndex) {
      this.focusItem(nextItem);
    }
  }

  focusFirstItem(): void {
    if (this.isItemFocusable(0)) {
      this.focusItem(0);
    } else {
      this.focusNextAvailableItem(0, +1);
    }
  }

  focusLastItem(): void {
    const items = this.getItems();
    if (this.isItemFocusable(items.length - 1)) {
      this.focusItem(items.length - 1);
    } else {
      this.focusNextAvailableItem(items.length - 1, -1);
    }
  }

  isItemFocusable(itemIndex: number): boolean {
    return !this.getItems()[itemIndex].disabled;
  }

  focusItemByFirstChar(char: string): void {
    const itemContents: string[] = this.menuItems.map(menuItem => menuItem.itemContentWrapper.nativeElement.textContent);
    let index = this.currentFocusItemIndex;
    let foundIndex = -1;
    do {
      index = (index + itemContents.length + 1) % itemContents.length;
      if (itemContents[index] && itemContents[index].trim().substr(0, 1).toLowerCase() === char.toLowerCase()
        && this.isItemFocusable(index)) {
        foundIndex = index;
      }
    } while (foundIndex === -1 && index !== this.currentFocusItemIndex);

    if (foundIndex >= 0) {
      this.focusItem(foundIndex);
    }
  }

  focusSubItem(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): void {
    if (this.menuData[itemIndex].menuItem.popupMenuItems.length > 0) {
      const menuDataIndex = subItemIndex;
      this.menuData[itemIndex].currentFocusSubItemIndex = subItemIndex;
      this.menuData[itemIndex].currentFocusSubSubItemIndex = subSubItemIndex;
      const elem = this.menuData[itemIndex].menuItem.popupMenuItems.toArray()[menuDataIndex].nativeElement;
      if (elem) {
        const aElem = elem.querySelector('a');
        if (aElem) {
          aElem.focus();
        } else {
          elem.focus();
        }
      }
      else {
        console.error('No subitem to focus');
      }
    } else {
      console.error('No subitems');
    }
  }

  focusNextAvailableSubItem(itemIndex: number, fromItemSubIndex: number, fromItemSubSubIndex: number, step: number): void {
    let nextSubIndex = fromItemSubIndex;
    let nextSubSubIndex = fromItemSubSubIndex;
    let allSubItemsChecked;
    const items = this.getItems();
    do {
      const nextIndexes = this.getNextSubItemIndexes(items, itemIndex, nextSubIndex, nextSubSubIndex, step);
      nextSubIndex = nextIndexes.subIndex;
      nextSubSubIndex = nextIndexes.subSubIndex;
      allSubItemsChecked = (fromItemSubIndex === nextSubIndex && fromItemSubSubIndex === nextSubSubIndex);
    } while (!this.isSubItemFocusable(itemIndex, nextSubIndex, nextSubSubIndex) && !allSubItemsChecked);

    if (!allSubItemsChecked) {
      this.focusSubItem(itemIndex, nextSubIndex, nextSubSubIndex);
    }
  }

  focusFirstSubItem(itemIndex: number): void {
    let firstSubSubItem = null;
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    if (subItems && subItems.length > 0) {
      firstSubSubItem = 0;
    }

    if (this.isSubItemFocusable(itemIndex, 0, firstSubSubItem)) {
      this.focusSubItem(itemIndex, 0, firstSubSubItem);
    } else {
      this.focusNextAvailableSubItem(itemIndex, 0, firstSubSubItem, +1);
    }
  }

  focusLastSubItem(itemIndex: number): void {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    const lastItemIndex = subItems.length - 1;
    const lastItem = subItems[lastItemIndex];
    let lastSubSubItem = null;

    if (this.isSubItemFocusable(itemIndex, lastItemIndex, lastSubSubItem)) {
      this.focusSubItem(itemIndex, lastItemIndex, lastSubSubItem);
    } else {
      this.focusNextAvailableSubItem(itemIndex, lastItemIndex, lastSubSubItem, -1);
    }
  }

  isSubItemFocusable(itemIndex: number, subItemIndex: number, subSubItemIndex?: number): boolean {
    const items = this.getItems();
    const subItems = this.getItemSubitems(items[itemIndex]);
    let item: MenuNavigationItemSubItem | MenuNavigationItemSubItemSubItem = subItems[subItemIndex];

    let isFocusable = false;
    if (item.role !== 'separator' && item.role !== 'none') {
      isFocusable = true;
    }
    return isFocusable;
  }

  focusSubItemByFirstChar(itemIndex: number, char: string): void {
    const itemContents: string[] = this.menuItems.toArray()[itemIndex].popupMenuItems.map(menuItem => menuItem.nativeElement.textContent);
    let subItemIndex = this.menuData[itemIndex].currentFocusSubItemIndex;
    let subSubItemIndex = this.menuData[itemIndex].currentFocusSubSubItemIndex;
    let foundSubIndex = -1;
    let foundSubSubIndex = null;
    let allSubItemsChecked = false;
    const items = this.getItems();
    do {
      const nextIndexes = this.getNextSubItemIndexes(items, itemIndex, subItemIndex, subSubItemIndex, +1);
      subItemIndex = nextIndexes.subIndex;
      subSubItemIndex = nextIndexes.subSubIndex;
      const index = subItemIndex;
      if (itemContents[index] && itemContents[index].trim().substr(0, 1).toLowerCase() === char.toLowerCase()
        && this.isSubItemFocusable(itemIndex, subItemIndex, subSubItemIndex)) {
        foundSubIndex = subItemIndex;
        foundSubSubIndex = subSubItemIndex;
      }
      allSubItemsChecked = (this.menuData[itemIndex].currentFocusSubItemIndex === subItemIndex
        && this.menuData[itemIndex].currentFocusSubSubItemIndex === subSubItemIndex);
    } while (foundSubIndex === -1 && !allSubItemsChecked);

    if (foundSubIndex >= 0) {
      this.focusSubItem(itemIndex, foundSubIndex, foundSubSubIndex);
    }
  }

  /*
   * Metodos para facilitar contenido
   */

  getItems(): MenuNavigationItem[] | MenuNavigationItemComponent[] {
    let items;
    if (this.itemComponentList && this.itemComponentList.length > 0) {
      items = this.itemComponentList.toArray();
    } else {
      items = this.items;
    }
    return items;
  }

  getItemSubitems(item: MenuNavigationItem | MenuNavigationItemComponent): MenuNavigationItemSubItem[] | MenuNavigationSubitemComponent[] {
    let subItems = [];
    if (item instanceof MenuNavigationItemComponent) {
      subItems = item.getSubItems();
    }

    if (!subItems || subItems.length === 0) {
      subItems = item.sub ? item.sub.items : [];
    }
    return subItems;
  }

  getNextSubItemIndexes(items, itemIndex, nextSubIndex, nextSubSubIndex, step): { subIndex: number, subSubIndex?: number } {
    const subItems = this.getItemSubitems(items[itemIndex]);

    let checkNextSubItem = true;
    if (checkNextSubItem) {
      nextSubIndex = (nextSubIndex + step + subItems.length) % subItems.length;
    }
    return { subIndex: nextSubIndex, subSubIndex: nextSubSubIndex };
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : this.id + 'menunavigation-item';
  }

  getItemId(item, index: number): string {
    let id: string;
    if (item) {
      if (item.id) {
        id = item.id;
      } else {
        id = `${this.getIdPrefix()}-${++index}`;
      }
    }
    return id;
  }

  getSubItemId(item, index: number, parentId: string): string {
    let id: string;
    if (item) {
      if (item.id) {
        id = item.id;
      } else {
        id = `sub-${parentId}-${++index}`;
      }
    }
    return id;
  }

}
