import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuNavigationSubitemComponent } from './menu-navigation-subitem.component';

xdescribe('MenuNavigationSubitemComponent', () => {
  let component: MenuNavigationSubitemComponent;
  let fixture: ComponentFixture<MenuNavigationSubitemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuNavigationSubitemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuNavigationSubitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
