import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuNavigationItemComponent } from './menu-navigation-item.component';

xdescribe('MenuNavigationItemComponent', () => {
  let component: MenuNavigationItemComponent;
  let fixture: ComponentFixture<MenuNavigationItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuNavigationItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuNavigationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
