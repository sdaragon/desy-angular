import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DividerData } from '../../../../../desy-commons/interfaces';
import { ContentBaseComponent } from '../../../../../shared/components';
import { MenuNavigationItemSubItem } from '../../../../interfaces';

@Component({
  selector: 'desy-menu-navigation-subitem',
  templateUrl: './menu-navigation-subitem.component.html'
})
export class MenuNavigationSubitemComponent extends ContentBaseComponent implements MenuNavigationItemSubItem {
  
  @Input() href?: string;
  @Input() fragment: string;
  @Input() routerLink: string;
  @Input() routerLinkActiveClasses: string|string[];
  @Input() target?: string;
  @Input() text: string;
  @Input() html: string;
  @Input() id?: string;
  @Input() expanded?: boolean;
  @Input() divider?: DividerData;
  @Input() disabled?: boolean;
  @Input() sub?: MenuNavigationItemSubItem[];
  @Input() classes?: string;
  @Input() active?: boolean;

  @Output() selected = new EventEmitter<any>();
  @Output() checkedChange = new EventEmitter<boolean>();

}
