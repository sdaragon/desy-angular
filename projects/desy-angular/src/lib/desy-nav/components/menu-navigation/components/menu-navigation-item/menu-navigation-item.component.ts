import { Component, ContentChildren, EventEmitter, Input, Output, QueryList } from '@angular/core';
import { DividerData } from '../../../../../desy-commons/interfaces';
import { ContentBaseComponent } from '../../../../../shared/components/content-base/content-base.component';
import { MenuNavigationItem, MenuNavigationItemSubItem } from '../../../../interfaces';
import { MenuNavigationSubitemComponent } from '../menu-navigation-subitem/menu-navigation-subitem.component';

@Component({
  selector: 'desy-menu-navigation-item',
  templateUrl: './menu-navigation-item.component.html'
})
export class MenuNavigationItemComponent extends ContentBaseComponent implements MenuNavigationItem {
  
  @Input() href?: string;
  @Input() fragment: string;
  @Input() routerLink: string;
  @Input() routerLinkActiveClasses: string|string[];
  @Input() target?: string;
  @Input() text: string;
  @Input() html: string;
  @Input() id?: string;
  @Input() expanded?: boolean;
  @Input() divider?: DividerData;
  @Input() disabled?: boolean;
  @Input() sub?: MenuNavigationItemSubItem;
  @Input() classes?: string;

  @Input() titleModal?: string;
  @Input() active?: boolean;

  @Output() selected = new EventEmitter<any>();

  @ContentChildren(MenuNavigationSubitemComponent) subItems: QueryList<MenuNavigationSubitemComponent>;

  public getSubItems(): MenuNavigationSubitemComponent[] {
    return this.subItems.toArray();
  }

}
