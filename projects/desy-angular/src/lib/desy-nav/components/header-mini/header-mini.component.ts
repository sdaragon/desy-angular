import {Component, Input} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';

@Component({
  selector: 'desy-header-mini',
  templateUrl: './header-mini.component.html'
})
export class HeaderMiniComponent extends AccessibilityComponent {
  @Input() classes: string;
  @Input() hasContainer: boolean = true;
  @Input() containerClasses: string;
  @Input() homepageUrl: string;
  @Input() homepageRouterLink: string;
  @Input() homepageFragment: string;
}
