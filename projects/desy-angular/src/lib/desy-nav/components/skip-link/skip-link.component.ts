import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';

@Component({
  selector: 'desy-skip-link',
  templateUrl: './skip-link.component.html'
})
export class SkipLinkComponent extends AccessibilityComponent {

  _html: string;
  _text: string;

  @Input()
  set html(value: string) {
    this._html = value
    this.hasContent = value ? false : this.text ? false : true
  }
  get html() {
    return this._html;
  }
  @Input()
  set text(value: string) {
    this._text = value
    this.hasContent = value ? false : true
  }
  get text() {
    return this._text;
  }

  @Input() fragment: string;
  @Input() classes: string;
  @Input() id: string;

  hasContent = true

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  getClassNames(): string {
    let classNames = 'c-skip-link sr-only active:not-sr-only focus:not-sr-only focus:outline-none focus:shadow-outline-focus block p-base bg-warning-base text-center text-black underline';
    if (this.classes) {
      classNames += ' ' + this.classes;
    }
    return classNames;
  }

  scrollTo(fragment: string) {
    const elem = document.querySelector('#' + fragment);
    if (elem) {
      elem.scrollIntoView({ behavior: 'smooth' });
      const focusableElement = elem.querySelector('button, [tabindex], input, select, textarea, a[href], summary, [contenteditable], [role="button"], [role="link"], [role="checkbox"], [role="radio"], [role="menuitem"], [role="option"], [role="tab"], [role="tabpanel"]') as HTMLElement;
      if (focusableElement) {
        focusableElement.focus();
      }
    }
  }

}
