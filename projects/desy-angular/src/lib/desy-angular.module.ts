import { NgModule } from '@angular/core';

import { DesyButtonsModule } from './desy-buttons/desy-buttons.module';
import { DesyCommonsModule } from './desy-commons/desy-commons.module';
import { DesyFormsModule } from './desy-forms/desy-forms.module';
import { DesyModalsModule } from './desy-modals/desy-modals.module';
import { DesyNavModule } from './desy-nav/desy-nav.module';
import { DesyPaginationModule } from './desy-pagination/desy-pagination.module';
import { DesyTablesModule } from './desy-tables/desy-tables.module';
import { DesyViewsModule } from './desy-views/desy-views.module';
import { SharedExternalModule } from './shared/sharedExternal.module';

@NgModule({
  imports: [
    DesyButtonsModule,
    DesyCommonsModule,
    DesyFormsModule,
    DesyModalsModule,
    DesyNavModule,
    DesyTablesModule,
    DesyViewsModule,
    DesyPaginationModule,
    SharedExternalModule,
  ],
  exports: [
    DesyButtonsModule,
    DesyCommonsModule,
    DesyFormsModule,
    DesyModalsModule,
    DesyNavModule,
    DesyTablesModule,
    DesyViewsModule,
    DesyPaginationModule,
    SharedExternalModule,
  ]
})
export class DesyAngularModule { }
