// componentes
import { NgModule } from '@angular/core';
import { IconComponent } from './components/icon/icon.component';
import { ContentComponent } from './components/content/content.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { NgxTippyModule } from 'ngx-tippy-wrapper';
import { TitleComponent } from './components/title/title.component';
import { DescriptionComponent } from './components/description/description.component';
import { DividerComponent } from './components/divider/divider.component';

@NgModule({
  declarations: [
    IconComponent,
    ContentComponent,
    DescriptionComponent,
    TitleComponent,
    DividerComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxTippyModule
  ],
  exports: [
    IconComponent,
    ContentComponent,
    DescriptionComponent,
    TitleComponent,
    DividerComponent
  ]
})

export class DesyCommonsModule { }

