import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../shared/components';
import {DescriptionData} from '../../interfaces';

@Component({
  selector: 'desy-description',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class DescriptionComponent extends ContentBaseComponent implements DescriptionData {
  @Input() classes: string;
  @Input() visuallyHiddenTitle?: string;
}
