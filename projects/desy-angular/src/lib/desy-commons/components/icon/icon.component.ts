import {Component, Input} from '@angular/core';
import {IconData} from '../../interfaces';
import {ContentBaseComponent} from '../../../shared/components';

@Component({
    selector: 'desy-icon',
    templateUrl: './icon.component.html'
})
export class IconComponent extends ContentBaseComponent implements IconData {

  @Input() type: string;
  @Input() containerClasses?: string;
}
