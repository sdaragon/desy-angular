import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../shared/components';
import {TitleData} from '../../interfaces';

@Component({
  selector: 'desy-title',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class TitleComponent extends ContentBaseComponent implements TitleData {
  @Input() classes: string;
}
