import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../shared/components';
import { DividerData } from '../../interfaces';

@Component({
  selector: 'desy-divider',
  templateUrl: './divider.component.html'
})
export class DividerComponent extends ContentBaseComponent implements DividerData {
  @Input() text?: string;
  @Input() html?: string;

}
