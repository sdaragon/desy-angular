import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../shared/components';
import {ContentData} from '../../interfaces';

@Component({
    selector: 'desy-content',
    templateUrl: './content.component.html'
})
export class ContentComponent extends ContentBaseComponent implements ContentData {
  @Input() classes: string;
}
