export interface DescriptionData {
    text?: string;
    html?: string;
    classes?: string;
    visuallyHiddenTitle?: string;
}
