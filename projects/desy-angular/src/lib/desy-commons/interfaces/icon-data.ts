export interface IconData {
  html?: string;
  type?: 'info'|'alert'|string;
  containerClasses?: string;
}
