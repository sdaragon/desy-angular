import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface TitleData extends AccessibilityData {
    text?: string;
    html?: string;
    classes?: string;
}
