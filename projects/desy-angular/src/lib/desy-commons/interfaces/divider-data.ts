export interface DividerData {
    text?: string;
    html?: string;
    classes?: string;
}
