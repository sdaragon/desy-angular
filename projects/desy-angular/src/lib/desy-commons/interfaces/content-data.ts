export interface ContentData {
  text?: string;
  html?: string;
  classes?: string;
}
