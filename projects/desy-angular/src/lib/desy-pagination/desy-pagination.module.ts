import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaginationItemPerPageComponent } from './components/pagination-item-perpage/pagination-item-perpage.component';
import { PaginationListboxLabelComponent } from './components/pagination-listbox-label/pagination-listbox-label.component';

import { DesyButtonsModule } from '../desy-buttons/desy-buttons.module';
import { DesyNavModule } from '../desy-nav/desy-nav.module';
import { SharedModule } from '../shared/shared.module';

import { FormsModule } from '@angular/forms';
import { DesyFormsModule } from '../desy-forms/desy-forms.module';
import { PaginationComponent } from './components/pagination/pagination.component';


@NgModule({
  declarations: [
    PaginationComponent, 
    PaginationListboxLabelComponent,
    PaginationItemPerPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DesyNavModule,
    DesyButtonsModule,
    DesyFormsModule,
    FormsModule
  ],
  exports: [
    PaginationComponent, 
    PaginationListboxLabelComponent,
    PaginationItemPerPageComponent
  ]
})
export class DesyPaginationModule { }
