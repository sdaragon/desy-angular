import { NavItemData } from '../../desy-nav/interfaces';

export interface PaginationItemData extends NavItemData {
  text: any;
}
