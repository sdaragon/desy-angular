import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { AfterViewInit, ChangeDetectorRef, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesyAngularModule } from '../../../desy-angular.module';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

describe('PaginationComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DummyPaginationDefaultComponent,
        DummyPaginationWithParamsComponent,
        DummyPaginationWithItemsPagComponent
      ],
      imports: [
        CommonModule,
        DesyAngularModule,
        RouterTestingModule
      ],
      teardown: { destroyAfterEach: false }
    })
    .compileComponents();
  });

  describe('Sin selector', () => {
    let component: DummyPaginationDefaultComponent;
    let fixture: ComponentFixture<DummyPaginationDefaultComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(DummyPaginationDefaultComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('Se crea el componete de ejemplo', () => {
      expect(fixture).toBeTruthy();
    });
  });

  describe('Con items por parámetro', () => {
    let component: DummyPaginationWithParamsComponent;
    let fixture: ComponentFixture<DummyPaginationWithParamsComponent>;

    const initDummyParamsComponent = () => {
      fixture = TestBed.createComponent(DummyPaginationWithParamsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    };

    it('Se crea el componete de ejemplo', () => {
      initDummyParamsComponent();
      expect(component).toBeTruthy();
    });

    it('Se cambia a la pagina anterior', () => {
      initDummyParamsComponent();
      expect(component).toBeTruthy();

      // Observamos las invocaciones a handlePageChange
      const handlePageChange = spyOn(component, 'handlePageChange');

      // Click en el botón anterior
      let buttonPrev = fixture.debugElement.query(By.css('#selector-previous'));
      expect(buttonPrev).toBeTruthy();
      buttonPrev.nativeElement.click();
      fixture.detectChanges();

      // Comprobamos el evento emitido con la página anterior (2 -> 1)
      expect(handlePageChange).toHaveBeenCalledOnceWith(1);

    });

    it('Se cambia a la pagina siguiente', () => {
      initDummyParamsComponent();
      expect(component).toBeTruthy();

      // Observamos las invocaciones a handlePageChange
      const handlePageChange = spyOn(component, 'handlePageChange');

      // Click en el botón siguiente
      let buttonNext = fixture.debugElement.query(By.css('#selector-next'));
      expect(buttonNext).toBeTruthy();
      buttonNext.nativeElement.click();
      fixture.detectChanges();

      // Comprobamos el evento emitido con la página siguiente (2 -> 3)
      expect(handlePageChange).toHaveBeenCalledOnceWith(3);

    });

    it('Se cambia a pagina arbitraria desde selector', () => {
      initDummyParamsComponent();
      expect(component).toBeTruthy();

      // Observamos las invocaciones a handlePageChange
      const handlePageChange = spyOn(component, 'handlePageChange');

      // Click en el selector
      let button = fixture.debugElement.query(By.css('#withSelector-listbox-button'));
      expect(button).toBeTruthy();
      button.nativeElement.click();
      fixture.detectChanges();

      // Click en pagina 5
      let option = fixture.debugElement.query(By.css('#selector-4')); // Empieza en selector-0
      expect(option).toBeTruthy();
      expect(option.nativeElement).toBeTruthy();
      option.nativeElement.click();
      //option.nativeElement.dispatchEvent(new Event('click'));
      fixture.detectChanges();

      // Comprobamos el evento emitido con la página siguiente (2 -> 5)
      expect(handlePageChange).toHaveBeenCalledOnceWith(5);

    });
  });

  describe('Con items por página', () => {
    let component: DummyPaginationWithItemsPagComponent;
    let fixture: ComponentFixture<DummyPaginationWithItemsPagComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(DummyPaginationWithItemsPagComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('Se crea el componete de ejemplo', () => {
      expect(fixture).toBeTruthy();
    });

    it('Se modifica el itemsPerPage', () => {
      expect(fixture).toBeTruthy();

      // Observamos las invocaciones a handlePageChange
      const handleItemsPerPageChange = spyOn(component, 'handleItemsPerPageChange');

      // Click en selector
      let itemsPerPageButton = fixture.debugElement.query(By.css('#items-listbox-selector-button'));
      expect(itemsPerPageButton).toBeTruthy();
      expect(itemsPerPageButton.nativeElement.textContent).toContain('25');
      itemsPerPageButton.nativeElement.click();
      fixture.detectChanges();

      // Click en opcion 50
      let option = fixture.debugElement.query(By.css('#items-listbox-selector-listbox-item-2'));
      expect(option).toBeTruthy();
      expect(option.nativeElement).toBeTruthy();
      option.nativeElement.click();
      //option.nativeElement.dispatchEvent(new Event('click'));
      fixture.detectChanges();

      // Se modifica el texto en el botón
      expect(itemsPerPageButton.nativeElement.textContent).toContain('50');
      expect(handleItemsPerPageChange).toHaveBeenCalledOnceWith(50);
    });
  });

	/**
	 * Componente host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-pagination-default`,
		template: `
      <desy-pagination [id]="'default'" [itemsPerPage]="10" [totalItems]="59" idPrefix="simple" [(currentPage)]="selectedPageDefault"></desy-pagination>
    `,
	})
	class DummyPaginationDefaultComponent implements AfterViewInit {

    selectedPageDefault = 2;

    constructor(private changeDetector: ChangeDetectorRef){}

    ngAfterViewInit(): void {
      this.changeDetector.detectChanges();
    }

	}

  
	/**
	 * Componente host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-pagination-params`,
		template: `
      <desy-pagination [id]="'withSelector'" [itemsPerPage]="10" [totalItems]="64" [hasSelect]="true" [hasNext]="true" [hasPrevious]="true" [idPrefix]="'selector'" previousText="Anterior" nextText="Siguiente" [(currentPage)]="selectedPage" (currentPageChange)="handlePageChange($event)"></desy-pagination>
    `,
	})
	class DummyPaginationWithParamsComponent implements AfterViewInit {

    activeItemEditable: boolean;
    selectedPage = 2;

    constructor(private changeDetector: ChangeDetectorRef){}

    ngAfterViewInit(): void {
      this.changeDetector.detectChanges();
    }

    handlePageChange(nextPage: number): void {

    }

	}

  
	/**
	 * Componente host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-pagination-params`,
		template: `
      <desy-pagination [id]="'withSelector'"
                       [itemsPerPage]="25" (itemsPerPageChange)="handleItemsPerPageChange($event)"
                       [totalItems]="64" [hasSelect]="true" [hasNext]="true" [hasPrevious]="true"
                       [idPrefix]="'selector'" previousText="Anterior" nextText="Siguiente"
                       [(currentPage)]="selectedPage" (currentPageChange)="handlePageChange($event)"
                       [hasSelectItemsPerPage]="true">
        <desy-pagination-listbox-label>Label</desy-pagination-listbox-label>
        <desy-pagination-item-perpage [value]="1">1</desy-pagination-item-perpage>
        <desy-pagination-item-perpage [value]="25">25</desy-pagination-item-perpage>
        <desy-pagination-item-perpage [value]="50">50</desy-pagination-item-perpage>
        <desy-pagination-item-perpage [value]="100">100</desy-pagination-item-perpage>
      </desy-pagination>
    `,
	})
	class DummyPaginationWithItemsPagComponent implements AfterViewInit {

    activeItemEditable: boolean;
    selectedPage = 2;

    constructor(private changeDetector: ChangeDetectorRef){}

    ngAfterViewInit(): void {
      this.changeDetector.detectChanges();
    }

    handlePageChange(nextPage: number): void {

    }

    handleItemsPerPageChange(nextVal: number): void {

    }

	}
});
