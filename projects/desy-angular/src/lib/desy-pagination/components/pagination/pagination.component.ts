
import { AfterContentInit, ChangeDetectorRef, Component, ContentChildren, EventEmitter, HostBinding, Input, OnChanges, Output, QueryList, SimpleChanges } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { PaginationItemPerPageComponent } from '../pagination-item-perpage/pagination-item-perpage.component';
import { PaginationListboxLabelComponent } from '../pagination-listbox-label/pagination-listbox-label.component';


@Component({
  selector: 'desy-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent extends AccessibilityComponent implements OnChanges, AfterContentInit {

  @Input() hasSelect: boolean;
  @Input() idPrefix: string;
  @Input() totalItems: number;
  @Input() hasPrevious: boolean = true;
  @Input() hasNext: boolean = true;
  @Input() hasFirst: boolean = true;
  @Input() hasLast: boolean = true;
  @Input() showPrevious: boolean = true;
  @Input() showNext: boolean = true;
  @Input() showFirst: boolean = false;
  @Input() showLast: boolean = false;
  @Input() previousText: string;
  @Input() nextText: string;
  @Input() firstText: string;
  @Input() lastText: string;
  @Input() hasSelectItemsPerPage: string | boolean;
  @Input() classes: string;

  @Input() @HostBinding('class') classesContainer: any;
  @Input() @HostBinding('attr.id') id: any;

  @DesyContentChild()
  @ContentChildren(PaginationListboxLabelComponent) listboxLabel: PaginationListboxLabelComponent;

  @ContentChildren(PaginationItemPerPageComponent) listboxItemsPerPage: QueryList<PaginationItemPerPageComponent>;

  @Input() currentPage = 1; // == currentPageindex + 1
  @Output() currentPageChange = new EventEmitter<number>();


  @Input() itemsPerPage: number;
  @Output() itemsPerPageChange = new EventEmitter<number>();

  nPages: number;
  items: PaginationItemPerPageComponent[];
  value: number;
  itemsPerPageList: PaginationItemPerPageComponent[];
  valuePerPage: number;
  nPagesHistory = 0;

  defaultText: boolean = false;

  readonly previousIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140"  width="1em" height="1em" class="self-center h-2.5 w-2.5 mr-2" aria-hidden="true" focusable="false"><path d="M54.87 71.77a2.5 2.5 0 010-3.54L106 17.07A10 10 0 1091.89 2.93L35.43 59.39a15 15 0 000 21.22l56.46 56.46A10 10 0 10106 122.93z" fill="currentColor" /></svg>';
  readonly nextIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140" width="1em" height="1em" class="self-center h-2.5 w-2.5 ml-2" aria-hidden="true" focusable="false"><path d="M34 137.07a10 10 0 010-14.14l51.13-51.16a2.5 2.5 0 000-3.54L34 17.07A10 10 0 0148.11 2.93l56.46 56.46a15 15 0 010 21.22l-56.46 56.46a10 10 0 01-14.11 0z" fill="currentColor" /></svg>';
  readonly firstIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" class="self-center h-2.5 w-2.5 mr-2" aria-hidden="true" focusable="false"><g><path d="M10.42,12a2.64,2.64,0,0,1,.77-1.88L20.73.58a1.77,1.77,0,0,1,2.5,2.5l-8.74,8.74a.27.27,0,0,0,0,.36l8.74,8.74a1.77,1.77,0,0,1-2.5,2.5l-9.54-9.54A2.64,2.64,0,0,1,10.42,12Z" fill="currentColor"></path><path d="M.25,12A2.65,2.65,0,0,1,1,10.12L10.57.58a1.77,1.77,0,0,1,2.5,2.5L4.33,11.82a.25.25,0,0,0,0,.36l8.74,8.74a1.77,1.77,0,0,1-2.5,2.5L1,13.88A2.65,2.65,0,0,1,.25,12Z" fill="currentColor"></path></g></svg>';
  readonly lastIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" class="self-center h-2.5 w-2.5 ml-2" aria-hidden="true" focusable="false"><g><path d="M13.58,12a2.64,2.64,0,0,1-.77,1.88L3.27,23.42a1.77,1.77,0,0,1-2.5-2.5l8.74-8.74a.27.27,0,0,0,0-.36L.77,3.08A1.77,1.77,0,0,1,3.27.58l9.54,9.54A2.64,2.64,0,0,1,13.58,12Z" fill="currentColor"></path><path d="M23.75,12A2.65,2.65,0,0,1,23,13.88l-9.54,9.54a1.77,1.77,0,0,1-2.5-2.5l8.74-8.74a.25.25,0,0,0,0-.36L10.93,3.08a1.77,1.77,0,0,1,2.5-2.5L23,10.12A2.65,2.65,0,0,1,23.75,12Z" fill="currentColor"></path></g></svg>';
  readonly prefix = '<span class="sr-only">Página&nbsp;</span>';

  constructor(private changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.currentPage === null || this.currentPage === undefined) {
      this.currentPage = 1;
    }
    this.checkChanges();
    this.classesContainer = this.classesContainer ? this.classesContainer += ' lg:flex lg:flex-wrap lg:items-center lg:gap-base' : 'lg:flex lg:flex-wrap lg:items-center lg:gap-base'
    this.id = this.id ? this.id : null
  }

  ngAfterContentInit(): void {
    if (this.listboxItemsPerPage) {
      this.setupItemsPerPage();
      this.listboxItemsPerPage.changes.subscribe(() => {
        this.setupItemsPerPage();
      })
    }
  }

  checkChanges(): void {
    this.nPages = Math.ceil(this.totalItems / this.itemsPerPage);

    if (!this.currentPage || this.currentPage < 1 || this.currentPage > this.nPages) {
      this.currentPage = 1;
      this.currentPageChange.emit(this.currentPage);
    }
    if (!this.firstText) {
      this.firstText = 'Primera';
    }
    if (!this.previousText) {
      this.previousText = 'Anterior';
    }
    if (!this.nextText) {
      this.nextText = 'Siguiente';
    }
    if (!this.lastText) {
      this.lastText = 'Última';
    }
    this.buildPages();
  }

  getSuffix(page: number, isNumber = false): string {
    return page >= 0 && page * this.itemsPerPage < this.totalItems ?
      (isNumber ? `<span class="sr-only">:&nbsp; con los resultados del ${page * this.itemsPerPage + 1} al ${this.getLastItemNumber(page)}</span>` : `<span class="sr-only">:&nbsp;Página ${page + 1} con los resultados del ${page * this.itemsPerPage + 1} al ${this.getLastItemNumber(page)}</span>`)
      : '';
  }

  getLastItemNumber(pageIndex: number): number {
    return Math.min((pageIndex + 1) * this.itemsPerPage, this.totalItems);
  }

  buildPages(): void {
    if (this.nPagesHistory != this.nPages) {
      this.nPagesHistory = this.nPages
      const itemList = [];
      for (let i = 0; i < this.nPages; i++) {
        const item = {
          value: i + 1,
          text: i + 1,
          selected: ((i + 1) === this.currentPage)
        };
        itemList.push(item);
      }
      this.value = this.currentPage
      this.items = itemList;
    }
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : 'pagination-item';
  }

  getButtonId(index: number): string {
    return this.getIdPrefix() + '-' + ++index;
  }

  changePage(current: number): void {
    const hasPageChange = current !== this.currentPage;
    this.currentPage = current;
    if (hasPageChange) {
      this.currentPageChange.emit(this.currentPage);
    }
    for (let i = 0; i < this.items.length; i++) {
      const item = this.items[i];
      item.selected = ((i + 1) === this.currentPage);
      this.value = this.currentPage
    }
  }

  previous(): void {
    if (this.currentPage > 1) {
      this.changePage(this.currentPage - 1);
    }
  }

  next(): void {
    if (this.currentPage < this.nPages) {
      this.changePage(this.currentPage + 1);
    }
  }

  first(): void {
    if (this.currentPage > 1) {
      this.changePage(1);
    }
  }

  last(): void {
    if (this.currentPage < this.nPages) {
      this.changePage(this.nPages);
    }
  }

  getListboxLabel(): PaginationListboxLabelComponent {
    return this.listboxLabel ? this.listboxLabel : null;
  }

  changeItemsPerPage(event: any) {
    if (event) {
      let newValue = event;
      this.itemsPerPage = newValue;
      this.checkChanges();
      this.itemsPerPageChange.emit(newValue);
      this.changeDetector.detectChanges();
    }
  }

  handleActiveItemChange(event): void {
    if (event !== null && event !== undefined) {
      this.changePage(event);
    }
  }

  setupItemsPerPage(): void {
    this.itemsPerPageList = this.listboxItemsPerPage && this.listboxItemsPerPage.length > 0 ? this.listboxItemsPerPage.toArray() : null;
    if (this.itemsPerPageList) {
      const currentItemsPerPageItem = this.itemsPerPageList.find(i => i.value === this.itemsPerPage);
      if (currentItemsPerPageItem) {
        currentItemsPerPageItem.selected = true;
      }
      const hasActive = this.itemsPerPageList.find(i => i.selected);
      if (!hasActive && this.itemsPerPageList.length > 0) {
        this.itemsPerPageList[0].selected = true;
      }
      this.changeDetector.detectChanges();
    }
  }
}
