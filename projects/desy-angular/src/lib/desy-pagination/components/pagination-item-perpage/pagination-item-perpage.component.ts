import { Component, Input } from '@angular/core';
import { SelectItemData } from '../../../desy-forms/interfaces/select-item-data';
import { ContentBaseComponent } from './../../../shared/components/content-base/content-base.component';


@Component({
  selector: 'desy-pagination-item-perpage',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class PaginationItemPerPageComponent extends ContentBaseComponent implements SelectItemData {
  @Input() classes?: string;
  @Input() value: number;
  selected: boolean;
}
