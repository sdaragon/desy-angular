import { ContentBaseComponent } from './../../../shared/components/content-base/content-base.component';
import { Component, Input } from '@angular/core';


@Component({
  selector: 'desy-pagination-listbox-label',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class PaginationListboxLabelComponent extends ContentBaseComponent {
  @Input() classes?: string;

}
