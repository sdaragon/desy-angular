import { ConfigurableFocusTrap, ConfigurableFocusTrapFactory } from '@angular/cdk/a11y';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef, ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  TemplateRef,
  Type,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { FocusUtils } from '../../../shared/utils/focus-utils';

let numDialogsOpened = 0;
type DialogCreationCallback = ((dialog: DialogComponent, caller: ComponentRef<any>) => void);

@Component({
  selector: 'desy-dialog',
  templateUrl: './dialog.component.html'
})
export class DialogComponent extends AccessibilityComponent implements OnChanges, OnDestroy, AfterViewInit {

  static readonly DIALOG_OPEN_CLASS = 'has-dialog';
  static readonly KEY_CODE_ESC = 'Escape';

  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;
  @ViewChild('focusTrapWrapper') focusTrapWrapper: ElementRef;

  @Input() active: boolean;
  @Output() activeChange = new EventEmitter<boolean>();

  @Input() id: any;
  @Input() caller: TemplateRef<any>;
  @Input() focusOnClose: string|HTMLElement;
  @Input() dismissOnBackdrop: boolean;
  @Input() classes: string;
  @Input() focusOnShow: string|HTMLElement;
  
  public callerType: Type<any>;
  public onCallerCreationCallback: DialogCreationCallback;

  lastActiveState = false;
  focusTrap: ConfigurableFocusTrap;
  clickOutsideEnabled = false;
  childElem: Element;

  constructor(private cdRef: ChangeDetectorRef,
              private focusTrapFactory: ConfigurableFocusTrapFactory,
              private factoryResolver: ComponentFactoryResolver) {
    super();
  }

  ngOnChanges(): void {
    if (this.focusTrapWrapper) {
      this.handleActiveState();
    }
  }

  ngAfterViewInit(): void {
    
    if (this.callerType) {
      const factory = this.factoryResolver.resolveComponentFactory(this.callerType);
      const component = this.container.createComponent(factory);
      if (this.onCallerCreationCallback) {
        this.onCallerCreationCallback(this, component);
      }
    }

    if (this.focusTrapWrapper.nativeElement.children && this.focusTrapWrapper.nativeElement.children.length === 1) {
      this.childElem = this.focusTrapWrapper.nativeElement.children[0];
    }

    this.handleActiveState();
    this.cdRef.detectChanges();
    let focusElem: HTMLElement;
    if (this.focusOnShow) {
      if (typeof this.focusOnShow === 'string') {
        focusElem = this.focusTrapWrapper.nativeElement.querySelector('#' + this.focusOnShow) as HTMLElement;
      } else {
        focusElem = this.focusOnShow;
      }
    } 
    let focused: HTMLElement;
    focused = document.activeElement as HTMLElement;
    focused.blur();
    if (focusElem) {
      setTimeout(() => {focusElem.focus()});
    }else{
      setTimeout(() => {FocusUtils.getFirstFocusableElement(this.childElem as HTMLElement).focus()});
    }
  }

  ngOnDestroy(): void {
    this.active = false;
    this.handleActiveState();
  }


  public dismiss(): void {
    this.activeChange.emit(false);
  }


  @HostListener('document:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent): void {
    if (event.key === DialogComponent.KEY_CODE_ESC) {
      event.stopPropagation();
      this.dismiss();
    }
  }

  handleActiveState(): void {
    if (!this.focusTrap) {
      this.focusTrap = this.focusTrapFactory.create(this.focusTrapWrapper.nativeElement);
    }

    if (this.active !== this.lastActiveState) {
      this.lastActiveState = this.active;

      // Habilitar/deshabilitar scroll
      if (this.active && numDialogsOpened === 0) {
        document.body.classList.add(DialogComponent.DIALOG_OPEN_CLASS);
      } else if (!this.active && numDialogsOpened === 1) {
        document.body.classList.remove(DialogComponent.DIALOG_OPEN_CLASS);
        if (document.body.classList.length === 0) {
          document.body.removeAttribute('class');
        }
      }

      if (this.active) {
        setTimeout(() => this.focusTrap.focusInitialElement());
        numDialogsOpened++;
      } else {
        let focusElem: HTMLElement;
        this.focusTrap.destroy();
        if (this.focusOnClose) {
          if (typeof this.focusOnClose === 'string') {
            focusElem = document.getElementById(this.focusOnClose);
          } else {
            focusElem = this.focusOnClose;
          }
        } else {
          focusElem = FocusUtils.getFirstFocusableElement();
        }
        if (focusElem) {
          setTimeout(() => focusElem.focus());
        }
        numDialogsOpened--;
      }

      setTimeout(() => this.clickOutsideEnabled = this.active);
    }
  }

  handleClickOutside(): void {
    if (this.dismissOnBackdrop) {
      this.dismiss();
    }
  }
}
