import { ButtonLoaderComponent } from './../../../../desy-buttons/components/button-loader/button-loader.component';
import { ButtonComponent } from './../../../../desy-buttons/components/button/button.component';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {ModalButtonData} from '../../../interfaces';

@Component({
  selector: 'desy-modal-button-loader-primary',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ModalButtonLoaderPrimaryComponent extends ButtonLoaderComponent implements ModalButtonData {
  @Input() declare classes: string;
  @Input() declare state: string;
  @Output() clickEvent = new EventEmitter();
}
