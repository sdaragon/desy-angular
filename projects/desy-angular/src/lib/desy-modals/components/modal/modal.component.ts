import {
  ChangeDetectorRef,
  Component,
  ContentChildren, ElementRef,
  EventEmitter,
  Input,
  Output,
  QueryList,
  TemplateRef
} from '@angular/core';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { DescriptionComponent } from '../../../desy-commons/components/description/description.component';
import { IconComponent } from '../../../desy-commons/components/icon/icon.component';
import { TitleComponent } from '../../../desy-commons/components/title/title.component';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { ModalButtonData, ModalData, ModalIconData } from '../../interfaces';
import { ModalButtonLoaderPrimaryComponent } from './modal-button-loader-primary/modal-button-loader-primary.component';
import { ModalButtonLoaderSecondaryComponent } from './modal-button-loader-secondary/modal-button-loader-secondary.component';
import { ModalButtonPrimaryComponent } from './modal-button-primary/modal-button-primary.component';
import { ModalButtonSecondaryComponent } from './modal-button-secondary/modal-button-secondary.component';

@Component({
  selector: 'desy-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent extends AccessibilityComponent {

  @Input() titleModal: ModalData;
  @Input() description: ModalData;
  @Input() itemsPrimary: ModalButtonData[];
  @Input() itemsSecondary: ModalButtonData[];
  @Input() itemsLoaderPrimary: ModalButtonData[];
  @Input() itemsLoaderSecondary: ModalButtonData[];
  @Input() icon: ModalIconData;
  @Input() headingLevel: number;

  @Input() isDismissible: boolean;
  @Input() id: string;
  @Input() classes: string;
  @Input() caller: TemplateRef<any>;

  @Output() closeModal: EventEmitter<void> = new EventEmitter();
  @Output() clickButton: EventEmitter<any> = new EventEmitter();

  @DesyContentChild()
  @ContentChildren(TitleComponent) titleComponent: TitleComponent;

  @DesyContentChild()
  @ContentChildren(DescriptionComponent) descriptionComponent: DescriptionComponent;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  @ContentChildren(ModalButtonPrimaryComponent) primaryButtonComponents: QueryList<ModalButtonPrimaryComponent>;
  @ContentChildren(ModalButtonSecondaryComponent) secondaryButtonComponents: QueryList<ModalButtonSecondaryComponent>;

  @ContentChildren(ModalButtonLoaderPrimaryComponent) primaryButtonLoaderComponents: QueryList<ModalButtonLoaderPrimaryComponent>;
  @ContentChildren(ModalButtonLoaderSecondaryComponent) secondaryButtonLoaderComponents: QueryList<ModalButtonLoaderSecondaryComponent>;

  @DesyContentChild()
  @ContentChildren(IconComponent) iconComponent: IconComponent;

  hasIconContent = true;
  descriptionIsHtml = true;
  descriptionIsEmpty = false;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  closeDialog(): void {
    this.closeModal.emit();
  }

  getType(): string {
    let type: string;
    if (this.iconComponent) {
      type = this.iconComponent.type ? this.iconComponent.type.toLocaleLowerCase() : null;
    } else if (this.icon.type) {
      type = this.icon.type ? this.icon.type.toLocaleLowerCase() : null;
    }

    return type;
  }

  checkDescriptionContent(element: ElementRef): void {
    const childs: NodeListOf<ChildNode> = element.nativeElement.childNodes;
    let hasHtml = false;
    let hasContent = false;
    for (let i = 0; i < childs.length && !hasHtml; i++) {
      const item = childs.item(i);
      if (item.nodeType === Node.TEXT_NODE && item.textContent.length > 0) {
        hasContent = true;
      } else if (item.nodeType === Node.ELEMENT_NODE || item.nodeType === Node.CDATA_SECTION_NODE) {
        hasHtml = true;
        hasContent = true;
      }
    }

    const descriptionWasEmpty = this.descriptionIsEmpty;
    this.descriptionIsEmpty = !hasContent;
    this.descriptionIsHtml = hasHtml;
    this.changeDetectorRef.detectChanges();
  }

  clickButtonEmit(item: ModalButtonData, event): void{
    this.clickButton.emit(event);

    if (item instanceof ModalButtonPrimaryComponent || item instanceof ModalButtonSecondaryComponent || item instanceof ModalButtonLoaderPrimaryComponent || item instanceof ModalButtonLoaderSecondaryComponent) {
      item.clickEvent.emit(event);
    }
  }

  handleIconContentEmpty(isEmpty: boolean): void {
    this.hasIconContent = !isEmpty;
    this.changeDetectorRef.detectChanges();
  }

  getTitleClasses(): string {
    let classes = 'c-h2 px-base text-center focus:outline-none focus:underline';
    if (this.titleComponent && this.titleComponent.classes) {
      classes = this.titleComponent.classes;
    } else if (this.titleModal && this.titleModal.classes) {
      classes = this.titleModal.classes;
    }
    return classes;
  }

  getDescriptionClasses(): string {
    let classes = 'c-paragraph-base my-base text-center';
    if (this.descriptionComponent && this.descriptionComponent.classes) {
      classes = this.descriptionComponent.classes;
    } else if (this.description && this.description.classes) {
      classes = this.description.classes;
    }
    return classes;
  }

  getPrimaryItems(): ModalButtonData[]|ModalButtonPrimaryComponent[] {
    return this.primaryButtonComponents && this.primaryButtonComponents.length > 0 ? this.primaryButtonComponents.toArray()
      : this.itemsPrimary;
  }

  getSecondaryItems(): ModalButtonData[]|ModalButtonPrimaryComponent[] {
    return this.secondaryButtonComponents && this.secondaryButtonComponents.length > 0 ? this.secondaryButtonComponents.toArray()
      : this.itemsSecondary;
  }

  getPrimaryLoaderItems():  ModalButtonData[]|ModalButtonLoaderPrimaryComponent[] {
    return this.primaryButtonLoaderComponents && this.primaryButtonLoaderComponents.length > 0 ? this.primaryButtonLoaderComponents.toArray() : this.itemsLoaderPrimary;
  }

  getSecondaryLoaderItems():  ModalButtonData[]|ModalButtonLoaderSecondaryComponent[] {
    return this.secondaryButtonLoaderComponents && this.secondaryButtonLoaderComponents.length > 0 ? this.secondaryButtonLoaderComponents.toArray() : this.itemsLoaderSecondary;
  }
}
