import { ButtonComponent } from './../../../../desy-buttons/components/button/button.component';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {ModalButtonData} from '../../../interfaces';

@Component({
  selector: 'desy-modal-button-secondary',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ModalButtonSecondaryComponent extends ButtonComponent implements ModalButtonData {
  @Input() declare classes: string;

  @Output() clickEvent = new EventEmitter();
}
