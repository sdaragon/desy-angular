import { ButtonComponent } from './../../../../desy-buttons/components/button/button.component';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {ModalButtonData} from '../../../interfaces';

@Component({
  selector: 'desy-modal-button-primary',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ModalButtonPrimaryComponent extends ButtonComponent implements ModalButtonData {
  @Input() declare classes: string;

  @Output() clickEvent = new EventEmitter();
}
