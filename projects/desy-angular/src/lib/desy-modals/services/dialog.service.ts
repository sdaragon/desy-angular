import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef, EmbeddedViewRef,
  Injectable,
  Injector,
  TemplateRef,
  Type
} from '@angular/core';
import { DialogComponent } from '../components/dialog/dialog.component';
import { ModalComponent } from '../components/modal/modal.component';
import { DialogOptions, ModalOptions, OpenDialogResult } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private factoryResolver: ComponentFactoryResolver,
              private defaultInjector: Injector,
              private appRef: ApplicationRef) { }

  openDialog(caller: Type<any>|ModalOptions|TemplateRef<any>,
             dialogOptions: DialogOptions): Promise<OpenDialogResult> {
    return new Promise<OpenDialogResult>((resolve, reject) => {

      if (!caller) {
        reject('caller is required');
      }

      if (!dialogOptions) {
        reject('dialogOptions is required');
      }

      const dialog = this.createDialog(dialogOptions);
      const result: OpenDialogResult = {
        dialog
      };

      if (caller instanceof Type) {
        dialog.instance.callerType = caller;
        dialog.instance.onCallerCreationCallback = (d, c) => {
          result.component = c;
          resolve(result);
        };
      } else if (caller instanceof TemplateRef) {
        dialog.instance.caller = caller;
        resolve(result);
      } else {
        dialog.instance.callerType = ModalComponent;
        dialog.instance.onCallerCreationCallback = (d, c) => {
          this.initModal(d, c, caller);
          result.component = c;
          resolve(result);
        };
      }
    });
  }

  closeDialog(dialog: ComponentRef<DialogComponent>|DialogComponent): void {
    if (dialog) {
      if (dialog instanceof ComponentRef) {
        dialog.instance.dismiss();
      } else {
        dialog.dismiss();
      }
    }
  }

  onCloseDialog(dialog: ComponentRef<DialogComponent>|DialogComponent): Promise<void> {
    return new Promise<void>((resolve => {
      let d;
      if (dialog instanceof ComponentRef) {
        d = dialog.instance;
      } else {
        d = dialog;
      }

      const subscription = d.activeChange.subscribe(() => {
        resolve();
        subscription.unsubscribe();
      });
    }));
  }

  private createDialog(dialogOptions: DialogOptions): ComponentRef<DialogComponent> {
    const factory = this.factoryResolver.resolveComponentFactory(DialogComponent);
    let dialog;
    if (dialogOptions.rootViewContainer) {
      dialog = dialogOptions.rootViewContainer.createComponent<DialogComponent>(factory);
    } else {
      dialog = factory.create(this.defaultInjector);
      this.appRef.attachView(dialog.hostView);
      const componentElement = (dialog.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
      document.body.appendChild(componentElement);
    }

    dialog.instance.active = true;
    if (dialogOptions) {
      Object.assign(dialog.instance, dialogOptions);
    }

    this.onCloseDialog(dialog).then(() => dialog.destroy());

    return dialog;
  }

  private initModal(dialog: DialogComponent, modalRef: ComponentRef<ModalComponent>, modalOptions: ModalOptions): void {
    modalRef.instance.titleModal = modalOptions.titleModal;
    modalRef.instance.description = modalOptions.description;
    modalRef.instance.itemsPrimary = modalOptions.itemsPrimary;
    modalRef.instance.itemsSecondary = modalOptions.itemsSecondary;
    modalRef.instance.icon = modalOptions.icon;

    modalRef.instance.isDismissible = modalOptions.isDismissible;
    modalRef.instance.id = modalOptions.id;
    modalRef.instance.classes = modalOptions.classes;
    modalRef.instance.caller = modalOptions.caller;

    if (modalOptions.isDismissible) {
      const subscription = modalRef.instance.closeModal.subscribe(() => {
        this.closeDialog(dialog);
        subscription.unsubscribe();
      });
    }
  }

}
