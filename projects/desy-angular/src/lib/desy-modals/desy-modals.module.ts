import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { A11yModule } from '@angular/cdk/a11y';
import { DesyButtonsModule } from '../desy-buttons/desy-buttons.module';

// components
import { ModalComponent } from './components/modal/modal.component';
import { ModalButtonPrimaryComponent } from './components/modal/modal-button-primary/modal-button-primary.component';
import { ModalButtonSecondaryComponent } from './components/modal/modal-button-secondary/modal-button-secondary.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { ModalButtonLoaderPrimaryComponent } from './components/modal/modal-button-loader-primary/modal-button-loader-primary.component';
import { ModalButtonLoaderSecondaryComponent } from './components/modal/modal-button-loader-secondary/modal-button-loader-secondary.component';

@NgModule({
  declarations: [
    ModalComponent,
    ModalButtonPrimaryComponent,
    ModalButtonSecondaryComponent,
    ModalButtonLoaderPrimaryComponent,
    ModalButtonLoaderSecondaryComponent,
    DialogComponent
  ],
  imports: [
    CommonModule,
    A11yModule,
    SharedModule,
    DesyButtonsModule
  ],
  exports: [
    ModalComponent,
    ModalButtonPrimaryComponent,
    ModalButtonSecondaryComponent,
    ModalButtonLoaderPrimaryComponent,
    ModalButtonLoaderSecondaryComponent,
    DialogComponent
  ],
})
export class DesyModalsModule { }
