import { DialogComponent } from '../components/dialog/dialog.component';
import { ComponentRef } from '@angular/core';

export interface OpenDialogResult {
  dialog: ComponentRef<DialogComponent>;
  component?: ComponentRef<any>;
}
