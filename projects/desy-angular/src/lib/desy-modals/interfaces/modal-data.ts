export interface ModalData{
    text: string;
    html: string;
    classes: string;
}
