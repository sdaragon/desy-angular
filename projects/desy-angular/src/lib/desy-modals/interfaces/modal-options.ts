import { TemplateRef } from '@angular/core';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { ModalButtonData } from './modal-button-data';
import { ModalData } from './modal-data';
import { ModalIconData } from './modal-icon-data';

export interface ModalOptions extends AccessibilityData {

  titleModal?: ModalData;
  description?: ModalData;
  itemsPrimary?: ModalButtonData[];
  itemsSecondary?: ModalButtonData[];
  icon?: ModalIconData;

  isDismissible?: boolean;
  id?: string;
  classes?: string;

  caller?: TemplateRef<any>;
}
