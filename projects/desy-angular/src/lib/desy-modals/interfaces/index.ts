export * from './modal-data';
export * from './modal-button-data';
export * from './modal-icon-data';

// Interfaces relativas a servicios
export * from './dialog-options';
export * from './modal-options';
export * from './open-dialog-result';
