export interface ModalButtonData {
    text?: string;
    classes?: string;

    id?: string;
    html?: string;

    element?: string; // 'a', 'button' o 'input'
    name?: string;
    type?: string;
    value?: any;
    disabled?: boolean;
    href?: string;
    target?: string;
    preventDoubleClick?: boolean;

    routerLink?: string | any[];
    routerLinkActiveClasses?: string | string[];

    //Button loader params
    loaderText?: string;
    loaderClasses?: string;
    state?: string;
    successText?: string;
}
