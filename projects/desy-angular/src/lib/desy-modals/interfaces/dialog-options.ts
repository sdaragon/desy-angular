import { ViewContainerRef } from '@angular/core';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface DialogOptions extends AccessibilityData {
  id: any;
  classes?: string;
  focusOnClose?: string|HTMLElement;
  dismissOnBackdrop?: boolean;
  rootViewContainer?: ViewContainerRef;
  focusOnShow?: string|HTMLElement;
}
