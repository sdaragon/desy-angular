import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'makeHtmlList'
})
export class MakeHtmlListPipe implements PipeTransform {

  transform(classes: string[], valueForEmpty?: string): string|null {
    let filteredList = classes.filter(c => c).join(' ');
    if (filteredList.length === 0 && valueForEmpty) {
      filteredList = valueForEmpty;
    }
    return filteredList.length > 0 ? filteredList : null;
  }
}
