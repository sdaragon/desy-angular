import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'externalHref'
})
export class ExternalHrefPipe implements PipeTransform {

  /**
   * Asegura que un enlace externo sea correcto
   * @param href enlace
   */
  transform(href: string): string {
    let externalHref: string;
    if (href) {
      if (href.match('http(s)?://') || href.match('mailto?:')) {
        externalHref = href;
      } else {
        externalHref = 'http://' + href;
      }
    } else {
      externalHref = '#';
    }
    return externalHref;
  }

}
