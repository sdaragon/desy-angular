import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// components
import {
  SpinnerComponent
} from './components';

// Have to create this module to use the SpinnerComponent internally in other components and to avoid problem when importing modules inside others creates a loop.

@NgModule({
  declarations: [
    // components
    SpinnerComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    SpinnerComponent,
  ]
})
export class SharedExternalModule { }
