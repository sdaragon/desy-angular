import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessibilityAndTextOrHtmlRequiredComponent } from './accessibility-and-text-or-html-required.component';

xdescribe('AccessibilityAndTextOrHtmlRequiredComponent', () => {
  let component: AccessibilityAndTextOrHtmlRequiredComponent;
  let fixture: ComponentFixture<AccessibilityAndTextOrHtmlRequiredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessibilityAndTextOrHtmlRequiredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessibilityAndTextOrHtmlRequiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
