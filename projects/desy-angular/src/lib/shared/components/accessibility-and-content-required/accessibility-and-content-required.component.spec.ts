import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessibilityAndContentRequiredComponent } from './accessibility-and-content-required.component';

xdescribe('AccessibilityAndTextOrHtmlRequiredComponent', () => {
  let component: AccessibilityAndContentRequiredComponent;
  let fixture: ComponentFixture<AccessibilityAndContentRequiredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessibilityAndContentRequiredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessibilityAndContentRequiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
