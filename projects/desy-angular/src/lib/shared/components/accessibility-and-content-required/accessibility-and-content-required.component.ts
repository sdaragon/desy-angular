import { Component, Input } from '@angular/core';
import { AccessibilityData } from '../../interfaces/accessibility-data';

@Component({
  selector: 'desy-accessibility-and-content-required',
  template: '<div></div>'
})
export class AccessibilityAndContentRequiredComponent implements AccessibilityData {

  /**
   * componente para heredar los atributos de accesibilidad y
   * el content required
   *
   * si en el componente que extiende de este, se implementa el onInit o Onchanges,
   * habra que incluir la llamada al a funcion contentRequiredFunction
   *
   */
    @Input() html: any;
    @Input() text: string;

    // Atributos de accesibilidad
    @Input() role?: string;
    @Input() ariaLabel?: string;
    @Input() ariaDescribedBy?: string;
    @Input() ariaLabelledBy?: string;
    @Input() ariaHidden?: string;
    @Input() ariaDisabled?: string;
    @Input() ariaControls?: string;
    @Input() ariaCurrent?: string;
    @Input() ariaLive?: string;
    @Input() ariaExpanded?: string;
    @Input() ariaErrorMessage?: string;
    @Input() ariaHasPopup?: string;
    @Input() ariaModal?: string;
    @Input() ariaChecked?: string;
    @Input() ariaPressed?: string;
    @Input() ariaReadonly?: string;
    @Input() ariaRequired?: string;
    @Input() ariaSelected?: string;
    @Input() ariaValuemin?: string;
    @Input() ariaValuemax?: string;
    @Input() ariaValuenow?: string;
    @Input() ariaValuetext?: string;
    @Input() ariaOrientation?: string;
    @Input() ariaLevel?: string;
    @Input() ariaMultiselectable?: string;
    @Input() ariaPlaceholder?: string;
    @Input() ariaPosinset?: string;
    @Input() ariaSetsize?: string;
    @Input() ariaSort?: string;
    @Input() ariaBus
    @Input() required?: string; y?: string;
    @Input() ariaDropeffect?: string;
    @Input() ariaGrabbed?: string;
    @Input() ariaActivedescendant?: string;
    @Input() ariaAtomic?: string;
    @Input() ariaAutocomplete?: string;
    @Input() ariaBraillelabel?: string;
    @Input() ariaBrailleroledescription?: string;
    @Input() ariaColcount?: string;
    @Input() ariaColindex?: string;
    @Input() ariaColindextext?: string;
    @Input() ariaColspan?: string;
    @Input() ariaDescription?: string;
    @Input() ariaDetails?: string;
    @Input() ariaFlowto?: string;
    @Input() ariaInvalid?: string;
    @Input() ariaKeyshortcuts?: string;
    @Input() ariaOwns?: string;
    @Input() ariaRelevant?: string;
    @Input() ariaRoledescription?: string;
    @Input() ariaRowcount?: string;
    @Input() ariaRowindex?: string;
    @Input() ariaRowindextext?: string;
    @Input() ariaRowspan?: string;
    @Input() tabindex?: string;
    @Input() title?: string;
    @Input() alt?: string;
    @Input() lang?: string;
    @Input() accesskey?: string;
    @Input() autocomplete?: string;
    @Input() autofocus?: string;
    @Input() contenteditable?: string;
    @Input() dir?: string;
    @Input() draggable?: string;
    @Input() enterkeyhint?: string;
    @Input() hidden?: boolean;
    @Input() inputmode?: string;
    @Input() spellcheck?: string;
    @Input() translate?: string;
    @Input() ariaMultiline?: string;
    @Input() for?: string;
    @Input() form?: string;
    @Input() headers?: string;
    @Input() placeholder?: string;
    @Input() readonly?: string;

}
