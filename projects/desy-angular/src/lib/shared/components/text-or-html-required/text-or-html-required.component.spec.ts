import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextOrHtmlRequiredComponent } from './text-or-html-required.component';

xdescribe('TextOrHtmlRequiredComponent', () => {
  let component: TextOrHtmlRequiredComponent;
  let fixture: ComponentFixture<TextOrHtmlRequiredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextOrHtmlRequiredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextOrHtmlRequiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
