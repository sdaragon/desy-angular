import { Component, Input } from '@angular/core';

@Component({
  selector: 'desy-text-or-html-required',
  template: '<div></div>'
})
export class TextOrHtmlRequiredComponent {

  /***
   * componente base para el comportamiento de texto o html required - YA NO ES REQUIRIDO
   * 
   **/

  @Input() text: string;
  @Input() html: string;

}

