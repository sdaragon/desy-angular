import {Component, TemplateRef, ViewChild} from '@angular/core';
import {AccessibilityComponent} from '../accessibility/accessibility.component';
/**
 * Componente base para proyectar contenido
 */
@Component({
  selector: 'desy-content-base',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ContentBaseComponent extends AccessibilityComponent {

  @ViewChild('contentTemplate', { static: true }) private content: TemplateRef<any>;

  public getContent(): TemplateRef<any> {
    return this.content;
  }
}
