import {QueryList} from '@angular/core';

/**
 * Decorador para convertir una queryList de componentes obtenidos con ContentChildren a un único componente, de forma que sea similar a un
 * ContentChild pero admitiendo únicamente los compontentes insertados en el primer nivel.
 * Debe utilizarse siempre junto a un ContentChildren
 * Si en el constructor de la clase en la que se utiliza se inyecta el Change detector con nombre 'changeDetectorRef', se indicará que hay
 * cambios cuando se detecten.
 * @param options opciones adicionales:
 *  - onSetCallbackName: nombre del método al que llamar tras asociarse el valor. Esto puede utilizarse, por ejemplo, para sobreescribir
 *                       parámetros definidos externamente en el componente.
 *  - onDeleteCallbackName: nombre del método al que llamar tras desasociarse el valor.
 */
export function DesyContentChild(options?: { onSetCallbackName?: string, onDeleteCallbackName?: string }): PropertyDecorator {
  return (target: any, propertyKey: string) => {
    const privateKeyName = `_${propertyKey}`;
    const privateQueryListKeyName = `_${propertyKey}Querylist`;

    Object.defineProperty(target, propertyKey, {
      set(value): void {
        const queryList = value instanceof QueryList ? value as QueryList<any> : null;
        if (queryList && !this[privateQueryListKeyName]) {
          this[privateQueryListKeyName] = queryList;
          queryList.changes.subscribe(() => {
            const previousValue = this[privateKeyName];
            this[privateKeyName] = queryList && queryList.length > 0 ? queryList.first : null;

            if (options && options.onSetCallbackName && this[options.onSetCallbackName] && this[privateKeyName] ) {
              this[options.onSetCallbackName](this[privateKeyName]);
            }

            if (options && options.onDeleteCallbackName && this[options.onDeleteCallbackName] && previousValue && !this[privateKeyName] ) {
              this[options.onDeleteCallbackName]();
            }
          });
          queryList.notifyOnChanges();
        }
      },
      get(): any {
        return this[privateKeyName];
      }
    });
  };
}
