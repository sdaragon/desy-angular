/**
 * Decorador para controlar los cambios de un input
 * @param callbackName nombre de la función a la que se llamará tras aplicar el cambio
 */
export function DesyOnInputChange(callbackName: string): PropertyDecorator {
  return (target: any, propertyKey: string) => {
    const privateKeyName = `_${propertyKey}`;

    Object.defineProperty(target, propertyKey, {
      set(value): void {
        this[privateKeyName] = value;
        this[callbackName]();
      },
      get(): any {
        return this[privateKeyName];
      }
    });
  };
}
