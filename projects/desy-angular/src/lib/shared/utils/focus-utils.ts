export class FocusUtils {

  public static getFirstFocusableElement(parent?: HTMLElement): HTMLElement {
    const selectionableElements = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
    let focusableList;
    if (parent) {
      focusableList = parent.querySelectorAll(selectionableElements);
    } else {
      focusableList = document.querySelectorAll(selectionableElements);
    }

    if (!focusableList || focusableList.length === 0) {
      return null;
    }

    let firstFocusable = null;
    let i = 0;
    while (i < focusableList.length) {
      try {
        const element = focusableList.item(i) as HTMLInputElement;
        if (FocusUtils.isFocusable(element)) {
          firstFocusable = element;
          break;
        }
      } catch (e) {

      }
      i++;
    }

    return firstFocusable;
  }

  public static isFocusable(element: any): boolean {
    if (element.tabIndex > 0 || (element.tabIndex === 0 && element.getAttribute('tabIndex') !== null)) {
      return true;
    }

    if (element.disabled) {
      return false;
    }

    switch (element.nodeName) {
      case 'A':
        return !!element.href && element.rel !== 'ignore';
      case 'INPUT':
        return element.type !== 'hidden' && element.type !== 'file';
      case 'BUTTON':
      case 'SELECT':
      case 'TEXTAREA':
        return true;
      default:
        return false;
    }
  }

}
