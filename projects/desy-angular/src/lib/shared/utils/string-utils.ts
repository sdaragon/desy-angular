/**
 * Facilita funciones de utilidad para trabajar con strings
 */
export class StringUtils {

  /**
   * Escapa los caracteres propios de HTML para mostrarlos como contenido
   * @param value texto en formato html
   */
  public static escapeHtml(value: string): string {
    return value ? value
      .replace(new RegExp(`&`, 'g'), '&amp;')
      .replace(new RegExp(`<`, 'g'), '&lt;')
      .replace(new RegExp(`>`, 'g'), '&gt;')
      .replace(new RegExp(`"`, 'g'), '&quot;')
      .replace(new RegExp(`'`, 'g'), '&#39;') : '';
  }
}
