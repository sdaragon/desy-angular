import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// components
import {
  AccessibilityAndContentRequiredComponent,
  AccessibilityAndTextOrHtmlRequiredComponent,
  AccessibilityComponent,
  ContentBaseComponent,
  TextOrHtmlRequiredComponent
} from './components';

// pipes
import { ExternalHrefPipe } from './pipes/external-href.pipe';
import { InnerHtmlPipe } from './pipes/inner-html.pipe';
import { MakeHtmlListPipe } from './pipes/make-html-list.pipe';

// directives
import { AccessibilityDirective } from './directives/accessibility-directive';
import { AttributeChangeDirective } from './directives/attribute-change.directive';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { ContentChangeDirective } from './directives/content-change.directive';
import { CustomInnerContentDirective } from './directives/custom-inner-content.directive';
import { InnerContentDirective } from './directives/inner-content.directive';


@NgModule({
  declarations: [
    // components
    AccessibilityComponent,
    ContentBaseComponent,
    TextOrHtmlRequiredComponent,
    AccessibilityAndTextOrHtmlRequiredComponent,
    AccessibilityAndContentRequiredComponent,

    // pipes
    InnerHtmlPipe,
    MakeHtmlListPipe,
    ExternalHrefPipe,

    // directives
    AttributeChangeDirective,
    InnerContentDirective,
    ClickOutsideDirective,
    ContentChangeDirective,
    CustomInnerContentDirective,
    AccessibilityDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    RouterModule,

    // components
    AccessibilityComponent,
    ContentBaseComponent,
    TextOrHtmlRequiredComponent,
    AccessibilityAndTextOrHtmlRequiredComponent,
    AccessibilityAndContentRequiredComponent,

    // pipes
    InnerHtmlPipe,
    MakeHtmlListPipe,
    ExternalHrefPipe,

    // directives
    AttributeChangeDirective,
    InnerContentDirective,
    ClickOutsideDirective,
    ContentChangeDirective,
    CustomInnerContentDirective,
    AccessibilityDirective,
  ]
})
export class SharedModule { }
