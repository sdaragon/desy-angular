import {Directive, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[desyAttributeChange]'
})
export class AttributeChangeDirective implements OnDestroy {

  private changes: MutationObserver;

  @Input() attributesToCheck: string|string[];

  @Output()
  public desyAttributeChange = new EventEmitter();

  @Output()
  public desyAttributeChangeMutation = new EventEmitter();

  @Input() desyAttributeChangeEnabled = true;


  constructor(private elementRef: ElementRef) {
    const element = this.elementRef.nativeElement;

    if (this.desyAttributeChangeEnabled) {
      this.changes = new MutationObserver((mutations: MutationRecord[]) => {
          mutations.forEach((mutation: MutationRecord) => {
            if (this.attributesToCheck) {
              const attrs = typeof (this.attributesToCheck) === 'string' ? [this.attributesToCheck] : this.attributesToCheck;
              if (attrs.findIndex(v => v === mutation.attributeName) >= 0) {
                this.desyAttributeChange.emit(this.elementRef.nativeElement.innerHTML);
                this.desyAttributeChangeMutation.emit(mutation);
              }
            }
          });
        }
      );
  
      this.changes.observe(element, {
        attributes: true,
        childList: false,
        characterData: false,
        subtree: false
      });
    }
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }
}
