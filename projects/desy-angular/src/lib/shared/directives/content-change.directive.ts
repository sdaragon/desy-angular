import {Directive, ElementRef, EventEmitter, Input, OnDestroy, Output} from '@angular/core';

/**
 * Permite comprobar cuándo se produce un cambio en el contenido de un elemento.
 * La propia directiva emite el html interno del elemento en formato string, mientras que desyContentChangeMutation emite los cambios.
 */
@Directive({
  selector: '[desyContentChange]'
})
export class ContentChangeDirective implements OnDestroy {

  private changes: MutationObserver;

  @Input() observeProperties: { attributes: boolean, childList: boolean, characterData: boolean, subtree: boolean } = {
    attributes: false,
    childList: true,
    characterData: true,
    subtree: true
  };

  @Output()
  public desyContentChange = new EventEmitter<ElementRef>();

  @Output()
  public desyContentChangeMutation = new EventEmitter();

  constructor(private elementRef: ElementRef) {
    const element = this.elementRef.nativeElement;

    this.changes = new MutationObserver((mutations: MutationRecord[]) => {
        mutations.forEach((mutation: MutationRecord) => {
          this.desyContentChange.emit(this.elementRef);
          this.desyContentChangeMutation.emit(mutation);
        });
      }
    );

    this.changes.observe(element, this.observeProperties);
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }
}
