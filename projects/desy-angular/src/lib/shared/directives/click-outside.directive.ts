import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[desyClickOutside]'
})
export class ClickOutsideDirective {

  @Input('desyClickOutside') active = true;
  @Output() clickOutside = new EventEmitter<void>();
  @Input() desyClickOutsideElem: Element;

  constructor(private hostElementRef: ElementRef) { }

  @HostListener('document:click', ['$event.target'])
  public onClick(target): void {
    let clickedInside;
    if (this.desyClickOutsideElem) {
      clickedInside = this.desyClickOutsideElem.contains(target);
    } else {
      clickedInside = (this.hostElementRef.nativeElement as Element).contains(target);
    }
    if (!clickedInside && this.active) {
      this.clickOutside.emit();
    }
  }

}
