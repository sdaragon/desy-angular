import {Directive, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';
@Directive({
  selector: '[desyInnerContent]'
})
export class InnerContentDirective implements OnChanges {

  @Input('desyInnerContent') content: string;
  @Input() isHtml: boolean;
  @Input() deleteContentIfEmpty = true;

  constructor(private el: ElementRef) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.content) {
      if (this.isHtml) {
        this.el.nativeElement.innerHTML = this.content;
      } else {
        this.el.nativeElement.innerText = this.content;
      }
    } else if (this.deleteContentIfEmpty) {
      this.el.nativeElement.innerText = null;
    }
  }

}
