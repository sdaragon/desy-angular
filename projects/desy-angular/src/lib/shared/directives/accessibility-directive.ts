import { Directive, DoCheck, ElementRef, Input, KeyValueDiffer, KeyValueDiffers, OnChanges, Renderer2, SimpleChanges } from '@angular/core';
import { ACCESSIBILITY_ATTRIBUTES } from '../components/accessibility/accessibility-attributes';

@Directive({
  selector: '[desyAppAccessibility]'
})
export class AccessibilityDirective implements OnChanges, DoCheck {
  @Input('desyAppAccessibility') accessibility: any;
  private differ: KeyValueDiffer<string, any>;

  constructor(private el: ElementRef, private renderer: Renderer2, private differs: KeyValueDiffers) {
    this.differ = this.differs.find({}).create();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['accessibility']) {
      this.applyAccessibilityAttributes();
    }
  }
  ngDoCheck() {
    if (this.accessibility) {
      const changes = this.differ.diff(this.accessibility);
      if (changes) {
        this.applyAccessibilityAttributes();
      }
    }
  }

  private applyAccessibilityAttributes() {
    if (this.accessibility) {
      Object.keys(this.accessibility).forEach(attr => {
        if (ACCESSIBILITY_ATTRIBUTES.includes(attr) && this.accessibility[attr] !== null && this.accessibility[attr] !== undefined) {
          const htmlAttr = this.camelToKebabCase(attr);
          this.renderer.setAttribute(this.el.nativeElement, htmlAttr, this.accessibility[attr]);
        }
      });
    }
  }

  private camelToKebabCase(str: string): string {
    if (str === str.toLowerCase()) {
      return str;
    }
    return str.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();
  }
}