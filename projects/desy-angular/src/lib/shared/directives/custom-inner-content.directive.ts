import {
  ComponentFactory,
  ComponentFactoryResolver,
  Directive,
  Injector,
  Input, OnChanges,
  SimpleChanges, TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { ContentBaseComponent } from '../components';

/**
 * Permite introducir contenido de forma dinámica en una posición determinada desde:
 * - component: Componente existente de tipo ContentBase
 * - html: texto html en formato string
 * - text: texto plano en formato string
 */
@Directive({
  selector: '[desyCustomInnerContent]'
})
export class CustomInnerContentDirective implements OnChanges {

  @Input() desyCustomInnerContent: { component?: any, template?: TemplateRef<any>, html?: string, text?: string, parentComponent?: any };

  private factory: ComponentFactory<ContentBaseComponent>;

  constructor(private viewContainerRef: ViewContainerRef, private resolver: ComponentFactoryResolver, private injector: Injector) {
    this.factory = this.resolver.resolveComponentFactory(ContentBaseComponent);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.viewContainerRef.clear();
    if (this.desyCustomInnerContent.component && this.desyCustomInnerContent.component instanceof ContentBaseComponent) {

      // Se crea la vista embebida basándose en el contenido del ContentBase indicado.
      const embeddedView = this.viewContainerRef.createEmbeddedView(this.desyCustomInnerContent.component.getContent());

      this.emitContentToParent(embeddedView);

    } else if (this.desyCustomInnerContent.template) {

      // Se crea la vista embebida utilizando directamente el template.
      this.viewContainerRef.createEmbeddedView(this.desyCustomInnerContent.template);
    } else if (this.desyCustomInnerContent.html) {

      // Se carga el contenido en un div temporal.
      const tmpWrapper = document.createElement('div');
      tmpWrapper.innerHTML = this.desyCustomInnerContent.html;

      // Los nodos hijos del div se proyectan dentro de un componente ContentBase.
      const nodeList = [];
      tmpWrapper.childNodes.forEach(node => nodeList.push(node));
      const componentRef = this.factory.create(this.injector, [nodeList]);

      // Finalmente se crea la vista embebida basándose en el contenido del ContentBase creado.
      // Se podría hacer con un this.viewContainerRef.createComponent(), pero así se evita el tag envolvente del componente en el DOM
      const embeddedView = this.viewContainerRef.createEmbeddedView(componentRef.instance.getContent());
      this.emitContentToParent(embeddedView);

    } else if (this.desyCustomInnerContent.text) {

      // Se carga el contenido en un nodo de texto
      const textNode = document.createTextNode(this.desyCustomInnerContent.text);

      // El nodo de texto se proyecta dentro de un componente ContentBase
      const componentRef = this.factory.create(this.injector, [[textNode]]);

      // Finalmente se crea la vista embebida basándose en el contenido del ContentBase creado.
      // Se podría hacer con un this.viewContainerRef.createComponent(), pero así se evita el tag envolvente del componente en el DOM
      const embeddedView = this.viewContainerRef.createEmbeddedView(componentRef.instance.getContent());
      this.emitContentToParent(embeddedView);
    }
  }

  // Esto solamente se usa para el table component, para poder extraer el contenido de las celdas de la tabla y poder añadirlos a un attr-data-label para la version movil.
  private emitContentToParent(embeddedView: any): void {
    if (this.desyCustomInnerContent.parentComponent && typeof this.desyCustomInnerContent.parentComponent.handleContentExtracted === 'function') {
      let textContent = '';
      embeddedView.rootNodes.forEach(node => {
        if (node.nodeType === Node.TEXT_NODE) {
          textContent += node.textContent.trim() + ' ';
        } else if (node.nodeType === Node.ELEMENT_NODE) {
          textContent += (node as HTMLElement).innerText.trim() + ' ';
        }
      });
      this.desyCustomInnerContent.parentComponent.handleContentExtracted(textContent.trim());
    }
  }

}
