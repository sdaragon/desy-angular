import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { NgxTippyModule } from 'ngx-tippy-wrapper';
import { DesyCommonsModule } from '../desy-commons/desy-commons.module';
import { DesyFormsModule } from '../desy-forms/desy-forms.module';
import { AccordionHistoryItemComponent } from './components/accordion-history/accordion-history-item/accordion-history-item.component';
import { AccordionHistoryComponent } from './components/accordion-history/accordion-history.component';
import { AccordionItemHideButtonComponent } from './components/accordion-history/accordion-item-hide-button/accordion-item-hide-button.component';
import { AccordionItemShowButtonComponent } from './components/accordion-history/accordion-item-show-button/accordion-item-show-button.component';
import { AccordionHeaderComponent } from './components/accordion/accordion-header/accordion-header.component';
import { AccordionItemComponent } from './components/accordion/accordion-item/accordion-item.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { AlertComponent } from './components/alert/alert.component';
import { CardLeftComponent } from './components/card/card-sectors/card-left.component';
import { CardRightComponent } from './components/card/card-sectors/card-right.component';
import { CardSubComponent } from './components/card/card-sectors/card-sub.component';
import { CardSuperComponent } from './components/card/card-sectors/card-super.component';
import { CardComponent } from './components/card/card.component';
import { CollapsibleComponent } from './components/collapsible/collapsible.component';
import { DefinitionComponent } from './components/description-list/definition/definition.component';
import { DescriptionItemComponent } from './components/description-list/description-item/description-item.component';
import { DescriptionListComponent } from './components/description-list/description-list.component';
import { TermComponent } from './components/description-list/term/term.component';
import { DetailsComponent } from './components/details/details.component';
import { ItemContentBottomComponent } from './components/item/item-content-bottom/item-content-bottom.component';
import { ItemContentRightComponent } from './components/item/item-content-right/item-content-right.component';
import { ItemItemComponent } from './components/item/item-item/item-item.component';
import { ItemComponent } from './components/item/item.component';
import { MediaObjectFigureComponent } from './components/media-object/media-object-figure/media-object-figure.component';
import { MediaObjectComponent } from './components/media-object/media-object.component';
import { StatusItemComponent } from './components/status-item/status-item.component';
import { StatusComponent } from './components/status/status.component';
import { PanelComponent } from './components/tabs/panel/panel.component';
import { TabItemComponent } from './components/tabs/tab-item/tab-item.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { TooltipContentComponent } from './components/tooltip/tooltip-content/tooltip-content.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';

@NgModule({
  declarations: [
    AlertComponent,
    AccordionComponent,
    AccordionHeaderComponent,
    AccordionHistoryComponent,
    AccordionHistoryItemComponent,
    AccordionItemShowButtonComponent,
    AccordionItemHideButtonComponent,
    AccordionItemComponent,
    DetailsComponent,
    CollapsibleComponent,
    DescriptionListComponent,
    DescriptionItemComponent,
    TermComponent,
    DefinitionComponent,
    ItemComponent,
    ItemContentBottomComponent,
    ItemContentRightComponent,
    ItemItemComponent,
    MediaObjectComponent,
    StatusComponent,
    StatusItemComponent,
    TabsComponent,
    TooltipComponent,
    TooltipContentComponent,
    PanelComponent,
    TabItemComponent,
    MediaObjectFigureComponent,
    CardComponent,
    CardLeftComponent,
    CardRightComponent,
    CardSuperComponent,
    CardSubComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    DesyFormsModule,
    DesyCommonsModule,
    NgxTippyModule
  ],
  exports: [
    AlertComponent,
    AccordionComponent,
    AccordionHeaderComponent,
    AccordionHistoryComponent,
    AccordionHistoryItemComponent,
    AccordionItemShowButtonComponent,
    AccordionItemHideButtonComponent,
    AccordionItemComponent,
    DetailsComponent,
    CollapsibleComponent,
    DescriptionListComponent,
    DescriptionItemComponent,
    TermComponent,
    DefinitionComponent,
    ItemComponent,
    ItemContentBottomComponent,
    ItemContentRightComponent,
    ItemItemComponent,
    MediaObjectComponent,
    StatusComponent,
    StatusItemComponent,
    TabsComponent,
    TooltipComponent,
    TooltipContentComponent,
    PanelComponent,
    TabItemComponent,
    MediaObjectFigureComponent,
    CardComponent,
    CardLeftComponent,
    CardRightComponent,
    CardSuperComponent,
    CardSubComponent,
  ]
})
export class DesyViewsModule { }
