import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface AccordionItemData extends AccessibilityData {
  headerText?: string;
  headerHtml?: string;
  text?: string;
  html?: string;
  id?: string;
  open?: boolean;
  classes?: string;
  status?: string;
}
