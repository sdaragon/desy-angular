import { TemplateRef } from '@angular/core';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface TabsPanelData extends AccessibilityData {
    id?: string;
    text?: string;
    html?: TemplateRef<any>;
    classes?: string;
}
