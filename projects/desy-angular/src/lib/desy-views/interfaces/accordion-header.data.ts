export interface AccordionHeaderData {
  text?: string;
  html?: string;
  classes?: string;
}
