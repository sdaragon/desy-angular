import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { StatusIconData } from './status-icon-data';

export interface StatusData extends AccessibilityData {
  text: string;
  icon: StatusIconData;
  id?: string;
  classes?: string;
}
