import { ViewContainerRef } from '@angular/core';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface AlertOptions extends AccessibilityData {
  place: ViewContainerRef;
  id: string;
  classes?: string;
  focusFirst?: boolean;
}
