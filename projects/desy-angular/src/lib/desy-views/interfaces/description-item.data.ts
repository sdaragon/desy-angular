import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { TermDefinitionData } from './term-definition.data';

export interface DescriptionItemData extends AccessibilityData {
    term: TermDefinitionData;
    definition: TermDefinitionData;
    classes?: string;
    id?: string;
}
