import { ComponentRef } from '@angular/core';
import { AlertComponent } from '../components/alert/alert.component';

export interface OpenAlertResult {
  alert: ComponentRef<AlertComponent>;
  component?: ComponentRef<any>;
}
