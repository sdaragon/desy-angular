import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface TermDefinitionData extends AccessibilityData {
    text?: string;
    html?: string;
    classes?: string;
    id?: string;
}
