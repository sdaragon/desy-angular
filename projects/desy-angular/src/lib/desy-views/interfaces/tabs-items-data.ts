import { ContentComponent } from '../../desy-commons/components/content/content.component';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { TabsPanelData } from './tabs-panel-data';

export interface TabsItemsData extends AccessibilityData {
    id?: string;
    text?: string;
    html?: string;
    disabled?: boolean;
    active?: boolean;
    panel?: TabsPanelData;
    contentComponent?: ContentComponent;
}
