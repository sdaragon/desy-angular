export interface StatusItemTitleData {
    text?: string;
    html?: string;
    classes?: string;
}
