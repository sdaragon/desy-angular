import {Component, ContentChildren, Input} from '@angular/core';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {ContentBaseComponent} from '../../../../shared/components';
import {AccordionHeaderData} from '../../../interfaces';

@Component({
  selector: 'desy-accordion-header',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class AccordionHeaderComponent extends ContentBaseComponent implements AccordionHeaderData {

  @Input() classes?: string;

}
