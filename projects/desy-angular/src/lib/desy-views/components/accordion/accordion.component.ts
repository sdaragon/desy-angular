import { Component, ContentChildren, ElementRef, HostBinding, Input, OnChanges, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { AccordionHeaderData, AccordionItemData } from '../../interfaces';
import { AccordionHeaderComponent } from './accordion-header/accordion-header.component';
import { AccordionItemComponent } from './accordion-item/accordion-item.component';


@Component({
  selector: 'desy-accordion',
  templateUrl: './accordion.component.html'
})
export class AccordionComponent extends AccessibilityComponent implements OnChanges {

  @ViewChildren('itemSelector') itemSelectors: QueryList<ElementRef>;
  
  @Input() id: string;
  @Input() idPrefix: string;
  @Input() allowToggle: boolean;
  @Input() allowMultiple: boolean;
  @Input() heading: AccordionHeaderData;
  @Input() headingLevel;
  @Input() showControl: boolean;
  @Input() items: AccordionItemData[];

  @Input() @HostBinding('class') classes: any = "block";
  @HostBinding('attr.role') role: string = null;
  @HostBinding('attr.aria-label') ariaLabel: string = null;
  @HostBinding('attr.aria-describedby') ariaDescribedBy: string = null;
  @HostBinding('attr.aria-labelledby') ariaLabelledBy: string = null;
  @HostBinding('attr.aria-hidden') ariaHidden: string = null;
  @HostBinding('attr.aria-disabled') ariaDisabled: string = null;
  @HostBinding('attr.aria-controls') ariaControls: string = null;
  @HostBinding('attr.aria-current') ariaCurrent: string = null;
  @HostBinding('attr.aria-live') ariaLive: string = null;
  @HostBinding('attr.aria-expanded') ariaExpanded: string = null;
  @HostBinding('attr.aria-errormessage') ariaErrorMessage: string = null;
  @HostBinding('attr.aria-haspopup') ariaHasPopup: string = null;
  @HostBinding('attr.aria-modal') ariaModal: string = null;
  @HostBinding('attr.aria-checked') ariaChecked: string = null;
  @HostBinding('attr.aria-pressed') ariaPressed: string = null;
  @HostBinding('attr.aria-readonly') ariaReadonly: string = null;
  @HostBinding('attr.aria-required') ariaRequired: string = null;
  @HostBinding('attr.aria-selected') ariaSelected: string = null;
  @HostBinding('attr.aria-valuemin') ariaValuemin: string = null;
  @HostBinding('attr.aria-valuemax') ariaValuemax: string = null;
  @HostBinding('attr.aria-valuenow') ariaValuenow: string = null;
  @HostBinding('attr.aria-valuetext') ariaValuetext: string = null;
  @HostBinding('attr.aria-orientation') ariaOrientation: string = null;
  @HostBinding('attr.aria-level') ariaLevel: string = null;
  @HostBinding('attr.aria-multiselectable') ariaMultiselectable: string = null;
  @HostBinding('attr.aria-placeholder') ariaPlaceholder: string = null;
  @HostBinding('attr.aria-posinset') ariaPosinset: string = null;
  @HostBinding('attr.aria-setsize') ariaSetsize: string = null;
  @HostBinding('attr.aria-sort') ariaSort: string = null;
  @HostBinding('attr.aria-busy') ariaBusy: string = null;
  @HostBinding('attr.aria-dropeffect') ariaDropeffect: string = null;
  @HostBinding('attr.aria-grabbed') ariaGrabbed: string = null;
  @HostBinding('attr.aria-activedescendant') ariaActivedescendant: string = null;
  @HostBinding('attr.aria-atomic') ariaAtomic: string = null;
  @HostBinding('attr.aria-autocomplete') ariaAutocomplete: string = null;
  @HostBinding('attr.aria-braillelabel') ariaBraillelabel: string = null;
  @HostBinding('attr.aria-brailleroledescription') ariaBrailleroledescription: string = null;
  @HostBinding('attr.aria-colcount') ariaColcount: string = null;
  @HostBinding('attr.aria-colindex') ariaColindex: string = null;
  @HostBinding('attr.aria-colindextext') ariaColindextext: string = null;
  @HostBinding('attr.aria-colspan') ariaColspan: string = null;
  @HostBinding('attr.aria-description') ariaDescription: string = null;
  @HostBinding('attr.aria-details') ariaDetails: string = null;
  @HostBinding('attr.aria-flowto') ariaFlowto: string = null;
  @HostBinding('attr.aria-invalid') ariaInvalid: string = null;
  @HostBinding('attr.aria-keyshortcuts') ariaKeyshortcuts: string = null;
  @HostBinding('attr.aria-owns') ariaOwns: string = null;
  @HostBinding('attr.aria-relevant') ariaRelevant: string = null;
  @HostBinding('attr.aria-roledescription') ariaRoledescription: string = null;
  @HostBinding('attr.aria-rowcount') ariaRowcount: string = null;
  @HostBinding('attr.aria-rowindex') ariaRowindex: string = null;
  @HostBinding('attr.aria-rowindextext') ariaRowindextext: string = null;
  @HostBinding('attr.aria-rowspan') ariaRowspan: string = null;
  @HostBinding('attr.tabindex') tabindex: string = null;
  @HostBinding('attr.title') title: string = null;
  @HostBinding('attr.alt') alt: string = null;
  @HostBinding('attr.lang') lang: string = null;
  @HostBinding('attr.accesskey') accesskey: string = null;
  @HostBinding('attr.autocomplete') autocomplete: string = null;
  @HostBinding('attr.autofocus') autofocus: string = null;
  @HostBinding('attr.contenteditable') contenteditable: string = null;
  @HostBinding('attr.dir') dir: string = null;
  @HostBinding('attr.draggable') draggable: string = null;
  @HostBinding('attr.enterkeyhint') enterkeyhint: string = null;
  @HostBinding('attr.hidden') hidden: boolean = null;
  @HostBinding('attr.inputmode') inputmode: string = null;
  @HostBinding('attr.spellcheck') spellcheck: string = null;
  @HostBinding('attr.translate') translate: string = null;
  @HostBinding('attr.aria-multiline') ariaMultiline: string = null;
  @HostBinding('attr.for') for: string = null;
  @HostBinding('attr.form') form: string = null;
  @HostBinding('attr.headers') headers: string = null;
  @HostBinding('attr.placeholder') placeholder: string = null;
  @HostBinding('attr.readonly') readonly: string = null;
  @HostBinding('attr.required') required: string = null;

  @DesyContentChild()
  @ContentChildren(AccordionHeaderComponent) headingComponent: AccordionHeaderComponent;
  @ContentChildren(AccordionItemComponent) itemComponentList: QueryList<AccordionItemComponent>;

  showAll = true;
  currentItem: number;

  ngOnChanges(changes: SimpleChanges): void {
    this.classes = this.classes ? this.classes + " block" : null;
  }

  getItemId(item, index: number): string {
    let id;
    if (item.id) {
      id = item.id;
    } else {
      id = this.idPrefix ? this.idPrefix + '-' + (index + 1) : (index + 1);
    }
    return id;
  }

  changeAll(): void {
    const items = this.getItems();
    items.forEach(item => {
      const isChange = item.open !== this.showAll;
      item.open = this.showAll;
      if (isChange && item instanceof AccordionItemComponent) {
        item.openChange.emit(item.open);
      }
    });
    this.showAll = !this.showAll;
  }

  toggleItem(item: AccordionItemData): void {
    if (!item.open || this.allowToggle) {
      const newOpenValue = !item.open;
      if (!this.allowMultiple) {
        const items = this.getItems();
        items.forEach(it => {
          const wasOpen = it.open;
          it.open = false;
          if (wasOpen && it !== item && it instanceof AccordionItemComponent) {
            it.openChange.emit(it.open);
          }
        });
      }
      item.open = newOpenValue;
      if (item instanceof AccordionItemComponent) {
        item.openChange.emit(item.open);
      }
    }
  }

  focusFirstItem(event): void {
    event.preventDefault();
    this.itemSelectors.toArray()[0].nativeElement.focus();
  }

  focusLastItem(event): void {
    event.preventDefault();
    this.itemSelectors.toArray()[this.itemSelectors.length - 1].nativeElement.focus();
  }

  focusNextItem(event, focusedItem: number): void {
    event.preventDefault();
    const selectors = this.itemSelectors.toArray();
    if (selectors[(focusedItem + 1) % selectors.length] != null) {
      selectors[(focusedItem + 1) % selectors.length].nativeElement.focus();
    }
  }

  focusPreviousItem(event, focusedItem: number): void {
    event.preventDefault();
    const selectors = this.itemSelectors.toArray();
    if (selectors[(focusedItem + selectors.length - 1) % selectors.length] != null) {
      selectors[(focusedItem + selectors.length - 1) % selectors.length].nativeElement.focus();
    }
  }

  selectItem(i: number): void {
    this.currentItem = i;
  }

  getTitleClasses(): string {
    let classes = 'c-h2 mb-base';
    if (this.headingComponent && this.headingComponent.classes) {
      classes = this.headingComponent.classes;
    } else if (this.heading && this.heading.classes) {
      classes = this.heading.classes;
    }
    return classes;
  }

  getItems(): AccordionItemData[] {
    const items = this.itemComponentList && this.itemComponentList.length > 0 ? this.itemComponentList.toArray() : this.items;
    return items;
  }

  getItemHeader(item: AccordionItemData): AccordionHeaderComponent {
    return item instanceof AccordionItemComponent ? item.headerComponent : null;
  }

  getItemContent(item: AccordionItemData): ContentComponent {
    return item instanceof AccordionItemComponent ? item.contentComponent : null;
  }

}
