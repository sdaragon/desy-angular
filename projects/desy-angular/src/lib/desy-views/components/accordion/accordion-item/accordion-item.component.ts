import {Component, ContentChildren, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccordionItemData} from '../../../interfaces';
import {AccordionHeaderComponent} from '../accordion-header/accordion-header.component';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {ContentComponent} from '../../../../desy-commons/components/content/content.component';
import {AccessibilityComponent} from '../../../../shared/components';
import {AccordionItemShowButtonComponent} from '../../accordion-history/accordion-item-show-button/accordion-item-show-button.component';
import {AccordionItemHideButtonComponent} from '../../accordion-history/accordion-item-hide-button/accordion-item-hide-button.component';

@Component({
  selector: 'desy-accordion-item',
  template: '',
})
export class AccordionItemComponent extends AccessibilityComponent implements AccordionItemData {

  @Input() id: string;
  @Input() open: boolean;
  @Input() classes: string;

  @Output() openChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @DesyContentChild()
  @ContentChildren(AccordionHeaderComponent) headerComponent: AccordionHeaderComponent;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  @DesyContentChild()
  @ContentChildren(AccordionItemShowButtonComponent) showHeaderButton: AccordionItemShowButtonComponent;
  
  @DesyContentChild()
  @ContentChildren(AccordionItemHideButtonComponent) hideHeaderButton: AccordionItemHideButtonComponent;

}
