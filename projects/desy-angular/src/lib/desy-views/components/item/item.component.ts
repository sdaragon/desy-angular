import {
  ChangeDetectorRef,
  Component,
  ContentChildren,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  QueryList,
  SimpleChanges,
  TemplateRef
} from '@angular/core';
import { DescriptionComponent } from '../../../desy-commons/components/description/description.component';
import { IconComponent } from '../../../desy-commons/components/icon/icon.component';
import { TitleComponent } from '../../../desy-commons/components/title/title.component';
import { ContentData, DescriptionData, IconData, TitleData } from '../../../desy-commons/interfaces';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { MakeHtmlListPipe } from '../../../shared/pipes/make-html-list.pipe';
import { ItemContentBottomComponent } from './item-content-bottom/item-content-bottom.component';
import { ItemContentRightComponent } from './item-content-right/item-content-right.component';
import { ItemItemComponent } from './item-item/item-item.component';

@Component({
  selector: 'desy-item',
  templateUrl: './item.component.html'
})
export class ItemComponent extends AccessibilityComponent implements OnChanges, OnInit {

  @Input() titleItem: TitleData;
  @Input() description: DescriptionData;
  @Input() headingLevel: number;
  @Input() items: string[];
  @Input() content: ContentData;
  @Input() icon: IconData;
  @Input() isDraggable: boolean;
  @Input() isLocked: boolean;
  @Input() caller: TemplateRef<any>;

  @Input() classes: any;
  @Input() @HostBinding('attr.id') id: any;

  @HostBinding('class') hostClasses;

  @DesyContentChild()
  @ContentChildren(TitleComponent) titleComponent: TitleComponent;

  @DesyContentChild()
  @ContentChildren(DescriptionComponent) descriptionComponent: DescriptionComponent;

  @DesyContentChild()
  @ContentChildren(IconComponent) iconComponent: IconComponent;

  @DesyContentChild()
  @ContentChildren(ItemContentBottomComponent) contentBottomComponent: ItemContentBottomComponent;

  @DesyContentChild()
  @ContentChildren(ItemContentRightComponent) contentRightComponent: ItemContentRightComponent;

  @ContentChildren(ItemItemComponent) itemComponents: QueryList<ItemItemComponent>;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.checkClasses();
  }

  getItems() {
    let items = [];
    if (this.itemComponents.length > 0) {
      items = this.itemComponents.toArray();
    } else if (this.items) {
      items = this.items;
    }
    return items;
  }

  handleIconContentEmpty(isEmpty: boolean): void {
    this.changeDetectorRef.detectChanges();
  }

  getTitle(): TitleData {
    return this.titleComponent ? this.titleComponent : (this.titleItem ? this.titleItem : null);
  }

  getIcon(): IconData {
    return this.iconComponent ? this.iconComponent : (this.icon ? this.icon : null)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.checkClasses();
  }

  checkClasses(): void {
    this.hostClasses = new MakeHtmlListPipe().transform([this.classes, 'flex flex-wrap p-base bg-white border border-neutral-base rounded'], null);
  }

}
