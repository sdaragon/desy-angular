import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {ContentData} from '../../../../desy-commons/interfaces';

@Component({
  selector: 'desy-item-content-bottom',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ItemContentBottomComponent extends ContentBaseComponent implements ContentData {
  @Input() classes: string;

}
