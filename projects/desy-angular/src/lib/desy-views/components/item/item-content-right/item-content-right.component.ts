import { Component } from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-item-content-right',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ItemContentRightComponent extends ContentBaseComponent {

}
