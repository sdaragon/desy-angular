import { Component } from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-item-item',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ItemItemComponent extends ContentBaseComponent {

}
