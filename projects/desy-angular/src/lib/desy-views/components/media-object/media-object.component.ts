import { Component, ContentChildren, ElementRef, HostBinding, Input, OnChanges, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { MediaObjectFigureComponent } from './media-object-figure/media-object-figure.component';

@Component({
  selector: 'desy-media-object',
  templateUrl: './media-object.component.html'
})
export class MediaObjectComponent extends AccessibilityComponent implements OnChanges {  

  @Input() figureHtml: string;
  @Input() center: boolean;
  @Input() reverse: boolean;
  @Input() figureClasses: string;
  @Input() contentClasses: string;
  
  @Input() caller: TemplateRef<any>;
  @Input() id: string;
  
  @Input() @HostBinding('class') classes: any = "flex";
  @HostBinding('class.items-center') cfgr: boolean;

  //Accessibility
  @HostBinding('attr.id') idData: any;
  @HostBinding('attr.role') role: string = null;
  @HostBinding('attr.aria-label') ariaLabel: string = null;
  @HostBinding('attr.aria-describedby') ariaDescribedBy: string = null;
  @HostBinding('attr.aria-labelledby') ariaLabelledBy: string = null;
  @HostBinding('attr.aria-hidden') ariaHidden: string = null;
  @HostBinding('attr.aria-disabled') ariaDisabled: string = null;
  @HostBinding('attr.aria-controls') ariaControls: string = null;
  @HostBinding('attr.aria-current') ariaCurrent: string = null;
  @HostBinding('attr.aria-live') ariaLive: string = null;
  @HostBinding('attr.aria-expanded') ariaExpanded: string = null;
  @HostBinding('attr.aria-errormessage') ariaErrorMessage: string = null;
  @HostBinding('attr.aria-haspopup') ariaHasPopup: string = null;
  @HostBinding('attr.aria-modal') ariaModal: string = null;
  @HostBinding('attr.aria-checked') ariaChecked: string = null;
  @HostBinding('attr.aria-pressed') ariaPressed: string = null;
  @HostBinding('attr.aria-readonly') ariaReadonly: string = null;
  @HostBinding('attr.aria-required') ariaRequired: string = null;
  @HostBinding('attr.aria-selected') ariaSelected: string = null;
  @HostBinding('attr.aria-valuemin') ariaValuemin: string = null;
  @HostBinding('attr.aria-valuemax') ariaValuemax: string = null;
  @HostBinding('attr.aria-valuenow') ariaValuenow: string = null;
  @HostBinding('attr.aria-valuetext') ariaValuetext: string = null;
  @HostBinding('attr.aria-orientation') ariaOrientation: string = null;
  @HostBinding('attr.aria-level') ariaLevel: string = null;
  @HostBinding('attr.aria-multiselectable') ariaMultiselectable: string = null;
  @HostBinding('attr.aria-placeholder') ariaPlaceholder: string = null;
  @HostBinding('attr.aria-posinset') ariaPosinset: string = null;
  @HostBinding('attr.aria-setsize') ariaSetsize: string = null;
  @HostBinding('attr.aria-sort') ariaSort: string = null;
  @HostBinding('attr.aria-busy') ariaBusy: string = null;
  @HostBinding('attr.aria-dropeffect') ariaDropeffect: string = null;
  @HostBinding('attr.aria-grabbed') ariaGrabbed: string = null;
  @HostBinding('attr.aria-activedescendant') ariaActivedescendant: string = null;
  @HostBinding('attr.aria-atomic') ariaAtomic: string = null;
  @HostBinding('attr.aria-autocomplete') ariaAutocomplete: string = null;
  @HostBinding('attr.aria-braillelabel') ariaBraillelabel: string = null;
  @HostBinding('attr.aria-brailleroledescription') ariaBrailleroledescription: string = null;
  @HostBinding('attr.aria-colcount') ariaColcount: string = null;
  @HostBinding('attr.aria-colindex') ariaColindex: string = null;
  @HostBinding('attr.aria-colindextext') ariaColindextext: string = null;
  @HostBinding('attr.aria-colspan') ariaColspan: string = null;
  @HostBinding('attr.aria-description') ariaDescription: string = null;
  @HostBinding('attr.aria-details') ariaDetails: string = null;
  @HostBinding('attr.aria-flowto') ariaFlowto: string = null;
  @HostBinding('attr.aria-invalid') ariaInvalid: string = null;
  @HostBinding('attr.aria-keyshortcuts') ariaKeyshortcuts: string = null;
  @HostBinding('attr.aria-owns') ariaOwns: string = null;
  @HostBinding('attr.aria-relevant') ariaRelevant: string = null;
  @HostBinding('attr.aria-roledescription') ariaRoledescription: string = null;
  @HostBinding('attr.aria-rowcount') ariaRowcount: string = null;
  @HostBinding('attr.aria-rowindex') ariaRowindex: string = null;
  @HostBinding('attr.aria-rowindextext') ariaRowindextext: string = null;
  @HostBinding('attr.aria-rowspan') ariaRowspan: string = null;
  @HostBinding('attr.tabindex') tabindex: string = null;
  @HostBinding('attr.title') title: string = null;
  @HostBinding('attr.alt') alt: string = null;
  @HostBinding('attr.lang') lang: string = null;
  @HostBinding('attr.accesskey') accesskey: string = null;
  @HostBinding('attr.autocomplete') autocomplete: string = null;
  @HostBinding('attr.autofocus') autofocus: string = null;
  @HostBinding('attr.contenteditable') contenteditable: string = null;
  @HostBinding('attr.dir') dir: string = null;
  @HostBinding('attr.draggable') draggable: string = null;
  @HostBinding('attr.enterkeyhint') enterkeyhint: string = null;
  @HostBinding('attr.hidden') hidden: boolean = null;
  @HostBinding('attr.inputmode') inputmode: string = null;
  @HostBinding('attr.spellcheck') spellcheck: string = null;
  @HostBinding('attr.translate') translate: string = null;
  @HostBinding('attr.aria-multiline') ariaMultiline: string = null;
  @HostBinding('attr.for') for: string = null;
  @HostBinding('attr.form') form: string = null;
  @HostBinding('attr.headers') headers: string = null;
  @HostBinding('attr.placeholder') placeholder: string = null;
  @HostBinding('attr.readonly') readonly: string = null;
  @HostBinding('attr.required') required: string = null;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;
  
  @DesyContentChild()
  @ContentChildren(MediaObjectFigureComponent) mediaObjectFigureComponent: MediaObjectFigureComponent;

  @ViewChild('mediaFigureContent', { read: ElementRef, static: true }) mediaFigureContent: ElementRef;

  ngOnChanges(changes: SimpleChanges): void {
    this.classes = 'flex' + (this.classes ? ' ' + this.classes : '') + (this.center==true ? ' ' : '');
    this.cfgr = this.center;
    this.idData = this.id ? this.id : null;
  }

}
