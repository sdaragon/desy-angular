import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaObjectFigureComponent } from './media-object-figure.component';

xdescribe('MediaObjectFigureComponent', () => {
  let component: MediaObjectFigureComponent;
  let fixture: ComponentFixture<MediaObjectFigureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediaObjectFigureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaObjectFigureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
