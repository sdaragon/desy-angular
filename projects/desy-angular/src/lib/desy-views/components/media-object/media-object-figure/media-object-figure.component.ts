import { Component } from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-media-object-figure',
  templateUrl: './media-object-figure.component.html'
})
export class MediaObjectFigureComponent extends ContentBaseComponent {

}
