import {Component, ContentChildren, Input, QueryList} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { DescriptionItemData } from '../../interfaces';
import {DescriptionItemComponent} from './description-item/description-item.component';

@Component({
  selector: 'desy-description-list',
  templateUrl: './description-list.component.html'
})
export class DescriptionListComponent extends AccessibilityComponent {

  @Input() items: Array<DescriptionItemData>;
  @ContentChildren(DescriptionItemComponent) itemComponents: QueryList<DescriptionItemComponent>;
  @Input() classes: string;
  @Input() id: string;


  getItems(): DescriptionItemData[]|DescriptionItemComponent[] {
    const items =  (this.itemComponents.length > 0) ? this.itemComponents.toArray() : this.items;
    return items;
  }

}
