import {Component, ContentChildren, Input} from '@angular/core';
import {DescriptionItemData} from '../../../interfaces';
import {TermComponent} from '../term/term.component';
import {DefinitionComponent} from '../definition/definition.component';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';

@Component({
  selector: 'desy-description-item',
  template: ''
})
export class DescriptionItemComponent implements DescriptionItemData {

  @DesyContentChild()
  @ContentChildren(TermComponent) term: TermComponent;

  @DesyContentChild()
  @ContentChildren(DefinitionComponent) definition: DefinitionComponent;

  @Input() classes: string;
  @Input() id: string;

}
