import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {TermDefinitionData} from '../../../interfaces';

@Component({
  selector: 'desy-definition',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class DefinitionComponent extends ContentBaseComponent implements TermDefinitionData {

  @Input() classes: string;
  @Input() id: string;
}
