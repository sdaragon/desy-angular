import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {TermDefinitionData} from '../../../interfaces';

@Component({
  selector: 'desy-term',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class TermComponent extends ContentBaseComponent implements TermDefinitionData {

  @Input() classes: string;
  @Input() id: string;

}
