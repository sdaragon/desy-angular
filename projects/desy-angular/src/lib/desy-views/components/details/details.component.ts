import { Component, ContentChildren, Input, OnInit } from '@angular/core';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';

@Component({
  selector: 'desy-details',
  templateUrl: './details.component.html'
})
export class DetailsComponent extends AccessibilityComponent implements OnInit{

  @Input() summaryText: string;
  @Input() summaryHtml: string;
  @Input() summaryClasses: string;
  @Input() containerClasses: string;
  @Input() id: string;
  @Input() open: boolean;
  @Input() classes: string;

  isHtml: boolean;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;
  
  ngOnInit(): void {
    this.isHtml = Boolean(this.summaryHtml);
  }

}
