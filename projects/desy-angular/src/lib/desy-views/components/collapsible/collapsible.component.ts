import { Component, ContentChildren, Input, OnChanges, OnInit, TemplateRef } from '@angular/core';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { AccessibilityAndContentRequiredComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';

@Component({
  selector: 'desy-collapsible',
  templateUrl: './collapsible.component.html'
})
export class CollapsibleComponent extends AccessibilityAndContentRequiredComponent implements OnInit, OnChanges {

  @Input() headerText: string;
  @Input() headerHtml: string;
  @Input() id: string;
  @Input() open: boolean;
  @Input() classes: string;
  @Input() buttonClasses: string;
  @Input() showClasses: string;
  @Input() hideClasses: string;
  @Input() contentClasses: string;
  
  show: string;
  htmlTemplate: TemplateRef<any>;
  
  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;
  
  isOpen(): void {
    this.open = !this.open;
    this.toggleOpen();
  }

  toggleOpen(): void {
    if (this.open){
      this.show = 'Ocultar';
    } else {
      this.show = 'Mostrar';
    }
  }

  ngOnInit(): void {
    if(this.html){
      this.htmlTemplate = this.html;
    }
    this.toggleOpen();
  }

  ngOnChanges(): void {
    this.toggleOpen();
  }

}
