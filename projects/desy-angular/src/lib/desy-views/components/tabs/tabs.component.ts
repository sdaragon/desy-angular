import {
  AfterViewInit, ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  QueryList,
  SimpleChanges,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { StringUtils } from '../../../shared/utils/string-utils';
import { TabsItemsData } from '../../interfaces';
import { TabItemComponent } from './tab-item/tab-item.component';

@Component({
  selector: 'desy-tabs',
  templateUrl: './tabs.component.html'
})
export class TabsComponent extends AccessibilityComponent implements AfterViewInit, OnChanges {

  @ContentChildren(TabItemComponent) itemComponents: QueryList<TabItemComponent>;
  @ViewChildren('tabSelector') tabSelectors: QueryList<ElementRef>;
  @ViewChildren('tabSelectorContentWrapper') tabSelectorContentWrapper: QueryList<ElementRef>;
  @ViewChild('tabSrOnly') tabSrOnly: ElementRef;


  @Input() id: string;
  @Input() idPrefix: string;
  @Input() headingLevel: number;

  @Input() titleModal: string;
  @Input() tablistAriaLabel: string;
  @Input() items: TabsItemsData[];

  @Input() @HostBinding('class') classes: any;

  //Accessibility
  @HostBinding('attr.id') idData: any;
  @HostBinding('attr.role') role: string = null;
  @HostBinding('attr.aria-label') ariaLabel: string = null;
  @HostBinding('attr.aria-describedby') ariaDescribedBy: string = null;
  @HostBinding('attr.aria-labelledby') ariaLabelledBy: string = null;
  @HostBinding('attr.aria-hidden') ariaHidden: string = null;
  @HostBinding('attr.aria-disabled') ariaDisabled: string = null;
  @HostBinding('attr.aria-controls') ariaControls: string = null;
  @HostBinding('attr.aria-current') ariaCurrent: string = null;
  @HostBinding('attr.aria-live') ariaLive: string = null;
  @HostBinding('attr.aria-expanded') ariaExpanded: string = null;
  @HostBinding('attr.aria-errormessage') ariaErrorMessage: string = null;
  @HostBinding('attr.aria-haspopup') ariaHasPopup: string = null;
  @HostBinding('attr.aria-modal') ariaModal: string = null;
  @HostBinding('attr.aria-checked') ariaChecked: string = null;
  @HostBinding('attr.aria-pressed') ariaPressed: string = null;
  @HostBinding('attr.aria-readonly') ariaReadonly: string = null;
  @HostBinding('attr.aria-required') ariaRequired: string = null;
  @HostBinding('attr.aria-selected') ariaSelected: string = null;
  @HostBinding('attr.aria-valuemin') ariaValuemin: string = null;
  @HostBinding('attr.aria-valuemax') ariaValuemax: string = null;
  @HostBinding('attr.aria-valuenow') ariaValuenow: string = null;
  @HostBinding('attr.aria-valuetext') ariaValuetext: string = null;
  @HostBinding('attr.aria-orientation') ariaOrientation: string = null;
  @HostBinding('attr.aria-level') ariaLevel: string = null;
  @HostBinding('attr.aria-multiselectable') ariaMultiselectable: string = null;
  @HostBinding('attr.aria-placeholder') ariaPlaceholder: string = null;
  @HostBinding('attr.aria-posinset') ariaPosinset: string = null;
  @HostBinding('attr.aria-setsize') ariaSetsize: string = null;
  @HostBinding('attr.aria-sort') ariaSort: string = null;
  @HostBinding('attr.aria-busy') ariaBusy: string = null;
  @HostBinding('attr.aria-dropeffect') ariaDropeffect: string = null;
  @HostBinding('attr.aria-grabbed') ariaGrabbed: string = null;
  @HostBinding('attr.aria-activedescendant') ariaActivedescendant: string = null;
  @HostBinding('attr.aria-atomic') ariaAtomic: string = null;
  @HostBinding('attr.aria-autocomplete') ariaAutocomplete: string = null;
  @HostBinding('attr.aria-braillelabel') ariaBraillelabel: string = null;
  @HostBinding('attr.aria-brailleroledescription') ariaBrailleroledescription: string = null;
  @HostBinding('attr.aria-colcount') ariaColcount: string = null;
  @HostBinding('attr.aria-colindex') ariaColindex: string = null;
  @HostBinding('attr.aria-colindextext') ariaColindextext: string = null;
  @HostBinding('attr.aria-colspan') ariaColspan: string = null;
  @HostBinding('attr.aria-description') ariaDescription: string = null;
  @HostBinding('attr.aria-details') ariaDetails: string = null;
  @HostBinding('attr.aria-flowto') ariaFlowto: string = null;
  @HostBinding('attr.aria-invalid') ariaInvalid: string = null;
  @HostBinding('attr.aria-keyshortcuts') ariaKeyshortcuts: string = null;
  @HostBinding('attr.aria-owns') ariaOwns: string = null;
  @HostBinding('attr.aria-relevant') ariaRelevant: string = null;
  @HostBinding('attr.aria-roledescription') ariaRoledescription: string = null;
  @HostBinding('attr.aria-rowcount') ariaRowcount: string = null;
  @HostBinding('attr.aria-rowindex') ariaRowindex: string = null;
  @HostBinding('attr.aria-rowindextext') ariaRowindextext: string = null;
  @HostBinding('attr.aria-rowspan') ariaRowspan: string = null;
  @HostBinding('attr.tabindex') tabindex: string = null;
  @HostBinding('attr.title') title: string = null;
  @HostBinding('attr.alt') alt: string = null;
  @HostBinding('attr.lang') lang: string = null;
  @HostBinding('attr.accesskey') accesskey: string = null;
  @HostBinding('attr.autocomplete') autocomplete: string = null;
  @HostBinding('attr.autofocus') autofocus: string = null;
  @HostBinding('attr.contenteditable') contenteditable: string = null;
  @HostBinding('attr.dir') dir: string = null;
  @HostBinding('attr.draggable') draggable: string = null;
  @HostBinding('attr.enterkeyhint') enterkeyhint: string = null;
  @HostBinding('attr.hidden') hidden: boolean = null;
  @HostBinding('attr.inputmode') inputmode: string = null;
  @HostBinding('attr.spellcheck') spellcheck: string = null;
  @HostBinding('attr.translate') translate: string = null;
  @HostBinding('attr.aria-multiline') ariaMultiline: string = null;
  @HostBinding('attr.for') for: string = null;
  @HostBinding('attr.form') form: string = null;
  @HostBinding('attr.headers') headers: string = null;
  @HostBinding('attr.placeholder') placeholder: string = null;
  @HostBinding('attr.readonly') readonly: string = null;
  @HostBinding('attr.required') required: string = null;

  currentTab = 0;
  currentPanel;

  constructor(private changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.classes = this.classes ? 'c-tabs ' + this.classes : 'c-tabs';
    this.idData = this.id ? this.id : null;
  }

  ngAfterViewInit(): void {
    const items = this.getItems();
    if (items) {
      let selectedItem = items.findIndex(value => (!value.disabled && value.active));
      this.selectTab(selectedItem !== -1 ? selectedItem : items.findIndex(value => !value.disabled));
    }
    this.changeDetector.detectChanges();
  }

  getItems(): TabsItemsData[] | TabItemComponent[] {
    const items = (this.itemComponents.length > 0) ? this.itemComponents.toArray() : this.items;
    return items;
  }

  getItemPanel(tab: number): TabItemComponent {
    let panel;
    const items = this.getItems();
    const item = items[tab];
    if (item instanceof TabItemComponent) {
      panel = item.panelComponent;
    }
    return panel;
  }


  getIdPrefix(): string {
    if (this.idPrefix) {
      return this.idPrefix;
    }
  }

  getItemId(index: number): string {
    let id;
    const item = this.getItems();
    if (item[index].id) {
      id = item[index].id;
    } else {
      id = this.getIdPrefix() + '-' + index;
    }
    return id;
  }

  isDisabled(item: TabsItemsData): boolean {
    return item.disabled ? true : null;
  }


  selectTab(i: number): void {
    this.currentTab = i;
    const currentItem = this.getItems()[i];
    this.currentPanel = currentItem instanceof TabItemComponent ? currentItem.panelComponent : currentItem.panel;
    this.tabSrOnly.nativeElement.innerHTML = this.tabSelectorContentWrapper.toArray()[this.currentTab].nativeElement.innerHTML;
  }

  focusFirstTab(event: Event): void {
    if (event) {
      event.preventDefault();
    }

    const enabledTabs = this.tabSelectors.filter((item, index) => !item[index].disabled);
    enabledTabs[0].nativeElement.focus();
  }

  focusLastTab(event: Event): void {
    if (event) {
      event.preventDefault();
    }

    const enabledTabs = this.tabSelectors.filter((item, index) => !item[index].disabled);
    enabledTabs[enabledTabs.length - 1].nativeElement.focus();
  }

  focusNextTab(focusedTab: number): void {
    const item = this.getItems();
    let nextTab = (focusedTab + 1) % item.length;
    while (nextTab !== focusedTab) {
      if (!item[nextTab].disabled) {
        this.tabSelectors.find((i, index) => index === nextTab).nativeElement.focus();
        break;
      }
      nextTab = (nextTab + 1) % item.length;
    }
  }

  focusPreviousTab(focusedTab: number): void {
    const item = this.getItems();
    let previousTab = (focusedTab - 1 + item.length) % item.length;
    while (previousTab !== focusedTab) {
      if (!item[previousTab].disabled) {
        this.tabSelectors.find((i, index) => index === previousTab).nativeElement.focus();
        break;
      }
      previousTab = (previousTab - 1 + item.length) % item.length;
    }
  }

  getPanelContent(tab: number): string {
    const item = this.getItems();
    return item[tab].panel && item[tab].panel.text ? `<p>${StringUtils.escapeHtml(item[tab].panel.text)}</p>` : null;
  }
}
