import { Component, ContentChildren, Input } from '@angular/core';
import { ContentComponent } from '../../../../desy-commons/components/content/content.component';
import { AccessibilityComponent } from '../../../../shared/components';
import { DesyContentChild } from '../../../../shared/decorators/desy-content-child.decorator';
import { TabsItemsData, TabsPanelData } from '../../../interfaces';
import { PanelComponent } from '../panel/panel.component';

@Component({
  selector: 'desy-tab-item',
  template: ''
})
export class TabItemComponent extends AccessibilityComponent implements TabsItemsData {

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  @DesyContentChild()
  @ContentChildren(PanelComponent) panelComponent: PanelComponent;

  @Input() classes: string;
  @Input() disabled: boolean;
  @Input() active: boolean;
  @Input() id: string;
  panel?: TabsPanelData;

}
