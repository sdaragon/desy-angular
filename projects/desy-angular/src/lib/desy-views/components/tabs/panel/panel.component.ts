import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {TabsPanelData} from '../../../interfaces';

@Component({
  selector: 'desy-panel',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class PanelComponent extends ContentBaseComponent implements TabsPanelData {

  @Input() classes: string;
  @Input() id: string;

}