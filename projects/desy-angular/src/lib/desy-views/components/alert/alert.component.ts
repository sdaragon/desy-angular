import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  TemplateRef, Type,
  ViewChild, ViewContainerRef
} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { FocusUtils } from '../../../shared/utils/focus-utils';

type AlertCreationCallback = ((dialog: AlertComponent, caller: ComponentRef<any>) => void);

@Component({
  selector: 'desy-alert',
  templateUrl: './alert.component.html'
})
export class AlertComponent extends AccessibilityComponent implements OnDestroy, AfterViewInit {

  @ViewChild('alertWrapper') alertWrapper: ElementRef;
  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

  @Input() id: string;
  @Input() classes: string;
  @Input() caller: TemplateRef<any>;
  @Input() focusFirst: boolean;

  @Input() set active(value: boolean) {
    setTimeout(() => {
      this._active = value;
      this.cdRef.detectChanges();
      this.handleActiveState();
    });
  }
  get active(): boolean {
    return this._active;
  }
  @Output() activeChange = new EventEmitter<boolean>();

  private _active: boolean;
  public callerContext: any;
  public callerType: Type<any>;
  public onCallerCreationCallback: AlertCreationCallback;

  private lastActiveState: boolean = undefined;
  private hasViewinit = false;
  private createdCallerFromType = false;

  constructor(private cdRef: ChangeDetectorRef, private factoryResolver: ComponentFactoryResolver) {
    super();
  }

  ngOnDestroy(): void {
    this.active = false;
  }

  ngAfterViewInit(): void {

    this.hasViewinit = true;
    this.handleActiveState();
  }

  dismiss(): void {
    this.activeChange.emit(false);
  }

  handleActiveState(): void {
    if (this._active && this.callerType && !this.createdCallerFromType) {
      const factory = this.factoryResolver.resolveComponentFactory(this.callerType);
      const component = this.container.createComponent(factory);
      if (this.onCallerCreationCallback) {
        this.onCallerCreationCallback(this, component);
      }
      this.createdCallerFromType = true;
    }

    if (this._active !== this.lastActiveState && this.hasViewinit) {
      this.lastActiveState = this._active;

      if (this._active && this.focusFirst) {
        this.cdRef.detectChanges();
        const focusElem = FocusUtils.getFirstFocusableElement(this.alertWrapper.nativeElement as HTMLElement);
        if (focusElem) {
          focusElem.focus();
        }
      }
    }
  }
}
