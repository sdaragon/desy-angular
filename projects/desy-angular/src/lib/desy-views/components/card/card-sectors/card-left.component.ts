import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {ContentData} from '../../../../desy-commons/interfaces';

@Component({
  selector: 'desy-card-left',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class CardLeftComponent extends ContentBaseComponent implements ContentData {
  @Input() classes: string;
  @Input() backgroundColor: string;
  @Input() backgroundImageUrl: string;
}
