import { Component, Input } from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-card-right',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class CardRightComponent extends ContentBaseComponent {
  @Input() classes: string;
  @Input() backgroundColor: string;
  @Input() backgroundImageUrl: string;
}
