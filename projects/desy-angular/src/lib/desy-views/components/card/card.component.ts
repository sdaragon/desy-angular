import {
  Component,
  ContentChild,
  Input,
} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { CardSuperComponent } from './card-sectors/card-super.component';
import { CardLeftComponent } from './card-sectors/card-left.component';
import { CardRightComponent } from './card-sectors/card-right.component';
import { CardSubComponent } from './card-sectors/card-sub.component';


@Component({
  selector: 'desy-card',
  templateUrl: './card.component.html'
})
export class CardComponent extends AccessibilityComponent {

  @ContentChild(CardSuperComponent) cardSuperComponent: CardSuperComponent;
  @ContentChild(CardLeftComponent) cardLeftComponent: CardLeftComponent;
  @ContentChild(CardRightComponent) cardRightComponent: CardRightComponent;
  @ContentChild(CardSubComponent) cardSubComponent: CardSubComponent;

  @Input() classes: string;
  @Input() containerClasses: string;

  constructor() {
    super();
  }

  public getStyle(cardSector: CardLeftComponent | CardRightComponent | CardSubComponent | CardSuperComponent) {
    let style = {}
    if (cardSector.backgroundColor) {
      style = { ...style, ...{ 'background-color': cardSector.backgroundColor } }
    }
    if (cardSector.backgroundImageUrl) {
      style = { ...style, ...{ 'background-image': 'url(' + cardSector.backgroundImageUrl + ')' } }
    }
    return style
  }
  
}
