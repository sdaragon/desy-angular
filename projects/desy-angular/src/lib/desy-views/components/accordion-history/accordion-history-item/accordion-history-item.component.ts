import { Component, ContentChildren, Input } from '@angular/core';
import { DesyContentChild } from '../../../../shared/decorators/desy-content-child.decorator';
import { AccordionItemComponent } from '../../accordion/accordion-item/accordion-item.component';
import { AccordionItemHideButtonComponent } from '../accordion-item-hide-button/accordion-item-hide-button.component';
import { AccordionItemShowButtonComponent } from '../accordion-item-show-button/accordion-item-show-button.component';

@Component({
  selector: 'desy-accordion-history-item',
  template: '',
})
export class AccordionHistoryItemComponent extends AccordionItemComponent {

  @Input() status: 'current' | 'pending' | 'muted' | 'currentmuted' | any;
  @Input() disabled = false;

  @DesyContentChild()
  @ContentChildren(AccordionItemShowButtonComponent) showButton: AccordionItemShowButtonComponent;
  
  @DesyContentChild()
  @ContentChildren(AccordionItemHideButtonComponent) hideButton: AccordionItemHideButtonComponent;
}
