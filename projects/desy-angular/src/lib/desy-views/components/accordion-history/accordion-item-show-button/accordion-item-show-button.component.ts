import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';

@Component({
  selector: 'desy-accordion-item-show-button',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class AccordionItemShowButtonComponent extends ContentBaseComponent  {

  @Input() classes: string;
}
