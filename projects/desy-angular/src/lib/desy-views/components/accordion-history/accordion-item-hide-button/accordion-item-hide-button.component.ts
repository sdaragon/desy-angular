import { Component, Input } from '@angular/core';
import { ContentBaseComponent } from '../../../../shared/components';

@Component({
  selector: 'desy-accordion-item-hide-button',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class AccordionItemHideButtonComponent extends ContentBaseComponent  {

  @Input() classes: string;
}
