import { Component, ContentChildren, QueryList } from '@angular/core';
import { AccordionComponent } from '../accordion/accordion.component';
import { AccordionHistoryItemComponent } from './accordion-history-item/accordion-history-item.component';

@Component({
  selector: 'desy-accordion-history',
  templateUrl: './accordion-history.component.html'
})
export class AccordionHistoryComponent extends AccordionComponent {

  // Sobreescribe el tipo del listado de items
  @ContentChildren(AccordionHistoryItemComponent) declare itemComponentList: QueryList<AccordionHistoryItemComponent>;
}
