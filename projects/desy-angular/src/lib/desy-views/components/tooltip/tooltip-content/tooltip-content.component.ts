import {Component} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-tooltip-content',
  templateUrl: './tooltip-content.component.html'
})
export class TooltipContentComponent extends ContentBaseComponent {
}
