import {
  AfterContentInit, ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { inlinePositioning } from 'tippy.js';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { IconComponent } from '../../../desy-commons/components/icon/icon.component';
import { IconData } from '../../../desy-commons/interfaces';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { TooltipContentComponent } from './tooltip-content/tooltip-content.component';

@Component({
  selector: 'desy-tooltip',
  templateUrl: './tooltip.component.html'
})
export class TooltipComponent extends AccessibilityComponent implements AfterContentInit, OnChanges {

  static readonly KEY_CODE_ESC = 'Escape';

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  @DesyContentChild()
  @ContentChildren(IconComponent) iconComponent: IconComponent;

  @DesyContentChild()
  @ContentChildren(TooltipContentComponent) tooltipContentComponent: TooltipContentComponent;

  @ViewChild('tooltipContent', { read: ElementRef, static: true }) tooltipContent: ElementRef;

  @Input() id: string;
  @Input() text: string;
  @Input() html: string;
  @Input() complex: boolean;
  @Input() classesTooltip: string;
  @Input() icon: IconData;
  @Input() caller: TemplateRef<any>;
  @Input() @HostBinding('class') classes: any;
  // @HostBinding('data-module') dataModule = "c-tabs";

  //Accessibility
  @HostBinding('attr.id') idData: any;
  @HostBinding('attr.role') role: string = null;
  @HostBinding('attr.aria-label') ariaLabel: string = null;
  @HostBinding('attr.aria-describedby') ariaDescribedBy: string = null;
  @HostBinding('attr.aria-labelledby') ariaLabelledBy: string = null;
  @HostBinding('attr.aria-hidden') ariaHidden: string = null;
  @HostBinding('attr.aria-disabled') ariaDisabled: string = null;
  @HostBinding('attr.aria-controls') ariaControls: string = null;
  @HostBinding('attr.aria-current') ariaCurrent: string = null;
  @HostBinding('attr.aria-live') ariaLive: string = null;
  @HostBinding('attr.aria-expanded') ariaExpanded: string = null;
  @HostBinding('attr.aria-errormessage') ariaErrorMessage: string = null;
  @HostBinding('attr.aria-haspopup') ariaHasPopup: string = null;
  @HostBinding('attr.aria-modal') ariaModal: string = null;
  @HostBinding('attr.aria-checked') ariaChecked: string = null;
  @HostBinding('attr.aria-pressed') ariaPressed: string = null;
  @HostBinding('attr.aria-readonly') ariaReadonly: string = null;
  @HostBinding('attr.aria-required') ariaRequired: string = null;
  @HostBinding('attr.aria-selected') ariaSelected: string = null;
  @HostBinding('attr.aria-valuemin') ariaValuemin: string = null;
  @HostBinding('attr.aria-valuemax') ariaValuemax: string = null;
  @HostBinding('attr.aria-valuenow') ariaValuenow: string = null;
  @HostBinding('attr.aria-valuetext') ariaValuetext: string = null;
  @HostBinding('attr.aria-orientation') ariaOrientation: string = null;
  @HostBinding('attr.aria-level') ariaLevel: string = null;
  @HostBinding('attr.aria-multiselectable') ariaMultiselectable: string = null;
  @HostBinding('attr.aria-placeholder') ariaPlaceholder: string = null;
  @HostBinding('attr.aria-posinset') ariaPosinset: string = null;
  @HostBinding('attr.aria-setsize') ariaSetsize: string = null;
  @HostBinding('attr.aria-sort') ariaSort: string = null;
  @HostBinding('attr.aria-busy') ariaBusy: string = null;
  @HostBinding('attr.aria-dropeffect') ariaDropeffect: string = null;
  @HostBinding('attr.aria-grabbed') ariaGrabbed: string = null;
  @HostBinding('attr.aria-activedescendant') ariaActivedescendant: string = null;
  @HostBinding('attr.aria-atomic') ariaAtomic: string = null;
  @HostBinding('attr.aria-autocomplete') ariaAutocomplete: string = null;
  @HostBinding('attr.aria-braillelabel') ariaBraillelabel: string = null;
  @HostBinding('attr.aria-brailleroledescription') ariaBrailleroledescription: string = null;
  @HostBinding('attr.aria-colcount') ariaColcount: string = null;
  @HostBinding('attr.aria-colindex') ariaColindex: string = null;
  @HostBinding('attr.aria-colindextext') ariaColindextext: string = null;
  @HostBinding('attr.aria-colspan') ariaColspan: string = null;
  @HostBinding('attr.aria-description') ariaDescription: string = null;
  @HostBinding('attr.aria-details') ariaDetails: string = null;
  @HostBinding('attr.aria-flowto') ariaFlowto: string = null;
  @HostBinding('attr.aria-invalid') ariaInvalid: string = null;
  @HostBinding('attr.aria-keyshortcuts') ariaKeyshortcuts: string = null;
  @HostBinding('attr.aria-owns') ariaOwns: string = null;
  @HostBinding('attr.aria-relevant') ariaRelevant: string = null;
  @HostBinding('attr.aria-roledescription') ariaRoledescription: string = null;
  @HostBinding('attr.aria-rowcount') ariaRowcount: string = null;
  @HostBinding('attr.aria-rowindex') ariaRowindex: string = null;
  @HostBinding('attr.aria-rowindextext') ariaRowindextext: string = null;
  @HostBinding('attr.aria-rowspan') ariaRowspan: string = null;
  @HostBinding('attr.tabindex') tabindex: string = null;
  @HostBinding('attr.title') title: string = null;
  @HostBinding('attr.alt') alt: string = null;
  @HostBinding('attr.lang') lang: string = null;
  @HostBinding('attr.accesskey') accesskey: string = null;
  @HostBinding('attr.autocomplete') autocomplete: string = null;
  @HostBinding('attr.autofocus') autofocus: string = null;
  @HostBinding('attr.contenteditable') contenteditable: string = null;
  @HostBinding('attr.dir') dir: string = null;
  @HostBinding('attr.draggable') draggable: string = null;
  @HostBinding('attr.enterkeyhint') enterkeyhint: string = null;
  @HostBinding('attr.hidden') hidden: boolean = null;
  @HostBinding('attr.inputmode') inputmode: string = null;
  @HostBinding('attr.spellcheck') spellcheck: string = null;
  @HostBinding('attr.translate') translate: string = null;
  @HostBinding('attr.aria-multiline') ariaMultiline: string = null;
  @HostBinding('attr.for') for: string = null;
  @HostBinding('attr.form') form: string = null;
  @HostBinding('attr.headers') headers: string = null;
  @HostBinding('attr.placeholder') placeholder: string = null;
  @HostBinding('attr.readonly') readonly: string = null;
  @HostBinding('attr.required') required: string = null;

  tippyProperties: any;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.classes = this.classes ? this.classes : null;
    this.idData = this.id ? this.id : null;
  }

  ngAfterContentInit(): void {
    const contentTooltip = this.tooltipContent.nativeElement as HTMLElement;
    this.tippyProperties = {
      placement: 'top',
      inlinePositioning: true,
      content: contentTooltip,
      allowHTML: true, // Make sure you are sanitizing any user data if rendering HTML to prevent XSS attacks.
      trigger: 'mouseenter focus',
      hideOnClick: false,
      theme: '',
      plugins: [{
        name: 'hideOnEsc',
        defaultValue: true,
        fn({ hide }): any {

          function onKeyDown(event: KeyboardEvent): void {
            if (event.key === TooltipComponent.KEY_CODE_ESC) {
              hide();
            }
          }

          return {
            onShow(): void {
              document.addEventListener('keydown', onKeyDown);
            },
            onHide(): void {
              document.removeEventListener('keydown', onKeyDown);
            },
          };
        },
      }, inlinePositioning],
      role: 'tooltip',
      aria: {
        content: this.complex ? 'describedby' : 'labelledby',
      },
    };
  }

  getIdButton(): string {
    return this.id + '-button';
  }

  getIdTooltip(): string {
    return this.id + '-tooltip';
  }

  getMainContent(): string {
    let content;
    if (this.html) {
      content = this.html;
    } else if (this.text) {
      content = `<p>${this.text}</p>`;
    }
    return content;
  }

  getIconContent(icon: IconData): string {
    let iconContent;
    if (icon.html) {
      iconContent = this.icon.html;
    } else if (icon.type === 'info') {
      iconContent = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140" width="1em" height="1em" class="w-4 h-4 text-primary-base" role="img" aria-label="Información"><path d="M70 0a70 70 0 1070 70A70.08 70.08 0 0070 0zm7.5 105a7.5 7.5 0 01-15 0V70a7.5 7.5 0 0115 0zM70 50a10 10 0 1110-10 10 10 0 01-10 10z" fill="currentColor"/></svg>';
    } else if (icon.type === 'alert') {
      iconContent = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140" width="1em" height="1em" class="w-4 h-4 text-alert-base" role="img" aria-label="Alerta"><path d="M138.42 118.29l-55-110a15 15 0 00-26.84 0l-55 110A15 15 0 0015 140h110a15 15 0 0013.42-21.71zM62.5 50a7.5 7.5 0 0115 0v30a7.5 7.5 0 01-15 0zm7.5 70a10 10 0 1110-10 10 10 0 01-10 10z" fill="currentColor"/></svg>';
    } else {
      iconContent = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140" width="1em" height="1em" class="w-4 h-4 text-primary-base" role="img" aria-label="Ayuda"><path d="M70 0a70 70 0 1070 70A70.08 70.08 0 0070 0zm0 117.51a10 10 0 1110-10 10 10 0 01-10 10zm9.17-39.08a2.5 2.5 0 00-1.67 2.36v1.71a7.5 7.5 0 01-15 0v-10A7.5 7.5 0 0170 65a12.5 12.5 0 10-12.5-12.5 7.5 7.5 0 01-15 0 27.5 27.5 0 1136.67 25.93z" fill="currentColor"/></svg>';
    }
    return iconContent;
  }
}
