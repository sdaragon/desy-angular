import {
  ComponentFactoryResolver, ComponentRef,
  Injectable,
  TemplateRef, Type
} from '@angular/core';
import { AlertOptions } from '../interfaces/alert-options';
import { OpenAlertResult } from '../interfaces/open-alert-result';
import { AlertComponent } from '../components/alert/alert.component';
import { NotificationComponent } from '../../desy-nav/components/notification/notification.component';
import { NotificationOptions } from '../../desy-nav/interfaces/notification-options';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private factoryResolver: ComponentFactoryResolver) { }

  public openAlert(caller: TemplateRef<any>|NotificationOptions, alertOptions: AlertOptions): Promise<OpenAlertResult> {
    return new Promise<OpenAlertResult>((resolve, reject) => {

      if (!caller) {
        reject('caller is required');
      }

      if (!alertOptions) {
        reject('alertOptions is required');
      }

      const alert = this.createAlert(alertOptions);
      const result: OpenAlertResult = {
        alert
      };


      if (caller instanceof TemplateRef) {
        alert.instance.caller = caller;
        resolve(result);
      } else {
        alert.instance.callerType = NotificationComponent;
        alert.instance.onCallerCreationCallback = (d, c) => {
          this.initNotification(d, c, caller);
          result.component = c;
          resolve(result);
        };
      }
    });
  }

  public closeAlert(alert: ComponentRef<AlertComponent>|AlertComponent): void {
    if (alert) {
      if (alert instanceof ComponentRef) {
        alert.instance.dismiss();
      } else {
        alert.dismiss();
      }
    }
  }

  public onCloseAlert(alert: ComponentRef<AlertComponent>|AlertComponent): Promise<void> {
    return new Promise<void>((resolve => {
      let a;
      if (alert instanceof ComponentRef) {
        a = alert.instance;
      } else {
        a = alert;
      }

      const subscription = a.activeChange.subscribe(() => {
        resolve();
        subscription.unsubscribe();
      });
    }));
  }


  private createAlert(alertOptions: AlertOptions): ComponentRef<AlertComponent> {
    const factory = this.factoryResolver.resolveComponentFactory(AlertComponent);
    const alert = alertOptions.place.createComponent<AlertComponent>(factory);

    alert.instance.active = true;
    if (alertOptions) {
      Object.assign(alert.instance, alertOptions);
    }

    this.onCloseAlert(alert).then(() => alert.destroy());

    return alert;
  }

  private initNotification(alert: AlertComponent, notificationRef: ComponentRef<NotificationComponent>,
                           options: NotificationOptions): void {
    Object.assign(notificationRef.instance, options);
    if (options.isDismissible) {
      const subscription = notificationRef.instance.isOpenChange.subscribe(() => {
        this.closeAlert(alert);
        subscription.unsubscribe();
      });
    }
  }
}
