import { A11yModule } from '@angular/cdk/a11y';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DesyFormsModule } from '../desy-forms/desy-forms.module';
import { SharedModule } from '../shared/shared.module';

import { TableAdvancedFooterCellComponent } from './components/table-advanced/components/table-advanced-footer-cell.component';
import { TableAdvancedFooterComponent } from './components/table-advanced/components/table-advanced-footer.component';
import { TableAdvancedHeaderCellComponent } from './components/table-advanced/components/table-advanced-header-cell.component';
import { TableAdvancedHeaderComponent } from './components/table-advanced/components/table-advanced-header.component';
import { TableAdvancedRowCellComponent } from './components/table-advanced/components/table-advanced-row-cell.component';
import { TableAdvancedRowComponent } from './components/table-advanced/components/table-advanced-row.component';
import { TableAdvancedSelectComponent } from './components/table-advanced/components/table-advanced-select.component';
import { TableAdvancedComponent } from './components/table-advanced/table-advanced.component';
import { TableCaptionComponent } from './components/table/components/table-caption.component';
import { TableCellComponent } from './components/table/components/table-cell.component';
import { TableFooterComponent } from './components/table/components/table-footer.component';
import { TableHeaderComponent } from './components/table/components/table-header.component';
import { TableRowComponent } from './components/table/components/table-row.component';
import { TableComponent } from './components/table/table.component';
import { CellDirective } from './directives/cell.directive';
import { FocusClickedCellDirective } from './directives/focus-clicked-cell.directive';
import { RowDirective } from './directives/row.directive';


@NgModule({
  declarations: [
    TableComponent,
    TableCaptionComponent,
    TableCellComponent,
    TableHeaderComponent,
    TableFooterComponent,
    TableRowComponent,
    TableAdvancedComponent,
    TableAdvancedHeaderComponent,
    TableAdvancedHeaderCellComponent,
    TableAdvancedRowComponent,
    TableAdvancedRowCellComponent,
    TableAdvancedSelectComponent,
    TableAdvancedFooterComponent,
    TableAdvancedFooterCellComponent,

    FocusClickedCellDirective,
    CellDirective,
    RowDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    A11yModule,
    SharedModule,
    DesyFormsModule
  ],
  exports: [
    TableComponent,
    TableCaptionComponent,
    TableCellComponent,
    TableHeaderComponent,
    TableFooterComponent,
    TableRowComponent,
    TableAdvancedComponent,
    TableAdvancedHeaderComponent,
    TableAdvancedHeaderCellComponent,
    TableAdvancedRowComponent,
    TableAdvancedRowCellComponent,
    TableAdvancedSelectComponent,
    TableAdvancedFooterComponent,
    TableAdvancedFooterCellComponent,
  ]
})
export class DesyTablesModule { }
