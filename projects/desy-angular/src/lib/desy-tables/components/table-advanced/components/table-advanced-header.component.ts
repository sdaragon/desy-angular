import {Component, ContentChildren, QueryList} from '@angular/core';
import {TableAdvancedHeaderCellComponent} from './table-advanced-header-cell.component';

@Component({
  selector: 'desy-table-advanced-header',
  template: ''
})
export class TableAdvancedHeaderComponent {
  @ContentChildren(TableAdvancedHeaderCellComponent) cells: QueryList<TableAdvancedHeaderCellComponent>;
}
