import { OptionComponent } from './../../../../desy-forms/components/select/option/option.component';
import { SelectComponent } from './../../../../desy-forms/components/select/select.component';
import { Component, ContentChildren, EventEmitter, Input, Output, QueryList, ViewChild } from "@angular/core";

@Component({
    selector: 'desy-table-advanced-select',
    template: ''
})
export class TableAdvancedSelectComponent extends SelectComponent{
    isSelect = true;

    @Input() declare value: any;
    @Output() valueChange = new EventEmitter<any>();

    @ContentChildren(OptionComponent) selectItemComponentList: QueryList<OptionComponent>;
}