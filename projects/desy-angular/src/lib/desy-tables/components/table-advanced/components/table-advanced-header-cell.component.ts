import { ContentComponent } from './../../../../desy-commons/components/content/content.component';
import { TableAdvancedSelectComponent } from './table-advanced-select.component';
import { DesyContentChild } from '../../../../shared/decorators/desy-content-child.decorator';
import {Component, ContentChildren, Input} from '@angular/core';
import {HeadCellModelData, OrderBy} from '../../../interfaces';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-table-advanced-header-cell',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class TableAdvancedHeaderCellComponent extends ContentBaseComponent implements HeadCellModelData {
  @Input() classes: string;
  @Input() id: string;
  @Input() colspan: number;
  @Input() rowspan: number;
  @Input() orderBy: OrderBy; // Possible values `none`, `asc`, `desc`.
  @Input() hasFilter: boolean;
  @Input() filterClasses: string;
  @Input() hasSelect?: boolean;
  @Input() identifier?: string;
  valueFilter?: string;
  activeFilter?: boolean;
  valueSelect?: string;
  activeSelect?: boolean;

  @DesyContentChild()
  @ContentChildren(TableAdvancedSelectComponent) selectComponent?: TableAdvancedSelectComponent;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent?: ContentComponent;
}
