import { Component, ContentChildren, EventEmitter, Input, Output, QueryList } from '@angular/core';
import { TableAdvancedFooterCellComponent } from './table-advanced-footer-cell.component';

@Component({
  selector: 'desy-table-advanced-footer',
  template: ''
})
export class TableAdvancedFooterComponent {

  @ContentChildren(TableAdvancedFooterCellComponent) cellsList: QueryList<TableAdvancedFooterCellComponent>;

  @Input() id: string;
  @Input() checked: boolean;
  @Output() checkedChange: EventEmitter<boolean> = new EventEmitter();
}
