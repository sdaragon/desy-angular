import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';
import {CellData} from '../../../interfaces';

@Component({
  selector: 'desy-table-advanced-row-cell',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class TableAdvancedRowCellComponent extends ContentBaseComponent implements CellData {
  @Input() classes?: string;
  @Input() id?: string;
  @Input() colspan?: number;
  @Input() rowspan?: number;
}
