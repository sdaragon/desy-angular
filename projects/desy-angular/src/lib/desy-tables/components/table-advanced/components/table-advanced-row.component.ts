import {Component, ContentChildren, EventEmitter, Input, Output, QueryList} from '@angular/core';
import {TableAdvancedRowCellComponent} from './table-advanced-row-cell.component';

@Component({
  selector: 'desy-table-advanced-row',
  template: ''
})
export class TableAdvancedRowComponent {

  @ContentChildren(TableAdvancedRowCellComponent) cellsList: QueryList<TableAdvancedRowCellComponent>;

  @Input() id: string;
  @Input() checked: boolean;
  @Output() checkedChange: EventEmitter<boolean> = new EventEmitter();
}
