import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { SelectItemData } from '../../../desy-forms/interfaces/select-item-data';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { SearchUtils } from '../../../shared/utils/search-utils';
import { CellDirective } from '../../directives/cell.directive';
import { RowDirective } from '../../directives/row.directive';
import { HeadCellModelData, OrderBy, RowData, WrapperData } from '../../interfaces';
import { RecalculateTableParams } from '../../interfaces/recalculate-table-params';
import { TableCaptionComponent } from '../table/components/table-caption.component';
import { ContentComponent } from './../../../desy-commons/components/content/content.component';
import { TableAdvancedFooterComponent } from './components/table-advanced-footer.component';
import { TableAdvancedHeaderCellComponent } from './components/table-advanced-header-cell.component';
import { TableAdvancedHeaderComponent } from './components/table-advanced-header.component';
import { TableAdvancedRowComponent } from './components/table-advanced-row.component';
import { TableAdvancedSelectComponent } from './components/table-advanced-select.component';

@Component({
  selector: 'desy-table-advanced',
  templateUrl: './table-advanced.component.html'
})
export class TableAdvancedComponent extends AccessibilityComponent implements AfterViewInit, AfterContentInit, OnDestroy {

  _rows: RowData[]; // original data
  @Input()
  set rows(rows: RowData[]) {
    this._rows = rows;
    this.recalculateVisibleElements()
  }
  get rows(): RowData[] {
    return this._rows
  }
  @Input() head: HeadCellModelData[];
  @Input() foot: RowData[];

  @Input() caption: string;
  @Input() captionClasses: string;
  @Input() firstCellIsHeader: boolean;
  @Input() hasCheckboxes: boolean;
  @Input() idPrefix: string;
  @Input() classes: string;
  @Input() checkboxClasses: string;
  @Input() id: string;
  @Input() wrapper: WrapperData;


  @DesyContentChild()
  @ContentChildren(TableCaptionComponent) captionComponent: TableCaptionComponent;

  @DesyContentChild()
  @ContentChildren(TableAdvancedHeaderComponent) headerComponent: TableAdvancedHeaderComponent;

  @ContentChildren(TableAdvancedFooterComponent) footerComponent: QueryList<TableAdvancedFooterComponent>;

  @ContentChildren(TableAdvancedRowComponent) rowComponentList: QueryList<TableAdvancedRowComponent>;
  @ViewChildren(RowDirective) rowData: QueryList<RowDirective>;

  @Output() rowsChange = new EventEmitter();
  @Output() rowsChecked = new EventEmitter();
  @Output() recalculateTable = new EventEmitter<RecalculateTableParams>();

  visibleRows: RowData[] | TableAdvancedRowComponent[];
  orderByType = OrderBy;
  isReset: boolean;

  constructor(private el: ElementRef, private changeDetector: ChangeDetectorRef) {
    super();
  }
  private rowsSubscription: Subscription;

  ngAfterContentInit(): void {
    this.rowsSubscription = this.rowComponentList.changes.subscribe(() => this.recalculateVisibleElements())
  }

  ngOnDestroy() {
    if (this.rowsSubscription) {
      this.rowsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    const elemntsFocus = this.el.nativeElement.querySelector('[tabindex]');
    if (elemntsFocus) {
      elemntsFocus.setAttribute('tabindex', '0');
    }
    this.recalculateVisibleElements();
  }

  /**
   * Refresca las filas visibles tras realizarse una búsqueda o una ordenación
   */
  recalculateVisibleElements(): void {
    const recalculateParams = this.buildRecalculateParams();

    // Si se captura el evento recalculateTable se deja al usuario la responsabilidad de filtrar y ordenar los elementos.
    if (this.recalculateTable.observers.length > 0) {
      this.recalculateTable.emit(recalculateParams);
      this.changeDetector.markForCheck();
    } else {

      // Se deben mostrar todas las filas temporalmente para poder aplicar el filtro y la ordenación sobre el contenido insertado
      this.visibleRows = this.getRows();
      this.changeDetector.detectChanges();

      // Se aplica el filtro y la ordenación
      let rowData = this.filterRows(recalculateParams, this.rowData.toArray());
      rowData = this.sortRows(recalculateParams, rowData);
      this.visibleRows = rowData.map(r => r.desyRow) as RowData[] | TableAdvancedRowComponent[];
      this.changeDetector.detectChanges();
    }

  }

  /**
   * Construye los parámetros necesarios para recalcular la tabla, indicando los filtros y la ordenación
   */
  private buildRecalculateParams(): RecalculateTableParams {
    const params = {
      filters: [],
      sort: null
    };

    const head = this.getHeaderCells() as (HeadCellModelData | TableAdvancedHeaderCellComponent)[];
    if (head) {
      head.forEach((headerCell, headerCellIndex) => {
        if (headerCell.hasFilter && headerCell.activeFilter) {
          params.filters.push({ columnIndex: headerCellIndex, filterText: headerCell.valueFilter, identifier: headerCell.identifier });
        }

        if (headerCell.hasSelect && headerCell.activeSelect) {
          params.filters.push({ columnIndex: headerCellIndex, filterText: headerCell.valueSelect, identifier: headerCell.identifier });
        }
      });

      const orderByColumnIndex = head.findIndex(cell => cell.orderBy && cell.orderBy !== OrderBy.none);
      params.sort = (orderByColumnIndex >= 0) ? { columnIndex: orderByColumnIndex, order: head[orderByColumnIndex].orderBy } : null;
    }
    return params;
  }

  /**
   * funciones de recoger ids, y caracteristicas
   */

  hasFilters(): boolean {
    const head = this.getHeaderCells();
    return head ? head.findIndex(cell => (cell.hasFilter) || (cell.hasSelect)) >= 0 : false;
  }

  getHeaderCells(): HeadCellModelData[] | TableAdvancedHeaderCellComponent[] {
    return this.headerComponent && this.headerComponent.cells.length > 0 ? this.headerComponent.cells.toArray() : this.head;
  }

  getFooterCells(): RowData[] | TableAdvancedFooterComponent[] {
    return this.footerComponent && this.footerComponent.length > 0 ? this.footerComponent.toArray() : this.foot;
  }

  getRows() {
    let rows = [];
    if (this.rowComponentList?.length > 0) {
      rows = this.rowComponentList.toArray();
    } else if (this.rows) {
      rows = this.rows;
    }
    return rows;
  }

  /**
   * Si se sobreescribe  el comportamiento por defecto de filtrado y ordenación, se utilizarán las indicadas desde fuera del
   * componente (ya sea por parámetro o por contenido).
   * Si no se sobreescribe, devuelve las filas calculadas.
   */
  getVisibleRows() {
    return this.recalculateTable.observers.length > 0 ? this.getRows() : this.visibleRows;
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : 'id-table-row';
  }

  getIdHeader(index: number): string {
    return this.getIdPrefix() + '-header-' + index;
  }

  getIdRowCheckbox(index: number): string {
    return this.getIdPrefix() + '-checkbox-' + index;
  }

  getIdHeaderFilter(index: number): string {
    return this.getIdPrefix() + '-header-filter-' + index;
  }

  getIdHeaderSelect(index: number): string {
    return this.getIdPrefix() + '-header-select-' + index;
  }

  getCaptionClass(): string {
    return this.captionComponent ? this.captionComponent.classes : this.captionClasses;
  }

  getStringOrderBy(orderBy: OrderBy): string {
    if (orderBy === OrderBy.asc) {
      return 'ascending';
    } else if (orderBy === OrderBy.desc) {
      return 'descending';
    } else if (orderBy === OrderBy.none) {
      return 'none';
    }
  }

  getSelectForItem(item: any): TableAdvancedSelectComponent {
    return item.selectComponent ? item.selectComponent : null;
  }

  getItemSelectOptions(item: TableAdvancedSelectComponent): SelectItemData[] {
    return item.selectItemComponentList && item.selectItemComponentList.length > 0 ?
      item.selectItemComponentList.toArray() : item.items;
  }

  getContentComponent(item: TableAdvancedHeaderCellComponent): ContentComponent {
    return item.contentComponent ? item.contentComponent : null;
  }


  /**
   * checks
   */
  handleCheckboxChange(row: RowData | TableAdvancedRowComponent): void {
    if (row instanceof TableAdvancedRowComponent) {
      row.checkedChange.emit(row.checked);
    }
    this.notifyCheckedChange();
  }

  /**
   * Permite desmarcar todas las filas
   */
  private uncheckAll(): void {
    this.getRows().forEach(r => {
      const mustEmit = r.checked && r instanceof TableAdvancedRowComponent;
      r.checked = false;
      if (mustEmit) {
        (r as TableAdvancedRowComponent).checkedChange.emit(r.checked);
      }
    });
    this.notifyCheckedChange();
  }

  /**
   * Notifica cambios en las filas
   */
  private notifyCheckedChange(): void {
    if (this.rowComponentList.length === 0) {
      this.rowsChange.emit(this.rows);
    }

    const rowChecks: any = {};
    this.getRows().forEach(r => rowChecks[r.id] = !!r.checked); // Se aplican también a las no visibles
    this.rowsChecked.emit(rowChecks);
  }


  /**
   * busqueda
   */
  handleInputSearch(item: HeadCellModelData): void {
    if (item.hasFilter) { // si tiene buscador
      item.activeFilter = !!item.valueFilter;
      this.uncheckAll();
    }
    if (item.hasSelect) { //Si tiene seleccionable
      item.activeSelect = !!item.valueSelect;
      this.uncheckAll();
    }

    this.recalculateVisibleElements();
  }

  /**
   * Se filtran las filas según lo especificado en la barra de búsqueda de cada columna.
   * Si no hay búsqueda sse devuelven todas las filas.
   * @param recalculateParams parametros para recalcular los elementos visibles
   * @param rows lista con todas las filas
   * @return lista de filas filtrada
   */
  private filterRows(recalculateParams: RecalculateTableParams, rows: RowDirective[]): RowDirective[] {
    let filteredRows = rows;
    recalculateParams.filters.forEach(filter => {
      filteredRows = filteredRows.filter(row =>
        SearchUtils.containsAnyWordFrom(row.getCell(filter.columnIndex).getContent(), filter.filterText));
    });
    return filteredRows;
  }

  /**
   * ordenacion
   */

  /**
   * Permite indicar que se debe ordenar la tabla
   * @param index indice de la columna
   */
  handleSortByColumn(index: number, event): void {
    const head = this.getHeaderCells();
    if (head) {
      head.forEach((cell, cellIndex) => {
        if (cell.orderBy) {
          cell.orderBy = cellIndex === index ? this.getNextDirection(cell) : OrderBy.none;
        }
      });
    }

    this.recalculateVisibleElements();
    event?.preventDefault();
  }


  /**
   * Ordena las filas indicadas según la columna especificada en la cabecera
   * @param recalculateParams parametros para recalcular los elementos visibles
   * @param rows filas de la tabla a ordenar
   * @return mismas filas pero ordenadas por la columna indicada
   */
  private sortRows(recalculateParams: RecalculateTableParams, rows: RowDirective[]): RowDirective[] {
    let sortedRows = rows;
    if (recalculateParams.sort) {
      const columnIndex = recalculateParams.sort.columnIndex;
      const isAsc = recalculateParams.sort.order === OrderBy.asc;
      sortedRows = rows.sort((a, b) =>
        this.compareCellContent(a.getCell(columnIndex), b.getCell(columnIndex), isAsc));
    }

    return sortedRows;
  }

  /**
   * Compara el contenido de dos celdas. Este se invierte si el orden especificado es descendente.
   */
  compareCellContent(a: CellDirective, b: CellDirective, isAsc: boolean): number {
    let result;
    const aComparable = this.cleanCurrency(a.getContent()).trim();
    const bComparable = this.cleanCurrency(b.getContent()).trim();
    const numberRegex = /^-?\d+(\.|,)?\d*$/;
    const dateRegex = /^(\d{2})(\/|-)(\d{2})(\/|-)(\d{4})( (\d{2}):(\d{2})(:(\d{2}))?)?$/;
    if (aComparable && bComparable) {
      if (numberRegex.test(aComparable) && numberRegex.test(bComparable)) { // Numeros
        result = (parseFloat(aComparable) - parseFloat(bComparable))
      }
      else if (dateRegex.test(aComparable) && dateRegex.test(bComparable)) { // Fechas
        result = this.parseDateTime(aComparable) - this.parseDateTime(bComparable);
      } else { // Texto
        result = aComparable.localeCompare(bComparable, 'es', { sensitivity: 'base', ignorePunctuation: true });
      }
    }
    return result * (isAsc ? 1 : -1);
  }

  private parseDateTime(input: string) { // 31/12/2023 12:12:12 or 31-12-2023 12:12:12 fechas que acepta, hh:mm:ss es opcional
    let [datePart, timePart] = input.split(' ');
    let [day, month, year] = datePart.split(/\/|-/).map(Number);
    let [hours = 0, minutes = 0, seconds = 0] = (timePart ? timePart.split(':') : []).map(Number);
    let date = new Date(year, month - 1, day, hours, minutes, seconds);
    return date.getTime();
  }

  /**
   * se calcula la ordenación nueva
   */
  private getNextDirection(item: HeadCellModelData): OrderBy {
    if (item.orderBy === OrderBy.none || item.orderBy === OrderBy.desc) {
      return OrderBy.asc;
    } else {
      return OrderBy.desc;
    }
  }

  /**
   * si es una cantidad de dinero se quita € para poder comparar los numeros
   */
  private cleanCurrency(txt: string): string {
    txt = txt.replace(/€/g, '');
    return txt;
  }

  selectFilter(item: TableAdvancedSelectComponent, itemValue: any): void {
    item.valueChange.emit(itemValue);

    const head = this.getHeaderCells();
    const rows = this.getRows();
    if (head && rows) {
      head.forEach((cell: TableAdvancedHeaderCellComponent, cellIndex) => {
      });
      rows.forEach(row => {
      });
    }
  }
}
