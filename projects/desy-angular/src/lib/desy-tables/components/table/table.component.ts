import { Component, ContentChildren, Input, QueryList } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { CellData, WrapperData } from '../../interfaces';
import { TableCaptionComponent } from './components/table-caption.component';
import { TableCellComponent } from './components/table-cell.component';
import { TableFooterComponent } from './components/table-footer.component';
import { TableHeaderComponent } from './components/table-header.component';
import { TableRowComponent } from './components/table-row.component';


@Component({
  selector: 'desy-table',
  templateUrl: './table.component.html'
})
export class TableComponent extends AccessibilityComponent {

  @Input() rows: Array<CellData[]>;
  @Input() head: CellData[];
  @Input() foot: CellData[];
  @Input() caption: string;
  @Input() captionClasses: string;
  @Input() firstCellIsHeader: boolean;
  @Input() classes: string;
  @Input() id: string;
  @Input() wrapper: WrapperData;

  @DesyContentChild()
  @ContentChildren(TableCaptionComponent) captionComponent: TableCaptionComponent;

  @DesyContentChild()
  @ContentChildren(TableHeaderComponent) headerComponent: TableHeaderComponent;

  @DesyContentChild()
  @ContentChildren(TableFooterComponent) footerComponent: TableFooterComponent;

  @ContentChildren(TableRowComponent) rowComponentList: QueryList<TableRowComponent>;

  extractedTexts: string[] = [];

  handleContentExtracted(extractedText: string) {
    this.extractedTexts.push(extractedText);
  }

  getCaptionClass(): string {
    return this.captionComponent ? this.captionComponent.classes : this.captionClasses;
  }

  getHeaderCells(): TableCellComponent[] | CellData[] {
    let headerCells = [];
    if (this.headerComponent && this.headerComponent.cells) {
      headerCells = this.headerComponent.cells.toArray();
    } else if (this.head) {
      headerCells = this.head;
    }
    return headerCells;
  }

  getFooterCells(): TableCellComponent[] | CellData[] {
    let footCells = [];
    if (this.footerComponent && this.footerComponent.cells) {
      footCells = this.footerComponent.cells.toArray();
    } else if (this.foot) {
      footCells = this.foot;
    }
    this.assignExtractedTextsToCells(footCells);
    return footCells;
  }

  getRows() {
    let rowList = [];
    if (this.rowComponentList && this.rowComponentList.length > 0) {
      rowList = this.rowComponentList.toArray();
    } else if (this.rows) {
      rowList = this.rows;
    }
    return rowList;
    //this.rowComponentList && this.rowComponentList.length > 0 ? this.rowComponentList.toArray() : this.rows;
  }

  getRowCells(row: TableRowComponent | CellData[]): TableCellComponent[] | CellData[] {
    const cells = row instanceof TableRowComponent ? row.cells.toArray() : row;
    this.assignExtractedTextsToCells(cells);
    return cells;
  }

  private assignExtractedTextsToCells(cells: TableCellComponent[] | CellData[]): void {
    let textIndex = 0;
    cells.forEach(cell => {
      if (this.extractedTexts[textIndex]) {
        cell.header = this.extractedTexts[textIndex];
      }
      const colspan = cell.colspan || 1;
      textIndex += colspan;
    });
  }
}

