import { Component, ContentChildren, QueryList } from '@angular/core';
import { TableCellComponent } from './table-cell.component';

@Component({
  selector: 'desy-table-footer',
  template: ''
})
export class TableFooterComponent {
  @ContentChildren(TableCellComponent) cells: QueryList<TableCellComponent>;
}
