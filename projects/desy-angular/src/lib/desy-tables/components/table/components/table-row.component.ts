import {Component, ContentChildren, QueryList} from '@angular/core';
import {TableCellComponent} from './table-cell.component';

@Component({
  selector: 'desy-table-row',
  template: ''
})
export class TableRowComponent {
  @ContentChildren(TableCellComponent) cells: QueryList<TableCellComponent>;
}
