import {Component, ContentChildren, QueryList} from '@angular/core';
import {TableCellComponent} from './table-cell.component';

@Component({
  selector: 'desy-table-header',
  template: ''
})
export class TableHeaderComponent {
  @ContentChildren(TableCellComponent) cells: QueryList<TableCellComponent>;
}
