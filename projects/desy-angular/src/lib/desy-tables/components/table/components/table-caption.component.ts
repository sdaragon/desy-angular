import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-table-caption',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class TableCaptionComponent extends ContentBaseComponent {
  @Input() classes: string;
}
