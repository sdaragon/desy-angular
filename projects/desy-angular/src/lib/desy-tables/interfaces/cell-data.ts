import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface CellData extends AccessibilityData {
    text?: string;
    html?: string;
    classes?: string;
    id?: string;
    colspan?: number;
    rowspan?: number;
}
