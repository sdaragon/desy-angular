import {OrderBy} from './head-cell-data';

export interface RecalculateTableParams {

  filters: {
    columnIndex: number,
    filterText: string
  }[];

  sort: {
    columnIndex: number,
    order: OrderBy
  };
}
