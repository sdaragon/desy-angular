import { CellData } from './cell-data';

export interface RowData {
    id?: string;
    cellsList: CellData[];
    checked?: boolean;
}
