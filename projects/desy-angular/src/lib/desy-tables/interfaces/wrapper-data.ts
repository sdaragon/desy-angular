import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface WrapperData extends AccessibilityData {
    classes?: string;
}
