import { ContentComponent } from '../../desy-commons/components/content/content.component';
import { CellData } from './cell-data';

export interface HeadCellData extends CellData {
    orderBy?: OrderBy; // Possible values `none`, `asc`, `desc`.
    hasFilter?: boolean;
    filterClasses?: string;
    hasSelect?: boolean;
    identifier?: string;
}

export interface HeadCellModelData extends HeadCellData {
    valueFilter?: string;
    activeFilter?: boolean;
    valueSelect?: string;
    activeSelect?: boolean;
    contentComponent?: ContentComponent;
}

export enum OrderBy {
    none = 'none',
    asc = 'asc',
    desc = 'desc'
}
