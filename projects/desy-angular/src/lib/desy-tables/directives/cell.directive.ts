import {Directive, ElementRef, Input} from '@angular/core';
import {RowData} from '../interfaces';
import {TableAdvancedRowComponent} from '../components/table-advanced/components/table-advanced-row.component';

/**
 * Permite gestionar celdas de una fila de la tabla
 */
@Directive({
  selector: '[desyCell]'
})
export class CellDirective {

  @Input() row: RowData|TableAdvancedRowComponent;
  @Input() rowIndex: number;
  @Input() columnIndex: number;

  constructor(private element: ElementRef) { }

  public getContent(): string {
    return this.element.nativeElement.textContent;
  }
}
