import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[desyFocusClickedCell]'
})
export class FocusClickedCellDirective {

  constructor(private el: ElementRef) { }

  @HostListener('click')
  onClick(): void {
    this.handleFocus();
  }

  @HostListener('keydown', ['$event'])
  manageArrows(event): void {
    if (event.code === 'ArrowDown' || event.code === 'ArrowUp' || event.code === 'ArrowRight' || event.code === 'ArrowLeft') {
      let next = null;
      if (event.code === 'ArrowDown' || event.code === 'ArrowUp') {
        let tr = null;
        if (event.code === 'ArrowDown') {
          tr = this.el.nativeElement.parentNode.nextElementSibling;
        } else if (event.code === 'ArrowUp') {
          tr = this.el.nativeElement.parentNode.previousElementSibling;
        }

        next = tr ? tr.children[this.findIndex()] : null;
        event.preventDefault();
      }

      if (event.code === 'ArrowRight' || event.code === 'ArrowLeft') {
        next = this.el.nativeElement;
        do {
          if (event.code === 'ArrowRight') {
            next = next.nextElementSibling;
          } else if (event.code === 'ArrowLeft') {
            next = next.previousElementSibling;
          }
        } while (next && !next.getAttribute('tabindex'));
      }

      if (next && next.getAttribute('tabindex')) {
        this.inactiveTabindex();
        next.setAttribute('tabindex', '0');
        next.focus();
      }
    }

  }

  private handleFocus() {
    const hostElem = this.el.nativeElement; // la tabla - offsetParent
    const elemntsFocus = hostElem.offsetParent.querySelectorAll('[tabindex]'); // buscamos todos que pueden tener tabindex

    for (const iterator of elemntsFocus) {
      if (!iterator.closest('desy-tooltip')) { // Se añade esto para evitar que el tooltip pierda el focus a raiz de este soporte https://paega2.atlassian.net/browse/SDADESY-406
        iterator.setAttribute('tabindex', '-1'); // se les quita
      }
    }
    this.activeTabindex();
  }

  private activeTabindex(): void {
    this.el.nativeElement.setAttribute('tabindex', '0');
  }

  private inactiveTabindex(): void {
    this.el.nativeElement.setAttribute('tabindex', '-1');
  }

  private findIndex(): number {
    const mynode = this.el.nativeElement;
    const parentNode = this.el.nativeElement.parentNode;
    let index = -1;
    for (let i = 0; i < parentNode.children.length; i++) {
      const otherNode = parentNode.children[i];
      if (mynode.isEqualNode(otherNode)) {
        index = i;
        break;
      }
    }
    return index;
  }

}
