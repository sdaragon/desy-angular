import {ContentChildren, Directive, Input, QueryList} from '@angular/core';
import {CellDirective} from './cell.directive';
import {RowData} from '../interfaces';
import {TableAdvancedRowComponent} from '../components/table-advanced/components/table-advanced-row.component';

/**
 * Permite gestionar filas de la tabla
 */
@Directive({
  selector: '[desyRow]'
})
export class RowDirective {

  @ContentChildren(CellDirective) contentCells: QueryList<CellDirective>;
  @Input() desyRow: RowData|TableAdvancedRowComponent;


  /**
   * Devuelve la celda ubicada en la columna indicada
   * @param column índice de la columna
   */
  public getCell(column: number): CellDirective {
    return this.contentCells.find(cell => cell.columnIndex === column);
  }
}
