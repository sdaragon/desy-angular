import { Directive, Input, OnChanges, TemplateRef } from '@angular/core';
import { ItemCheckboxData, ItemRadioWithTemplateData } from '../interfaces';

@Directive({
  selector: '[desyCondition]'
})
export class ConditionDirective implements OnChanges {

  /**
   * Condition, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir objeto ItemCheckboxData o ItemRadioWithTemplateData (interfaces expuesta)
   * 2) incluir value y el listado de ItemCheckboxData[] o ItemRadioWithTemplateData[]
   */
  @Input() item: ItemCheckboxData|ItemRadioWithTemplateData;

  @Input() value: any;
  @Input() items: ItemCheckboxData[]|ItemRadioWithTemplateData[];

  constructor(public templateRef: TemplateRef<any>) {}

  ngOnChanges(): void {
    if (this.item) {
      this.item.conditionalHtml = this.templateRef;
    } else if (this.value && this.items) {
      for (const item of this.items) {
        if (item.value === this.value) {
          item.conditionalHtml = this.templateRef;
        }
      }
    }
  }

}
