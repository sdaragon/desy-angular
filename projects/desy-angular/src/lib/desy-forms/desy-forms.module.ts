import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
// componentes
import { CharacterCountComponent } from './components/character-count/character-count.component';
import { CheckboxItemComponent } from './components/checkboxes/checkbox-item/checkbox-item.component';
import { CheckboxesComponent } from './components/checkboxes/checkboxes.component';
import { DateInputDayComponent } from './components/date-input/date-input-day/date-input-day.component';
import { DateInputMonthComponent } from './components/date-input/date-input-month/date-input-month.component';
import { DateInputYearComponent } from './components/date-input/date-input-year/date-input-year.component';
import { DateInputComponent } from './components/date-input/date-input.component';
import { ErrorMessageComponent } from './components/error-message/error-message.component';
import { FieldsetComponent } from './components/fieldset/fieldset.component';
import { LegendComponent } from './components/fieldset/legend/legend.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FormFieldComponent } from './components/form-field/form-field.component';
import { HintComponent } from './components/hint/hint.component';
import { InputGroupComponent } from './components/input-group/input-group.component';
import { InputComponent } from './components/input/input.component';
import { LabelComponent } from './components/label/label.component';
import { RadioItemComponent } from './components/radios/radio-item/radio-item.component';
import { RadiosComponent } from './components/radios/radios.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { OptionComponent } from './components/select/option/option.component';
import { SelectComponent } from './components/select/select.component';
import { TemplateDrivenWrapperComponent } from './components/template-driven-wrapper/template-driven-wrapper.component';
import { TextareaComponent } from './components/textarea/textarea.component';

// directivas
import { ConditionDirective } from './directives/condition.directive';

import { DesyButtonsModule } from '../desy-buttons/desy-buttons.module';
import { DesyCommonsModule } from '../desy-commons/desy-commons.module';
import { SharedExternalModule } from '../shared/sharedExternal.module';
import { DateInputDividerComponent } from './components/date-input/date-input-divider/date-input-divider.component';
import { DateInputItemComponent } from './components/date-input/date-input-item/date-input-item.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { FormatDatePipe } from './components/datepicker/datepicker.pipe';
import { InputGroupDividerComponent } from './components/input-group/input-group-divider/input-group-divider.component';
import { InputGroupInputComponent } from './components/input-group/input-group-input/input-group-input.component';
import { InputGroupItemComponent } from './components/input-group/input-group-item/input-group-item.component';
import { InputGroupSelectComponent } from './components/input-group/input-group-select/input-group-select.component';
import { OptionGroupComponent } from './components/select/option-group/option-group.component';
import { SelectItemComponent } from './components/select/select-item/select-item.component';
import { TreeCheckboxComponent } from './components/tree/tree-checkbox/tree-checkbox.component';
import { TreeItemComponent } from './components/tree/tree-item/tree-item.component';
import { TreeItemsGeneratorComponent } from './components/tree/tree-items-generator/tree-items-generator.component';
import { TreeSubComponent } from './components/tree/tree-sub/tree-sub.component';
import { TreeComponent } from './components/tree/tree.component';



@NgModule({
  declarations: [
    TemplateDrivenWrapperComponent,
    FormFieldComponent,
    HintComponent,
    LabelComponent,
    ErrorMessageComponent,
    FieldsetComponent,
    LegendComponent,
    TextareaComponent,
    CharacterCountComponent,
    InputComponent,
    SelectComponent,
    FileUploadComponent,
    InputGroupComponent,
    InputGroupItemComponent,
    DateInputItemComponent,
    InputGroupDividerComponent,
    DateInputDividerComponent,
    InputGroupInputComponent,
    InputGroupSelectComponent,
    RadiosComponent,
    RadioItemComponent,
    CheckboxesComponent,
    CheckboxItemComponent,
    DateInputComponent,
    SearchBarComponent,
    OptionComponent,
    OptionGroupComponent,
    SelectItemComponent,
    DateInputDayComponent,
    DateInputMonthComponent,
    DateInputYearComponent,
    TreeComponent,
    TreeSubComponent,
    TreeItemComponent,
    TreeCheckboxComponent,
    TreeItemsGeneratorComponent,
    DatepickerComponent,

    ConditionDirective,
    FormatDatePipe,

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    DesyCommonsModule,
    DesyButtonsModule,
    SharedExternalModule,
  ],
  exports: [
    HintComponent,
    LabelComponent,
    ErrorMessageComponent,
    FieldsetComponent,
    LegendComponent,
    TextareaComponent,
    CharacterCountComponent,
    InputComponent,
    SelectComponent,
    FileUploadComponent,
    InputGroupComponent,
    InputGroupDividerComponent,
    DateInputDividerComponent,
    InputGroupInputComponent,
    InputGroupSelectComponent,
    RadiosComponent,
    RadioItemComponent,
    CheckboxesComponent,
    CheckboxItemComponent,
    DateInputComponent,
    SearchBarComponent,
    OptionComponent,
    OptionGroupComponent,
    DateInputDayComponent,
    DateInputMonthComponent,
    DateInputYearComponent,
    TreeComponent,
    TreeSubComponent,
    TreeItemComponent,
    TreeItemsGeneratorComponent,
    DatepickerComponent,

    ConditionDirective
  ],
})
export class DesyFormsModule { }
