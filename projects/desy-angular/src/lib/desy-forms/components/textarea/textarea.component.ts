import {
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnChanges,
  ViewChild,
  HostBinding
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormFieldComponent } from '../form-field/form-field.component';

@Component({
  selector: 'desy-textarea',
  templateUrl: './textarea.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true
    }
  ]
})
export class TextareaComponent extends FormFieldComponent<string> implements OnChanges {

  protected static readonly DEFAULT_ROWS = 5;

  @ViewChild('textarea') textareaElement: ElementRef;

  @Input() name: string;
  @Input() rows: number;
  @Input() placeholder: string;
  @Input() describedBy: string;

  //  form group class - asignar clases a la etiqueta desy-input
  @HostBinding('class.c-form-group') cfg = true;
  @HostBinding('class.c-form-group--error') cfgr: boolean;
  @Input() @HostBinding('class') formGroupClasses: any;
  @Input() classes: string;
  @Input() autocomplete: string;
  @Input() maxlength: number;

  constructor() {
    super();
  }

  inputTransform = (value: string): string => value;

  ngOnChanges(): void {
    this.writeValue(this.value);
    this.cfgr = (this.hasErrorsMessage() || this.formGroupClasses === 'c-form-group--error');
  }


  getRows(): number {
    return this.rows ? this.rows : TextareaComponent.DEFAULT_ROWS;
  }

  onInput(event: any): void{
    const value = (event.target as HTMLTextAreaElement).value;
    super.onInput(this.inputTransform(this.ensureText(value)));
  }

  writeValue(value: any): void {
    this.value = this.inputTransform(this.ensureText(value));
    this.onChange(this.value);
  }

  registerInputTransform(fn: (value: string) => string): void {
    this.inputTransform = fn;
  }

  private ensureText(value: any): string {
    return value ? value || '' : '';
  }
}
