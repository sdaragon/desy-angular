import {ChangeDetectorRef, Component, Input} from '@angular/core';
import {AccessibilityComponent} from '../../../shared/components';

@Component({
  selector: 'desy-error-message',
  templateUrl: './error-message.component.html'
})
export class ErrorMessageComponent extends AccessibilityComponent {

  @Input() id: string;
  @Input() classes: string;
  @Input() html: string;
  @Input() text: string;
  @Input() visuallyHiddenText: string;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  public detectChanges(): void {
    this.changeDetectorRef.detectChanges();
  }
}
