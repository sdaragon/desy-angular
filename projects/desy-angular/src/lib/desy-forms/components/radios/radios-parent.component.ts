
import { Component } from '@angular/core';
import { FormFieldComponent } from '../form-field/form-field.component';

/**
 * Clase abstracta que permite comunicar al radio-item con el radios
 * sin que existan dependencias circulares.
 * El esquema de dependencias resulta:
 *  - RadiosComponent -> RadiosParentComponent
 *  - RadiosComponent -> RadiosItemComponent
 *  - RadiosItemComponent -> RadiosParentComponent
 */
@Component({
  selector: 'desy-radios-parent',
  template: ''
})
export abstract class RadiosParentComponent extends FormFieldComponent<any> {
  idPrefix?: string;
  name?: string;
  hasDividers?: boolean;
  describedBy?: string;

  abstract hasFieldset(): boolean;

  abstract markForUpdateRadiosIds(): void;
  abstract updateValueFromRadioItems(): void;
}