import { ComponentFixture, TestBed, fakeAsync, flush, tick } from '@angular/core/testing';

import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { DesyAngularModule } from '../../../desy-angular.module';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('RadiosComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DummyRadiosExampleComponent,
        DummyRadiosReactiveFormAsyncComponent
      ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DesyAngularModule
      ],
      teardown: { destroyAfterEach: false }
    })
    .compileComponents();
  });

  
  describe('Ejemplo', () => {
    let component: DummyRadiosExampleComponent;
    let fixture: ComponentFixture<DummyRadiosExampleComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(DummyRadiosExampleComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('Se crea el componente de ejemplo', () => {
      expect(component).toBeTruthy();
    });
  });
  describe('Formulario reactivo con items asíncronos', () => {
    let component: DummyRadiosReactiveFormAsyncComponent;
    let fixture: ComponentFixture<DummyRadiosReactiveFormAsyncComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(DummyRadiosReactiveFormAsyncComponent);
      component = fixture.componentInstance;
    });

    it('Se obtienen los items de forma asíncrona', fakeAsync(() => {
      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3'
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');

      flush();
    }));

    it('Se obtienen los items de forma asíncrona y se establece el valor del formulario', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: 'val',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3'
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');

      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se comprueba que el primer input está seleccionado
      const firstInput = fixture.debugElement.query(By.css('input'));
      expect(firstInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(firstInputElem.checked).toBe(true);

      // Se comprueba que se emite correctamente el valor del formulario
      expect(fixture.debugElement.nativeElement.textContent).toContain('{"rad":"val"}');

      fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se obtienen los items de forma asíncrona y se establece el valor del formulario con booleanos valor=true', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: false,
          name: 'Opcion 1'
        },
        {
          id: true,
          name: 'Opcion 2',
          sel: true
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');

      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();
      tick();
      fixture.detectChanges();

      // Se comprueba que el primer input está seleccionado
      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      const secondInput = fixture.debugElement.query(By.css('#dummy-2-1'));
      expect(firstInput).toBeTruthy();
      expect(secondInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      const secondInputElem = (secondInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(secondInputElem).toBeTruthy();
      expect(firstInputElem.checked).toBe(false);
      expect(secondInputElem.checked).toBe(true);

      // Se comprueba que se emite correctamente el valor del formulario
      expect(fixture.debugElement.nativeElement.textContent).toContain('{"rad":true}');

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se obtienen los items de forma asíncrona y se establece el valor del formulario con booleanos valor=false', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: false,
          name: 'Opcion 1',
          sel: true
        },
        {
          id: true,
          name: 'Opcion 2'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');

      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();
      tick();
      fixture.detectChanges();

      // Se comprueba que el primer input está seleccionado
      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      const secondInput = fixture.debugElement.query(By.css('#dummy-2-1'));
      expect(firstInput).toBeTruthy();
      expect(secondInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      const secondInputElem = (secondInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(secondInputElem).toBeTruthy();
      expect(firstInputElem.checked).toBe(true);
      expect(secondInputElem.checked).toBe(false);

      // Se comprueba que se emite correctamente el valor del formulario
      expect(fixture.debugElement.nativeElement.textContent).toContain('{"rad":false}');

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se obtienen los items de forma asíncrona y se marca un item como activo', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3'
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se comprueba que el primer input está seleccionado
      const firstInput = fixture.debugElement.query(By.css('input'));
      expect(firstInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      firstInputElem.click();

      // 1a detección -> Marcar items como activos e informar al padre
      fixture.detectChanges();
      tick();

      // 2a detección -> Aplicar en checkboxes
      fixture.detectChanges();

      // Se comprueba que se emite correctamente el valor del formulario
      expect(fixture.debugElement.nativeElement.textContent).toContain('{"rad":"1"}');

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se deshabilitan los inputs al deshabilitar el formulario', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3',
          disabled: true
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se deshabilita el formulario
      component.formRadios.disable();
      fixture.detectChanges();

      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      const thirdInput = fixture.debugElement.query(By.css('#dummy-2-2'));
      expect(firstInput).toBeTruthy();
      expect(thirdInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      const thirdInputElem = (thirdInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(thirdInputElem).toBeTruthy();
      expect(firstInputElem.disabled).toBe(true);
      expect(thirdInputElem.disabled).toBe(true);

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Los inputs explícitamente deshabilitados permaneces así después de rehabilitar el formulario', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3',
          disabled: true
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se deshabilita el formulario
      component.formRadios.disable();
      fixture.detectChanges();

      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      const thirdInput = fixture.debugElement.query(By.css('#dummy-2-2'));
      expect(firstInput).toBeTruthy();
      expect(thirdInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      const thirdInputElem = (thirdInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(thirdInputElem).toBeTruthy();
      expect(firstInputElem.disabled).toBe(true);
      expect(thirdInputElem.disabled).toBe(true);

      // Se rehabilita el formulario
      component.formRadios.enable();
      fixture.detectChanges();

      expect(firstInputElem.disabled).toBe(false);
      expect(thirdInputElem.disabled).toBe(true);

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se establece como obligatorio de forma dinámica', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3',
          disabled: true
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se deshabilita el formulario
      component.setRequired();
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      expect(firstInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      //expect(firstInputElem.ariaInvalid).toBe('true');

      fixture.autoDetectChanges(true);
      flush();
    }));
  });

	/**
	 * Clase host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-radios-example`,
		template: `
    <desy-radios [(ngModel)]="valueContent" [idPrefix]="'example-content'" [name]="'radiosNgModel'" [classes]="'flex'"
                   [role]="'radio-selector'">
      <desy-fieldset [legendText]="'Ejemplo con contenido'"></desy-fieldset>
      <desy-hint>Selecciona una o varias opciones</desy-hint>
      <desy-error-message *ngIf="!valueContent || valueContent.length === 0">Debes seleccionar al menos una opción</desy-error-message>
      <desy-radio-item [value]="'opcion1'" [conditional]="true">
        <desy-label>Opción 1</desy-label>
        <desy-hint>Tiene contenido condicional</desy-hint>
        <desy-content>Contenido condicional</desy-content>
      </desy-radio-item>
      <desy-radio-item [value]="'opcion2'">
        Opcion 2
        <desy-hint>No se especifica label</desy-hint>
      </desy-radio-item>
    </desy-radios>
    `,
	})
	class DummyRadiosExampleComponent {
    valueContent?: string;
	}

	/**
	 * Clase host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-radios-reactive-form`,
		template: `
    <form [formGroup]="formRadios">
      <desy-error-message *ngIf="!formRadios.valid">Es necesario seleccionar un item</desy-error-message>
      <desy-radios formControlName="rad" [idPrefix]="'dummy-2'">
        <desy-radio-item *ngFor="let item of list" [value]="item.id" [disabled]="item.disabled">{{item.name}}</desy-radio-item>
      </desy-radios>

      <p>Form value: {{ formValue }}</p>
      <p>Form disabled: {{ formRadios.disabled }}</p>
      <p>Form valid: {{ formRadios.valid }}</p>
    </form>
    `,
	})
	class DummyRadiosReactiveFormAsyncComponent implements OnInit {
    formRadios = new FormGroup({
      rad: new FormControl(null)
    });

    list = [];

    ngOnInit(): void {
      this.getItems().then(v => {
        this.list = v;

        // Si hay un elemento con este valor, se establece como valor del formulario
        if (this.list && this.list.find(v => v.id === 'val' || v.sel)) {
          const item = this.list.find(v => v.id === 'val' || v.sel);
          this.formRadios.get('rad').setValue(item.id);
        }
      });
    }

    setRequired(): void {
      this.formRadios.get('rad').setValidators(Validators.required);
      this.formRadios.get('rad').updateValueAndValidity();
    }

    getItems(): Promise<any[]> {
      return of([]).toPromise();
    }

    get formValue() {
      return JSON.stringify(this.formRadios.value);
    }
	}
});
