import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Host,
  Input,
  OnChanges, OnDestroy, OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {ItemRadioData} from '../../../interfaces';
import {ContentBaseComponent} from '../../../../shared/components';
import {LabelComponent} from '../../label/label.component';
import {ContentComponent} from '../../../../desy-commons/components/content/content.component';
import {HintComponent} from '../../hint/hint.component';
import { RadiosParentComponent } from '../radios-parent.component';


@Component({
  selector: 'desy-radio-item',
  templateUrl: './radio-item.component.html'
})
export class RadioItemComponent extends ContentBaseComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit, ItemRadioData {

  @ViewChild('input') inputElement: ElementRef;

  @Input() id: string;
  @Input() name: string;
  @Input() value: any;
  @Input() conditional: boolean;
  @Input() disabled: boolean;
  @Input() divider: string;
  @Input() classes: string;

  @ContentChildren(LabelComponent) labelComponentList: QueryList<LabelComponent>;
  @ContentChildren(HintComponent) hintComponentList: QueryList<HintComponent>;
  @ContentChildren(ContentComponent) conditionalContentList: QueryList<ContentComponent>;

  @Input() checked: boolean;
  @Output() checkedChange: EventEmitter<any> = new EventEmitter<any>();

  index = 0;
  lastChecked;
  lastValue;
  radioId;

  parentDisabled: boolean;

  constructor(private radios: RadiosParentComponent, private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.radios.markForUpdateRadiosIds();
  }

  ngOnDestroy(): void {
    this.radios.markForUpdateRadiosIds();
  }

  ngOnChanges(changes: SimpleChanges): void {
     if (this.checked !== this.lastChecked && this.isInit()) {
       this.setChecked(this.checked);
      this.radios.updateValueFromRadioItems();
     } else if (this.lastValue !== undefined && this.lastValue !== this.value) {
      this.radios.updateValueFromRadioItems();
     }
     this.lastValue = this.value;

     if (this.id !== this.radioId) {
      this.radios.markForUpdateRadiosIds();
    }
  }

  ngAfterViewInit(): void {
    this.setChecked(this.checked);
    if (this.checked !== null && this.checked !== undefined) {
      this.radios.updateValueFromRadioItems();
    }
  }

  setChecked(checked: boolean): void {
    this.checked = checked;
    this.lastChecked = checked;
    this.checkedChange.emit(checked);
    this.changeDetectorRef.detectChanges();
  }

  /*
   * Funciones para aportar información desde el componente checkboxes que lo contiene
   */

  getItemHintId(): string {
    return this.hintComponentList && this.hintComponentList.length > 0 && this.radioId ? this.radioId + '-item-hint' : null;
  }

  hasChanged(): void {
    this.setChecked(this.isInputChecked());
    this.radios.updateValueFromRadioItems();
  }

  isInit(): boolean {
    return !!this.inputElement;
  }

  isInputChecked(): boolean {
    return this.inputElement ? this.inputElement.nativeElement.checked : null;
  }

  updateRadioId(index?: number): void {
    if (this.id) {
       this.radioId = this.id;
     } else {
      const idPrefix = this.radios.idPrefix ? this.radios.idPrefix : this.radios.name;
       if (index === 0) {
         this.radioId = idPrefix;
       } else {
         this.radioId = `${idPrefix}-${index}`;
       }
     }

     if (this.labelComponentList && this.labelComponentList.length > 0) {
       this.labelComponentList.first.for = this.radioId;
     }

     if (this.hintComponentList && this.hintComponentList.length > 0) {
       this.hintComponentList.first.id = this.getItemHintId();
     }

     this.changeDetectorRef.detectChanges();
  }

  /*
  * Funciones para aportar información desde el componente checkboxes que lo contiene
  */

  hasDividers(): boolean {
    return this.radios.hasDividers;
  }

  hasError(): boolean {
    return this.radios.hasErrorsMessage();
  }

  getNameRadio(): string {
    return this.radios.name;
  }

  getDescribedBy(): string {
    return !this.radios.hasFieldset() ? this.radios.describedBy : null;
  }

}
