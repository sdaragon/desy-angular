import {
  AfterContentInit,
  Component,
  ContentChildren,
  DoCheck,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  QueryList,
  TemplateRef, ViewChild,
  ViewChildren
} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {FieldsetData, ItemRadioData, LegendData} from '../../interfaces';
import {LegendComponent} from '../fieldset/legend/legend.component';
import {RadioItemComponent} from './radio-item/radio-item.component';
import {FieldsetComponent} from '../fieldset/fieldset.component';
import {MakeHtmlListPipe} from '../../../shared/pipes/make-html-list.pipe';
import {HintComponent} from '../hint/hint.component';
import {ErrorMessageComponent} from '../error-message/error-message.component';
import {DesyContentChild} from '../../../shared/decorators/desy-content-child.decorator';
import {DesyOnInputChange} from '../../../shared/decorators/desy-on-input-change.decorator';
import { RadiosParentComponent } from './radios-parent.component';

@Component({
  selector: 'desy-radios',
  templateUrl: './radios.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadiosComponent),
      multi: true
    },
    {
      provide: RadiosParentComponent,
      useExisting: RadiosComponent,
    }
  ]
})
export class RadiosComponent extends RadiosParentComponent implements DoCheck, AfterContentInit {
    
  /**
   * fieldset, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir componente FieldsetComponent
   * 2) incluir objeto FieldsetData (interfaz expuesta)
   * 3) incluir componente LegendComponent
   * 4) incluir template legend
   * 5) incluir legendData - incluir un fieldset con una legenda con los parametros pasados  (interfaz expuesta)
   * 6) incluir legendText - incluir un fieldset con un texto en la legenda
   */
  @Input() fieldsetData: FieldsetData;
  @Input() legendRef: TemplateRef<LegendComponent>;
  @Input() legendData: LegendData;
  @Input() legendText: string;
  
  @DesyOnInputChange('onIdPrefixChange')
  @Input() declare idPrefix: string;
  
  @DesyOnInputChange('onNameChange')
  @Input() declare name: string;
  
  @Input() items: ItemRadioData[];
  @Output() itemsChange = new EventEmitter<ItemRadioData[]>();
  
  @Input() formGroupClasses: string;
  @Input() declare hasDividers: boolean;
  @Input() hasError: boolean;
  @Input() classes: string;
  @Input() declare describedBy: string;
  
  @DesyContentChild({ onSetCallbackName: 'overrideFieldsetParams'})
  @ContentChildren(FieldsetComponent) fieldsetComponent: FieldsetComponent;
  
  @DesyContentChild()
  @ContentChildren(LegendComponent) legendComponent: LegendComponent;
  
  @ContentChildren(RadioItemComponent) radioComponentItems: QueryList<RadioItemComponent>;
  
  @ViewChild('innerHtml', { static: true }) innerHtml: TemplateRef<any>;
  @ViewChildren(RadioItemComponent) radioComponentItems2: QueryList<RadioItemComponent>;
  
  // Flags
  private nextValue = null;                      // Próximo valor en el componente
  private updateRadiosIdsFlag = false;           // Indica que se han modificado los ids de los items
  private contentInitFlag = false;               // Indica que se inicializado el conenido
  private updateValueFromRadioItemsFlag = false; // Indica que se ha seleccionado/deseleccionado un item
  private updateValueToRadioItemsFlag = false;   // Indica que se debe seleccionar/deseleccionar un item
  private updateItemListFlag = false;            // Indica que se ha creado/modificado el listado de items

  ngDoCheck(): void {

    
    // TODO Esta gestión resulta engorrosa y dificil de seguir. Estudiar si es mejorable utilizando signals a partir de angular 16
    
    if (this.updateValueFromRadioItemsFlag) {
      this.updateValueFromRadioItemsFlag = false;

      /*
       * Se modifica el valor desde los items, a no ser que la lista se acabe de crear
       * y ya tenga un valor. En ese caso, prevalece el valor existente.
       */
      if (!this.updateItemListFlag || (this.value === null || this.value === undefined)) {
        const items = this.getRadioItems();
        if (items && items.length > 0 && items.findIndex(item => !item.isInit() && !item.divider) < 0) {
          const checkedItem = items.find(item => item.isInputChecked() && !item.divider);
          this.nextValue = checkedItem ? checkedItem.value : null;
        }

        if (this.items && this.items.length > 0) {
          this.itemsChange.emit(this.items);
        }
      }
    }

    
    if (this.nextValue !== null && (this.nextValue !== '' || (this.value && this.value !== ''))) {
      this.value = this.nextValue;
      this.nextValue = null;
      this.updateValueToRadioItemsFlag = true;
      setTimeout(()  => {
        this.onChange(this.value);
      });
    }

    if (this.updateRadiosIdsFlag) {
      const radiosItems = this.getRadioItems();
      if (radiosItems.length > 0) {
        radiosItems.forEach((item, index) => item.updateRadioId(index));
      }
      this.updateRadiosIdsFlag = false;
    }

    if (this.updateValueToRadioItemsFlag) {
      const radioItems = this.getRadioItems();
      if (radioItems.length > 0) {
        this.updateValueToRadioItemsFlag = false;
        const updateItems = this.updateRadioItemsChecked();
        if (updateItems && this.items && this.items.length > 0) {
          this.itemsChange.emit(this.items);
        }
      }
    }

    if (this.updateItemListFlag) {
      this.updateItemListFlag = false;
    }
  }

  ngAfterContentInit(): void {
    this.contentInitFlag = true;
    this.radioComponentItems.changes.subscribe(change => {
      this.updateItemListFlag = true;
    });
  }

  /**
   * Se sobrescribe el writeValue de FormField para gestionar en un único punto el valor de value
   * @param value nuevo valor a establecer
   */
  writeValue(value: any): void {
    if (value !== null && value !== undefined) {
      this.nextValue = value;
    }
  }

  onIdPrefixChange(): void {
    if (this.contentInitFlag) {
      this.markForUpdateRadiosIds();
    }
    this.propagateNewIdPrefixValue();
  }


  onNameChange(): void {

    if (this.contentInitFlag) {
      this.markForUpdateRadiosIds();
    }

    this.propagateNewIdPrefixValue();
  }

  /**
   * Actualiza los componentes externos que tienen valores dependientes de idPrefix
   * @private
   */
  private propagateNewIdPrefixValue(): void {
    if (this.hintComponent) {
      this.overrideHintParams(this.hintComponent);
    }

    if (this.errorMessageComponent) {
      this.overrideErrorMessageParams(this.errorMessageComponent);
    }

    // Al hacer el override de hint o error ya se aplica sobre el fieldset, por lo que sólo es necesario aplicarlo directamente
    // sobre el fieldset cuando ninguno de estos existe
    if (!this.hintComponent && !this.errorMessageComponent && this.fieldsetComponent) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  markForUpdateRadiosIds(): void {
    this.updateRadiosIdsFlag = true;
  }

  /**
   * Actualiza el estado de los items según el valor asignado al componente
   */
  private updateRadioItemsChecked(): boolean {
    let itemsUpdated = false;
    const radiosItems = this.getRadioItems();
    if (radiosItems.length > 0) {
      radiosItems.forEach(item => {
        if (item.value === this.value && !item.lastChecked) {
          item.setChecked(true);
          itemsUpdated = true;
        } else if (item.value !== this.value && item.lastChecked) {
          item.setChecked(false);
          itemsUpdated = true;
        }
      });
    }
    return itemsUpdated;
  }

  /**
   * Actualiza el valor cuando se produce un cambio en los items, tanto interno como externo
   */
  updateValueFromRadioItems(): void {
    this.updateValueFromRadioItemsFlag = true;
  }

  /**
   * Reemplazo de funciones de FormField
   */
  setDisabledState(isDisabled: boolean): void {
    super.setDisabledState(isDisabled);
    const items = this.getRadioItems();
    if (items && items.length > 0) {
      items.forEach(i => i.parentDisabled = isDisabled);
    }
  }


  /*
   * Funciones para reemplazar el contenido del fieldset, label, hint o errormessage
   */

  overrideFieldsetParams(fieldset: FieldsetComponent): void {
    fieldset.caller = this.innerHtml;
    fieldset.errorId = this.getErrorId();
    fieldset.describedBy = new MakeHtmlListPipe().transform([this.getHintId(), this.getErrorId()], null);
    fieldset.detectChanges();
  }

  overrideHintParams(hint: HintComponent): void {
    hint.id = this.getHintId();
    hint.detectChanges();

    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  overrideErrorMessageParams(errorMessage: ErrorMessageComponent): void {
    errorMessage.id = this.getErrorId();
    errorMessage.detectChanges();

    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  onDeleteHint(): void {
    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  onDeleteErrorMessage(): void {
    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }


  /*
   * Funciones para recuperar valores comunes
   */

  hasFieldset(): boolean {
    return !!(this.hasFieldsetComponent() || this.hasLegendComponent() || this.legendRef || this.fieldsetData ||
      (this.legendData && (this.legendData.text || this.legendData.html)) || this.legendText);
  }

  hasFieldsetComponent(): boolean {
    return !!this.fieldsetComponent;
  }

  hasLegendComponent(): boolean {
    return !!this.legendComponent;
  }

  getErrorId(): string {
    return this.hasErrorsMessage() ? this.getIdPrefix() + '-error' : null;
  }

  getHintId(): string {
    return this.hasHint() ? this.getIdPrefix() + '-hint' : null;
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : this.name;
  }

  getRadioItems(): RadioItemComponent[] {
    let radiosItems = [];
    if (this.radioComponentItems && this.radioComponentItems.length > 0) {
      radiosItems = this.radioComponentItems.toArray();
    } else if (this.radioComponentItems2 && this.radioComponentItems2.length > 0) {
      radiosItems = this.radioComponentItems2.toArray();
    }
    return radiosItems;
  }
}
