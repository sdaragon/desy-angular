import {Component, ContentChildren, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild} from '@angular/core';
import { ItemDateInputData } from '../../../interfaces';
import { LabelData } from '../../../interfaces';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {LabelComponent} from '../../label/label.component';
import { DateInputItemComponent } from '../date-input-item/date-input-item.component';

@Component({
  selector: 'desy-input-month',
  templateUrl: './date-input-month.component.html',
  providers: [{provide: DateInputItemComponent, useExisting: forwardRef(() => DateInputMonthComponent)}],
})
export class DateInputMonthComponent extends DateInputItemComponent implements ItemDateInputData {
  @Input() id: string;
  @Input() name: string;
  @Input() labelText: string;
  @Input() labelData: LabelData;
  @Input() disabled: boolean;
  @Input() hasErrors: boolean;
  @Input() value: any;
  @Input() autocomplete: string;
  @Input() pattern: string;
  @Input() declare classes: string;
  @Input() maxlength: number;
  @Input() placeholder: string;

  @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();

  @DesyContentChild()
  @ContentChildren(LabelComponent) labelComponent: LabelComponent;

  @ViewChild('label', { static: true }) labelRef: TemplateRef<any>;

}
