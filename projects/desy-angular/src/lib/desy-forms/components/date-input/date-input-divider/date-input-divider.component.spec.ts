import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateInputDividerComponent } from './date-input-divider.component';

xdescribe('InputGroupDividerComponent', () => {
  let component: DateInputDividerComponent;
  let fixture: ComponentFixture<DateInputDividerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateInputDividerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateInputDividerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
