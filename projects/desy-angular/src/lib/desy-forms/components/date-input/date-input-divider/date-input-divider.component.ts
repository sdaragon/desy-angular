import {Component, forwardRef, TemplateRef, ViewChild} from '@angular/core';
import { ItemDividerDateInputData } from '../../../interfaces/item-divider-date-input-data';
import { DateInputItemComponent } from '../date-input-item/date-input-item.component';

@Component({
  selector: 'desy-date-input-divider',
  templateUrl: './date-input-divider.component.html',
  providers: [{provide: DateInputItemComponent, useExisting: forwardRef(() => DateInputDividerComponent)}],
})
export class DateInputDividerComponent extends DateInputItemComponent implements ItemDividerDateInputData {

  divider = this;

  @ViewChild('dividerContent', { static: true }) dividerContent: TemplateRef<any>;
}
