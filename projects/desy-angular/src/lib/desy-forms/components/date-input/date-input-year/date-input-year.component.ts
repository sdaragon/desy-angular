import {Component, ContentChildren, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild} from '@angular/core';
import { ItemDateInputData } from '../../../interfaces';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {LabelComponent} from '../../label/label.component';
import { DateInputItemComponent } from '../date-input-item/date-input-item.component';

@Component({
  selector: 'desy-input-year',
  templateUrl: './date-input-year.component.html',
  providers: [{provide: DateInputItemComponent, useExisting: forwardRef(() => DateInputYearComponent)}],
})
export class DateInputYearComponent extends DateInputItemComponent implements ItemDateInputData {
  @Input() id: string;
  @Input() name: string;
  @Input() disabled: boolean;
  @Input() hasErrors: boolean;
  @Input() value: any;
  @Input() autocomplete: string;
  @Input() pattern: string;
  @Input() declare classes: string;
  @Input() maxlength: number;
  @Input() placeholder: string;

  @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();

  @DesyContentChild()
  @ContentChildren(LabelComponent) labelComponent: LabelComponent;

  @ViewChild('label', { static: true }) labelRef: TemplateRef<any>;

}
