import {
  AfterContentInit,
  Component,
  ContentChildren, EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  Optional, Output,
  QueryList,
  TemplateRef,
  ViewChild,
  HostBinding,
  SimpleChanges,
  OnInit
} from '@angular/core';
import { ControlContainer, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormFieldComponent } from '../form-field/form-field.component';
import { FieldsetData, LegendData, ItemDateInputData, LabelData } from '../../interfaces';
import { LegendComponent } from '../fieldset/legend/legend.component';
import { FieldsetComponent } from '../fieldset/fieldset.component';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { DateInputDayComponent } from './date-input-day/date-input-day.component';
import { DateInputMonthComponent } from './date-input-month/date-input-month.component';
import { DateInputYearComponent } from './date-input-year/date-input-year.component';
import {MakeHtmlListPipe} from '../../../shared/pipes/make-html-list.pipe';
import {DesyOnInputChange} from '../../../shared/decorators/desy-on-input-change.decorator';
import { DateInputDividerComponent } from './date-input-divider/date-input-divider.component';
import { StringUtils } from '../../../shared/utils/string-utils';
import { DateInputItemComponent } from './date-input-item/date-input-item.component';
import { ErrorMessageComponent } from '../error-message/error-message.component';

@Component({
  selector: 'desy-date-input',
  templateUrl: './date-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateInputComponent),
      multi: true
    }
  ]
})
export class DateInputComponent extends FormFieldComponent<any> implements OnInit, AfterContentInit, OnChanges {

  private static defaultItems = [
    {
      name: 'day',
      classes: 'w-14',
      maxlength: 2
    },
    {
      name: 'month',
      classes: 'w-14',
      maxlength: 2
    },
    {
      name: 'year',
      classes: 'w-20',
      maxlength: 4
    }
  ];

  @Input() ngModelGroup: string;

  @DesyOnInputChange('onExternalChange')
  @Input() items: ItemDateInputData[];
  @Output() itemsChange: EventEmitter<ItemDateInputData[]> = new EventEmitter<ItemDateInputData[]>();

  @DesyOnInputChange('onExternalChange')
  @Input() namePrefix: string;
  @Input() classes: string;
  @Input() errorMessage: string;
  @Input() declare id: string;
  //  form group class - asignar clases a la etiqueta desy-input
  @HostBinding('class.c-form-group') cfg = true;
  @HostBinding('class.c-form-group--error') cfgr: boolean;
  @Input() @HostBinding('class') formGroupClasses: any;

  @DesyContentChild({ onSetCallbackName: 'overrideFieldsetParams'})
  @ContentChildren(FieldsetComponent) fieldsetComponent: FieldsetComponent;

  @DesyContentChild({ onSetCallbackName: 'overrideErrorMessageParams'})
  @ContentChildren(ErrorMessageComponent) declare errorMessageComponent: ErrorMessageComponent;

  @DesyContentChild()
  @ContentChildren(LegendComponent) legendComponent: LegendComponent;

  @ContentChildren(DateInputItemComponent) dateInputItemComponents: QueryList<DateInputItemComponent>;

  /**
   * fieldset, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir objeto FieldsetData (interfaz expuesta)
   * 2) incluir template legend
   * 3) incluir legendData - incluir un fieldset con una legenda con los parametros pasados  (interfaz expuesta)
   * 4) incluir legendText - incluir un fieldset con un texto en la legenda
   */
  @Input() fieldsetData: FieldsetData;
  @Input() legendRef: TemplateRef<LegendComponent>;
  @Input() legendData: LegendData;
  @Input() legendText: string;

  @Input() placeholder: string;

  @ViewChild('innerHtml', { static: true }) innerHtml: TemplateRef<any>;

  private contentInit = false;
  public ogFormGroup: FormGroup;

  constructor(@Optional() public controlContainer: ControlContainer) {
    super();
  }

  ngOnInit() {
    if(this.controlContainer){
      this.ogFormGroup = this.controlContainer.control as FormGroup;
    }
    if(this.placeholder){
      let arr = this.placeholder.split('/'); 
      if(this.items && this.items.length > 0 && arr && arr.length > 0){
        for(let i = 0; i < this.items.length; i++){
          this.items[i].placeholder = arr[i];
        }
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.cfgr = (this.hasErrorsMessage() || this.formGroupClasses === 'c-form-group--error');
  }

  ngAfterContentInit(): void {
    this.contentInit = true;
  }

  initDefaultItems(): void {
    if (!this.items) {
      this.items = [
        {
          name: 'día',
          classes: 'w-14',
          maxlength: 2
        },
        {
          name: 'mes',
          classes: 'w-14',
          maxlength: 2
        },
        {
          name: 'año',
          classes: 'w-20',
          maxlength: 4
        }
      ];
    }
  }

  writeValue(value: any): void {
    this.value = value;
    if (this.value) {
      const items = this.getItems();
      items.forEach(item => item.value = this.value[this.getItemName(item)]);

      if (this.items) {
        this.itemsChange.emit(this.items);
      }
    }
    this.onChange(value);
  }

  overrideFieldsetParams(fieldset: FieldsetComponent): void {
    fieldset.caller = this.innerHtml;
    fieldset.errorId = this.getErrorId();
    fieldset.describedBy = new MakeHtmlListPipe().transform([this.getHintId(), this.getErrorId()], null);
    fieldset.role = 'group';
    fieldset.detectChanges();
  }

  overrideErrorMessageParams(errorMessage: ErrorMessageComponent): void {
    if (errorMessage && !errorMessage.id) {
      errorMessage.id = this.getErrorId();
      errorMessage.detectChanges();
    }
  }

  onExternalChange(): void {
    if (!this.contentInit) {
      return;
    }

    if (!this.value) {
      this.value = {};
    }

    const items = this.getItems();
    items.forEach(item => this.value[this.getItemName(item)] = item.value);
    const newVal = {};
    Object.assign(newVal, this.value);
    this.onChange(newVal);
  }

  onInternalChange(item: ItemDateInputData, itemValue: number): void {
    const itemName = this.getItemName(item);
    if (!this.value) {
      this.value = {};
    }

    if (this.value[itemName] !== itemValue) {
      this.value[itemName] = itemValue ? +itemValue : undefined;
      const newVal = {};
      Object.assign(newVal, this.value);
      this.onChange(newVal);
      if (item instanceof DateInputDayComponent || item instanceof DateInputMonthComponent || item instanceof DateInputYearComponent) {
        item.valueChange.emit(itemValue);
      }

      if (this.items) {
        item.value = itemValue;
        this.itemsChange.emit(this.items);
      }
    }
  }

  hasFieldset(): boolean {
    return !!(this.hasFieldsetComponent() || this.hasLegendComponent() || this.legendRef || this.fieldsetData
      || (this.legendData && (this.legendData.text || this.legendData.html)) || this.legendText);
  }

  hasFieldsetComponent(): boolean {
    return !!this.fieldsetComponent;
  }

  hasLegendComponent(): boolean {
    return !!this.legendComponent;
  }

  hasErrorMessageComponent(): boolean {
    return !!this.errorMessageComponent;
  }

  getItemName(item: ItemDateInputData): string {
    return this.namePrefix ? this.namePrefix + '-' + item.name : item.name;
  }

  getItemLabel(item: ItemDateInputData): LabelData {
    let labelData: LabelData;
    if (item.labelData && (item.labelData.text || item.labelData.html)) {
      labelData = item.labelData;
    } else {
      labelData = {
        text: item.labelText ? item.labelText : (item.name ? item.name.toUpperCase() : null)
      };
    }
    return labelData;
  }

  getItemLabelRef(item: ItemDateInputData): TemplateRef<any> {
    return item instanceof DateInputDayComponent || item instanceof DateInputMonthComponent || item instanceof DateInputYearComponent ?
      item.labelRef : null;
  }

  getItems(): ItemDateInputData[] {
    let items;
    if (this.dateInputItemComponents && this.dateInputItemComponents.length > 0) {
      items = this.dateInputItemComponents.toArray();
    } else if (this.items) {
      items = this.items;
    } else if (this.contentInit) {
      items = DateInputComponent.defaultItems;
    } else {
      items = [];
    }


    return items;
  }

  getItemDividerTemplate(item: ItemDateInputData): TemplateRef<any> {
    return item.divider instanceof DateInputDividerComponent ? item.divider.dividerContent : null;
  }
  getItemDividerHtml(item: ItemDateInputData): string {
    return item.divider.html ? item.divider.html : `<p>${ StringUtils.escapeHtml(item.divider.text) }</p>`;
  }

}
