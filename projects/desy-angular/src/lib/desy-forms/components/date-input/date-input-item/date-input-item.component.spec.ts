import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateInputItemComponent } from './date-input-item.component';

xdescribe('InputGroupItemComponent', () => {
  let component: DateInputItemComponent;
  let fixture: ComponentFixture<DateInputItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateInputItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateInputItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
