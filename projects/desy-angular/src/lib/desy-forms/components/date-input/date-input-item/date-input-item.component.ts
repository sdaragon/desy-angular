import { Component, Input } from '@angular/core';
import { ItemDateInputData } from '../../../interfaces';

@Component({
  selector: 'desy-date-input-item',
  template: ''
})
export class DateInputItemComponent implements ItemDateInputData {
  @Input() classes?: string;
  @Input() role?: string;
  @Input() ariaLabel?: string;
  @Input() ariaDescribedBy?: string;
  @Input() ariaLabelledBy?: string;
  @Input() ariaHidden?: string;
  @Input() ariaDisabled?: string;
  @Input() ariaControls?: string;
  @Input() ariaCurrent?: string;
  @Input() ariaLive?: string;
  @Input() ariaExpanded?: string;
  @Input() ariaErrorMessage?: string;
  @Input() ariaHasPopup?: string;
  @Input() tabindex?: string;

}
