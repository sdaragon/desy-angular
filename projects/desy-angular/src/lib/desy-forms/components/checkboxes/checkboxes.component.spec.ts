import { ComponentFixture, TestBed, fakeAsync, flush, tick } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { DesyAngularModule } from '../../../desy-angular.module';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('CheckboxesComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DummyCheckboxesExampleComponent,
        DummyCheckboxesReactiveFormAsyncComponent
      ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DesyAngularModule
      ],
      teardown: { destroyAfterEach: false }
    })
    .compileComponents();
  });

 describe('Ejemplo', () => {
    let component: DummyCheckboxesExampleComponent;
    let fixture: ComponentFixture<DummyCheckboxesExampleComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(DummyCheckboxesExampleComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('Se crea el componente de ejemplo', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Formulario reactivo con items asíncronos', () => {
    let component: DummyCheckboxesReactiveFormAsyncComponent;
    let fixture: ComponentFixture<DummyCheckboxesReactiveFormAsyncComponent>;

    beforeEach(() => {
      fixture = TestBed.createComponent(DummyCheckboxesReactiveFormAsyncComponent);
      component = fixture.componentInstance;
    });

    it('Se obtienen los items de forma asíncrona', fakeAsync(() => {
      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3'
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');

      flush();
    }));

    it('Se obtienen los items de forma asíncrona y se establece el valor del formulario', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: 'val',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3'
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');

      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se comprueba que el primer input está seleccionado
      const firstInput = fixture.debugElement.query(By.css('input'));
      expect(firstInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(firstInputElem.checked).toBe(true);

      // Se comprueba que se emite correctamente el valor del formulario
      expect(fixture.debugElement.nativeElement.textContent).toContain('{"ch":["val"]}');

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se obtienen los items de forma asíncrona y se marca un item como activo', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3'
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se comprueba que el primer input está seleccionado
      const firstInput = fixture.debugElement.query(By.css('input'));
      expect(firstInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      firstInputElem.click();

      // 1a detección -> Marcar items como activos e informar al padre
      fixture.detectChanges();
      tick();

      // 2a detección -> Aplicar en checkboxes
      fixture.detectChanges();

      // Se comprueba que se emite correctamente el valor del formulario
      expect(fixture.debugElement.nativeElement.textContent).toContain('{"ch":["1"]}');

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se deshabilitan los inputs al deshabilitar el formulario', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3',
          disabled: true
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se deshabilita el formulario
      component.formCheckboxes.disable();
      fixture.detectChanges();

      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      const thirdInput = fixture.debugElement.query(By.css('#dummy-2-2'));
      expect(firstInput).toBeTruthy();
      expect(thirdInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      const thirdInputElem = (thirdInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(thirdInputElem).toBeTruthy();
      expect(firstInputElem.disabled).toBe(true);
      expect(thirdInputElem.disabled).toBe(true);

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Los inputs explícitamente deshabilitados permaneces así después de rehabilitar el formulario', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3',
          disabled: true
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se deshabilita el formulario
      component.formCheckboxes.disable();
      fixture.detectChanges();

      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      const thirdInput = fixture.debugElement.query(By.css('#dummy-2-2'));
      expect(firstInput).toBeTruthy();
      expect(thirdInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      const thirdInputElem = (thirdInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      expect(thirdInputElem).toBeTruthy();
      expect(firstInputElem.disabled).toBe(true);
      expect(thirdInputElem.disabled).toBe(true);

      // Se rehabilita el formulario
      component.formCheckboxes.enable();
      fixture.detectChanges();

      expect(firstInputElem.disabled).toBe(false);
      expect(thirdInputElem.disabled).toBe(true);

      //fixture.autoDetectChanges(true);
      flush();
    }));

    it('Se establece como obligatorio de forma dinámica', fakeAsync(() => {

      // Se mockean los items y se inicia el componente
      spyOn(component, 'getItems').and.resolveTo([
        {
          id: '1',
          name: 'Opcion 1'
        },
        {
          id: '2',
          name: 'Opcion 2'
        },
        {
          id: '3',
          name: 'Opcion 3',
          disabled: true
        },
        {
          id: '4',
          name: 'Opcion 4'
        },
      ]);
      fixture.detectChanges();

      // Se espera a que se resuelva la promesa y se pintan los items
      tick(100);
      fixture.detectChanges();
      expect(fixture.nativeElement.textContent).toContain('Opcion 1');
      
      // Los cambios se emiten en la próxima detección
      fixture.detectChanges();

      // Se deshabilita el formulario
      component.setRequired();
      fixture.detectChanges();
      tick(100);
      fixture.detectChanges();

      const firstInput = fixture.debugElement.query(By.css('#dummy-2'));
      expect(firstInput).toBeTruthy();
      const firstInputElem = (firstInput.nativeElement as HTMLInputElement)
      expect(firstInputElem).toBeTruthy();
      //expect(firstInputElem.ariaInvalid).toBe('true');

      fixture.autoDetectChanges(true);
      flush();
    }));
  });

	/**
	 * Clase host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-checkboxes-example`,
		template: `
    <desy-checkboxes [(ngModel)]="valueContent" [idPrefix]="'example-content'" [name]="'checkboxNgModel'" [classes]="'flex'">
      <desy-fieldset [legendText]="'Ejemplo con contenido'"></desy-fieldset>
      <desy-hint>Selecciona una o varias opciones</desy-hint>
      <desy-error-message *ngIf="!valueContent || valueContent.length === 0">Debes seleccionar al menos una opción</desy-error-message>
      <desy-checkbox-item [value]="'opcion1'" [conditional]="true">
        <desy-label>Opción 1</desy-label>
        <desy-hint>Tiene contenido condicional</desy-hint>
        <desy-content>Contenido condicional</desy-content>
      </desy-checkbox-item>
      <desy-checkbox-item [value]="'opcion2'">
        Opcion 2
        <desy-hint>No se especifica label</desy-hint>
      </desy-checkbox-item>
    </desy-checkboxes>
    `,
	})
	class DummyCheckboxesExampleComponent {
    valueContent?: string;
	}

	/**
	 * Clase host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-checkboxes-reactive-form`,
		template: `
    <form [formGroup]="formCheckboxes">
      <desy-error-message *ngIf="!formCheckboxes.valid">Es necesario seleccionar un item</desy-error-message>
      <desy-checkboxes formControlName="ch" [idPrefix]="'dummy-2'">
        <desy-checkbox-item *ngFor="let item of list" [value]="item.id" [disabled]="item.disabled">{{item.name}}</desy-checkbox-item>
      </desy-checkboxes>

      <p>Form value: {{ formValue }}</p>
      <p>Form disabled: {{ formCheckboxes.disabled }}</p>
      <p>Form valid: {{ formCheckboxes.valid }}</p>
    </form>
    `,
	})
	class DummyCheckboxesReactiveFormAsyncComponent implements OnInit {
    formCheckboxes = new FormGroup({
      ch: new FormControl(null)
    });

    list = [];

    ngOnInit(): void {
      this.getItems().then(v => {
        this.list = v;

        // Si hal un elemento con este valor, se establece como valor del formulario
        if (this.list && this.list.find(v => v.id === 'val')) {
          this.formCheckboxes.get('ch').setValue(['val']);
        }
      });
    }

    setRequired(): void {
      this.formCheckboxes.get('ch').setValidators(Validators.required);
      this.formCheckboxes.get('ch').updateValueAndValidity();
    }

    getItems(): Promise<any[]> {
      return of([]).toPromise();
    }

    get formValue() {
      return JSON.stringify(this.formCheckboxes.value);
    }
	}
});
