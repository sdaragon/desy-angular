import {
  AfterViewInit, ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input, OnChanges, OnDestroy, OnInit,
  Output, QueryList, SimpleChanges,
  ViewChild
} from '@angular/core';
import {ItemCheckboxData} from '../../../interfaces';
import {ContentComponent} from '../../../../desy-commons/components/content/content.component';
import {LabelComponent} from '../../label/label.component';
import {HintComponent} from '../../hint/hint.component';
import {AccessibilityComponent} from '../../../../shared/components';
import {DesyOnInputChange} from "../../../../shared/decorators/desy-on-input-change.decorator";
import { CheckboxesParentComponent } from '../checkboxes-parent.component';
import {MakeHtmlListPipe} from '../../../../shared/pipes/make-html-list.pipe';

@Component({
  selector: 'desy-checkbox-item',
  templateUrl: './checkbox-item.component.html'
})
export class CheckboxItemComponent extends AccessibilityComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit, ItemCheckboxData {

  @ViewChild('input') inputElement: ElementRef;

  @Input() id: string;
  @Input() value: any;
  @Input() name: string;
  @Input() conditional: boolean;
  @Input() disabled: boolean;
  @Input() isIndeterminate: boolean;

  @DesyOnInputChange('setIndeterminateStatus')
  @Input() indeterminateChecked: boolean;
  @Input() classes: any;
  @HostBinding('class') classesHost: any;

  @ContentChildren(LabelComponent) labelComponentList: QueryList<LabelComponent>;
  @ContentChildren(HintComponent) hintComponentList: QueryList<HintComponent>;
  @ContentChildren(ContentComponent) conditionalContentList: QueryList<ContentComponent>;

  @Input() checked: boolean;

  @Output() checkedChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() indeterminateChange: EventEmitter<any> = new EventEmitter<any>();

  lastChecked;
  lastIndeterminate;
  lastValue;
  checkboxId;

  parentDisabled: boolean;

  constructor(private checkboxes: CheckboxesParentComponent, private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.checkboxes.markForUpdateCheckboxIds();
  }

  ngOnDestroy(): void {
    this.checkboxes.markForUpdateCheckboxIds();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.classesHost = new MakeHtmlListPipe().transform(['block', this.checkboxes.hasDividers ? 'border-t border-b border-neutral-base -mb-px' : null, this.classes], null);
    if (this.checked !== this.lastChecked && this.isInit()) {
       this.setChecked(this.checked);
      this.checkboxes.updateValueFromCheckboxItems();
     } else if (this.lastValue !== undefined && this.lastValue !== this.value) {
      this.checkboxes.updateValueFromCheckboxItems();
     }

     if(this.indeterminateChecked !== this.lastIndeterminate && this.isInit()){
       this.setIndeterminateChecked(this.indeterminateChecked);
      this.checkboxes.updateValueFromCheckboxItems();
     }


     this.lastValue = this.value;

     if (this.id !== this.checkboxId) {
      this.checkboxes.markForUpdateCheckboxIds();
     }
  }

  ngAfterViewInit(): void {
    this.setChecked(this.checked);
    this.setIndeterminateChecked(this.indeterminateChecked);
    if (this.checked !== null && this.checked !== undefined) {
      this.checkboxes.updateValueFromCheckboxItems();
    }
  }

  getItemHintId(): string {
    return this.hintComponentList && this.hintComponentList.length > 0 && this.checkboxId ? this.checkboxId + '-item-hint' : null;
  }

  hasChanged(): void {
    // Se establece el cambio a visualizar en el input
    const input = this.inputElement.nativeElement;
    if (this.isIndeterminate) {
      if (input.readOnly) {
        input.checked = false;
        input.readOnly = false;
      } else if (!input.checked) {
        input.readOnly = true;
        input.indeterminate = true;
      }
    }
    this.setChecked(input.checked);
    this.setIndeterminateChecked(input.indeterminate);
    this.checkboxes.updateValueFromCheckboxItems();
  }

  setChecked(checked: boolean): void {
    this.checked = checked;
    this.lastChecked = checked;
    this.checkedChange.emit(checked);
    this.changeDetectorRef.detectChanges(); // Avisa al elemento input para que actualice su estado
  }

  setIndeterminateChecked(indeterminateChecked: boolean): void {
    this.indeterminateChecked = indeterminateChecked;
    this.lastIndeterminate = indeterminateChecked;
    this.indeterminateChange.emit(indeterminateChecked);
    this.changeDetectorRef.detectChanges(); // Avisa al elemento input para que actualice su estado
  }

  setIndeterminateStatus(): void {
    const input = this.inputElement?.nativeElement;
    if (input) {
      if (this.indeterminateChecked) {
        input.readOnly = true;
        input.indeterminate = true;
      } else if (input.readOnly) {
        input.readOnly = false;
        input.indeterminate = false;
      }
    }
  }

  isInit(): boolean {
    return !!this.inputElement;
  }

  getValue(): any {
    const input = this.inputElement.nativeElement;
    return input.checked || input.indeterminate ? this.value : null;
  }

  updateCheckboxId(index?: number): void {
    if (this.id) {
       this.checkboxId = this.id;
    } else {
      const idPrefix = this.checkboxes.idPrefix ? this.checkboxes.idPrefix : this.checkboxes.name;
      if (index === 0) {
        this.checkboxId = idPrefix;
      } else {
        this.checkboxId = `${idPrefix}-${index}`;
      }
    }

    if (this.labelComponentList && this.labelComponentList.length > 0) {
      this.labelComponentList.first.for = this.checkboxId;
    }

    if (this.hintComponentList && this.hintComponentList.length > 0) {
      this.hintComponentList.first.id = this.getItemHintId();
    }

    this.changeDetectorRef.detectChanges();
  }

  /*
   * Funciones para aportar información desde el componente checkboxes que lo contiene
   */

  hasDividers(): boolean {
    return this.checkboxes.hasDividers;    
  }

  hasError(): boolean {
    return this.checkboxes.hasErrorsMessage();
  }

  getNameCheckbox(): string {
    return this.checkboxes.name;
  }

  getDescribedBy(): string {
    return !this.checkboxes.hasFieldset() ? this.checkboxes.describedBy : null;
  }
}
