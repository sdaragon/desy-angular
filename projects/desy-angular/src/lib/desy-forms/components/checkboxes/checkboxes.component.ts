import {
  AfterContentInit,
  Component,
  ContentChildren, DoCheck, EventEmitter,
  forwardRef,
  Input, Output,
  QueryList,
  SimpleChanges,
  TemplateRef, ViewChild, ViewChildren
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FieldsetData, LegendData, ItemCheckboxData } from '../../interfaces';
import { LegendComponent } from '../fieldset/legend/legend.component';
import {CheckboxItemComponent} from './checkbox-item/checkbox-item.component';
import {HintComponent} from '../hint/hint.component';
import {ErrorMessageComponent} from '../error-message/error-message.component';
import {FieldsetComponent} from '../fieldset/fieldset.component';
import {MakeHtmlListPipe} from '../../../shared/pipes/make-html-list.pipe';
import {DesyContentChild} from '../../../shared/decorators/desy-content-child.decorator';
import {DesyOnInputChange} from '../../../shared/decorators/desy-on-input-change.decorator';
import { CheckboxesParentComponent } from './checkboxes-parent.component';


@Component({
  selector: 'desy-checkboxes',
  templateUrl: './checkboxes.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxesComponent),
      multi: true
    },
    {
      provide: CheckboxesParentComponent,
      useExisting: CheckboxesComponent,
    }
  ]
})
export class CheckboxesComponent extends CheckboxesParentComponent implements AfterContentInit, DoCheck {
  
  /**
   * fieldset, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir componente FieldsetComponent
   * 2) incluir objeto FieldsetData (interfaz expuesta)
   * 3) incluir componente LegendComponent
   * 4) incluir template legend
   * 5) incluir legendData - incluir un fieldset con una legenda con los parametros pasados  (interfaz expuesta)
   * 6) incluir legendText - incluir un fieldset con un texto en la legenda
   */
  @Input() fieldsetData: FieldsetData;
  @Input() legendRef: TemplateRef<LegendComponent>;
  @Input() legendData: LegendData;
  @Input() legendText: string;

  @DesyOnInputChange('onIdPrefixChange')
  @Input() declare idPrefix: string;
  
  @DesyOnInputChange('onNameChange')
  @Input() declare name: string;
  
  @Input() items: ItemCheckboxData[];
  @Output() itemsChange = new EventEmitter<ItemCheckboxData[]>();
  
  @Input() formGroupClasses: string;
  @Input() declare hasDividers: boolean;
  @Input() hasError: boolean;
  @Input() classes: string;
  @Input() declare describedBy: string;
  
  @DesyContentChild({ onSetCallbackName: 'overrideFieldsetParams'})
  @ContentChildren(FieldsetComponent) fieldsetComponent: FieldsetComponent;
  
  @DesyContentChild()
  @ContentChildren(LegendComponent) legendComponent: LegendComponent;
  
  @ContentChildren(CheckboxItemComponent) checkboxComponentItems: QueryList<CheckboxItemComponent>;
  
  @ViewChild('innerHtml', { static: true }) innerHtml: TemplateRef<any>;
  @ViewChildren(CheckboxItemComponent) checkboxComponentItems2: QueryList<CheckboxItemComponent>;

  // Flags
  private nextValue = null;                         // Próximo valor en el componente
  private updateChekboxIdsFlag = false;             // Indica que se han modificado los ids de los items
  private contentInitFlag = false;                  // Indica que se inicializado el conenido
  private updateValueFromCheckboxItemsFlag = false; // Indica que se ha seleccionado/deseleccionado un item
  private updateValueToCheckboxItemsFlag = false;   // Indica que se debe seleccionar/deseleccionar un item
  private updateItemListFlag = false;               // Indica que se ha creado/modificado el listado de items

  /**
   * Centraliza y coordina los cambios en el componente
   */
  ngDoCheck(): void {
    
    // TODO Esta gestión resulta engorrosa y dificil de seguir. Estudiar si es mejorable utilizando signals a partir de angular 16
    
    if (this.updateValueFromCheckboxItemsFlag) {
      this.updateValueFromCheckboxItemsFlag = false;
      
      /*
       * Se modifica el valor desde los items, a no ser que la lista se acabe de crear
       * y ya tenga un valor. En ese caso, prevalece el valor existente.
       */
      if (!this.updateItemListFlag || (!this.value || this.value.length === 0)) {
        const items = this.getCheckboxItems();
        if (items && items.length > 0 && items.findIndex(item => !item.isInit()) < 0) {
          this.nextValue = items.filter(item => item.checked).map(item => item.getValue());
        }
      }
  
      if (this.items && this.items.length > 0) {
        this.itemsChange.emit(this.items);
      }
    }

    if (this.nextValue !== null && (this.nextValue.length > 0 || (this.value && this.value.length > 0))) {
      this.value = this.nextValue;
      this.nextValue = null;
      this.updateValueToCheckboxItemsFlag = true;

      setTimeout(()  => {
        this.onChange(this.value);
        if (this.items && this.items.length > 0) {
          this.itemsChange.emit(this.items);
        }
      });
    }

    if (this.updateChekboxIdsFlag) {
      const checkboxItems = this.getCheckboxItems();
      if (checkboxItems.length > 0) {
        checkboxItems.forEach((item, index) => item.updateCheckboxId(index));
      }
      this.updateChekboxIdsFlag = false;
    }

    if (this.updateValueToCheckboxItemsFlag) {
      const checkboxItems = this.getCheckboxItems();
      if (checkboxItems.length > 0) {
        this.updateValueToCheckboxItemsFlag = false;
        checkboxItems.forEach(item => {
          const itemChecked = Array.isArray(this.value) ? this.value.findIndex(v => v === item.value) > -1 : this.value === item.value;
          if (itemChecked !== item.lastChecked) {
            item.setChecked(itemChecked);
          }
        });
      }
    }

    if (this.updateItemListFlag) {
      this.updateItemListFlag = false;
    }
  }

  ngAfterContentInit(): void {
    this.contentInitFlag = true;
    this.checkboxComponentItems.changes.subscribe(change => {
      this.updateItemListFlag = true;
    });
  }

  /**
   * Se sobrescribe el writeValue de FormField para gestionar en un único punto el valor de value
   * @param value nuevo valor a establecer
   */
  writeValue(value: any[]): void {
    if (value) {
      this.nextValue = value;
    }
  }

  onIdPrefixChange(): void {
    if (this.contentInitFlag) {
      this.markForUpdateCheckboxIds();
    }
    this.propagateNewIdPrefixValue();
  }


  onNameChange(): void {

    if (this.contentInitFlag) {
      this.markForUpdateCheckboxIds();
    }

    this.propagateNewIdPrefixValue();
  }

  /**
   * Actualiza los componentes externos que tienen valores dependientes de idPrefix
   * @private
   */
  private propagateNewIdPrefixValue(): void {
    if (this.hintComponent) {
      this.overrideHintParams(this.hintComponent);
    }

    if (this.errorMessageComponent) {
      this.overrideErrorMessageParams(this.errorMessageComponent);
    }

    // Al hacer el override de hint o error ya se aplica sobre el fieldset, por lo que sólo es necesario aplicarlo directamente
    // sobre el fieldset cuando ninguno de estos existe
    if (!this.hintComponent && !this.errorMessageComponent && this.fieldsetComponent) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  markForUpdateCheckboxIds(): void {
    this.updateChekboxIdsFlag = true;
  }

  /**
   * Actualiza el valor cuando se produce un cambio en los items, tanto interno como externo
   */
  updateValueFromCheckboxItems(): void {
    this.updateValueFromCheckboxItemsFlag = true;
  }

  /**
   * Reemplazo de funciones de FormField
   */
  setDisabledState(isDisabled: boolean): void {
    super.setDisabledState(isDisabled);
    const items = this.getCheckboxItems();
    if (items && items.length > 0) {
      items.forEach(i => i.parentDisabled = isDisabled);
    }
  }

  /*
   * Funciones para reemplazar el contenido del fieldset, label, hint o errormessage
   */

  overrideFieldsetParams(fieldset: FieldsetComponent): void {
    fieldset.caller = this.innerHtml;
    fieldset.errorId = this.getErrorId();
    fieldset.describedBy = new MakeHtmlListPipe().transform([this.getHintId(), this.getErrorId()], null);
    fieldset.detectChanges();
  }

  overrideHintParams(hint: HintComponent): void {
    hint.id = this.getHintId();
    hint.detectChanges();

    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  overrideErrorMessageParams(errorMessage: ErrorMessageComponent): void {
    errorMessage.id = this.getErrorId();
    errorMessage.detectChanges();

    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  onDeleteHint(): void {
    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }

  onDeleteErrorMessage(): void {
    if (this.hasFieldsetComponent()) {
      this.overrideFieldsetParams(this.fieldsetComponent);
    }
  }


  /*
   * Funciones para recuperar valores comunes
   */


  hasFieldset(): boolean {
    return !!(this.hasFieldsetComponent() || this.hasLegendComponent() || this.legendRef || this.fieldsetData ||
      (this.legendData && (this.legendData.text || this.legendData.html)) || this.legendText);
  }

  hasFieldsetComponent(): boolean {
    return !!this.fieldsetComponent;
  }

  hasLegendComponent(): boolean {
    return !!this.legendComponent;
  }

  getErrorId(): string {
    return this.hasErrorsMessage() ? this.getIdPrefix() + '-error' : undefined;
  }

  getHintId(): string {
    return this.hasHint() ? this.getIdPrefix() + '-hint' : undefined;
  }

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : this.name;
  }

  getCheckboxItems(): CheckboxItemComponent[] {
    let checkboxItems = [];
    if (this.checkboxComponentItems && this.checkboxComponentItems.length > 0) {
      checkboxItems = this.checkboxComponentItems.toArray();
    } else if (this.checkboxComponentItems2 && this.checkboxComponentItems2.length > 0) {
      checkboxItems = this.checkboxComponentItems2.toArray();
    }
    return checkboxItems;
  }
}
