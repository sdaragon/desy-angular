
import { Component } from '@angular/core';
import { FormFieldComponent } from '../form-field/form-field.component';

/**
 * Clase abstracta que permite comunicar al checkbox-item con el checkboxes
 * sin que existan dependencias circulares.
 * El esquema de dependencias resulta:
 *  - CheckboxesComponent -> CheckboxesParentComponent
 *  - CheckboxesComponent -> CheckboxItemComponent
 *  - CheckboxItemComponent -> CheckboxesParentComponent
 */
@Component({
  selector: 'desy-checkboxes-parent',
  template: ''
})
export abstract class CheckboxesParentComponent extends FormFieldComponent<any> {
  idPrefix?: string;
  name?: string;
  hasDividers?: boolean;
  describedBy?: string;

  abstract hasFieldset(): boolean;

  abstract markForUpdateCheckboxIds(): void;
  abstract updateValueFromCheckboxItems(): void;
}