import { ComponentFixture, TestBed, fakeAsync, flush, tick } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DesyAngularModule } from '../../../desy-angular.module';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('TreeComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DummyTreeExampleComponent,
        DummyTreeReactiveFormComponent
      ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DesyAngularModule
      ],
      teardown: { destroyAfterEach: false }
    })
    .compileComponents();
  });

  describe('Componente de ejemplo', () => {
    let component: DummyTreeExampleComponent;
    let fixture: ComponentFixture<DummyTreeExampleComponent>;
  
  
    beforeEach(() => {
      fixture = TestBed.createComponent(DummyTreeExampleComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Formulario reactivo', () => {
    let component: DummyTreeReactiveFormComponent;
    let fixture: ComponentFixture<DummyTreeReactiveFormComponent>;
  
  
    beforeEach(() => {
      fixture = TestBed.createComponent(DummyTreeReactiveFormComponent);
      component = fixture.componentInstance;
    });
  
    it('Se deshabilita el formulario', fakeAsync(() => {
      fixture.detectChanges();
      //fixture.autoDetectChanges(true);
      expect(component).toBeTruthy();
      tick();

      expect(fixture.nativeElement.textContent).toContain('Item 2');
      const input1 = fixture.debugElement.query(By.css('#id1-input'));
      expect(input1).toBeTruthy();
      const input1Elem = input1.nativeElement as HTMLInputElement;
      expect(input1Elem).toBeTruthy();
      expect(input1Elem.disabled).toBe(false)

      // Deshabilitar formulario
      component.formTree.disable();
      fixture.detectChanges();
      tick();

      const input2 = fixture.debugElement.query(By.css('#id1-input'));
      expect(input2).toBeTruthy();
      const input2Elem = input2.nativeElement as HTMLInputElement;
      expect(input2Elem).toBeTruthy();
      expect(input2Elem.disabled).toBe(true)

      flush();
    }));
  });

  
	/**
	 * Clase host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-tree-example`,
		template: `
    <desy-tree [name]="'exampleNameContent'" [type]="'checkbox'" [(ngModel)]="valueContent" 
           [idPrefix]="'prefix'"[expandedFirstLevel]="false" [decoupleChildFromParent]="true">
      <desy-fieldset>
        <desy-legend>Ejemplo de arbol</desy-legend>
      </desy-fieldset>
      <desy-hint [id]="'hint-gral'">Prueba arbol</desy-hint>
      <desy-search-bar [(ngModel)]="searchValue"
                      [labelText]="'Buscar en el árbol'"
                      [id]="'search-bar-tree'"
                      [placeholder]="'Buscar'">
      </desy-search-bar>
      <desy-tree-item [value]="'i1'" [id]="'id1'"><desy-label>Item 1</desy-label></desy-tree-item>
      <desy-tree-item [value]="'i2'" [checked]="true">
        <desy-label>Item 2</desy-label>
        <desy-hint>Item numero 2</desy-hint>
      </desy-tree-item>
      <desy-tree-item [value]="'i3'">
        <desy-label>Item 3</desy-label>
        <desy-tree-sub>
          <desy-tree-item [value]="'i4'"><desy-label>Item 4</desy-label>
            <desy-tree-sub>
              <desy-tree-item [value]="'i41'">
                <desy-label>Item 4.1</desy-label>
                <desy-hint [id]="'hint-interno'">Hint Item 4.1</desy-hint>
              </desy-tree-item>
              <desy-tree-item [value]="'i42'">
                <desy-label>Item 4.2</desy-label>
                <desy-tree-sub>
                  <desy-tree-item [value]="'i421'"><desy-label>Item 4.2.1</desy-label></desy-tree-item>
                  <desy-tree-item [value]="'i422'">
                    <desy-label>Item 4.2.2</desy-label>
                    <desy-tree-sub>
                      <desy-tree-item [value]="'i4221'"><desy-label>Item 4.2.2.1</desy-label></desy-tree-item>
                      <desy-tree-item [value]="'i4222'"><desy-label>Item 4.2.2.2</desy-label></desy-tree-item>
                    </desy-tree-sub>
                  </desy-tree-item>
                </desy-tree-sub>
              </desy-tree-item>
            </desy-tree-sub>
          </desy-tree-item>
          <desy-tree-item [value]="'i5'">
            <desy-label>Item 5 busc</desy-label>
          </desy-tree-item>
          <desy-tree-item [value]="'i6'" [disabled]="true">
            <desy-label>Item 5 busc</desy-label>
          </desy-tree-item>
          <desy-tree-item [value]="'i7'">
            <desy-label>Item 7</desy-label>
            <desy-hint>Item numero 7</desy-hint>
          </desy-tree-item>
        </desy-tree-sub>
      </desy-tree-item>
      <desy-tree-item [value]="'i8'" [disabled]="true"><desy-label>Item 8</desy-label></desy-tree-item>
    </desy-tree>
    `,
	})
	class DummyTreeExampleComponent {
    valueContent?: string;
    searchValue?: string;
	}

  
	/**
	 * Clase host para probar inputs y outputs
	 */
	@Component({
		selector: `desy-dummy-tree-reactive-form`,
		template: `
    <form [formGroup]="formTree">
      <desy-tree [name]="'exampleNameContent'" [type]="'checkbox'" formControlName="tree"
            [idPrefix]="'prefix'"[expandedFirstLevel]="false" [decoupleChildFromParent]="true">
        <desy-fieldset>
          <desy-legend>Ejemplo de arbol</desy-legend>
        </desy-fieldset>
        <desy-hint [id]="'hint-gral'">Prueba arbol</desy-hint>
        <desy-tree-item [value]="'i1'" [id]="'id1'"><desy-label>Item 1</desy-label></desy-tree-item>
        <desy-tree-item [value]="'i2'" [checked]="true">
          <desy-label>Item 2</desy-label>
          <desy-hint>Item numero 2</desy-hint>
        </desy-tree-item>
        <desy-tree-item [value]="'i3'">
          <desy-label>Item 3</desy-label>
          <desy-tree-sub>
            <desy-tree-item [value]="'i4'"><desy-label>Item 4</desy-label>
              <desy-tree-sub>
                <desy-tree-item [value]="'i41'">
                  <desy-label>Item 4.1</desy-label>
                  <desy-hint [id]="'hint-interno'">Hint Item 4.1</desy-hint>
                </desy-tree-item>
                <desy-tree-item [value]="'i42'">
                  <desy-label>Item 4.2</desy-label>
                  <desy-tree-sub>
                    <desy-tree-item [value]="'i421'"><desy-label>Item 4.2.1</desy-label></desy-tree-item>
                    <desy-tree-item [value]="'i422'">
                      <desy-label>Item 4.2.2</desy-label>
                      <desy-tree-sub>
                        <desy-tree-item [value]="'i4221'"><desy-label>Item 4.2.2.1</desy-label></desy-tree-item>
                        <desy-tree-item [value]="'i4222'"><desy-label>Item 4.2.2.2</desy-label></desy-tree-item>
                      </desy-tree-sub>
                    </desy-tree-item>
                  </desy-tree-sub>
                </desy-tree-item>
              </desy-tree-sub>
            </desy-tree-item>
            <desy-tree-item [value]="'i5'">
              <desy-label>Item 5 busc</desy-label>
            </desy-tree-item>
            <desy-tree-item [value]="'i6'" [disabled]="true">
              <desy-label>Item 5 busc</desy-label>
            </desy-tree-item>
            <desy-tree-item [value]="'i7'">
              <desy-label>Item 7</desy-label>
              <desy-hint>Item numero 7</desy-hint>
            </desy-tree-item>
          </desy-tree-sub>
        </desy-tree-item>
        <desy-tree-item [value]="'i8'" [disabled]="true"><desy-label>Item 8</desy-label></desy-tree-item>
      </desy-tree>
    </form>
    `,
	})
	class DummyTreeReactiveFormComponent {
    formTree = new FormGroup({
      tree: new FormControl(null)
    });
	}
});
