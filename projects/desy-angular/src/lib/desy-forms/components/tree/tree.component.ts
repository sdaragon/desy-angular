import {
  ChangeDetectorRef,
  Component,
  ContentChildren, EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnDestroy,
  Output, QueryList,
  SimpleChanges, TemplateRef, ViewChild
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { Subscription } from "rxjs";
import { DesyContentChild } from "../../../shared/decorators/desy-content-child.decorator";
import { DesyOnInputChange } from "../../../shared/decorators/desy-on-input-change.decorator";
import { FieldsetComponent } from "../fieldset/fieldset.component";
import { FormFieldComponent } from "../form-field/form-field.component";
import { SearchBarComponent } from "../search-bar/search-bar.component";
import { QuitTreeItemFocusOptions } from "./interfaces/quit-tree-item-focus-options";
import { TreeItemComponent } from "./tree-item/tree-item.component";

@Component({
  selector: 'desy-tree',
  templateUrl: './tree.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TreeComponent),
      multi: true
    }
  ]
})
export class TreeComponent extends FormFieldComponent<any[]> implements OnChanges, OnDestroy {

  @Input() id: string;
  @Input() idPrefix: string;
  @Input() classes: string;
  @Input() name: string;
  @Input() type: 'radio'|'checkbox'|null|undefined;
  @Input() describedBy: string;

  @Input() formGroupClasses: any;

  @Input() expandedFirstLevel = true;
  @Input() decoupleChildFromParent = false;

  @Input() disableDefaultSearch = false;

  @DesyOnInputChange('onSearchMatchValuesChange')
  @Input() searchMatchValues: string[];
  @Output() searchMatchValuesChange: EventEmitter<string[]> = new EventEmitter<string[]>();

  @DesyOnInputChange('configureAllItems')
  @ContentChildren(TreeItemComponent, { descendants: true }) allItems: QueryList<TreeItemComponent>;

  @DesyContentChild({ onSetCallbackName: 'overrideFieldsetParams'})
  @ContentChildren(FieldsetComponent) fieldsetComponent: FieldsetComponent;

  @DesyContentChild({ onSetCallbackName: 'overrideSearchBarParams'})
  @ContentChildren(SearchBarComponent) searchBarComponent: SearchBarComponent;

  @ViewChild('innerHtml', { static: true }) innerHtml: TemplateRef<any>;

  private rootItems: TreeItemComponent[] = [];
  private _itemListSubscription: Subscription;
  private _allItemListSubscription: Subscription;
  private _itemListCheckedSubscriptions: Subscription[] = [];
  private _itemListQuitFocusSubscriptions: Subscription[] = [];
  private _alreadyConfiguringItems = false;

  constructor(private changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnDestroy(): void {
    this.clearSubscriptions(this._itemListCheckedSubscriptions);
    this.clearSubscriptions(this._itemListQuitFocusSubscriptions);
    this._itemListSubscription?.unsubscribe();
    this._allItemListSubscription?.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.configureAllItems();
  }

  /**
   * Se sobrescribe el valor para actualizar los items
   * @Override
   */
  writeValue(value: any): void {
    this.value = value;
    this.configureAllItems(true);
    this.onChange(this.value);
  }

  /**
   * Se sobrescribe el valor para actualizar los items
   */
  setDisabledState(isDisabled: boolean): void {
    super.setDisabledState(isDisabled);
    this.configureAllItems();
  }

  /*
   * Eventos
   */

  /**
   * Actualiza el valor del componente al checkquearse un ítem
   * @param originItem ítem modificado
   */
  updateValueFromItems(originItem?: TreeItemComponent): void {

    // Si es radio, se desactivan el resto de items cuando se activa otro item
    if (this.type !== 'checkbox') {
      const items = this.getAllItemsInOrder();
      items.forEach(item => {
        if (item !== originItem && item.checked) {
          item.setChecked(false, true);
        }
      });
    } else if (!this.decoupleChildFromParent) {
      this.rootItems.forEach(item => item.setCheckedAutomaticallyDependingOnChildren());
    }

    this.value = [];
    const items = this.getAllItemsInOrder();
    items.forEach(item => {
      if (item.checked) {
        this.value.push(item.value);
      }
    });
    this.onChange(this.value);
  }

  /**
   * Gestiona cuando el foco abandona un ítem desde un evento de teclado controlado
   * @param options
   */
  handleItemQuitFocus(options: QuitTreeItemFocusOptions): void {
    if (options && options.nextElement) {
      const items = this.getAllItemsInOrder();
      const focusableItems = items.filter(item => !item.isHidden() && !item.disabled && item.allParentsExpanded);
      switch (options.nextElement) {
        case "first":
          focusableItems[0].focus();
          break;
        case "last":
          focusableItems[focusableItems.length - 1].focus();
          break;
        case "parent":
          const currentSubLevel = options.currentItem.subLevel;
          if (currentSubLevel > 0) {
            const currentItemIndex = focusableItems.findIndex(item => item === options.currentItem);
            const parent = focusableItems.slice(0, currentItemIndex).reverse().find(item => item.subLevel < currentSubLevel);
            if (parent) {
              parent.focus();
            }
          }
          break;
        case "firstChild":
        case "next":
          if (options.currentItem) {
            let currentItemIndex = focusableItems.findIndex(item => item === options.currentItem);
            if (currentItemIndex < focusableItems.length - 1) {
              focusableItems[currentItemIndex + 1].focus();
            }
          }
          break;
        case "previous":
          if (options.currentItem) {
            const currentItemIndex = focusableItems.findIndex(item => item === options.currentItem);
            if (currentItemIndex > 0) {
              focusableItems[currentItemIndex - 1].focus();
            }
          }
          break;
      }
    }
  }

  /**
   * Realiza la búsqueda interna de los items en el componente
   * @param value texto de búsqueda
   */
  onSearch(value: string): void {
    if (!this.disableDefaultSearch) {
      if (value && value.length > 0) {
        this.searchMatchValues = this.allItems.filter(item => item.matchesText(value)).map(item => item.value);
      } else {
        this.searchMatchValues = null;
      }
      this.searchMatchValuesChange.emit(this.searchMatchValues);
    }
  }

  /*
   * Eventos al cambiar propiedades
   */

  /**
   * Configura los items del árbol
   */
  configureAllItems(isValueChange = false): void {

    if (!this._alreadyConfiguringItems) {
      this._alreadyConfiguringItems = true;

      // Lo realizamos desde un setTimeout para que todos los inputs, values y demás vivan en paz y armonía
      setTimeout(() => {
        if (this.allItems?.length > 0) {
          this.clearSubscriptions(this._itemListCheckedSubscriptions);
          this.clearSubscriptions(this._itemListQuitFocusSubscriptions);
          this.allItems.forEach(item => {
            item.parentName = this.name;
            item.type = this.type;
            item.inheritedDescribedBy = this.getDescribedByForItems();
            item.inheritedExpandedFirstLevel = this.expandedFirstLevel;
            item.inheritedDecoupleChildFromParent = this.decoupleChildFromParent;
            item.treeDisabled = this.disabled;
            if ((this.value && this.value.length > 0) || isValueChange) {
              const valueList = this.value ? this.value : [];
              item.setChecked(!!valueList.find(v => v === item.value), true);
            }
            item.detectChanges();

            const checkedSubscription = item.checkedChangeForTree.subscribe(() => this.updateValueFromItems(item));
            this._itemListCheckedSubscriptions.push(checkedSubscription);

            const quitFocusSubscription = item.quitFocus.subscribe(options => this.handleItemQuitFocus(options));
            this._itemListQuitFocusSubscriptions.push(quitFocusSubscription);
          });

          this.setErrorInItems(this.hasErrorMessageComponent());

          this.buildTree();
          if (!this.value) {
            this.updateValueFromItems();
          }

          if (this.searchMatchValues) {
            this.onSearchMatchValuesChange();
          }
        }

        if (this.allItems && !this._allItemListSubscription) {
          this._allItemListSubscription = this.allItems.changes.subscribe(() => this.configureAllItems());
        }

        this.changeDetector.detectChanges();
        this._alreadyConfiguringItems = false;
      });
    }
  }

  /**
   * Modifica los items visibles según el filtro de búsqueda
   */
  onSearchMatchValuesChange(): void {
    if (this.allItems) {
      this.allItems.forEach(item => {
        item.inheritedMatchesSearch = true;
      });

      if (this.searchMatchValues !== null && this.searchMatchValues !== undefined) {
        this.allItems.forEach(item => {
          if (this.searchMatchValues.findIndex(value => item.value === value) < 0) {
            item.inheritedMatchesSearch = false;
          }
        })
      }
    }
  }

  /*
   * Getters and setters
   */

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : this.name;
  }

  getDescribedBy(): string {
    let describedBy = '';
    if (this.describedBy) {
      describedBy = this.describedBy;
    }

    if (this.hasHintComponent() && this.hintComponent.id) {
      describedBy = describedBy + ' ' + this.hintComponent.id;
    }

    if (this.hasErrorMessageComponent() && this.errorMessageComponent.id) {
      describedBy = describedBy + ' ' + this.errorMessageComponent.id;
    }

    return describedBy;
  }

  /**
   * Devuelve el valor de describedBy que se transmitirá a los items
   */
  getDescribedByForItems(): string {
    let describedBy;
    if (this.hasFieldsetComponent()) {
      describedBy = this.fieldsetComponent.describedBy;
    } else {
      describedBy = this.getDescribedBy();
    }

    return describedBy;
  }

  hasFieldsetComponent(): boolean {
    return !!this.fieldsetComponent;
  }

  /**
   * Overrides super.getHintId();
   */
  getHintId(): string {
    return this.idPrefix ? this.idPrefix + '-hint' : this.hintComponent?.id;
  }

  /**
   * Overrides super.getHintId();
   */
  getErrorId(): string {
    return this.idPrefix ? this.idPrefix + '-error' : this.errorMessageComponent?.id;
  }

  /*
   * Funciones para reemplazar el contenido del fieldset, label, hint o errormessage
   */

  overrideFieldsetParams(fieldset: FieldsetComponent): void {
    fieldset.caller = this.innerHtml;
    fieldset.errorId = this.getErrorId();
    fieldset.describedBy = this.getDescribedBy();
    fieldset.detectChanges();
  }

  overrideSearchBarParams(searchbar: SearchBarComponent): void {
    const defaultOnChange = searchbar.onChange;
    const newOnChange = value => {
      this.onSearch(value);
      defaultOnChange(value);
    }
    searchbar.registerOnChange(newOnChange);
  }

  /**
   * Se llama desde FormField
   */
  overrideHintParams(hint): void {
    hint.id = this.getHintId();
    hint.detectChanges();
  }

  /**
   * Se llama desde FormField
   */
  overrideErrorMessageParams(errorMessage): void {
    errorMessage.id = this.getErrorId();
    this.setErrorInItems(this.hasErrorMessageComponent());
    errorMessage.detectChanges();
  }

  /**
   * Se llama desde FormField
   */
  onDeleteErrorMessage(errorMessage): void {
    this.setErrorInItems(this.hasErrorMessageComponent());
  }

  /*
   * Métodos privados
   */

  /**
   * Configura la estructura del árbol
   * @private
   */
  private buildTree(): void {

    // Se crea un mapa para tener accesibles los items por key y se borran los listados existentes
    const itemMap = {}
    this.rootItems = [];
    this.allItems.forEach(item => {
      const key = item.getKey();
      itemMap[key] = item;
      if (item.sub) {
        item.sub.itemList = [];
      }
    });

    // Se añaden los items a los listados correspondientes
    this.allItems.forEach(item => {
      const parentKey = item.getParentKey();
      if (parentKey === 'root') {
        this.rootItems.push(item);
      } else if (parentKey) {
        const sub = itemMap[parentKey].sub;
        if (sub) {
          sub.itemList.push(item);
        }
      }
    });

    // Se configuran los items desde root
    let order = 0;
    this.rootItems.forEach((item, index) => {
      item.setSubLevel(0);
      item.setDefaultId(`${this.idPrefix}-${index}`);
      item.refreshAllParentsExpandedRecursive(true);
      if (!this.decoupleChildFromParent) {
        item.setCheckedAutomaticallyDependingOnChildren();
      }
      order = item.setOrderRecursively(order);
    });
  }

  /**
   * Si tiene un mensaje de error, se indica a los items
   * @param hasError
   * @private
   */
  private setErrorInItems(hasError: boolean): void {
    if (this.allItems?.length > 0) {
      this.allItems.forEach(item => {
        item.inheritedHasError = hasError;
        item.detectChanges();
      });
    }
  }

  /**
   * Limpia un listado de subscripciones
   * @param subscriptionList
   * @private
   */
  private clearSubscriptions(subscriptionList: Subscription[]): void {
    if (subscriptionList.length > 0) {
      subscriptionList.forEach(s => s.unsubscribe());
      subscriptionList.splice(0, subscriptionList.length)
    }
  }

  /**
   * Devuelve los items del árbol ordenados
   * @private
   */
  private getAllItemsInOrder(): TreeItemComponent[] {
    let list = [];
    if (this.allItems) {
      list = this.allItems.toArray().sort((a, b) => a.orderInTree > b.orderInTree ? 1 : -1)
    }
    return list
  }
  

}
