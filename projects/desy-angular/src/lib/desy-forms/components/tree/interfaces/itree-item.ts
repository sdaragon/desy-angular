export class ITreeItem {
  id: string;
  subLevel: number;
  defaultId: string;
}
