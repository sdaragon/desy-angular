import {TreeItemComponent} from "../tree-item/tree-item.component";

export interface QuitTreeItemFocusOptions {
  nextElement?: 'first'|'last'|'previous'|'next'|'firstChild'|'parent';
  currentItem?: TreeItemComponent;
}
