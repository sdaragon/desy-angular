import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {TreeItemComponent} from "../tree-item/tree-item.component";

@Component({
  selector: 'desy-tree-items-generator',
  templateUrl: './tree-items-generator.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreeItemsGeneratorComponent implements OnInit {

  @ViewChild('content', { static: true }) content: TemplateRef<any>;

  @Input() items: any[];
  @Input() itemTemplate: TemplateRef<TreeItemComponent>;

  constructor(private viewContainerRef: ViewContainerRef) { }

  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.content);
  }

}
