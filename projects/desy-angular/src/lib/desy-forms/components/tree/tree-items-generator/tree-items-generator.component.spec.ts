import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeItemsGeneratorComponent } from './tree-items-generator.component';

xdescribe('TreeItemsGeneratorComponent', () => {
  let component: TreeItemsGeneratorComponent;
  let fixture: ComponentFixture<TreeItemsGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreeItemsGeneratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeItemsGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
