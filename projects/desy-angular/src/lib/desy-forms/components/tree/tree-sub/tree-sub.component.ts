import {
  Component,
  Input,
} from '@angular/core';
import {ITreeItem} from "../interfaces/itree-item";
import {ContentBaseComponent} from "../../../../shared/components";

@Component({
  selector: 'desy-tree-sub',
  templateUrl: './tree-sub.component.html'
})
export class TreeSubComponent extends ContentBaseComponent {

  @Input() classes: string;

  itemList: ITreeItem[];

}
