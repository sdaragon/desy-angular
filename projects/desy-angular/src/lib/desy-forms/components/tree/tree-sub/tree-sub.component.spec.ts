import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeSubComponent } from './tree-sub.component';

xdescribe('TreeSubComponent', () => {
  let component: TreeSubComponent;
  let fixture: ComponentFixture<TreeSubComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreeSubComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
