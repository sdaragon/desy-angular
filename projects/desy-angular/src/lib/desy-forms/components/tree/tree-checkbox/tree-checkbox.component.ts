import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {LabelComponent} from "../../label/label.component";
import {HintComponent} from "../../hint/hint.component";
import {DesyOnInputChange} from "../../../../shared/decorators/desy-on-input-change.decorator";
import {SearchUtils} from "../../../../shared/utils/search-utils";
import {AccessibilityComponent} from "../../../../shared/components";

@Component({
  selector: 'desy-tree-checkbox',
  templateUrl: './tree-checkbox.component.html'
})
export class TreeCheckboxComponent extends AccessibilityComponent implements OnInit {

  @ViewChild('input', { static: true }) inputElement: ElementRef;
  @ViewChild('contentWrapper', { static: true }) contentWrapper: ElementRef;

  @DesyOnInputChange('onIdChange')
  @Input() id: string;
  @Input() name: string;
  @Input() type: 'radio'|'checkbox'|null|undefined;
  @Input() classes: string;

  @Input() value: any;

  @Input() checked: boolean;
  @Output() checkedChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() isIndeterminate: boolean;

  @DesyOnInputChange('setIndeterminateStatus')
  @Input() indeterminateChecked: boolean;
  @Output() indeterminateCheckedChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() disabled: boolean;
  @Input() hasDividers: boolean;

  @DesyOnInputChange('onDescribedByChange')
  @Input() describedBy: string;
  @Input() hasError = false;

  @DesyOnInputChange('overrideLabelParams')
  @Input() labelComponent: LabelComponent;

  @DesyOnInputChange('overrideHintParams')
  @Input()  hintComponent: HintComponent;

  itemDescribedBy: string;

  private _lastIndeterminate;
  private _hasInit = false;

  constructor(private changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this._hasInit = true;
    this.onDescribedByChange();
  }

  detectChanges(): void {
    this.changeDetector.detectChanges();
  }

  hasChanged(): void {
    if (this._hasInit) {

      const input = this.inputElement.nativeElement;

      // Se establece el cambio a visualizar en el input
      if (this.isIndeterminate) {
        if (input.readOnly) {
          input.checked = false;
          input.readOnly = false;
        } else if (!input.checked) {
          input.readOnly = true;
          input.indeterminate = true;
        }
      }

      this.setIndeterminateChecked(input.indeterminate);
      this.checkedChange.emit(input.checked);
    }
  }

  onIdChange(): void {
    this.overrideLabelParams();
    this.overrideHintParams();
  }

  setIndeterminateStatus(): void {
    const input = this.inputElement?.nativeElement;
    if (input) {
      if (this.indeterminateChecked) {
        input.readOnly = true;
        input.indeterminate = true;
      } else if (input.readOnly) {
        input.readOnly = false;
        input.indeterminate = false;
      }
    }
  }

  setIndeterminateChecked(indeterminateChecked: boolean): void {
    this.indeterminateChecked = indeterminateChecked;
    this._lastIndeterminate = indeterminateChecked;
    this.indeterminateCheckedChange.emit(indeterminateChecked);
    this.changeDetector.detectChanges(); // Avisa al elemento input para que actualice su estado
  }

  focus(): void {
    this.inputElement.nativeElement.focus();
  }

  matchesText(value: string): boolean {
    const checkboxText = this.contentWrapper.nativeElement.textContent;
    return SearchUtils.containsAnyWordFrom(checkboxText, value);
  }

  preventDefault(event: Event): void {
    event.preventDefault();
  }

  getHintId(): string {
    return this.hintComponent ? this.hintComponent.id : '';
  }

  overrideLabelParams(): void {
    if (this.labelComponent) {
      this.labelComponent.for = this.id + '-input';
      if (!this.labelComponent.classes) {
        this.labelComponent.classes = 'block relative -top-xs -left-8 pl-8 py-xs';
      }
      this.labelComponent.detectChanges();
    }
  }

  overrideHintParams(): void {
    if (this.hintComponent) {
      if (!this.hintComponent.id) {
        this.hintComponent.id = this.id + '-item-hint';
      }
      this.hintComponent.detectChanges();
    }
    this.onDescribedByChange();
  }

  getItemDescribedBy(): string {
    return (this.describedBy ? this.describedBy : '') + ' ' + this.getHintId();
  }

  onDescribedByChange(): void {
    if (this.ariaDescribedBy) {
      this.itemDescribedBy = this.ariaDescribedBy;
    } else {
      this.itemDescribedBy = this.getItemDescribedBy();
    }
  }

}
