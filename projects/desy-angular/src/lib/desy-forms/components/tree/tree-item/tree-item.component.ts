import {
  ChangeDetectorRef,
  Component,
  ContentChildren, ElementRef,
  EventEmitter,
  forwardRef, HostBinding,
  Input,
  OnChanges,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import { AccessibilityComponent } from "../../../../shared/components";
import { DesyContentChild } from "../../../../shared/decorators/desy-content-child.decorator";
import { DesyOnInputChange } from "../../../../shared/decorators/desy-on-input-change.decorator";
import { SearchUtils } from "../../../../shared/utils/search-utils";
import { HintComponent } from "../../hint/hint.component";
import { LabelComponent } from "../../label/label.component";
import { ITreeItem } from "../interfaces/itree-item";
import { QuitTreeItemFocusOptions } from "../interfaces/quit-tree-item-focus-options";
import { TreeCheckboxComponent } from "../tree-checkbox/tree-checkbox.component";
import { TreeSubComponent } from "../tree-sub/tree-sub.component";

@Component({
  selector: 'desy-tree-item',
  templateUrl: './tree-item.component.html',
  providers: [
    {
      provide: ITreeItem,
      useExisting: forwardRef(() => TreeItemComponent)
    }
  ]
})
export class TreeItemComponent extends AccessibilityComponent implements OnChanges, ITreeItem {

  private static _treeItemKeySuffix = 0; // Sufijo estático para generar siempre claves distintas
  @ViewChild('hiddenWrapper') hiddenWrapper: ElementRef;

  @DesyOnInputChange('setDefaultIdRecursive')
  @Input() id: string;
  @Input() name: string;
  @Input() value: string;
  @Input() classes: string;
  @Input() active: boolean;
  @Input() disabled: boolean;
  @Input() titleModal: string;
  @Input() hasDividers: boolean;
  @Input() isIndeterminate: boolean;

  @Input() checked: boolean;
  @Output() checkedChange: EventEmitter<any> = new EventEmitter<any>();
  checkedChangeForTree: EventEmitter<any> = new EventEmitter<any>();

  @Input() expanded: boolean;
  @Output() expandedChange: EventEmitter<any> = new EventEmitter<any>();

  @Input() indeterminateChecked: boolean;
  @Output() indeterminateCheckedChange: EventEmitter<any> = new EventEmitter<any>();

  @DesyContentChild({ onSetCallbackName: 'overrideSubValues'})
  @ContentChildren(TreeSubComponent) sub: TreeSubComponent;

  @DesyContentChild()
  @ContentChildren(HintComponent) hint: HintComponent;

  @DesyContentChild()
  @ContentChildren(LabelComponent) label: LabelComponent;

  @ViewChild(TreeCheckboxComponent) checkbox: TreeCheckboxComponent;

  /*
   * Identificador del wrapper del componente. Se utiliza para recorrer el árbol en sentido inverso y que cada item
   * sepa cuál es su item/árbol padre.
   * Esto es necesario ya que, a la hora de construir un árbol dinámico con templates,
   * el decorador @ContentChildren no funciona correctamente y no los detecta.
   * Dejo el enlace de un problema similar: https://github.com/angular/angular/issues/21751
   */
  @HostBinding('attr.desy-tree-item-key')
  readonly treeItemKey: string = TreeItemComponent.generateStaticItemKey();

  type: 'radio'|'checkbox'|null|undefined;
  subLevel: number; // Nivel dentro del arbol. 0 son los items a nivel de raiz.

  // Propiedades definidas por el componente árbol que el item puede utilizar, sobrescribir o ampliar
  parentName: string;
  defaultId;
  inheritedExpandedFirstLevel: boolean;
  inheritedDecoupleChildFromParent: boolean;
  inheritedDescribedBy: string;
  inheritedHasError: boolean;
  allParentsExpanded = true; // True si el elemento es visible según los elementos expandidos
  inheritedMatchesSearch = true; // True si el elemento es visible según los criterios de búsqueda
  orderInTree: number;
  treeDisabled = false;

  // Evento para emitir al componente arbol cuando se pretende abandonar el foco del item
  quitFocus: EventEmitter<QuitTreeItemFocusOptions> = new EventEmitter<QuitTreeItemFocusOptions>();

  isFocus = false;
  isHover = false;
  private _matchesValidText = false; // True si el contenido del item encaja con los criterios de búsqueda y se está filtrando


  constructor(private changeDetector: ChangeDetectorRef, private element: ElementRef) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.sub && this.sub.itemList) {
      this.sub.itemList.forEach(item => (item as TreeItemComponent).refreshAllParentsExpandedRecursive(this.allParentsExpanded && this.expanded));
    }

    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case 'checked': {
            // Reasignamos el valor de checked cuando detectamos el cambio en la entrada
            setTimeout(() => {
              this.setChecked(this.checked, false, false);
              if (this.inheritedExpandedFirstLevel && this.checked && !this.expanded) {
                this.expandSub(true);
              }
            });
            break;
          }
        }
      }
    }
  }

  /*
   * Métodos expuestos que permiten configurar al ítem de forma externa
   */

  focus(): void {
    this.checkbox?.focus();
    this.handleItemFocus(true);
  }

  setCheckedAutomaticallyDependingOnChildren(): void {
    if (this.sub) {
      this.sub.itemList.forEach(item => (item as TreeItemComponent).setCheckedAutomaticallyDependingOnChildren());
      if (this.areAllChildrenChecked()) {
        if (this.indeterminateChecked) {
          this.setIndeterminateChecked(false);
        }

        if (!this.checked) {
          this.setChecked(true, true);
        }
      } else if (this.hasChildrenCheckedOrIndeterminate()) {
        if (!this.indeterminateChecked) {
          this.setIndeterminateChecked(true);
        }

        if (this.checked) {
          this.setChecked(false, true);
        }
      } else {
        if (this.indeterminateChecked) {
          this.setIndeterminateChecked(false);
        }

        if (this.checked) {
          this.setChecked(false, true);
        }
      }
    }
  }

  refreshAllParentsExpandedRecursive(value): void {
    this.allParentsExpanded = value;
    if (this.sub) {
      this.sub.itemList.forEach(item => (item as TreeItemComponent).refreshAllParentsExpandedRecursive(value && this.expanded));
    }
  }

  detectChanges(): void {
    this.changeDetector.detectChanges();
  }

  expandSub(value: boolean): void {
    this.expanded = value;
    if (this.sub) {
      this.sub.itemList.forEach(item => (item as TreeItemComponent).refreshAllParentsExpandedRecursive(this.allParentsExpanded && value));
    }
    this.expandedChange.emit(value);
  }

  /*
   * Gestion de eventos
   */

  handleCheckboxChange(checked: boolean): void {
    this.setChecked(checked, false);
    if (this.inheritedExpandedFirstLevel && checked && !this.expanded) {
      this.expandSub(true);
    }
  }

  handleIndeterminateCheckedChange(indeterminate: boolean): void {
    this.indeterminateCheckedChange.emit(indeterminate);
  }

  handleItemFocus(value, condition=true): void {
    if (condition) {
      this.isFocus = value;
    }
  }

  handleItemHover(value, condition=true): void {
    if (condition) {
      this.isHover = value;
    }
  }

  handleArrowUp(event: Event, condition=true): void {
    if (condition) {
      event.preventDefault();
      this.quitFocus.emit({
        nextElement: 'previous',
        currentItem: this
      });
    }
  }

  handleHome(event: Event, condition=true): void {
    if (condition) {
      event.preventDefault();
      this.quitFocus.emit({
        nextElement: 'first',
        currentItem: this
      });
    }
  }

  handleEnd(event: Event, condition=true): void {
    if (condition) {
      event.preventDefault();
      this.quitFocus.emit({
        nextElement: 'last',
        currentItem: this
      });
    }
  }

  handleArrowDown(event: Event, condition=true): void {
    if (condition) {
      event.preventDefault();
      this.quitFocus.emit({
        nextElement: 'next',
        currentItem: this
      });
    }
  }

  handleArrowRight(event: Event, condition=true): void {
    if (condition && this.sub && this.sub.itemList?.length > 0) {
      event.preventDefault();
      if (this.expanded) {
        this.quitFocus.emit({
          nextElement: 'firstChild',
          currentItem: this
        });
      } else {
        this.expandSub(true);
      }
    }
  }

  handleArrowLeft(event: Event, condition=true): void {
    if (condition) {
      event.preventDefault();
      if (this.expanded) {
        this.expandSub(false);
      } else if (this.subLevel > 0) {
        this.quitFocus.emit({
          nextElement: 'parent',
          currentItem: this
        });
      }
    }
  }


  /*
   * Setters and getters
   */

  setChecked(checked: boolean, ignoreInTree: boolean, emitEvent = true): void {
    if (!this.disabled) {
      this.checked = checked;
    }

    if (!this.inheritedDecoupleChildFromParent && this.sub && this.sub.itemList && !this.indeterminateChecked) {
      this.sub.itemList.forEach(item => {
        (item as TreeItemComponent).setIndeterminateChecked(false);
        (item as TreeItemComponent).setChecked(checked, true)
      });
    }

    if (emitEvent) {
      this.checkedChange.emit(checked);
    }

    if (!ignoreInTree) {
      this.checkedChangeForTree.emit(checked);
    }
  }

  setIndeterminateChecked(indeterminate: boolean): void {
    if (!this.disabled) {
      this.indeterminateChecked = indeterminate;
      this.indeterminateCheckedChange.emit(indeterminate);
    }
  }

  setSubLevel(subLevel: number): void {
    this.subLevel = subLevel;
    if (this.sub && this.sub.itemList) {
      this.sub.itemList.forEach(item => (item as TreeItemComponent).setSubLevel(subLevel + 1));
    }
  }

  setOrderRecursively(order: number): number {
    this.orderInTree = order;
    let newOrder = order + 1;
    if (this.sub && this.sub.itemList) {
      this.sub.itemList.forEach(item => {
        newOrder = (item as TreeItemComponent).setOrderRecursively(newOrder);
      });
    }
    return newOrder;
  }

  setDefaultId(defaultId: string): void {
    this.defaultId = defaultId;
    this.setDefaultIdRecursive();
  }

  setDefaultIdRecursive(): void {
    if (this.sub && this.sub.itemList) {
      this.sub.itemList.forEach((item, index) => {
        (item as TreeItemComponent).setDefaultId(`sub-${this.getId()}-${index}`);
      });
    }
  }

  getId(): string {
    return this.id ? this.id : this.defaultId;
  }

  isHidden(): boolean {
    return !this.inheritedMatchesSearch && (!this.sub || this.areAllChildrenHidden());
  }

  areAllChildrenHidden(): boolean {
    const children = this.sub.itemList;
    return children.findIndex(child => !(child as TreeItemComponent).isHidden()) < 0;
  }

  areAllChildrenChecked(): boolean {
    const children = this.sub.itemList;
    return children.findIndex(child => !(child as TreeItemComponent).checked) < 0;
  }

  hasChildrenCheckedOrIndeterminate(): boolean {
    const children = this.sub.itemList;
    return children.findIndex(child => (child as TreeItemComponent).checked || (child as TreeItemComponent).indeterminateChecked) >= 0;
  }

  matchesText(value: string): boolean {
    let matches: boolean;
    if (this.allParentsExpanded && !this.isHidden()) {
      matches = this.checkbox?.matchesText(value);
    } else {
      const itemText = this.hiddenWrapper.nativeElement.textContent;
      matches = SearchUtils.containsAnyWordFrom(itemText, value);
    }
    this._matchesValidText = value ? matches : false;
    return matches;
  }

  getKey(): string {
    return this.treeItemKey;
  }

  getParentKey(): string {
    let parentKey: string;
    let parentElement = this.element.nativeElement.parentElement;
    while (parentElement && parentElement.tagName !== 'BODY' && parentElement.tagName !== 'DESY-TREE-ITEM' && parentElement.tagName !== 'DESY-TREE') {
      parentElement = parentElement.parentElement;
    }

    if (parentElement.tagName === 'BODY') {
      throw new Error('Parent for tree-item not found');
    }

    if (parentElement.tagName === 'DESY-TREE') {
      parentKey = 'root';
    }

    if (parentElement.tagName === 'DESY-TREE-ITEM') {
      parentKey = parentElement.getAttribute('desy-tree-item-key');
    }

    return parentKey;
  }

  isActive(): boolean {
    return this.active || this._matchesValidText;
  }

  /*
   * Métodos privados
   */

  /**
   * Genera una clave única para el componente
   * @private
   */
  private static generateStaticItemKey(): string {
    const key = 'tree-item-key-' + TreeItemComponent._treeItemKeySuffix;
    TreeItemComponent._treeItemKeySuffix++;
    return key;
  }
}
