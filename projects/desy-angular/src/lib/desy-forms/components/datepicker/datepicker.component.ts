import {
  AfterViewInit,
  Component,
  ContentChildren,
  DoCheck,
  ElementRef,
  forwardRef,
  Input,
  ViewChild
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { ErrorMessageComponent } from '../error-message/error-message.component';
import { FormFieldComponent } from '../form-field/form-field.component';

@Component({
  selector: 'desy-datepicker',
  templateUrl: './datepicker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatepickerComponent),
      multi: true
    }
  ]
})
export class DatepickerComponent extends FormFieldComponent<any> implements AfterViewInit, DoCheck {

  @Input() containerClasses: string;
  @Input() standalone: boolean = false;
  @Input() formGroupClasses: string;
  @Input() id: string;
  @Input() name: string;
  @Input() type: string;
  @Input() inputmode: string;
  @Input() describedBy: string;
  @Input() errorId: string;
  @Input() classes: string;
  @Input() autocomplete: string;
  @Input() pattern: string;
  @Input() placeholder: string;
  @Input() attributes: string;
  @Input() dropdownClasses: string;

  @ViewChild('calendar', { static: true }) calendar: ElementRef;
  
  @DesyContentChild()
  @ContentChildren(ErrorMessageComponent) errorMessage: ErrorMessageComponent;

  constructor() { super() }

  ngAfterViewInit() {
    const inputElement = this.calendar.nativeElement.getElementsByTagName('input')[0];
    if (inputElement && !this.standalone) {
      const dropdownId = inputElement.getAttribute('id');
      const datepickerDropdown = inputElement.parentNode.querySelector('desy-dropdown button');
      let isOpen = false;

      const focusCalendar = () => {
        const inputInitialValue = inputElement.value;
        requestAnimationFrame(() => {
          const datepickerSelect = this.calendar.nativeElement.querySelector(':is(select)');
          const datepickerCalendar = this.calendar.nativeElement.querySelector(':is(calendar-date, calendar-multi, calendar-range)');
          const datepickerInitialValue = datepickerCalendar?.value;
          if (this.value && datepickerCalendar) {
            console.log(datepickerCalendar)
            datepickerCalendar.value = this.value;
            inputElement.value = this.value;
            // datepickerCalendar.value = this.value.split('/').reverse().join('-');
            datepickerCalendar.focus()
          } else {
            this.value = datepickerInitialValue
            inputElement.value = this.value;
            this.onChange(this.value);
          }
          // if (datepickerCalendar) {
          //   datepickerCalendar.focus();
          //   datepickerCalendar.addEventListener('change', (e: any) => {
          //     // inputElement.value = e.target.value;
          //   });
          // }
          if (datepickerSelect) {
            datepickerSelect.addEventListener('change', function () {
              const selectedValue = this.value;
              datepickerCalendar.min = selectedValue + "-01-01";
              datepickerCalendar.max = selectedValue + "-31-12";
              datepickerCalendar.focusedDate = datepickerCalendar.min;
            });
            datepickerSelect.focus();
          }
          const datepickerCancel = this.calendar.nativeElement.querySelector('#' + dropdownId + '-cancel');
          const datepickerSubmit = this.calendar.nativeElement.querySelector('#' + dropdownId + '-submit');
          if (datepickerCancel) {
            datepickerCancel.onclick = () => {
              inputElement.value = inputInitialValue;
              datepickerCalendar.tentative = "";
              datepickerCalendar.value = datepickerInitialValue;
              closeDropdown();
            };
          }
          if (datepickerSubmit) {
            datepickerSubmit.onclick = () => {
              this.value = datepickerCalendar?.value;
              inputElement.value = this.value;
              this.onChange(this.value);
              closeDropdown();
            };
          }
        });
      };

      const closeDropdown = () => {
        setTimeout(() => {
          isOpen = false;
          inputElement.focus();
        }, 300);
      };

      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          if (mutation.type === 'attributes' && mutation.attributeName === 'aria-expanded') {
            const currentExpandedValue = (mutation.target as Element).getAttribute('aria-expanded');
            if (currentExpandedValue !== isOpen.toString()) {
              const datepickerTimeout = setTimeout(focusCalendar, 0);
              isOpen = currentExpandedValue === 'true';
              if (!isOpen) {
                clearTimeout(datepickerTimeout);
                closeDropdown();
              }
            }
          }
        });
      });
      observer.observe(datepickerDropdown, { attributes: true, attributeFilter: ['aria-expanded'] });
    } else {
      const datepickerSelect = this.calendar.nativeElement.querySelector(':is(select)');
      const datepickerCalendar = this.calendar.nativeElement.querySelector(':is(calendar-date, calendar-multi, calendar-range)');
      if (datepickerSelect) {
        datepickerSelect.addEventListener('change', function () {
          const selectedValue = this.value;
          datepickerCalendar.min = selectedValue + "-01-01";
          datepickerCalendar.max = selectedValue + "-31-12";
          datepickerCalendar.focusedDate = datepickerCalendar.min;
        });
      }
    }
  }

  onInput(event: any) {
    this.value = event.target.value;
    this.onChange(this.value);
  }

  ngDoCheck(): void {
    if (this.attributes) {
      this.setAttributes();
    }
  }

  setAttributes() {
    const input = this.calendar.nativeElement.getElementsByTagName('input')[0];
    const attr = this.attributes;
    Object.keys(attr).forEach(key => {
      input?.setAttribute(key, attr[key]);
    });
  }

  writeValue(value: any): void {
    if (value) {
      this.value = value || '';
    } else {
      this.value = '';
    }
    this.onChange(this.value);
  }


}
