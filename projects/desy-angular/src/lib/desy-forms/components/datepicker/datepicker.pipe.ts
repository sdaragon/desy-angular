import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform { // IN PROGRESS

    transform(value: string): string {
        if (!value) return '';

        const fechas = value.split('/');

        const fechasConvertidas = fechas.map(fecha => {
            if (fecha.includes('-')) {
                const [year, month, day] = fecha.split('-');
                if (year && month && day) {
                    return `${day}/${month}/${year}`;
                }
            } else if (fecha.includes('/')) {
                const [day, month, year] = fecha.split('/');
                if (day && month && year) {
                    return `${day}/${month}/${year}`; 
                }
            }
            return fecha;
        });

        return fechasConvertidas.join('-');
    }
}