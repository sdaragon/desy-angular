import { Component, ElementRef, forwardRef, HostBinding, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormFieldComponent } from '../form-field/form-field.component';

@Component({
  selector: 'desy-file-upload',
  templateUrl: './file-upload.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => FileUploadComponent),
    }
  ]
})
export class FileUploadComponent extends FormFieldComponent<File> implements ControlValueAccessor, OnChanges {
  private file: File | null = null;
  @ViewChild('inputFile') inputFile: ElementRef;

  @Input() name: string;
  @Input() describedBy: string;
  @Input() classes: string;
  @Input() accept: string;

  @HostBinding('class.c-form-group') cfg = true;
  @HostBinding('class.c-form-group--error') cfgr: boolean;
  @Input() @HostBinding('class') formGroupClasses: any;

  constructor() {
    super();
  }
  /***
   * nos devuelve un array de file : File[]
   * pero solo nos guardamos el primero
   * solo dejamos seleccionar uno
   */
  onInput(event: any): void {
    const value = (event.target as HTMLInputElement).files;
    if (value && value.length > 0 && value[0]) {
      this.file = value[0];
    } else {
      this.file = null;
    }
    this.onChange(this.file);
    this.onTouch();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.cfgr = (this.hasErrorsMessage() || this.formGroupClasses === 'c-form-group--error');
  }

  writeValue(file: File | null): void {
    this.file = file;
    if (file === null && this.inputFile && this.inputFile.nativeElement) {
      this.inputFile.nativeElement.value = file;
    }
  }
}
