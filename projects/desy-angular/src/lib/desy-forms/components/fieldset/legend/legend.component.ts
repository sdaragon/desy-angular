import { AfterViewInit, Component, Input, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { AccessibilityAndContentRequiredComponent } from '../../../../shared/components';

@Component({
  selector: 'desy-legend',
  templateUrl: './legend.component.html'
})
export class LegendComponent extends AccessibilityAndContentRequiredComponent implements OnInit, AfterViewInit {

  @Input() classes: string;
  @Input() isPageHeading: boolean;
  @Input() headingLevel: number;

  @ViewChild('template', { static: true }) template: TemplateRef<any>;

  isHtml: boolean;
  
  constructor(private viewContainerRef: ViewContainerRef) {
    super();
  }
  
  ngOnInit(): void {
    this.isHtml = Boolean(this.html);
    this.viewContainerRef.createEmbeddedView(this.template);
  }
  
  ngAfterViewInit(): void {
    this.viewContainerRef.element.nativeElement.remove()
  }
}
