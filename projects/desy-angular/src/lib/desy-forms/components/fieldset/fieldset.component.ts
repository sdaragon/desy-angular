import {ChangeDetectorRef, Component, ContentChildren, Input, TemplateRef} from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { LegendComponent } from './legend/legend.component';
import { LegendData } from '../../interfaces';
import {ContentComponent} from '../../../desy-commons/components/content/content.component';
import {DesyContentChild} from '../../../shared/decorators/desy-content-child.decorator';

@Component({
  selector: 'desy-fieldset',
  templateUrl: './fieldset.component.html'
})
export class FieldsetComponent extends AccessibilityComponent {

  @Input() describedBy: string;
  @Input() errorId: string;

  /**
   * legenda, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir legendComponent
   * 2) incluir template
   * 3) incluir objeto legend (interfaz expuesta)
   * 4) incluir legendText - incluir un label con solo un texto
   */
  @Input() legendRef: TemplateRef<LegendComponent>;
  @Input() legendData: LegendData;
  @Input() legendText: string;

  @Input() classes: string;
  @Input() caller: TemplateRef<any>;

  @Input() id: string;
  @Input() headingLevel: number;


  @DesyContentChild()
  @ContentChildren(LegendComponent) legendComponent: LegendComponent;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  public detectChanges(): void {
    this.changeDetectorRef.detectChanges();
  }

}
