import {
  Component,
  ContentChildren,
  EventEmitter,
  forwardRef,
  Input,
  Output, QueryList
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormFieldComponent } from '../form-field/form-field.component';
import { ButtonComponent } from '../../../desy-buttons/components/button/button.component';

@Component({
  selector: 'desy-search-bar',
  templateUrl: './search-bar.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SearchBarComponent),
      multi: true
    }
  ]
})
export class SearchBarComponent extends FormFieldComponent<string>{  
  
  @ContentChildren(ButtonComponent) button: QueryList<ButtonComponent>;

  @Input() describedBy: string;
  @Input() classes: any;
  @Input() buttonClasses: string;
  @Input() placeholder: string;
  @Output() clickEvent = new EventEmitter();
  
  value = '';
  
  onClick(event: any): void {
    if (!this.disabled) {
      this.clickEvent.emit(event);
    }
  }

  hasButton(): boolean {
    return this.button && this.button.length > 0;
  }

  onInput(event: any): void {
    this.value = (event.target as HTMLInputElement).value;
    this.onChange(this.value);
  }
}
