import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {AccessibilityComponent} from '../../../shared/components';


@Component({
  selector: 'desy-label',
  templateUrl: './label.component.html'
})
export class LabelComponent extends AccessibilityComponent implements OnInit{

  @Input() id: string;
  @Input() classes: string;
  @Input() html: string;
  @Input() text: string;
  @Input() isPageHeading: boolean;
  @Input() headingLevel: number;
  @Input() for: string;

  isHtml: boolean;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    this.isHtml = Boolean(this.html);
  }

  public detectChanges(): void {
    this.changeDetectorRef.detectChanges();
  }
}
