import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputGroupDividerComponent } from './input-group-divider.component';

xdescribe('InputGroupDividerComponent', () => {
  let component: InputGroupDividerComponent;
  let fixture: ComponentFixture<InputGroupDividerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputGroupDividerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputGroupDividerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
