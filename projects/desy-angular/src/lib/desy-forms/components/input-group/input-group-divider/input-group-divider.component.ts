import {Component, forwardRef, TemplateRef, ViewChild} from '@angular/core';
import {InputGroupItemComponent} from '../input-group-item/input-group-item.component';
import {ItemDividerInputGroupData} from '../../../interfaces';

@Component({
  selector: 'desy-input-group-divider',
  templateUrl: './input-group-divider.component.html',
  providers: [{provide: InputGroupItemComponent, useExisting: forwardRef(() => InputGroupDividerComponent)}],
})
export class InputGroupDividerComponent extends InputGroupItemComponent implements ItemDividerInputGroupData {

  isSelect = false;
  divider = this;

  @ViewChild('dividerContent', { static: true }) dividerContent: TemplateRef<any>;

}
