import {
  AfterContentInit, AfterViewChecked,
  Component,
  ContentChildren, EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Optional, Output,
  QueryList,
  TemplateRef, ViewChild,
  OnChanges
} from '@angular/core';
import {FieldsetData, LegendData, ItemInputGroupData, LabelData, SelectItemData} from '../../interfaces';
import { ControlContainer, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { LegendComponent } from '../fieldset/legend/legend.component';
import { FormFieldComponent } from '../form-field/form-field.component';
import {InputGroupItemComponent} from './input-group-item/input-group-item.component';
import {DesyOnInputChange} from '../../../shared/decorators/desy-on-input-change.decorator';
import {DesyContentChild} from '../../../shared/decorators/desy-content-child.decorator';
import {FieldsetComponent} from '../fieldset/fieldset.component';
import {MakeHtmlListPipe} from '../../../shared/pipes/make-html-list.pipe';
import {InputGroupInputComponent} from './input-group-input/input-group-input.component';
import {InputGroupSelectComponent} from './input-group-select/input-group-select.component';
import {InputGroupDividerComponent} from './input-group-divider/input-group-divider.component';
import {StringUtils} from '../../../shared/utils/string-utils';

@Component({
  selector: 'desy-input-group',
  templateUrl: './input-group.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputGroupComponent),
      multi: true
    }
  ]
})
export class InputGroupComponent extends FormFieldComponent<any> implements AfterContentInit, AfterViewChecked, OnChanges {

  @Input() ngModelGroup: string;

  @DesyOnInputChange('onExternalChange')
  @Input() items: ItemInputGroupData[];
  @Output() itemsChange: EventEmitter<ItemInputGroupData[]> = new EventEmitter<ItemInputGroupData[]>();

  @DesyOnInputChange('onExternalChange')
  @Input() namePrefix: string;
  @Input() classes: string;

  //  form group class - asignar clases a la etiqueta desy-input-group
  @HostBinding('class.c-form-group') cfg = true;
  @HostBinding('class.c-form-group--error') cfgr: boolean;
  @Input() @HostBinding('class') formGroupClasses: any;

  @Input() errorMessage: string;
  @Input() declare id: string;

  /**
   * fieldset, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir objeto FieldsetData (interfaz expuesta)
   * 2) incluir template legend
   * 3) incluir legendData - incluir un fieldset con una legenda con los parametros pasados  (interfaz expuesta)
   * 4) incluir legendText - incluir un fieldset con un texto en la legenda
   */
  @Input() fieldsetData: FieldsetData;
  @Input() legendRef: TemplateRef<LegendComponent>;
  @Input() legendData: LegendData;
  @Input() legendText: string;

  @ContentChildren(InputGroupItemComponent) itemsComponents: QueryList<InputGroupItemComponent>;

  @DesyContentChild({ onSetCallbackName: 'overrideFieldsetParams'})
  @ContentChildren(FieldsetComponent) fieldsetComponent: FieldsetComponent;

  @DesyContentChild()
  @ContentChildren(LegendComponent) legendComponent: LegendComponent;

  @ViewChild('innerHtml', { static: true }) innerHtml: TemplateRef<any>;

  private contentInit = false;
  //public ogFormGroup: FormGroup;

  constructor(@Optional() public controlContainer: ControlContainer) {
    super();
  }

  ngOnChanges(): void {
    this.cfgr = (this.hasErrorsMessage() || this.formGroupClasses === 'c-form-group--error');
  }

  get ogFormGroup(): FormGroup {
    return this.controlContainer.control as FormGroup;
  }

  ngAfterContentInit(): void {
    this.contentInit = true;
  }

  ngAfterViewChecked(): void {
    const items = this.getItems();
  }

  writeValue(value: any): void {
    if (value) {
      this.value = value;
      const items = this.getItems();
      items.forEach(item => {
        const v = this.value[this.getItemName(item)];
        item.value = v ? v : null;
      });

      if (this.items) {
        this.itemsChange.emit(this.items);
      }
    }
    this.onChange(value);
  }

  onExternalChange(): void {
    if (!this.contentInit) {
      return;
    }

    if (!this.value) {
      this.value = {};
    }

    const items = this.getItems();
    items.forEach(item => this.value[this.getItemName(item)] = item.value);
    this.onChange(this.value);
  }

  onInternalChange(item: ItemInputGroupData, itemValue: any): void {
    const itemName = this.getItemName(item);
    if (!this.value) {
      this.value = {};
    }

    if (this.value[itemName] !== itemValue) {
      this.value[itemName] = itemValue;
      this.onChange(this.value);

      if (item instanceof InputGroupInputComponent || item instanceof InputGroupSelectComponent) {
        item.valueChange.emit(itemValue);
      }

      if (this.items) {
        item.value = itemValue;
        this.itemsChange.emit(this.items);
      }
    }
  }

  overrideFieldsetParams(fieldset: FieldsetComponent): void {
    fieldset.caller = this.innerHtml;
    fieldset.errorId = this.getErrorId();
    fieldset.describedBy = new MakeHtmlListPipe().transform([this.getHintId(), this.getErrorId()], null);
    fieldset.role = 'group';
    fieldset.detectChanges();
  }

  /*
   * Métodos para proporcionar datos
   */
  getItems(): ItemInputGroupData[] {
    return this.itemsComponents && this.itemsComponents.length ? this.itemsComponents.toArray() : this.items;
  }

  getItemLabelRef(item: ItemInputGroupData): TemplateRef<any> {
    return item instanceof InputGroupInputComponent || item instanceof InputGroupSelectComponent ? item.labelRef : null;
  }

  getItemLabelData(item: ItemInputGroupData): LabelData {
    return item.labelData ? item.labelData : { text: item.labelText };
  }

  hasFieldset(): boolean {
    return !!(this.hasFieldsetComponent() || this.hasLegendComponent() || this.legendRef || this.fieldsetData
      || (this.legendData && (this.legendData.text || this.legendData.html)) || this.legendText);
  }

  hasFieldsetComponent(): boolean {
    return !!this.fieldsetComponent;
  }

  hasLegendComponent(): boolean {
    return !!this.legendComponent;
  }

  getItemName(item: ItemInputGroupData): string {
    return this.namePrefix ? this.namePrefix + '-' + item.name : item.name;
  }

  getItemSelectOptions(item: ItemInputGroupData): SelectItemData[] {
    return item instanceof InputGroupSelectComponent ?
      item.selectItemComponentList.toArray() : item.selectItems;
  }

  getItemDividerTemplate(item: ItemInputGroupData): TemplateRef<any> {
    return item.divider instanceof InputGroupDividerComponent ? item.divider.dividerContent : null;
  }

  getItemDividerHtml(item: ItemInputGroupData): string {
    return item.divider.html ? item.divider.html : `<p>${ StringUtils.escapeHtml(item.divider.text) }</p>`;
  }

  isSelectItem(item: ItemInputGroupData): boolean {
    let hasSelectItems;
    if (item instanceof InputGroupSelectComponent) {
      hasSelectItems = item.selectItemComponentList && item.selectItemComponentList.length > 0;
    } else {
      hasSelectItems = item.selectItems;
    }
    return item.isSelect && hasSelectItems;
  }

  hasItemLabel(item: ItemInputGroupData): boolean {
    return !!((item.labelData && (item.labelData.text || item.labelData.html)) || item.labelText);
  }

  getItemClasses(item: ItemInputGroupData): string{
    return new MakeHtmlListPipe().transform(['mb-0', item.classes, this.hasErrorsMessage() ? 'border-alert-base ring-2 ring-alert-base' : null], null);
  }

  getItemAriaDescribedBy(item: ItemInputGroupData): string{
    return new MakeHtmlListPipe().transform([item.ariaDescribedBy, this.getErrorId(), this.getHintId()], null);
  }

}
