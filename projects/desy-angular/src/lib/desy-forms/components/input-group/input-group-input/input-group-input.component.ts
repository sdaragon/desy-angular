import {
  Component,
  ContentChildren,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {InputGroupItemComponent} from '../input-group-item/input-group-item.component';
import {LabelComponent} from '../../label/label.component';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';

@Component({
  selector: 'desy-input-group-input',
  templateUrl: './input-group-input.component.html',
  providers: [{provide: InputGroupItemComponent, useExisting: forwardRef(() => InputGroupInputComponent)}],
})
export class InputGroupInputComponent extends InputGroupItemComponent {

  isSelect = false;

  @Input() id?: string;
  @Input() name: string;
  @Input() value?: any;
  @Output() valueChange = new EventEmitter<any>();
  @Input() formGroupClasses?: string;

  @Input() type?: string;
  @Input() inputmode?: string;
  @Input() autocomplete?: string;
  @Input() placeholder?: string;
  @Input() pattern?: string;
  @Input() maxlength?: number;
  @Input() disabled?: boolean;

  @ViewChild('label', { static: true }) labelRef: TemplateRef<any>;

  @DesyContentChild({ onSetCallbackName: 'overrideLabelParams'})
  @ContentChildren(LabelComponent) labelComponent: LabelComponent;

  overrideLabelParams(labelComponent: LabelComponent): void {
    if (labelComponent && !labelComponent.for) {
      labelComponent.for = this.id;
    }
  }

}
