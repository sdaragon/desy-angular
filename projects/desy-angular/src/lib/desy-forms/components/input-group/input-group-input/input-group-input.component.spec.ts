import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputGroupInputComponent } from './input-group-input.component';

xdescribe('InputGroupInputComponent', () => {
  let component: InputGroupInputComponent;
  let fixture: ComponentFixture<InputGroupInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputGroupInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputGroupInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
