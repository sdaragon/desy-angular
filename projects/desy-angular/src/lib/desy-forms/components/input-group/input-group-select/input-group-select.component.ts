import {
  Component,
  ContentChildren,
  EventEmitter,
  forwardRef,
  Input,
  Output,
  QueryList,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {InputGroupItemComponent} from '../input-group-item/input-group-item.component';
import {OptionComponent} from '../../select/option/option.component';
import {DesyContentChild} from '../../../../shared/decorators/desy-content-child.decorator';
import {LabelComponent} from '../../label/label.component';

@Component({
  selector: 'desy-input-group-select',
  templateUrl: './input-group-select.component.html',
  providers: [{provide: InputGroupItemComponent, useExisting: forwardRef(() => InputGroupSelectComponent)}],
})
export class InputGroupSelectComponent extends InputGroupItemComponent {

  isSelect = true;

  @Input() id?: string;
  @Input() name: string;
  @Input() value?: any;
  @Output() valueChange = new EventEmitter<any>();
  @Input() formGroupClasses?: string;
  @Input() disabled?: boolean;

  @ViewChild('label', { static: true }) labelRef: TemplateRef<any>;
  @ContentChildren(OptionComponent) selectItemComponentList: QueryList<OptionComponent>;

  @DesyContentChild({ onSetCallbackName: 'overrideLabelParams'})
  @ContentChildren(LabelComponent) labelComponent: LabelComponent;

  overrideLabelParams(labelComponent: LabelComponent): void {
    if (labelComponent && !labelComponent.for) {
      labelComponent.for = this.id;
    }
  }
}
