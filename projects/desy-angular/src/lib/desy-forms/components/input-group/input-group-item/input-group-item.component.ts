import { Component, Input } from '@angular/core';
import { ItemInputGroupData } from '../../../interfaces';

@Component({
  selector: 'desy-input-group-item',
  template: ''
})
export class InputGroupItemComponent implements ItemInputGroupData {
  @Input() classes?: string;
  @Input() role?: string;
  @Input() ariaLabel?: string;
  @Input() ariaDescribedBy?: string;
  @Input() ariaLabelledBy?: string;
  @Input() ariaHidden?: string;
  @Input() ariaDisabled?: string;
  @Input() ariaControls?: string;
  @Input() ariaCurrent?: string;
  @Input() ariaLive?: string;
  @Input() ariaExpanded?: string;
  @Input() ariaErrorMessage?: string;
  @Input() ariaHasPopup?: string;
  @Input() tabindex?: string;
}
