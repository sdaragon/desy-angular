import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputGroupItemComponent } from './input-group-item.component';

xdescribe('InputGroupItemComponent', () => {
  let component: InputGroupItemComponent;
  let fixture: ComponentFixture<InputGroupItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputGroupItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputGroupItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
