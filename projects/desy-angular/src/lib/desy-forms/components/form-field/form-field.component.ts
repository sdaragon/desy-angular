import {
  Component,
  ContentChildren,
  HostListener,
  Input,
  TemplateRef
} from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { AccessibilityComponent } from '../../../shared/components';
import { LabelComponent } from '../label/label.component';
import { HintComponent } from '../hint/hint.component';
import { ErrorMessageComponent } from '../error-message/error-message.component';
import { LabelData, HintData, ErrorMessageData } from '../../interfaces';
import {DesyContentChild} from '../../../shared/decorators/desy-content-child.decorator';


@Component({
  selector: 'desy-form-field',
  template: ''
})
export class FormFieldComponent<T>  extends AccessibilityComponent implements ControlValueAccessor {

  @Input() id: string;
  @Input() disabled: boolean;
  value: T;

  /*
   * label, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir componente
   * 2) incluir template
   * 3) incluir objeto label (interfaz expuesta)
   * 4) incluir legendText - incluir un label con solo un texto
   */
  @DesyContentChild({ onSetCallbackName: 'overrideLabelParams', onDeleteCallbackName: 'onDeleteLabel'})
  @ContentChildren(LabelComponent) labelComponent: LabelComponent;
  @Input() labelRef: TemplateRef<LabelComponent> | null | undefined;
  @Input() labelData: LabelData | null | undefined;
  @Input() labelText: string | null | undefined;

  /*
   * hint, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir componente
   * 2) incluir template
   * 3) incluir objeto label (interfaz expuesta)
   * 4) incluir hintText - incluir un hint con solo un texto
   */
  @DesyContentChild({ onSetCallbackName: 'overrideHintParams', onDeleteCallbackName: 'onDeleteHint'})
  @ContentChildren(HintComponent) hintComponent: HintComponent;
  @Input() hintRef: TemplateRef<HintComponent> | null | undefined;
  @Input() hintData: HintData | null | undefined;
  @Input() hintText: string | null | undefined;

  /*
   * errorMessage, diferentes formas de implementar, enumeradas por prioridad
   * 1) incluir componente
   * 2) incluir template
   * 3) incluir objeto label (interfaz expuesta)
   * 4) incluir errorText - incluir un errorText con solo un texto
   */
  @DesyContentChild({ onSetCallbackName: 'overrideErrorMessageParams', onDeleteCallbackName: 'onDeleteErrorMessage'})
  @ContentChildren(ErrorMessageComponent) errorMessageComponent: ErrorMessageComponent;
  @Input() errorMessageRef: TemplateRef<ErrorMessageComponent> | null | undefined;
  @Input() errorMessageData: ErrorMessageData | null | undefined;
  @Input() errorMessageText: string | null | undefined;


  @HostListener('focusout')
  onblur(): void {
    // DESY-77 SI TOCAN EL COMPONENTE Y SALEN SIN MODIFICAR EL VALOR, QUE SE ACTUALICE TOUCHED
    this.onTouch();
  }


  /*
   * Implementación de los métodos del ControlValueAccessor
   */

  onChange = (_: any) => {};
  onTouch = () => { };

  onInput(value: T): void{
    this.value = value;
    this.onTouch();
    this.onChange(this.value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(value: T): void {
    if (value){
      this.value = value;
    }
    this.onChange(value);
  }


  /*
   * Funciones para recuperar valores comunes
   */

  getHintId(): string {
    return this.hasHint() ? this.id + '-hint' : null;
  }

  getErrorId(): string {
    return this.hasErrorsMessage() ? this.id + '-error' : null;
  }

  hasLabel(): boolean {
    return !!(this.hasLabelComponent() || this.labelText || this.labelRef ||
      (this.labelData && (this.labelData.text || this.labelData.html)));
  }

  hasHint(): boolean {
    return !!(this.hasHintComponent() || this.hintText || this.hintRef || (this.hintData && (this.hintData.text || this.hintData.html)));
  }

  hasErrorsMessage(): boolean {
    return !!(this.hasErrorMessageComponent() || this.errorMessageText || this.errorMessageRef ||
      (this.errorMessageData && (this.errorMessageData.text || this.errorMessageData.html)));
  }

  hasLabelComponent(): boolean {
    return !!this.labelComponent;
  }

  hasHintComponent(): boolean {
    return !!this.hintComponent;
  }

  hasErrorMessageComponent(): boolean {
    return !!this.errorMessageComponent;
  }
}
