import { Component, DoCheck, ElementRef, HostBinding, Input, OnChanges, Renderer2, SimpleChanges, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormFieldComponent } from '../form-field/form-field.component';

@Component({
  selector: 'desy-input',
  templateUrl: './input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})
export class InputComponent extends FormFieldComponent<any> implements OnChanges, DoCheck {

  @Input() name: string;
  _type: string;
  @Input() 
  set type(value: string) {
    this._type = value;
    this.renderer.removeAttribute(this.el.nativeElement, 'type'); // This is for avoid tailwind type css that come from base(desy-html). To allow the use of type="" and not only [type]
  }
  get type(){
    return this._type
  }
  @Input() describedBy: string;
  @Input() pattern: string;
  @Input() inputmode: string;
  @Input() placeholder: string;

  //  form group class - asignar clases a la etiqueta desy-input
  @HostBinding('class.c-form-group') cfg = true;
  @HostBinding('class.c-form-group--error') cfgr: boolean;
  @Input() @HostBinding('class') formGroupClasses: any;

  @Input() classes: string;
  @Input() autocomplete: string;
  @Input() maxlength: number;
  @Input() errorId: string;
  @Input() attributes: any;
  
  constructor(private el: ElementRef, private renderer: Renderer2) { 
    super()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.cfgr = (this.hasErrorsMessage() || this.formGroupClasses === 'c-form-group--error');
  }

  ngDoCheck(): void {
    if (this.attributes) {
      this.setAttributes();
    }
  }

  setAttributes() {
    const input = document.getElementById(this.id ? this.id : 'input');
    const attr = this.attributes;
    Object.keys(attr).forEach(key => {
      input?.setAttribute(key, attr[key]);
    });
  }

  getErrorId(): string {
    return this.errorId ? this.errorId : super.getErrorId();
  }

  writeValue(value: any): void {
    if (value) {
      this.value = value || '';
    } else {
      this.value = '';
    }
    this.onChange(this.value);
  }

  onInput(event: InputEvent): void {
    if (event.data != '.') {
      this.value = (event.target as HTMLInputElement).value;
      this.onChange(this.value);
    }
  }
}
