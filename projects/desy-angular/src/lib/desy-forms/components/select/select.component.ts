import { AfterContentInit, Component, ContentChildren, HostBinding, Input, OnChanges, QueryList, SimpleChanges, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { SelectItemData } from '../../interfaces';
import { FormFieldComponent } from '../form-field/form-field.component';
import { OptionGroupComponent } from './option-group/option-group.component';
import { SelectItemComponent } from './select-item/select-item.component';

@Component({
  selector: 'desy-select',
  templateUrl: './select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent extends FormFieldComponent<any> implements AfterContentInit, OnChanges {

  @Input() name: string;
  @Input() items: SelectItemData[];
  @Input() describedBy?: string;


  //  form group class - asignar clases a la etiqueta desy-select
  @HostBinding('class.c-form-group') cfg = true;
  @HostBinding('class.c-form-group--error') cfgr: boolean;
  @Input() @HostBinding('class') formGroupClasses: any;

  @Input() classes: string;

  @ContentChildren(SelectItemComponent) itemComponents: QueryList<SelectItemComponent>;
  contentInit = false;

  ngOnChanges(changes: SimpleChanges): void {
    this.cfgr = (this.hasErrorsMessage() || this.formGroupClasses === 'c-form-group--error');
  }

  ngAfterContentInit(): void {
    this.contentInit = true;
    if (!this.value) {
      const items = this.getItems();
      const itemSelected = items?.find(item => item.selected);
      if (itemSelected) {
        setTimeout(() => this.writeValue(itemSelected.value));
      }
    }
  }

  writeValue(value: any): void {
    this.value = value;
    this.onChange(value);
  }

  getItems(): SelectItemData[] {
    const items =  (this.itemComponents.length > 0) ? this.itemComponents.toArray() : this.items;
    return items;
  }

  isItemSelected(item: SelectItemData): boolean {
    return this.value ? this.value == item.value : item.selected;
  }

  isOptionGroup(item: SelectItemData): boolean {
    return item instanceof OptionGroupComponent;
  }

  onInput(event: Event): void {
    let value = (event.target as HTMLSelectElement).value
    this.value = !isNaN(Number(value)) && value != null && value != "" && value != undefined ? Number(value) : value;
    this.onChange(this.value);
  }

}
