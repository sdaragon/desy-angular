import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-select-item',
  templateUrl: './select-item.component.html'
})
export class SelectItemComponent extends ContentBaseComponent {

  @Input() disabled: boolean;
}
