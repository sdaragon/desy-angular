import {Component, forwardRef, Input} from '@angular/core';
import {SelectItemData} from '../../../interfaces';
import {SelectItemComponent} from '../select-item/select-item.component';

@Component({
  selector: 'desy-option',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>',
  providers: [{provide: SelectItemComponent, useExisting: forwardRef(() => OptionComponent)}],
})
export class OptionComponent extends SelectItemComponent implements SelectItemData {

  @Input() value: string | number;
  @Input() selected: boolean;
  @Input() hidden: boolean;

}
