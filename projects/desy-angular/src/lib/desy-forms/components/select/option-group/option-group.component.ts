import {Component, ContentChildren, forwardRef, Input, QueryList} from '@angular/core';
import {SelectItemComponent} from '../select-item/select-item.component';
import {OptionComponent} from '../option/option.component';

@Component({
  selector: 'desy-option-group',
  templateUrl: './option-group.component.html',
  providers: [{provide: SelectItemComponent, useExisting: forwardRef(() => OptionGroupComponent)}],
})
export class OptionGroupComponent extends SelectItemComponent {

  @Input() label: string;

  @ContentChildren(OptionComponent) items: QueryList<OptionComponent>;
}
