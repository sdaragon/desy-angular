import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateDrivenWrapperComponent } from './template-driven-wrapper.component';

xdescribe('TemplateDrivenWrapperComponent', () => {
  let component: TemplateDrivenWrapperComponent;
  let fixture: ComponentFixture<TemplateDrivenWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemplateDrivenWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateDrivenWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
