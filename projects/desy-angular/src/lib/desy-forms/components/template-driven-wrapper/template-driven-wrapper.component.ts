import { Component } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'desy-template-driven-wrapper',
  templateUrl: './template-driven-wrapper.component.html',
  providers: [ { provide: ControlContainer, useExisting: NgForm,  } ]
})
export class TemplateDrivenWrapperComponent {
}
