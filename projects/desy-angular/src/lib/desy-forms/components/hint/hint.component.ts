import {ChangeDetectorRef, Component, Input} from '@angular/core';
import {AccessibilityComponent} from '../../../shared/components';

@Component({
  selector: 'desy-hint',
  templateUrl: './hint.component.html',
})
export class HintComponent extends AccessibilityComponent  {

  @Input() id: string;
  @Input() classes: string;
  @Input() html: string;
  @Input() text: string;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  public detectChanges(): void {
    this.changeDetectorRef.detectChanges();
  }
}
