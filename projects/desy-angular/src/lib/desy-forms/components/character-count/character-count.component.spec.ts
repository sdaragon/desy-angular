import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterCountComponent } from './character-count.component';

xdescribe('CharacterCountComponent', () => {
  let component: CharacterCountComponent;
  let fixture: ComponentFixture<CharacterCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
