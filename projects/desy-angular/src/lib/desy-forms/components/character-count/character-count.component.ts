import {
  AfterViewInit,
  Component,
  ContentChildren,
  forwardRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { ErrorMessageComponent } from '../error-message/error-message.component';
import { FormFieldComponent } from '../form-field/form-field.component';
import { HintComponent } from '../hint/hint.component';
import { LabelComponent } from '../label/label.component';
import { TextareaComponent } from '../textarea/textarea.component';

@Component({
  selector: 'desy-character-count',
  templateUrl: './character-count.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CharacterCountComponent),
      multi: true
    }
  ]
})
export class CharacterCountComponent extends FormFieldComponent<string> implements AfterViewInit, OnChanges {

  @ViewChild(TextareaComponent) textarea;

  @Input() name: string;
  @Input() type: string;
  @Input() rows: number;
  @Input() placeholder: string;
  @Input() maxlength: number;
  @Input() countbbdd: boolean;
  @Input() maxwords: number;
  @Input() threshold: number;
  @Input() formGroupClasses: string;
  @Input() countMessageClasses: string;
  @Input() classes: string;
  
  @DesyContentChild()
  @ContentChildren(LabelComponent) labelComponent: LabelComponent;
  @DesyContentChild()
  @ContentChildren(HintComponent) hintComponent: HintComponent;
  @DesyContentChild()
  @ContentChildren(ErrorMessageComponent) errorMessageComponent: ErrorMessageComponent;
  
  textareaElement: HTMLTextAreaElement;
  displayCountMessage: boolean;
  remaining: number;

  ngAfterViewInit(): void {
    this.textareaElement = (this.textarea.textareaElement.nativeElement as HTMLTextAreaElement);
    this.textarea.registerInputTransform(this.inputTransform.bind(this));
    this.registerOnChange(this.onChange);
    this.registerOnTouched(this.onTouch);
    this.textarea.setDisabledState(this.disabled);
    setTimeout(() => this.textarea.writeValue(this.value), 50);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.textarea) {
      setTimeout(() => this.textarea.ngOnChanges(changes), 50);
    }
  }

  inputTransform(value: string): string {
    if (value) {
      if (this.maxlength) {
        let length = this.calculateLenght(value);
        this.displayCountMessage = !this.threshold || length > (this.maxlength * this.threshold / 100);
        while (length > this.maxlength) {
          value = value.substring(0, value.length - 1);
          length = this.calculateLenght(value);
        }
        this.remaining = this.maxlength - length;
      } else {
        const words = value.match(/[\wáéíóúÁÉÍÓÚüÜñÑ]+/g) || [];
        this.displayCountMessage = !this.threshold || (words.length > this.maxwords * this.threshold / 100);
        while ( words.length > this.maxwords) {
          value = value.substring(0, value.lastIndexOf(words.pop()));
        }
        this.remaining = this.maxwords - words.length;
      }
    } else {
      value = '';
      this.displayCountMessage = !this.threshold;
      this.remaining = this.maxlength ? this.maxlength : this.maxwords;
    }

    this.textareaElement.value = value;
    return value;
  }

  calculateLenght(value: string): number {
    let length = value.length;
    if (this.countbbdd && value) {
      const specialChars = value.match(/[^A-z0-9_\s.,:;]/g);
      length += specialChars ? specialChars.length : 0;
    }
    return length;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    if (this.textarea) {
      this.textarea.registerOnChange(fn);
      this.onChange = (_: any) => {};
    }
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
    if (this.textarea) {
      this.textarea.registerOnTouched(fn);
      this.onTouch = () => { };
    }
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    if (this.textarea) {
      this.textarea.setDisabledState(isDisabled);
    }
  }

  writeValue(value: string): void {
    this.value = value;
    if (this.textarea) {
      this.textarea.writeValue(value);
    }
  }

}
