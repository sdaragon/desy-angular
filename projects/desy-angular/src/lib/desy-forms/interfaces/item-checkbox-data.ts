import { TemplateRef } from '@angular/core';
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { HintData } from './hint-data';
import { LabelData } from './label-data';

export interface ItemCheckboxData extends AccessibilityData {

  text?: string;
  html?: string;
  id?: string;
  value: any;

  labelData?: LabelData;
  hintData?: HintData;
  hintText?: string;

  name?: string;
  checked?: boolean;
  conditional?: boolean;
  conditionalHtml?: TemplateRef<any>;
  disabled?: boolean;
  isIndeterminate?: boolean;
  indeterminateChecked?: boolean;
  classes?: string;
}
