import { TemplateRef } from '@angular/core';
import { ItemRadioData } from './item-radio-data';

export interface ItemRadioWithTemplateData extends ItemRadioData {

    conditionalHtml: TemplateRef<any>; // implements ConditionDirective

}
