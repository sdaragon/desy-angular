import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { ItemDividerInputGroupData } from './item-divider-input-group-data';
import { LabelData } from './label-data';
import { SelectItemData } from './select-item-data';

export interface ItemInputGroupData extends AccessibilityData {
  id?: string;
  type?: string;
  inputmode?: string;
  name?: string;
  labelText?: string;
  labelData?: LabelData;
  value?: any;
  autocomplete?: string;
  placeholder?: string;
  pattern?: string;
  classes?: string;
  formGroupClasses?: string;
  isSelect?: boolean;
  selectItems?: SelectItemData[];
  divider?: ItemDividerInputGroupData;
  maxlength?: number;
  disabled?: boolean;
}
