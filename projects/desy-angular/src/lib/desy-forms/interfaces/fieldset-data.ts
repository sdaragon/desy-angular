
import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { LegendData } from './legend-data';

export interface FieldsetData extends AccessibilityData {
    legend: LegendData;
    id?: string;
    classes?: string;
    describedBy?: string;
    errorId?: string;
    headingLevel?: number;
}
