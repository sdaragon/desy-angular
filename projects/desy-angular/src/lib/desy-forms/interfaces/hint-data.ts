import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface HintData extends AccessibilityData {
  id?: string;
  classes?: string;
  text?: string;
  html?: string;
}
