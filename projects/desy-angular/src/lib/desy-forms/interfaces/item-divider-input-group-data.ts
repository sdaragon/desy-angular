export interface ItemDividerInputGroupData {
  text?: string;
  html?: string;
  classes?: string;
}
