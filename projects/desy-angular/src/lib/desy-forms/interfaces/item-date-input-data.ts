import { AccessibilityData } from '../../shared/interfaces/accessibility-data';
import { ItemDividerDateInputData } from './item-divider-date-input-data';
import { LabelData } from './label-data';

export interface ItemDateInputData extends AccessibilityData {
  id?: string;
  name?: string;
  labelText?: string;
  labelData?: LabelData;
  disabled?: boolean;
  hasErrors?: boolean;
  value?: any;
  autocomplete?: string;
  pattern?: string;
  classes?: string;
  divider?: ItemDividerDateInputData
  maxlength?: number;
  placeholder?: string;
}
