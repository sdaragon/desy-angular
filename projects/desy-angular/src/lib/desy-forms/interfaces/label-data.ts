import { AccessibilityData } from '../../shared/interfaces/accessibility-data';


export interface LabelData extends AccessibilityData {
  text?: string;
  html?: string;
  classes?: string;
  id?: string;
  for?: string;
  isPageHeading?: boolean;
  headingLevel?: number;
}
