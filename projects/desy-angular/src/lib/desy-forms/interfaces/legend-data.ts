export interface LegendData {
    text?: string;
    html?: string;
    classes?: string;
    isPageHeading?: boolean;
}
