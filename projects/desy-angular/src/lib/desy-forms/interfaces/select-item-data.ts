import { AccessibilityData } from "../../shared/interfaces/accessibility-data";

export interface SelectItemData extends AccessibilityData {
  value?: string | number;
  text?: any;
  selected?: boolean;
  disabled?: boolean;
  hidden?: boolean;
  label?: string;
  describedBy?: string;
  items?: SelectItemData[];

}
