import { AccessibilityData } from '../../shared/interfaces/accessibility-data';


export interface ErrorMessageData extends AccessibilityData {
  id?: string;
  classes?: string;
  text?: string;
  html?: string;
  visuallyHiddenText?: string;
}
