export interface ItemDividerDateInputData {
    text?: string;
    html?: string;
    classes?: string;
  }
  