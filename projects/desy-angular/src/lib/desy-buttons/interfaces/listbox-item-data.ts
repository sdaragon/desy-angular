import { AccessibilityData } from '../../shared/interfaces/accessibility-data';

export interface ListboxItemData extends AccessibilityData {
  text?: string;
  html?: string;
  id?: string;
  active?: boolean;
  classes?: string;
  title?: string;
}
