export interface ListboxLabelData {
  text?: string;
  html?: string;
  classes?: string;
}
