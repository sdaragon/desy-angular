import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxTippyModule } from 'ngx-tippy-wrapper';
import { SharedModule } from '../shared/shared.module';

// componentes
import { SharedExternalModule } from '../shared/sharedExternal.module';
import { ButtonLoaderComponent } from './components/button-loader/button-loader.component';
import { ButtonComponent } from './components/button/button.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { ListboxItemComponent } from './components/listbox/listbox-item/listbox-item.component';
import { ListboxLabelComponent } from './components/listbox/listbox-label/listbox-label.component';
import { ListboxComponent } from './components/listbox/listbox.component';
import { PillComponent } from './components/pill/pill.component';
import { ToggleOffStateComponent } from './components/toggle/toggle-off-state/toggle-off-state.component';
import { ToggleOnStateComponent } from './components/toggle/toggle-on-state/toggle-on-state.component';
import { ToggleComponent } from './components/toggle/toggle.component';


@NgModule({
  declarations: [
    ButtonComponent,
    ButtonLoaderComponent,
    DropdownComponent,
    ListboxComponent,
    PillComponent,

    ListboxItemComponent,
    ListboxLabelComponent,
    ToggleComponent,
    ToggleOnStateComponent,
    ToggleOffStateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgxTippyModule,
    SharedExternalModule,
  ],
  exports: [
    ButtonComponent,
    ButtonLoaderComponent,
    DropdownComponent,
    ListboxComponent,
    PillComponent,

    ListboxItemComponent,
    ListboxLabelComponent,
    ToggleComponent,
    ToggleOnStateComponent,
    ToggleOffStateComponent
  ]
})
export class DesyButtonsModule { }
