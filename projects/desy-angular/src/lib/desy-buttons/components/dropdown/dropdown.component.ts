import {
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { inlinePositioning } from 'tippy.js';
import { ContentComponent } from '../../../desy-commons/components/content/content.component';
import { AccessibilityAndContentRequiredComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { FocusUtils } from '../../../shared/utils/focus-utils';

@Component({
  selector: 'desy-dropdown',
  templateUrl: './dropdown.component.html'
})
export class DropdownComponent extends AccessibilityAndContentRequiredComponent implements OnInit, OnChanges {

  static readonly KEY_CODE_ESC = 'Escape';

  @ViewChild('dropdownContent', { read: ElementRef, static: true }) dropdownContent: ElementRef;
  @ViewChild('dropdownBtn', { read: ElementRef, static: true }) dropdownBtn: ElementRef;

  @Input() id: string;
  @Input() disabled: boolean;
  @Input() hiddenText: string;
  @Input() @HostBinding('class') classesContainer: any;
  @Input() classesTooltip: string;
  @Input() caller: TemplateRef<any>;
  @Input() classes: string;

  @DesyContentChild()
  @ContentChildren(ContentComponent) contentComponent: ContentComponent;

  @Output() clickEvent = new EventEmitter();

  @HostListener('click', ['$event.target'])
  detectClickToFocusBtn(event: any) {
    if (event && event.href) {
      document.dispatchEvent(new KeyboardEvent("keydown", {
        'key': 'Escape'
      }));
      setTimeout(() => {
        this.dropdownBtn.nativeElement.focus()
      }, 100);
    }
  }
  isOpen = false;
  clickOutsideEnabled = false;

  tippyProperties: any;

  ngOnInit(): any {
    let contentTooltip: HTMLElement;
    if (this.dropdownContent && this.dropdownContent.nativeElement) {
      contentTooltip = this.dropdownContent.nativeElement as HTMLElement;
      this.tippyProperties = {
        placement: 'bottom-start',
        inlinePositioning: true,
        content: contentTooltip,
        allowHTML: true, // Make sure you are sanitizing any user data if rendering HTML to prevent XSS attacks.
        trigger: 'click',
        hideOnClick: true,
        interactive: true,
        arrow: false,
        offset: [0, -10],
        theme: '',
        plugins: [
          {
            name: 'hideOnPopperBlur',
            defaultValue: true,
            fn(instance) {
              return {
                onCreate() {
                  instance.popper.addEventListener('focusout', (event) => {
                    if (
                      instance.props.hideOnPopperBlur &&
                      event.relatedTarget &&
                      !instance.popper.contains(event.relatedTarget)
                    ) {
                      instance.hide();
                    }
                  });
                },
              };
            }
          }, {
            name: 'hideOnEsc',
            defaultValue: true,
            fn({ hide }): any {

              function onKeyDown(event: KeyboardEvent): void {
                if (event.key === DropdownComponent.KEY_CODE_ESC) {
                  hide();
                }
              }

              return {
                onShow(): void {
                  document.addEventListener('keydown', onKeyDown);
                },
                onHide(): void {
                  document.removeEventListener('keydown', onKeyDown);
                },
              };
            },
          },
          inlinePositioning
        ],
        role: false,
        aria: {
          content: 'auto',
        },
        onShown: (instance): void => {
          const firstFocusable = FocusUtils.getFirstFocusableElement(contentTooltip);
          if (firstFocusable) {
            firstFocusable.focus();
          }
          this.isOpen = true;
        },
        onHide: (instance): void => {
          this.isOpen = false;
        }
      };
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.classesContainer = this.classesContainer ? this.classesContainer : 'relative block';
  }

  onClick(event: any): void {
    if (!this.isDisabled()) {
      this.clickEvent.emit(event);
    }
  }

  setOpen(isOpen: boolean): void {
    this.isOpen = isOpen;
  }

  isDisabled(): boolean {
    return this.disabled ? true : null;
  }
}
