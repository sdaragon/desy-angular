import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { AccessibilityAndContentRequiredComponent } from '../../../shared/components';

@Component({
  selector: 'desy-pill',
  templateUrl: './pill.component.html'
})
export class PillComponent  extends AccessibilityAndContentRequiredComponent implements OnInit{

  public static readonly TYPE_A: string = 'a';
  public static readonly TYPE_BUTTON: string = 'button';
  public static readonly TYPE_SPAN: string = 'span';

  @Input() type: string; // 'a', 'button' o 'span'
  @Input() href: string;
  @Input() target: string;
  @Input() fragment: string;
  @Input() routerLink: string|any[];
  @Input() routerLinkActiveClasses: string|string[];
  @Input() classes: string;
  @Input() id: string;

  @Output() clickEvent = new EventEmitter();

  @HostBinding('attr.tabindex') readonly hostTabIndex = null; // Fix para evitar el tab-index agregado por routerLink en el host

  isHtml: boolean;

  ngOnInit(): void {
    this.isHtml = Boolean(this.html);
  }

  onClick(event: any): void {
    this.clickEvent.emit(event);
  }

  getType(): string {
    let type: string;
    if (this.type){
      type = this.type.toLocaleLowerCase();
    } else {
      if (this.href) {
        type = PillComponent.TYPE_A;
      } else {
        type = PillComponent.TYPE_SPAN;
      }
    }
    return type;
  }

  getClassNames(): string {
      let classNames = 'c-pill';
      if (this.classes) {
        classNames += ' ' + this.classes;
      }
      return classNames;
    }

  get staticElementTypeA(): string {
    return PillComponent.TYPE_A;
  }

  get staticElementTypeButton(): string {
    return PillComponent.TYPE_BUTTON;
  }

  get staticElementTypeSpan(): string {
    return PillComponent.TYPE_SPAN;
  }

}
