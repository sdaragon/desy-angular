import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ButtonComponent } from '../button/button.component';

@Component({
  selector: 'desy-button-loader',
  templateUrl: './button-loader.component.html'
})
export class ButtonLoaderComponent extends ButtonComponent implements OnInit {

  public static readonly STATE_LOADING: string = 'is-loading';
  public static readonly STATE_SUCCESS: string = 'is-success';

  stateLoading = ButtonLoaderComponent.STATE_LOADING;
  stateSuccess = ButtonLoaderComponent.STATE_SUCCESS;

  private static readonly DEFAULT_LOADER_TEXT: string = 'Acción en curso';
  private static readonly DEFAULT_SUCCESS_TEXT: string = 'Acción realizada con éxito';

  @HostBinding('attr.tabindex') readonly hostTabIndex = null; // Fix para evitar el tab-index agregado por routerLink en el host

  @Input() loaderText: string;
  @Input() loaderClasses: string;
  @Input() state: string;
  @Input() successText: string;

  declare isHtml: boolean;

  ngOnInit(): void {
    this.isHtml = Boolean(this.html);
  }

  getClassNames(): string {
    let classNames = 'c-button-loader relative';
    if (this.classes) {
      classNames += ' ' + this.classes;
    }
    if (this.disabled) {
      classNames += ' c-button-loader--disabled';
    }
    return classNames;
  }

  getSpinnerText(): string {
    let spinnerText: string = null;

    if (this.state === ButtonLoaderComponent.STATE_LOADING) {
      spinnerText = this.loaderText ? this.loaderText : ButtonLoaderComponent.DEFAULT_LOADER_TEXT;
    }

    return spinnerText;
  }


  getSuccessText(): string {
    let successText: string = null;

    if (this.state === ButtonLoaderComponent.STATE_SUCCESS) {
      successText = this.successText ? this.successText : ButtonLoaderComponent.DEFAULT_SUCCESS_TEXT;
    }

    return successText;
  }
}
