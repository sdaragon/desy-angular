import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { ContentBaseComponent } from '../../../shared/components';

@Component({
  selector: 'desy-button',
  templateUrl: './button.component.html'
})
export class ButtonComponent extends ContentBaseComponent implements OnInit {

  public static readonly ELEMENT_A: string = 'a';
  public static readonly ELEMENT_BUTTON: string = 'button';
  public static readonly ELEMENT_INPUT: string = 'input';


  @HostBinding('attr.tabindex') readonly hostTabIndex = null; // Fix para evitar el tab-index agregado por routerLink en el host

  @Input() id: string;
  @Input() classes: string;
  @Input() html: string;
  @Input() text: string;

  @Input() element: string; // 'a', 'button' o 'input'
  @Input() name: string;
  @Input() type: string;
  @Input() value: any;
  @Input() disabled: boolean;
  @Input() href: string;
  @Input() target: string;
  @Input() preventDoubleClick: boolean;
  @Input() fragment: string;
  @Input() routerLink: string|any[];
  @Input() routerLinkActiveClasses: string|string[];

  @Output() clickEvent = new EventEmitter();

  avoidingDoubleClick = false;
  isHtml: boolean;

  ngOnInit(): void {
    this.isHtml = Boolean(this.html);
  }

  onClick(event: any): void {
    if (this.preventDoubleClick) {
      if (!this.avoidingDoubleClick){
        this.avoidingDoubleClick = true;
        this.clickEvent.emit(event);
        setTimeout(() => {
          this.avoidingDoubleClick = false;
        }, 1000);
      } else {
        event.preventDefault(); // evitamos propagar el evento por ejemplo un submit en un formulario
      }
    } else {
      this.clickEvent.emit(event);
    }
  }

  getElement(): string {
    let element: string;
    if (this.element) {
      element = this.element.toLocaleLowerCase();
    } else {
      if (this.href) {
        element = ButtonComponent.ELEMENT_A;
      } else {
        element = ButtonComponent.ELEMENT_BUTTON;
      }
    }
    return element;
  }

  getClassNames(): string {
    let classNames = 'c-button';
    if (this.classes) {
      classNames += ' ' + this.classes;
    }
    if (this.disabled) {
      classNames += ' c-button--disabled';
    }
    return classNames;
  }

  isDisabled(): boolean {
    return this.disabled ? true : null;
  }

  get staticElementTypeA(): string {
    return ButtonComponent.ELEMENT_A;
  }

  get staticElementTypeButton(): string {
    return ButtonComponent.ELEMENT_BUTTON;
  }

  get staticElementTypeInput(): string {
    return ButtonComponent.ELEMENT_INPUT;
  }
}
