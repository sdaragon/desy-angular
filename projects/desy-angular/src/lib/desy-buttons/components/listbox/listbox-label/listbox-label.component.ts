import {Component, Input} from '@angular/core';
import {ContentBaseComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-listbox-label',
  templateUrl: './listbox-label.component.html'
})
export class ListboxLabelComponent extends ContentBaseComponent  {

  @Input() classes: string;

}
