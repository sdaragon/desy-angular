import {
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {AccessibilityAndTextOrHtmlRequiredComponent} from '../../../shared/components';
import {ListboxLabelData} from '../../interfaces/listbox-label-data';
import {ListboxItemData} from '../../interfaces/listbox-item-data';
import {StringUtils} from '../../../shared/utils/string-utils';
import {ListboxLabelComponent} from './listbox-label/listbox-label.component';
import {ListboxItemComponent} from './listbox-item/listbox-item.component';
import {Subscription} from 'rxjs';
import {DesyContentChild} from '../../../shared/decorators/desy-content-child.decorator';
import {inlinePositioning} from 'tippy.js';

@Component({
  selector: 'desy-listbox',
  templateUrl: './listbox.component.html'
})
export class ListboxComponent extends AccessibilityAndTextOrHtmlRequiredComponent implements OnInit, OnChanges, AfterContentInit, AfterViewInit, AfterViewChecked {

  static readonly KEY_CODE_ESC = 'Escape';

  @ViewChild('button', { read: ElementRef, static: true }) listboxButton: ElementRef;
  @ViewChild('list', { read: ElementRef }) listboxList: ElementRef;
  @ViewChild('tooltip', { read: ElementRef, static: true }) listboxContent: ElementRef;
  @ViewChildren('option', { read: ElementRef }) listboxOptions: QueryList<ElementRef>;

  @DesyContentChild()
  @ContentChildren(ListboxLabelComponent) labelComponent: ListboxLabelComponent;
  @ContentChildren(ListboxItemComponent) itemComponentList: QueryList<ListboxItemComponent>;

  @Input() id: string; // required
  @Input() isMultiselectable: boolean;
  @Input() doesChangeButtonText: boolean;
  @Input() label: ListboxLabelData;
  @Input() classes: string;
  @Input() classesContainer: string;
  @Input() classesTooltip: string;
  @Input() idPrefix: string;
  @Input() disabled: boolean;
  @Input() items: ListboxItemData[];
  @Output() itemsChange = new EventEmitter();
  @Output() activeItemChange = new EventEmitter();

  tippyProperties: any;      // Propiedades de tippy
  currentFocusIndex: number; // Índice del elemento actual con focus
  buttonContentHtml: string; // Puede ser el pasado por parámetro o el item seleccionado en caso de currentFocusIndex=true
  isListVisible = false;     // Indica si la lista se está mostrando
  contentInit = false;       // Se ha iniciado el contenido
  pendingCheckActiveItemsAfterViewInit = false;
  pendingFocusOnList = false;

  // conditionalId y conditionalItem representan en desy-html el item activo. Aquí es mejor obtenerlo cuando se necesita
  lastActiveItems: { item: ListboxItemData, index: number }[];
  itemComponentSubscriptions: Subscription[];

  itemList: ListboxItemData[];

  constructor(private changeDetectorRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    if (this.listboxContent && this.listboxContent.nativeElement) {
      this.initTippy();
    }
  }

  ngOnChanges(): void {
    if (this.items && this.items.length > 0) {
      this.itemList = this.items;

      if (this.contentInit) {
        this.checkActiveItems();
      } else {
        this.pendingCheckActiveItemsAfterViewInit = true;
      }
    }
  }

  ngAfterViewInit(): void {

    if (this.pendingCheckActiveItemsAfterViewInit) {
      this.checkActiveItems();
      this.pendingCheckActiveItemsAfterViewInit = false;
      this.changeDetectorRef.detectChanges();
    }
  }


  ngAfterContentInit(): void {
    this.contentInit = true;
    if (this.itemComponentList && (!this.items || this.items.length === 0)) {
      this.setupItemComponentList();
      this.itemComponentList.changes.subscribe(() => {
        this.setupItemComponentList();
      })
    }
  }

  ngAfterViewChecked(): void {
    if (this.pendingFocusOnList) {
      this.pendingFocusOnList = false;
      this.listboxList.nativeElement.focus();
      this.changeDetectorRef.detectChanges();
    }
  }
  
  private setupItemComponentList(): void {
    if (this.itemComponentSubscriptions) {
      this.itemComponentSubscriptions.forEach(s => s.unsubscribe());
    }
    this.itemComponentSubscriptions = [];
    this.itemList = [];
    this.itemComponentList.forEach(item => {
      this.itemList.push(item);
    });
    this.itemComponentList.forEach(item => {
      const subscription = item.itemChange.subscribe(() => this.checkActiveItems());
      this.itemComponentSubscriptions.push(subscription);
    });
      
    this.changeDetectorRef.detectChanges();
    this.checkActiveItems();
    this.changeDetectorRef.detectChanges();
  }

  private getActiveItemsData(): { item: ListboxItemData, index: number }[] {
    return this.itemList
      .map((item, index) => item.active ? {item, index} : null)
      .filter(item => item !== null);
  }

  onChangeItemChecked(): void {
    this.checkActiveItems();
    this.changeDetectorRef.detectChanges();
  }

  checkActiveItems(): void {
    let activeItems: { item: ListboxItemData, index: number }[] = this.itemList
      .map((item, index) => item.active ? {item, index} : null)
      .filter(item => item !== null);

    // se comprueba si algún item ha cambiado su estado de active
    let hasActiveItemChange = false;
    if (this.lastActiveItems && activeItems.length === this.lastActiveItems.length) {
      for (let i = 0; i < activeItems.length; i++) {
        if (activeItems[i].index !== this.lastActiveItems[i].index) {
          hasActiveItemChange = true;
          break;
        }
      }
    } else {
      hasActiveItemChange = true;
    }

    // Si hay cambios externos y es selección única, anulamos la selección anterior
    if (this.lastActiveItems && !this.isMultiselectable && hasActiveItemChange) {
      if (activeItems.length !== this.lastActiveItems.length) {
        for (const activeItem of activeItems) {
          if (this.lastActiveItems.findIndex(i => i.index === activeItem.index) >= 0) {
            this.setItemActiveValue(activeItem.index, false);
          }

          activeItems = activeItems.filter(activeItem => activeItem.item.active);
        }
      }
    }

    this.lastActiveItems = activeItems;

    // Se cambia el contenido del botón
    if (this.doesChangeButtonText && !this.isMultiselectable && activeItems.length > 0) {
      const activeItemOption = this.listboxOptions.find((item, index) => index === activeItems[0].index);
      if (activeItemOption) {
        this.buttonContentHtml = activeItemOption.nativeElement.innerHTML;
      }
    } else {
      this.buttonContentHtml = this.html ? this.html : null;
    }

    // Si hay cambios, se emiten
    if (hasActiveItemChange) {
      this.itemsChange.emit(this.itemList);
      this.activeItemChange.emit(activeItems.length > 0 ? activeItems[0].item : null);
    }
  }

  onListShow(): void {
    this.isListVisible = true;
    if (this.listboxList) {
      this.pendingFocusOnList = true;
      //setTimeout(() => this.listboxList.nativeElement.focus());
    }
  }

  onListClose(): void {
    this.isListVisible = false;
  }

  onListFocus(): void {
    const activeItemsData = this.getActiveItemsData();
    if (activeItemsData.length > 0) {
      const activeElem = document.getElementById(this.getItemId(activeItemsData[0].item, activeItemsData[0].index));
      if (activeElem) {
        activeElem.focus();
        this.currentFocusIndex = activeItemsData[0].index;
      }
    } else if (this.itemList && this.itemList.length > 0) {
      this.currentFocusIndex = 0;
      if (!this.isMultiselectable) {
        this.selectItem(0);
      }
    } else {
      console.warn('No element to focus');
    }
  }

  moveFocus(position: number, event: Event): void {
    event.preventDefault();
    if (position >= 0 && position < this.itemList.length) {
      if (this.isMultiselectable) {
        this.currentFocusIndex = position;
      } else {
        this.selectItem(position);
      }
    }
  }

  onSpace(event: Event): void {
    event.preventDefault();
    if (this.isMultiselectable) {
      this.selectItem(this.currentFocusIndex);
    }
  }

  selectItem(index: number): void {
    if (!this.isMultiselectable) {
      this.itemList.forEach(item => this.setItemActiveValue(index, false));
      this.setItemActiveValue(index, true);
    } else {
      this.setItemActiveValue(index, !this.itemList[index].active);
    }
    this.currentFocusIndex = index;

    this.checkActiveItems();
  }

  private setItemActiveValue(index: number, active: boolean): void {
    if (this.itemComponentList !== null && this.itemComponentList.length > 0) {
      const deactivatedItem = this.itemComponentList.get(index);
      deactivatedItem?.setActive(active);
    } else {
      this.itemList[index].active = active;
    }
  }

  /*
   * Métodos para facilitar contenido al template
   */

  getIdPrefix(): string {
    return this.idPrefix ? this.idPrefix : this.id + '-listbox-item';
  }

  getItemId(item: ListboxItemData, index: number): string {
    let itemId: string;
    if (item.id) {
      itemId = item.id;
    } else {
      itemId = this.getIdPrefix();
      if (index > 0) {
        itemId += `-${index}`;
      }
    }
    return itemId;
  }

  hasLabel(): boolean {
    return !!(this.label || this.labelComponent);
  }

  getLabelContent(label: ListboxLabelData): string {
    return label.html ? label.html : `<p>${ StringUtils.escapeHtml(label.text) }</p>`;
  }

  initTippy(): void {
    const contentTooltip = this.listboxContent.nativeElement as HTMLElement;
    const listboxButtonElement = this.listboxButton.nativeElement;
    this.tippyProperties = {        
      placement: 'bottom-start',
      inlinePositioning: true,        
      content: contentTooltip,
      allowHTML: true, // Make sure you are sanitizing any user data if rendering HTML to prevent XSS attacks.
      trigger: 'click',
      hideOnClick: true,
      interactive: true,
      arrow: false,
      offset: [0, -10],
      theme: '',
      plugins: [
        {
          name: 'hideOnPopperBlur',
          defaultValue: true,
          fn(instance) {
            return {
              onCreate() {
                instance.popper.addEventListener('focusout', (event) => {
                  if (
                    instance.props.hideOnPopperBlur &&
                    event.relatedTarget &&
                    !instance.popper.contains(event.relatedTarget)
                  ) {
                    instance.hide();
                  }
                });
              },
            };
          }
        }, {
          name: 'hideOnEsc',
          defaultValue: true,
          fn({hide}): any {

            function onKeyDown(event: KeyboardEvent): void {
              if (event.key === ListboxComponent.KEY_CODE_ESC) {
                hide();
                listboxButtonElement.focus();
              }
            }

            return {
              onShow(): void {
                document.addEventListener('keydown', onKeyDown);
              },
              onHide(): void {
                document.removeEventListener('keydown', onKeyDown);
              },
            };
          },
        },
        inlinePositioning
      ],
      role: false,
      aria: {
        content: 'auto',
      },
      onShow: this.onListShow.bind(this),
      onHidden: this.onListClose.bind(this)
    };
  }
  
}
