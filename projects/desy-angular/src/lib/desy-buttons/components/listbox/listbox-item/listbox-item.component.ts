import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild } from '@angular/core';
import { AccessibilityComponent } from '../../../../shared/components';
import { ListboxItemData } from '../../../interfaces/listbox-item-data';

@Component({
  selector: 'desy-listbox-item',
  templateUrl: './listbox-item.component.html'
})
export class ListboxItemComponent extends AccessibilityComponent implements ListboxItemData, OnChanges {

  @ViewChild('childComponentTemplate') content;

  @Input() id?: string;
  @Input() active?: boolean;
  @Input() classes?: string;
  @Input() titleModal?: string;

  @Output() itemChange = new EventEmitter();
  @Output() activeChange = new EventEmitter();

  ngOnChanges(changes: SimpleChanges): void {
    this.itemChange.emit(changes);
  }

  setActive(value: boolean): void {
    this.active = value;
    this.activeChange.emit(value);
  }

}
