import { Component, Input, ViewChild } from '@angular/core';
import {AccessibilityComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-toggle-off-state',
  templateUrl: './toggle-off-state.component.html'
})
export class ToggleOffStateComponent  extends AccessibilityComponent  {
  
  @ViewChild('contentTemplateOffState', { static: true }) content;

  @Input() classes: string;

}