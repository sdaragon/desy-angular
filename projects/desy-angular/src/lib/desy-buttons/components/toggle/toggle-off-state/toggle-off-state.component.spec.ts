import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleOffStateComponent } from './toggle-off-state.component';

xdescribe('ToggleOffStateComponent', () => {
  let component: ToggleOffStateComponent;
  let fixture: ComponentFixture<ToggleOffStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToggleOffStateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleOffStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
