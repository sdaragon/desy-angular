import { Component, ContentChildren, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { AccessibilityComponent } from '../../../shared/components';
import { DesyContentChild } from '../../../shared/decorators/desy-content-child.decorator';
import { ToggleOffStateComponent } from './toggle-off-state/toggle-off-state.component';
import { ToggleOnStateComponent } from './toggle-on-state/toggle-on-state.component';

@Component({
  selector: 'desy-toggle',
  templateUrl: './toggle.component.html'
})
export class ToggleComponent extends AccessibilityComponent {
  
  @Input() id: string;
  @Input() isSwitch: boolean;
  @Input() pressed: boolean;
  @Input() classes: string;
  @Input() isExpandible: boolean;

  @Input() @HostBinding('class.relative') cfg = true;
  
  @Output() clickEvent = new EventEmitter();
  @Output() pressedChange: EventEmitter<any> = new EventEmitter<any>();
  
  @DesyContentChild()
  @ContentChildren(ToggleOnStateComponent) contentTemplateOnState: ToggleOnStateComponent;
  
  @DesyContentChild()
  @ContentChildren(ToggleOffStateComponent) contentTemplateOffState: ToggleOffStateComponent;

  onClick(event: any): void {
    this.pressed = !this.pressed;
    this.pressedChange.emit(this.pressed);
    this.clickEvent.emit(event);
  }

}
