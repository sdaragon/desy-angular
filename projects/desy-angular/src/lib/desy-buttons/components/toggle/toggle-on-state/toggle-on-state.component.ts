import { Component, Input, ViewChild } from '@angular/core';
import {AccessibilityComponent} from '../../../../shared/components';

@Component({
  selector: 'desy-toggle-on-state',
  templateUrl: './toggle-on-state.component.html'
})
export class ToggleOnStateComponent extends AccessibilityComponent {

  @ViewChild('contentTemplateOnState', { static: true }) content;

  @Input() classes: string;

}
