import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleOnStateComponent } from './toggle-on-state.component';

xdescribe('ToggleOnStateComponent', () => {
  let component: ToggleOnStateComponent;
  let fixture: ComponentFixture<ToggleOnStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToggleOnStateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleOnStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
