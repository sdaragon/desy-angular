import * as Code from './code-demo-header-mini';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-header-mini',
  templateUrl: './demo-header-mini.component.html',
  styles: [
  ]
})
export class DemoHeaderMiniComponent {

  nameComponent = 'Header Mini';
  mostrarCodigo = Code;

  // Header Mini
  classes: string;
  containerClasses: string;
  homepageUrl: string;
  homepageRouterLink: string;
  homepageFragment: string;
  hasContainer: boolean;

  // Accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;
}

