import * as Code from './code-demo-input-group';
import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorMessageData, FieldsetData, HintData, ItemInputGroupData } from 'desy-angular';
import { ErrorMessageModel } from '../../shared/models/error-message-model';
import { FieldsetModel } from '../../shared/models/fieldset-model';
import { HintModel } from '../../shared/models/hint-model';

@Component({
  selector: 'app-demo-input-group',
  templateUrl: './demo-input-group.component.html'
})
export class DemoInputGroupComponent implements AfterContentChecked , OnInit{

  nameComponent = 'Input-group';
  mostrarCodigo = Code;

  value: any;

  customItem: ItemInputGroupData = {
    name: 'numDoc',
    labelData: {
      html: 'Número'
    },
    classes: 'w-full lg:w-50',
    placeholder: 'Ej: 28999876V',
    disabled: false
  };

  items: ItemInputGroupData[] = [
    {
      labelText: 'Tipo',
      name: 'tipoDoc',
      classes: 'w-full lg:w-auto',
      formGroupClasses: 'mr-base',
      isSelect: true,
      selectItems: [
        {
          value: '1',
          text: 'NIF',
        },
        {
          value: '2',
          text: 'Pasaporte'
        }
      ]
    },
    {
      name: 'divider',
      divider: {
        text: 'y',
        classes: 'mr-base'
      }
    },
    this.customItem
  ];

  formContent: FormGroup = new FormGroup({
    date: new FormGroup({
      tipoDoc: new FormControl('2', Validators.required),
      numDoc: new FormControl('11H', Validators.required)
    })
  });
  formTypeClassesContent: string = null;
  formNumberClassesContent: string = null;

  fieldsetData: FieldsetData = new FieldsetModel();
  hintData: HintData = new HintModel();
  errorMessageData: ErrorMessageData = new ErrorMessageModel();
  
  namePrefix = 'field';
  classes: string;
  formGroupClasses: string;
  id: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  ngOnInit(): void {
    this.errorMessageData.html = "Hay un error en el formulario"
    this.hintData.html = "Algunos campos son obligatorios"
    this.fieldsetData.legend.html = "Documento de identidad"
  }
  
  ngAfterContentChecked(): void {
    
    if (this.customItem.name === 'numDoc') {
      if (this.formContent) {
        this.items.forEach(item => {
          const hasErrors = !item.divider && this.formContent.get('date').get(item.name).invalid;

          if (item.isSelect) {
            this.formTypeClassesContent = hasErrors ? 'c-select--error border-alert-base ring-2 ring-alert-base' : '';
          } else {
            this.formNumberClassesContent = hasErrors ? 'border-alert-base ring-2 ring-alert-base w-full lg:w-50' : 'w-full lg:w-50';
          }
        });
      }
    }
  }
}
