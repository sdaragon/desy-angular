import * as Code from './code-demo-radios';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorMessageData, FieldsetData, HintData, ItemRadioData } from 'desy-angular';


@Component({
  selector: 'app-demo-radios',
  templateUrl: './demo-radios.component.html'
})
export class DemoRadiosComponent implements OnInit {

  nameComponent = 'Radios';
  mostrarCodigo = Code;

  mainValue;
  valueContent;

  form: FormGroup;
  formGroupContactClasses: string;

  fieldsetData: FieldsetData = {
    legend: {
      html: 'Which part of the Housing Act was your licence issued under?'
    }
  };
  hintData: HintData = {
    html: 'Please, choose the right answer.'
  };
  errorMessageData: ErrorMessageData = {};

  itemsContact: ItemRadioData[] = [
    {
      value: 'phone',
      html: 'Phone',
      conditional: true
    },
    {
      value: 'email',
      html: 'Email',
      conditional: true
    },
    {
      value: 'text',
      html: 'Text message',
      conditional: true
    }
  ];
  itemChecked = false;

  itemRadioDataCustom: ItemRadioData = {
    html: 'opción que puedes modificar',
    labelData: {},
    hintData: {
      html: 'texto hint'
    },
    value: 3
  };

  formGroupClasses: string;

  idPrefix: string;
  name = 'example';
  hasDividers = false;
  classes: string;
  id: string;
  errorId: string;
  describedBy: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {

    this.form = new FormGroup({
      valueFormControl: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.email]),
      phone: new FormControl(null, [Validators.minLength(9)]),
      phoneTxt: new FormControl(null, [Validators.minLength(9)]),
    });

    this.form.valueChanges.subscribe(() => {
      if (this.form.invalid) {
        this.formGroupContactClasses = 'c-form-group--error';
      }else {
        this.formGroupContactClasses = '';
      }
    });
  }

  get valueFormControl(): AbstractControl { return this.form.get('valueFormControl'); }
  get email(): AbstractControl { return this.form.get('email'); }
  get phone(): AbstractControl { return this.form.get('phone'); }
  get phoneTxt(): AbstractControl { return this.form.get('phoneTxt'); }

  /**
   * Mejor sincronizar con la variable itemChecked antes que compartir itemRadioDataCustom.checked, ya que este se puede modificar despues
   * de la detección de cambios.
   * @param newValue nuevo valor para checked
   */
  detectChanges(newValue: boolean): void {
    this.itemChecked = newValue;
    this.itemRadioDataCustom.checked = newValue;
    this.changeDetectorRef.detectChanges();
  }

}
