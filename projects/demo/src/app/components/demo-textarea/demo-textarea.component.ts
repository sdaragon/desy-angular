import * as Code from './code-demo-textarea';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorMessageData, HintData, LabelData } from 'desy-angular';
import { ErrorMessageModel } from '../../shared/models/error-message-model';
import { HintModel } from '../../shared/models/hint-model';
import { LabelModel } from '../../shared/models/label-model';

@Component({
  selector: 'app-demo-textarea',
  templateUrl: './demo-textarea.component.html'
})
export class
DemoTextareaComponent implements OnInit {

  nameComponent = 'Textarea';
  mostrarCodigo = Code;

  value: string;
  form: FormGroup;

  id: string;
  name: string;
  placeholder: string;
  rows: number;
  describedBy: string;
  labelData: LabelData = new LabelModel();
  hintData: HintData = new HintModel();
  errorMessageData: ErrorMessageData = new ErrorMessageModel();
  formGroupClasses: string;
  classes: string;
  autocomplete: string;
  disabled: boolean;
  maxlength: number;

  ariaControls: string;
  ariaCurrent: string;
  ariaDescribedBy: string;
  ariaDisabled: string;
  ariaErrorMessage: string;
  ariaExpanded: string;
  ariaHasPopup: string;
  ariaHidden: string;
  ariaLabel: string;
  ariaLabelledBy: string;
  ariaLive: string;
  role: string;
  tabindex: string;

  constructor() { }

  ngOnInit(): void {
    this.id = 'textarea-id';
    this.name = 'textarea-name';
    this.labelData.html = 'Texto en label';
    this.hintData.html = 'Texto en hint';
    this.errorMessageData.html = 'Html en <b>errorMessage</b>';
    this.classes = 'w-full';
    this.form = new FormGroup({
      valueFormControl: new FormControl('Valor por defecto en form', [Validators.required])
    });
  }

}
