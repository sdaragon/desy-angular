/** Autogenerated with npm run code, do not modify here, only in original files. Here the changes will be overridden*/
export const codigoHtml0 = `<desy-status-item [id]="id + '-content'" [classes]="classes" [role]="role" [ariaLabel]="ariaLabel" [ariaDescribedBy]="ariaDescribedBy" [ariaLabelledBy]="ariaLabelledBy" [ariaHidden]="ariaHidden" [ariaDisabled]="ariaDisabled" [ariaControls]="ariaControls" [ariaCurrent]="ariaCurrent" [ariaLive]="ariaLive" [ariaExpanded]="ariaExpanded" [ariaErrorMessage]="ariaErrorMessage" [ariaHasPopup]="ariaHasPopup" [tabindex]="tabindex">
  <desy-title [classes]="title.classes">
    <ng-container *appCustomInnerContent="{ html: title.html }"></ng-container>
  </desy-title>
  <desy-hint [classes]="hint.classes" [id]="hint.id" [role]="hint.role" [ariaLabel]="hint.ariaLabel" [ariaDescribedBy]="hint.ariaDescribedBy" [ariaLabelledBy]="hint.ariaLabelledBy" [ariaHidden]="hint.ariaHidden" [ariaDisabled]="hint.ariaDisabled" [ariaControls]="hint.ariaControls" [ariaCurrent]="hint.ariaCurrent" [ariaLive]="hint.ariaLive" [ariaExpanded]="hint.ariaExpanded" [ariaErrorMessage]="hint.ariaErrorMessage" [ariaHasPopup]="hint.ariaHasPopup" [tabindex]="hint.tabindex">
    <ng-container *appCustomInnerContent="{ html: hint.html }"></ng-container>
  </desy-hint>
  <desy-error-message [visuallyHiddenText]="errorMessage.visuallyHiddenText" [id]="(errorId ? errorId : id + '-error')" [classes]="errorMessage.classes" [role]="errorMessage.role" [ariaLabel]="errorMessage.ariaLabel" [ariaDescribedBy]="(errorMessage.ariaDescribedBy ? errorMessage.ariaDescribedBy + '' + errorId : errorId)" [ariaLabelledBy]="errorMessage.ariaLabelledBy" [ariaHidden]="errorMessage.ariaHidden" [ariaDisabled]="errorMessage.ariaDisabled" [ariaControls]="errorMessage.ariaControls" [ariaCurrent]="errorMessage.ariaCurrent" [ariaLive]="errorMessage.ariaLive" [ariaExpanded]="errorMessage.ariaExpanded" [ariaErrorMessage]="errorMessage.ariaErrorMessage" [ariaHasPopup]="errorMessage.ariaHasPopup" [tabindex]="errorMessage.tabindex">
    <ng-container *appCustomInnerContent="{ html: errorMessage.html }"></ng-container>
  </desy-error-message>
  <desy-description-item *ngFor="let item of items" [id]="item.id" [classes]="item.classes">
    <desy-term [classes]="item.term.classes" [id]="item.term.id">
      <ng-container *appCustomInnerContent="{ html: item.term.html }"></ng-container>
    </desy-term>
    <desy-definition [classes]="item.definition.classes" [id]="item.definition.id">
      <ng-container *appCustomInnerContent="{ html: item.definition.html }"></ng-container>
    </desy-definition>
  </desy-description-item>
  <desy-content>
    <button class="c-button c-button--transparent">Modificar</button>
  </desy-content>
  <desy-status [icon]="status.icon" [classes]="status.classes" [id]="status.id" [role]="status.role" [ariaLabel]="status.ariaLabel" [ariaDescribedBy]="status.ariaDescribedBy" [ariaLabelledBy]="status.ariaLabelledBy" [ariaHidden]="status.ariaHidden" [ariaDisabled]="status.ariaDisabled" [ariaControls]="status.ariaControls" [ariaCurrent]="status.ariaCurrent" [ariaLive]="status.ariaLive" [ariaExpanded]="status.ariaExpanded" [ariaErrorMessage]="status.ariaErrorMessage" [ariaHasPopup]="status.ariaHasPopup" [tabindex]="status.tabindex">
    {{status.text}}
    <desy-icon [type]="status.icon.type">
      <ng-container *appCustomInnerContent="{ html: status.icon.html}"></ng-container>
    </desy-icon>
  </desy-status>
</desy-status-item>`;
export const codigoTs0 = `import * as Code from './code-demo-status-item';
import { Component } from '@angular/core';
import {
  DescriptionItemData,
  ErrorMessageData,
  HintData,
  StatusData,
  TitleData
} from 'desy-angular';

@Component({
  selector: 'app-demo-status-item',
  templateUrl: './demo-status-item.component.html'
})
export class DemoStatusItemComponent {

  nameComponent = 'Status-Item';
  mostrarCodigo = Code;

  id = 'editable-status-item';
  title: TitleData = {
    html: 'Titulo editable',
  };
  hint: HintData = {
    html: 'Hint editable',
  };
  errorMessage: ErrorMessageData = {
    html: 'Texto de error editable',
    classes: 'my-sm text-alert-base',
  };
  errorId: string;
  describedBy: string;
  item: DescriptionItemData = {
    classes: 'mb-base',
    term: {
      html: 'termino para editar',
    },
    definition: {
      html: 'definition para editar'
    }
  };

  items: DescriptionItemData[] = [
    {
      classes: 'mb-base',
      term: {
        html: 'Términos',
      },
      definition: {
        html: 'Definition',
        classes: 'text-lg',
      }
    },
    {
      classes: 'mb-base',
      term: {
        html: 'Términos para editar con <strong>HTML</strong>',
      },
      definition: {
        html: 'Definición para editar con <strong>HTML</strong>',
      }
    },
    this.item
  ];

  status: StatusData = {
    text: 'Estado editable',
    icon : {
      html: '<svg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 140 140\' width=\'1em\' height=\'1em\' class=\'w-4 h-4 text-alert-base\' aria-hidden=\'true\' focusable=\'false\'><path d=\'M70 75a7.5 7.5 0 007.5-7.5v-25a7.5 7.5 0 00-15 0v25A7.5 7.5 0 0070 75zM60 92.5a10 10 0 1020 0 10 10 0 10-20 0z\' fill=\'currentColor\'/><path d=\'M139.78 101.83L135 82.6a7.51 7.51 0 00-9.1-5.45l-19.22 4.8a7.5 7.5 0 00-2 13.71l6.11 3.66A55.31 55.31 0 0170 120.19a50.47 50.47 0 01-47.16-33.06 7.503 7.503 0 00-14.09 5.16A65.52 65.52 0 0070 135.19 71.27 71.27 0 00123.57 107l5.07 3a7.5 7.5 0 0011.14-8.25zM14.13 62.85l19.22-4.8a7.5 7.5 0 002.05-13.71L29.76 41A56.18 56.18 0 0170 19.81a50.47 50.47 0 0147.16 33.06 7.51 7.51 0 007 4.92 7.61 7.61 0 002.59-.46 7.51 7.51 0 004.46-9.62A65.52 65.52 0 0070 4.81 71.53 71.53 0 0016.83 33.2l-5.47-3.28A7.5 7.5 0 00.22 38.17L5 57.4a7.51 7.51 0 007.27 5.68 7.65 7.65 0 001.86-.23z\' fill=\'currentColor\'/></svg>',
    }
  };
  classes: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


}
`;
export const codigoHtml1 = `<desy-status-item id="default">
  <desy-title>
    Title
  </desy-title>
  <desy-content>
    <button class="c-button c-button--transparent">Modificar</button>
  </desy-content>
</desy-status-item>`;
export const codigoTs1 = `import * as Code from './code-demo-status-item';
import { Component } from '@angular/core';
import {
  DescriptionItemData,
  ErrorMessageData,
  HintData,
  StatusData,
  TitleData
} from 'desy-angular';

@Component({
  selector: 'app-demo-status-item',
  templateUrl: './demo-status-item.component.html'
})
export class DemoStatusItemComponent {

  nameComponent = 'Status-Item';
  mostrarCodigo = Code;

  id = 'editable-status-item';
  title: TitleData = {
    html: 'Titulo editable',
  };
  hint: HintData = {
    html: 'Hint editable',
  };
  errorMessage: ErrorMessageData = {
    html: 'Texto de error editable',
    classes: 'my-sm text-alert-base',
  };
  errorId: string;
  describedBy: string;
  item: DescriptionItemData = {
    classes: 'mb-base',
    term: {
      html: 'termino para editar',
    },
    definition: {
      html: 'definition para editar'
    }
  };

  items: DescriptionItemData[] = [
    {
      classes: 'mb-base',
      term: {
        html: 'Términos',
      },
      definition: {
        html: 'Definition',
        classes: 'text-lg',
      }
    },
    {
      classes: 'mb-base',
      term: {
        html: 'Términos para editar con <strong>HTML</strong>',
      },
      definition: {
        html: 'Definición para editar con <strong>HTML</strong>',
      }
    },
    this.item
  ];

  status: StatusData = {
    text: 'Estado editable',
    icon : {
      html: '<svg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 140 140\' width=\'1em\' height=\'1em\' class=\'w-4 h-4 text-alert-base\' aria-hidden=\'true\' focusable=\'false\'><path d=\'M70 75a7.5 7.5 0 007.5-7.5v-25a7.5 7.5 0 00-15 0v25A7.5 7.5 0 0070 75zM60 92.5a10 10 0 1020 0 10 10 0 10-20 0z\' fill=\'currentColor\'/><path d=\'M139.78 101.83L135 82.6a7.51 7.51 0 00-9.1-5.45l-19.22 4.8a7.5 7.5 0 00-2 13.71l6.11 3.66A55.31 55.31 0 0170 120.19a50.47 50.47 0 01-47.16-33.06 7.503 7.503 0 00-14.09 5.16A65.52 65.52 0 0070 135.19 71.27 71.27 0 00123.57 107l5.07 3a7.5 7.5 0 0011.14-8.25zM14.13 62.85l19.22-4.8a7.5 7.5 0 002.05-13.71L29.76 41A56.18 56.18 0 0170 19.81a50.47 50.47 0 0147.16 33.06 7.51 7.51 0 007 4.92 7.61 7.61 0 002.59-.46 7.51 7.51 0 004.46-9.62A65.52 65.52 0 0070 4.81 71.53 71.53 0 0016.83 33.2l-5.47-3.28A7.5 7.5 0 00.22 38.17L5 57.4a7.51 7.51 0 007.27 5.68 7.65 7.65 0 001.86-.23z\' fill=\'currentColor\'/></svg>',
    }
  };
  classes: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


}
`;
export const codigoHtml2 = `<desy-status-item id="only-items">
  <desy-description-item>
    <desy-term>término</desy-term>
    <desy-definition>definición</desy-definition>
  </desy-description-item>
  <desy-description-item>
    <desy-term>término</desy-term>
    <desy-definition>definición</desy-definition>
  </desy-description-item>
  <desy-description-item>
    <desy-term>término</desy-term>
    <desy-definition>definición</desy-definition>
  </desy-description-item>
  <desy-status>
    Estado
    <desy-icon type="success"></desy-icon>
  </desy-status>
  <desy-content>
    <button class="c-button c-button--transparent">Modificar</button>
  </desy-content>
</desy-status-item>`;
export const codigoTs2 = `import * as Code from './code-demo-status-item';
import { Component } from '@angular/core';
import {
  DescriptionItemData,
  ErrorMessageData,
  HintData,
  StatusData,
  TitleData
} from 'desy-angular';

@Component({
  selector: 'app-demo-status-item',
  templateUrl: './demo-status-item.component.html'
})
export class DemoStatusItemComponent {

  nameComponent = 'Status-Item';
  mostrarCodigo = Code;

  id = 'editable-status-item';
  title: TitleData = {
    html: 'Titulo editable',
  };
  hint: HintData = {
    html: 'Hint editable',
  };
  errorMessage: ErrorMessageData = {
    html: 'Texto de error editable',
    classes: 'my-sm text-alert-base',
  };
  errorId: string;
  describedBy: string;
  item: DescriptionItemData = {
    classes: 'mb-base',
    term: {
      html: 'termino para editar',
    },
    definition: {
      html: 'definition para editar'
    }
  };

  items: DescriptionItemData[] = [
    {
      classes: 'mb-base',
      term: {
        html: 'Términos',
      },
      definition: {
        html: 'Definition',
        classes: 'text-lg',
      }
    },
    {
      classes: 'mb-base',
      term: {
        html: 'Términos para editar con <strong>HTML</strong>',
      },
      definition: {
        html: 'Definición para editar con <strong>HTML</strong>',
      }
    },
    this.item
  ];

  status: StatusData = {
    text: 'Estado editable',
    icon : {
      html: '<svg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 140 140\' width=\'1em\' height=\'1em\' class=\'w-4 h-4 text-alert-base\' aria-hidden=\'true\' focusable=\'false\'><path d=\'M70 75a7.5 7.5 0 007.5-7.5v-25a7.5 7.5 0 00-15 0v25A7.5 7.5 0 0070 75zM60 92.5a10 10 0 1020 0 10 10 0 10-20 0z\' fill=\'currentColor\'/><path d=\'M139.78 101.83L135 82.6a7.51 7.51 0 00-9.1-5.45l-19.22 4.8a7.5 7.5 0 00-2 13.71l6.11 3.66A55.31 55.31 0 0170 120.19a50.47 50.47 0 01-47.16-33.06 7.503 7.503 0 00-14.09 5.16A65.52 65.52 0 0070 135.19 71.27 71.27 0 00123.57 107l5.07 3a7.5 7.5 0 0011.14-8.25zM14.13 62.85l19.22-4.8a7.5 7.5 0 002.05-13.71L29.76 41A56.18 56.18 0 0170 19.81a50.47 50.47 0 0147.16 33.06 7.51 7.51 0 007 4.92 7.61 7.61 0 002.59-.46 7.51 7.51 0 004.46-9.62A65.52 65.52 0 0070 4.81 71.53 71.53 0 0016.83 33.2l-5.47-3.28A7.5 7.5 0 00.22 38.17L5 57.4a7.51 7.51 0 007.27 5.68 7.65 7.65 0 001.86-.23z\' fill=\'currentColor\'/></svg>',
    }
  };
  classes: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


}
`;
export const codigoHtml3 = `<desy-status-item id="with-hint">
  <desy-title>Personas de la unidad familiar</desy-title>
  <desy-status>
    Aportado
    <desy-icon type="success"></desy-icon>
  </desy-status>
  <desy-hint>
    <a href="#" class="c-link"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140" width="1em" height="1em" class="inline-block self-center w-4 h-4 mr-sm no-underline" role="img" aria-hidden="true">
      <path d="M100.3 52.2a7.49 7.49 0 00-10.6 0L77.5 64.39V7.5a7.5 7.5 0 00-15 0v56.89L50.3 52.2a7.5 7.5 0 10-10.6 10.6l25 25a7.49 7.49 0 0010.6 0l25-25a7.49 7.49 0 000-10.6zM130 95a10 10 0 00-10 10v12.5a2.5 2.5 0 01-2.5 2.5h-95a2.5 2.5 0 01-2.5-2.5V105a10 10 0 00-20 0v15a20 20 0 0020 20h100a20 20 0 0020-20v-15a10 10 0 00-10-10z" fill="currentColor"></path>
    </svg>Descargar modelo</a>
  </desy-hint>
</desy-status-item>`;
export const codigoTs3 = `import * as Code from './code-demo-status-item';
import { Component } from '@angular/core';
import {
  DescriptionItemData,
  ErrorMessageData,
  HintData,
  StatusData,
  TitleData
} from 'desy-angular';

@Component({
  selector: 'app-demo-status-item',
  templateUrl: './demo-status-item.component.html'
})
export class DemoStatusItemComponent {

  nameComponent = 'Status-Item';
  mostrarCodigo = Code;

  id = 'editable-status-item';
  title: TitleData = {
    html: 'Titulo editable',
  };
  hint: HintData = {
    html: 'Hint editable',
  };
  errorMessage: ErrorMessageData = {
    html: 'Texto de error editable',
    classes: 'my-sm text-alert-base',
  };
  errorId: string;
  describedBy: string;
  item: DescriptionItemData = {
    classes: 'mb-base',
    term: {
      html: 'termino para editar',
    },
    definition: {
      html: 'definition para editar'
    }
  };

  items: DescriptionItemData[] = [
    {
      classes: 'mb-base',
      term: {
        html: 'Términos',
      },
      definition: {
        html: 'Definition',
        classes: 'text-lg',
      }
    },
    {
      classes: 'mb-base',
      term: {
        html: 'Términos para editar con <strong>HTML</strong>',
      },
      definition: {
        html: 'Definición para editar con <strong>HTML</strong>',
      }
    },
    this.item
  ];

  status: StatusData = {
    text: 'Estado editable',
    icon : {
      html: '<svg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 140 140\' width=\'1em\' height=\'1em\' class=\'w-4 h-4 text-alert-base\' aria-hidden=\'true\' focusable=\'false\'><path d=\'M70 75a7.5 7.5 0 007.5-7.5v-25a7.5 7.5 0 00-15 0v25A7.5 7.5 0 0070 75zM60 92.5a10 10 0 1020 0 10 10 0 10-20 0z\' fill=\'currentColor\'/><path d=\'M139.78 101.83L135 82.6a7.51 7.51 0 00-9.1-5.45l-19.22 4.8a7.5 7.5 0 00-2 13.71l6.11 3.66A55.31 55.31 0 0170 120.19a50.47 50.47 0 01-47.16-33.06 7.503 7.503 0 00-14.09 5.16A65.52 65.52 0 0070 135.19 71.27 71.27 0 00123.57 107l5.07 3a7.5 7.5 0 0011.14-8.25zM14.13 62.85l19.22-4.8a7.5 7.5 0 002.05-13.71L29.76 41A56.18 56.18 0 0170 19.81a50.47 50.47 0 0147.16 33.06 7.51 7.51 0 007 4.92 7.61 7.61 0 002.59-.46 7.51 7.51 0 004.46-9.62A65.52 65.52 0 0070 4.81 71.53 71.53 0 0016.83 33.2l-5.47-3.28A7.5 7.5 0 00.22 38.17L5 57.4a7.51 7.51 0 007.27 5.68 7.65 7.65 0 001.86-.23z\' fill=\'currentColor\'/></svg>',
    }
  };
  classes: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


}
`;
export const codigoHtml4 = `<desy-status-item id="with-status-alert" classes="border-l-4 border-alert-base">
  <desy-title>Datos adicionales del solicitante</desy-title>
  <desy-status classes="text-alert-base">
    Error
    <desy-icon type="error"></desy-icon>
  </desy-status>
  <desy-error-message classes="my-sm text-alert-base">
    Es necesario aportar este documento para enviar el trámite
  </desy-error-message>
</desy-status-item>`;
export const codigoTs4 = `import * as Code from './code-demo-status-item';
import { Component } from '@angular/core';
import {
  DescriptionItemData,
  ErrorMessageData,
  HintData,
  StatusData,
  TitleData
} from 'desy-angular';

@Component({
  selector: 'app-demo-status-item',
  templateUrl: './demo-status-item.component.html'
})
export class DemoStatusItemComponent {

  nameComponent = 'Status-Item';
  mostrarCodigo = Code;

  id = 'editable-status-item';
  title: TitleData = {
    html: 'Titulo editable',
  };
  hint: HintData = {
    html: 'Hint editable',
  };
  errorMessage: ErrorMessageData = {
    html: 'Texto de error editable',
    classes: 'my-sm text-alert-base',
  };
  errorId: string;
  describedBy: string;
  item: DescriptionItemData = {
    classes: 'mb-base',
    term: {
      html: 'termino para editar',
    },
    definition: {
      html: 'definition para editar'
    }
  };

  items: DescriptionItemData[] = [
    {
      classes: 'mb-base',
      term: {
        html: 'Términos',
      },
      definition: {
        html: 'Definition',
        classes: 'text-lg',
      }
    },
    {
      classes: 'mb-base',
      term: {
        html: 'Términos para editar con <strong>HTML</strong>',
      },
      definition: {
        html: 'Definición para editar con <strong>HTML</strong>',
      }
    },
    this.item
  ];

  status: StatusData = {
    text: 'Estado editable',
    icon : {
      html: '<svg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 140 140\' width=\'1em\' height=\'1em\' class=\'w-4 h-4 text-alert-base\' aria-hidden=\'true\' focusable=\'false\'><path d=\'M70 75a7.5 7.5 0 007.5-7.5v-25a7.5 7.5 0 00-15 0v25A7.5 7.5 0 0070 75zM60 92.5a10 10 0 1020 0 10 10 0 10-20 0z\' fill=\'currentColor\'/><path d=\'M139.78 101.83L135 82.6a7.51 7.51 0 00-9.1-5.45l-19.22 4.8a7.5 7.5 0 00-2 13.71l6.11 3.66A55.31 55.31 0 0170 120.19a50.47 50.47 0 01-47.16-33.06 7.503 7.503 0 00-14.09 5.16A65.52 65.52 0 0070 135.19 71.27 71.27 0 00123.57 107l5.07 3a7.5 7.5 0 0011.14-8.25zM14.13 62.85l19.22-4.8a7.5 7.5 0 002.05-13.71L29.76 41A56.18 56.18 0 0170 19.81a50.47 50.47 0 0147.16 33.06 7.51 7.51 0 007 4.92 7.61 7.61 0 002.59-.46 7.51 7.51 0 004.46-9.62A65.52 65.52 0 0070 4.81 71.53 71.53 0 0016.83 33.2l-5.47-3.28A7.5 7.5 0 00.22 38.17L5 57.4a7.51 7.51 0 007.27 5.68 7.65 7.65 0 001.86-.23z\' fill=\'currentColor\'/></svg>',
    }
  };
  classes: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


}
`;
