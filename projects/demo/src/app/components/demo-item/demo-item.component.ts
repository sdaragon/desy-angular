import * as Code from './code-demo-item';
import { Component } from '@angular/core';
import {
  ContentData,
  DescriptionData, IconData,
  TitleData
} from 'desy-angular';

@Component({
  selector: 'app-demo-item',
  templateUrl: './demo-item.component.html'
})
export class DemoItemComponent {

  nameComponent = 'item';
  mostrarCodigo = Code;

  titleHtml = 'Las entidades beneficiarias deberán tener su sede y actividad <strong class="font-bold">principal en Aragón.</strong>';
  descriptionHtml = 'Documento acreditativo de registro de alta de la <strong class="font-bold">asociación</strong>';
  contentHtml = 'Modelo: <a class="c-link break-all" href="#">aragon.es/tramites/19283-19348401/Modelofianza.pdf</a>';

  title: TitleData = {
    html: 'Registro de alta de la asociación',
    classes: 'font-bold'
  };
  description: DescriptionData = {
    html: 'Documento acreditativo de registro de alta de la asociación',
    classes: null
  };
  items: string[] = [ 'Desde modelo', 'Obligatorio previo a resolución', 'Condicionado'];
  contentBottom: ContentData = {
    html: 'Modelo: <a class="c-link break-all" href="#">aragon.es/tramites/19283-19348401/Modelofianza.pdf</a>',
    classes: 'text-neutral-dark'
  };
  contentRight: ContentData = {
    html: '<button class="c-button c-button--sm c-button--transparent">Eliminar</button><button class="c-button c-button--sm c-button--transparent">Editar</button>'
  };
  icon: IconData = {
    type: 'document',
    html: null
  };
  isDraggable: boolean;
  isLocked: boolean;
  headingLevel: number;
  id: string;
  classes: string;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  changeItems(event): void {
    this.items = [ 'Desde modelo', 'Obligatorio previo a resolución', 'Condicionado'];
    this.items.length = event;
  }

}
