import { Component } from '@angular/core';
import { ItemCheckboxData } from 'desy-angular';
import * as Code from './code-demo-links-list';

@Component({
  selector: 'app-demo-links-list',
  templateUrl: './demo-links-list.component.html',
})
export class DemoLinksListComponent {
  nameComponent = 'links-list';
  mostrarCodigo = Code;
  
  // Parametros componente
  hasNav = true;
  classes: string;
  idPrefix: string;

  // Parametros item custom
  itemContent = 'Item personalizado';
  itemContainerClasses: string;
  itemClasses: string;
  itemTitle: string;
  itemId: string;
  itemHref: string;
  itemRouterLink: string;
  itemRouterLinkActiveClasses: string;
  itemFragment: string;
  itemTarget: string;
  itemActive: boolean;
  itemDisabled: boolean;

  // Parametros item.icon custom
  itemIconContainerClasses: string;
  itemIconContent = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="1em" width="1em" class="inline-block align-middle"><path d="M19.71,20H4.29A2.28,2.28,0,0,1,2,17.71V6.29A2.28,2.28,0,0,1,4.29,4H7.72a2,2,0,0,1,1.44.61l1.19,1.24a.51.51,0,0,0,.36.15H20a2,2,0,0,1,2,2v9.71A2.28,2.28,0,0,1,19.71,20ZM4.29,6A.29.29,0,0,0,4,6.29V17.71a.29.29,0,0,0,.29.29H19.71a.29.29,0,0,0,.29-.29V8.5a.5.5,0,0,0-.5-.5h-9a2,2,0,0,1-1.44-.61L7.87,6.15A.55.55,0,0,0,7.51,6Z" fill="currentColor"></path></svg>';


  // Parametros item.sub custom
  itemSubClasses: string;
  itemSubContent = 'Contenido descriptivo';

  includeIconValue = [];
  includeIcon: ItemCheckboxData[] = [
    {
      value: 'include-icon',
      text: 'Incluir Icono personalizado',
      conditional: true
    },
  ];

  includeSubValue = [];
  includeSub: ItemCheckboxData[] = [
    {
      value: 'include-sub',
      text: 'Incluir Sub personalizado',
      conditional: true
    },
  ];
  
  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;
  
  // Atributos de accesibilidad
  itemRole: string;
  itemAriaLabel: string;
  itemAriaDescribedBy: string;
  itemAriaLabelledBy: string;
  itemAriaHidden: string;
  itemAriaDisabled: string;
  itemAriaControls: string;
  itemAriaCurrent: string;
  itemAriaLive: string;
  itemAriaExpanded: string;
  itemAriaErrorMessage: string;
  itemAriaHasPopup: string;
  itemTabindex: string;

}
