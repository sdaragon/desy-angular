import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoMenuNavigationComponent } from './demo-menu-navigation.component';

xdescribe('DemoMenuNavigationComponent', () => {
  let component: DemoMenuNavigationComponent;
  let fixture: ComponentFixture<DemoMenuNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoMenuNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoMenuNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
