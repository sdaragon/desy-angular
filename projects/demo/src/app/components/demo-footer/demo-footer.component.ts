import { Component, OnInit } from '@angular/core';
import { MetaData, MetaItemData, NavigationData, NavigationItemData } from '../../../../../desy-angular/src/lib/desy-nav/interfaces';
import * as Code from './code-demo-footer';

@Component({
  selector: 'app-demo-footer',
  templateUrl: './demo-footer.component.html'
})
export class DemoFooterComponent implements OnInit {
  
  nameComponent = 'footer';
  mostrarCodigo = Code;
  
  linksFooterItems = [];
  columnFooterNavigation = [];

  customMetaItem: MetaItemData = {
    text: 'Meta-item editable'
  };

  customMeta: MetaData = {
    visuallyHiddenTitle: 'hidden text',
    html: 'Meta sin items',
    items: []
  };

  customNavigationItem: NavigationItemData = {
    text: 'Navigation-item editable'
  };

  customNavigation: NavigationData = {
    title: 'Navigation editable',
    columns: 1,
    items: []
  };

  customFooter = {
    containerClasses: '',
    classes: '',
    meta: this.customMeta,
    navigation: [ this.customNavigation ],
    icon: {
      html: ''
    }, 
    description: {
      html: 'Descripción',
      text: '',
      visuallyHiddenTitle: 'Accesibilidad'
    },
    noLogo: false,
    url: '',
    logoContainerClasses: '',
    type: ''
  };

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;


  numMetaItems = 2;
  numNavigation = 1;
  numNavigationItems = 2;

  ngOnInit(): void {
    for (let i = 1; i <= 3; i++) {
      this.linksFooterItems.push({
        text: 'Item ' + i,
        routerLink: '#' + i
      });

      const nav: NavigationData = {
        title: 'Single column list ' + i,
        columns: 1,
        items: []
      };

      for (let j = 1; j <= 6; j++) {
        nav.items.push({
          routerLink: '#' + j,
          text: 'Navigation item ' + j
        });
      }
      this.columnFooterNavigation.push(nav);
    }

    this.recalculateMetaItems();
    this.recalculateNavigation();
  }

  recalculateNavigation(): void {
    this.customFooter.navigation = [];
    for (let i = 1; i < this.numNavigation; i++) {
      const items: NavigationItemData[] = [];
      for (let j = 1; j < this.numNavigationItems; j++) {
        items.push({
          href: '#' + j,
          text: 'Navigation item ' + j
        });
      }

      if (this.numNavigationItems > 0) {
        items.push(this.customNavigationItem);
      }

      this.customFooter.navigation.push({
        title: 'Single column list ' + i,
        columns: 1,
        items
      });
    }

    if (this.numNavigation > 0) {
      this.customNavigation.items = [];
      for (let j = 1; j < this.numNavigationItems; j++) {
        this.customNavigation.items.push({
          href: '#' + j,
          text: 'Navigation item ' + j
        });
      }

      if (this.numNavigationItems > 0) {
        this.customNavigation.items.push(this.customNavigationItem);
      }
      this.customFooter.navigation.push(this.customNavigation);
    }
  }

  recalculateMetaItems(): void {
    this.customMeta.items = [];
    for (let i = 1; i < this.numMetaItems; i++) {
      this.customMeta.items.push({
        text: 'Item ' + i,
        href: '#' + i
      });
    }

    if (this.numMetaItems > 0) {
      this.customMeta.items.push(this.customMetaItem);
    }
  }
}
