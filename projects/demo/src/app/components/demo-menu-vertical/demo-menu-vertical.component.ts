import * as Code from './code-demo-menu-vertical';
import { Component } from '@angular/core';
import { ItemCheckboxData, MenuVerticalItemsData, MenuVerticalSubItemsData } from 'desy-angular';

@Component({
  selector: 'app-demo-menu-vertical',
  templateUrl: './demo-menu-vertical.component.html'
})
export class DemoMenuVerticalComponent {

  nameComponent = 'menu-vertical';
  mostrarCodigo = Code;

  id = '';
  idPrefix = '';
  hasUnderline: boolean;
  classes: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


  subItem: MenuVerticalSubItemsData = {
    html: 'Subitem 1 - editable',
    href: null,
    routerLink: null,
    routerLinkActiveClasses: null,
    fragment: null,
    id: 'Subitem 1-id',
    disabled: null,
    active: null,
    divider: null,
    target: '',
    title: null
  };
  item: MenuVerticalItemsData = {
    id: 'itemId-2-editable',
    html: 'Item2 - editable',
    classes: '',
    href: '',
    routerLink: '/menu-vertical/item-editable-activo',
    routerLinkActiveClasses: 'uppercase',
    fragment: '',
    target: '',
    disabled: false,
    divider: false,
    sub: {
      html: '',
      classes: null,
      items: []
    }
  };
  subItems: MenuVerticalSubItemsData[] = [
    {
      html: 'Subitem 1',
      href: '',
      routerLink: '',
      routerLinkActiveClasses: '',
      fragment: '',
      id: 'Subitem 1-id',
      disabled: null,
      active: null,
      divider: null,
      target: '',
    },
    {
      html: 'Subitem 2',
      href: '',
      routerLink: '',
      routerLinkActiveClasses: '',
      fragment: '',
      id: 'Subitem 2-id',
      disabled: null,
      active: true,
      divider: null,
      target: '',
    },
    this.subItem
  ];


  hasSubItems: ItemCheckboxData[] = [
    {
      value: 'show',
      text: 'Incluir sub-items',
      conditional: true,
      name: 'showSubItems'
    }
  ];

  hasSubItemsModel: any;
  onChangeSubItems(): void {
    if (this.hasSubItemsModel && this.hasSubItemsModel.length > 0) {
      this.item.sub.items = this.subItems;
    } else {
      this.item.sub.items = [];
    }
  }
}
