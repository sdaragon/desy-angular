import * as Code from './code-demo-dropdown';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-dropdown',
  templateUrl: './demo-dropdown.component.html'
})
export class DemoDropdownComponent {

  nameComponent = 'dropdown';
  mostrarCodigo = Code;

  html = 'Desplegable';
  hiddenText: string;
  classes: string = "c-dropdown--sm";
  classesTooltip: string;
  classesContainer: string = "inline-block p-base bg-primary-light";
  id: string;
  disabled: boolean;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaErrorMessage: string;
  tabindex: string;

  clickCountContent = 0;
  clickContent: string;
  
  updateClickContent(): void {
    this.clickCountContent++;
    this.clickContent = 'Detectado click nº ' + this.clickCountContent;
  }

}
