import * as Code from './code-demo-accordion';
import {Component} from '@angular/core';
import {AccordionHeaderData, AccordionItemData, ItemCheckboxData} from 'desy-angular';


@Component({
  selector: 'app-demo-accordion',
  templateUrl: './demo-accordion.component.html'
})
export class DemoAccordionComponent {
  private static readonly defaultHtml = '<div class=" w-48 p-2 "><div class=" border-4 border-dashed border-gray-200 rounded-lg h-40 "></div></div>';

  nameComponent = 'Accordion';
  mostrarCodigo = Code;

  id: string;
  customIdPrefix = 'accordionId';
  customAllowToggle: boolean;
  customAllowMultiple: boolean;
  customShowControl: boolean;
  customClasses: string;
  headingLevel: number;

  heading: AccordionHeaderData = {
    text: 'Accordion example'
  };

  customItem: AccordionItemData = {
    headerText: 'Item parametrizado',
    html: DemoAccordionComponent.defaultHtml
  };
  items: AccordionItemData[] = [
    {
      headerText: 'Item 1',
      html: DemoAccordionComponent.defaultHtml
    },
    {
      headerText: 'Item 2',
      html: DemoAccordionComponent.defaultHtml
    },
    this.customItem
  ];

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  includeCustomButtonsValue = [];
  includeCustomButtons: ItemCheckboxData[] = [
    {
      value: 'custom-btn-show',
      text: 'Incluir botón Mostrar personalizado',
      conditional: true,
      name: 'itemButtons'
    },
    {
      value: 'custom-btn-hide',
      text: 'Incluir botón Ocultar personalizado',
      conditional: true,
      name: 'itemButtons'
    }
  ];

  customShowButton = '<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path d="M14 7a1 1 0 0 0-1-1H8.25A.25.25 0 0 1 8 5.75V1a1 1 0 0 0-2 0v4.75a.25.25 0 0 1-.25.25H1a1 1 0 0 0 0 2h4.75a.25.25 0 0 1 .25.25V13a1 1 0 0 0 2 0V8.25A.25.25 0 0 1 8.25 8H13a1 1 0 0 0 1-1Z" fill="currentColor" transform="scale(3.42857)"/></svg>';
  customHideButton = '<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path d="M13 8H1a1 1 0 0 1 0-2h12a1 1 0 0 1 0 2Z" fill="currentColor" transform="scale(3.42857)"/></svg>';
  customShowButtonClass: string;
  customHideButtonClass: string;

  hasCustomButton(value: string): boolean {
    return this.includeCustomButtonsValue.includes(value);
  }
}
