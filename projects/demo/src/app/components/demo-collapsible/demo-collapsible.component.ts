import { Component } from '@angular/core';
import * as Code from './code-demo-collapsible';

@Component({
  selector: 'app-demo-collapsible',
  templateUrl: './demo-collapsible.component.html'
})
export class DemoCollapsibleComponent {

  nameComponent = 'collapsible';
  mostrarCodigo = Code;

  phone: string;

  headerHtml: string = "headerText de prueba";
  contentHtml: string = "text de prueba";
  id: string;
  open: boolean;
  classes: string;
  buttonClasses: string;
  showClasses: string;
  hideClasses: string;
  contentClasses: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
