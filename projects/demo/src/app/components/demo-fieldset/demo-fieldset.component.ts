import * as Code from './code-demo-fieldset';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-fieldset',
  templateUrl: './demo-fieldset.component.html'
})
export class DemoFieldsetComponent {

  nameComponent = 'fieldset';
  mostrarCodigo = Code;

  describedBy: string;
  errorId: string;
  // legend
  legendHtml = '¿Cuál es tu número de teléfono?';
  legendclasses: string = "c-h1 mb-sm";
  legendisPageHeading: boolean = true;

  classes = 'p-base bg-warning-light';
  id: string;
  headingLevel: number = 1;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  phone: string;

  constructor() { }

}
