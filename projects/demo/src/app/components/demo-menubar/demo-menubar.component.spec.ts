import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoMenubarComponent } from './demo-menubar.component';

xdescribe('DemoMenubarComponent', () => {
  let component: DemoMenubarComponent;
  let fixture: ComponentFixture<DemoMenubarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoMenubarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoMenubarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
