import * as Code from './code-demo-description-list';
import { Component } from '@angular/core';
import { DescriptionItemData } from 'desy-angular';

@Component({
  selector: 'app-demo-description-list',
  templateUrl: './demo-description-list.component.html'
})
export class DemoDescriptionListComponent {

  nameComponent = 'description-list';
  mostrarCodigo = Code;

  classes = 'inline-block p-base border border-neutral-base rounded';
  id: string;
  item: DescriptionItemData = {
    classes: 'mb-base',
    term: {
      html: 'termino para editar',
    },
    definition: {
      html: 'definition para editar'
    }
  };

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
