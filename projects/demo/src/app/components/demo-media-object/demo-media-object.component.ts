import * as Code from './code-demo-media-object';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-media-object',
  templateUrl: './demo-media-object.component.html'
})
export class DemoMediaObjectComponent {

  nameComponent = 'media-object';
  mostrarCodigo = Code;

  classes: string;
  id: string;
  center: boolean;
  reverse: boolean;
  figureClasses: string;
  contentClasses: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  constructor() { }

}
