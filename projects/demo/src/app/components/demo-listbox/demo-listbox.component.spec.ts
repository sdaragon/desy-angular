import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoListboxComponent } from './demo-listbox.component';

xdescribe('DemoListboxComponent', () => {
  let component: DemoListboxComponent;
  let fixture: ComponentFixture<DemoListboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoListboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoListboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
