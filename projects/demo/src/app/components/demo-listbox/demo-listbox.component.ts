import * as Code from './code-demo-listbox';
import { ChangeDetectorRef, Component } from '@angular/core';
import { ListboxItemData, ListboxLabelData } from 'desy-angular';

@Component({
  selector: 'app-demo-listbox',
  templateUrl: './demo-listbox.component.html'
})
export class DemoListboxComponent {

  nameComponent = 'listbox';
  mostrarCodigo = Code;
  id = 'listbox-editable';
  item: ListboxItemData = {
    html: 'item editable'
  }
  label: ListboxLabelData = {
    html: 'Esto es un label',
    classes: ''
  };
  html = 'Ejemplo';
  isMultiselectable: boolean;
  doesChangeButtonText: boolean;
  classes: string;
  classesContainer: string;
  classesTooltip: string;
  idPrefix: string;
  disabled: boolean;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaErrorMessage: string;
  tabindex: string;

  activeItems = [];

  constructor(private changeDetectionRef: ChangeDetectorRef) { }

  onItemChange(event): void {
    this.activeItems = event;
    this.changeDetectionRef.detectChanges();
  }
}
