/** Autogenerated with npm run code, do not modify here, only in original files. Here the changes will be overridden*/
export const codigoHtml0 = `<desy-listbox [id]="id" (itemsChange)="onItemChange($event)" [isMultiselectable]="isMultiselectable" [doesChangeButtonText]="doesChangeButtonText" [classes]="classes" [classesContainer]="classesContainer" [classesTooltip]="classesTooltip" [idPrefix]="idPrefix" [disabled]="disabled" [role]="role" [ariaLabel]="ariaLabel" [ariaDescribedBy]="ariaDescribedBy" [ariaLabelledBy]="ariaLabelledBy" [ariaHidden]="ariaHidden" [ariaDisabled]="ariaDisabled" [ariaControls]="ariaControls" [ariaCurrent]="ariaCurrent" [ariaLive]="ariaLive" [ariaErrorMessage]="ariaErrorMessage" [tabindex]="tabindex">
  <ng-container *appCustomInnerContent="{html: html}"></ng-container>
  <desy-listbox-label [classes]="label.classes">
    <ng-container *appCustomInnerContent="{html: label.html}"></ng-container>
  </desy-listbox-label>
  <desy-listbox-item>Item1</desy-listbox-item>
  <desy-listbox-item>Item2 <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140" width="1em" height="1em" class="inline-block align-baseline ml-sm" aria-label="Archivo" focusable="false" role="img">
      <path d="M89.355 12.518l26.46 26.46a2.917 2.917 0 01.852 2.06v84.379a2.917 2.917 0 01-2.917 2.916h-87.5a2.917 2.917 0 01-2.917-2.916V14.583a2.917 2.917 0 012.917-2.916h61.046a2.917 2.917 0 012.059.851zM90.918 0H23.333a11.667 11.667 0 00-11.666 11.667v116.666A11.667 11.667 0 0023.333 140h93.334a11.667 11.667 0 0011.666-11.667V37.415a5.833 5.833 0 00-1.709-4.124L95.042 1.709A5.833 5.833 0 0090.918 0z" fill="currentColor"></path>
      <path d="M93.333 64.167h-52.5a5.833 5.833 0 010-11.667h52.5a5.833 5.833 0 010 11.667zM93.333 87.5h-52.5a5.833 5.833 0 010-11.667h52.5a5.833 5.833 0 010 11.667zM67.083 110.833h-26.25a5.833 5.833 0 010-11.666h26.25a5.833 5.833 0 010 11.666z" fill="currentColor"></path>
    </svg></desy-listbox-item>
  <desy-listbox-item [id]="item.id" [classes]="item.classes" [title]="item.title" [(active)]="item.active" [role]="item.role" [ariaLabel]="item.ariaLabel" [ariaDescribedBy]="item.ariaDescribedBy" [ariaLabelledBy]="item.ariaLabelledBy" [ariaHidden]="item.ariaHidden" [ariaDisabled]="item.ariaDisabled" [ariaControls]="item.ariaControls" [ariaCurrent]="item.ariaCurrent" [ariaLive]="item.ariaLive" [ariaErrorMessage]="item.ariaErrorMessage" [tabindex]="item.tabindex">
    <ng-container *appCustomInnerContent="{html: item.html}"></ng-container>
  </desy-listbox-item>
</desy-listbox>`;
export const codigoTs0 = `import * as Code from './code-demo-listbox';
import { ChangeDetectorRef, Component } from '@angular/core';
import { ListboxItemData, ListboxLabelData } from 'desy-angular';

@Component({
  selector: 'app-demo-listbox',
  templateUrl: './demo-listbox.component.html'
})
export class DemoListboxComponent {

  nameComponent = 'listbox';
  mostrarCodigo = Code;
  id = 'listbox-editable';
  item: ListboxItemData = {
    html: 'item editable'
  }
  label: ListboxLabelData = {
    html: 'Esto es un label',
    classes: ''
  };
  html = 'Ejemplo';
  isMultiselectable: boolean;
  doesChangeButtonText: boolean;
  classes: string;
  classesContainer: string;
  classesTooltip: string;
  idPrefix: string;
  disabled: boolean;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaErrorMessage: string;
  tabindex: string;

  activeItems = [];

  constructor(private changeDetectionRef: ChangeDetectorRef) { }

  onItemChange(event): void {
    this.activeItems = event;
    this.changeDetectionRef.detectChanges();
  }
}
`;
export const codigoHtml1 = `<desy-listbox>
  Por defecto
  <desy-listbox-label>Label</desy-listbox-label>
  <desy-listbox-item>Item1</desy-listbox-item>
  <desy-listbox-item>Item2</desy-listbox-item>
  <desy-listbox-item>Item3</desy-listbox-item>
</desy-listbox>`;
export const codigoTs1 = `import * as Code from './code-demo-listbox';
import { ChangeDetectorRef, Component } from '@angular/core';
import { ListboxItemData, ListboxLabelData } from 'desy-angular';

@Component({
  selector: 'app-demo-listbox',
  templateUrl: './demo-listbox.component.html'
})
export class DemoListboxComponent {

  nameComponent = 'listbox';
  mostrarCodigo = Code;
  id = 'listbox-editable';
  item: ListboxItemData = {
    html: 'item editable'
  }
  label: ListboxLabelData = {
    html: 'Esto es un label',
    classes: ''
  };
  html = 'Ejemplo';
  isMultiselectable: boolean;
  doesChangeButtonText: boolean;
  classes: string;
  classesContainer: string;
  classesTooltip: string;
  idPrefix: string;
  disabled: boolean;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaErrorMessage: string;
  tabindex: string;

  activeItems = [];

  constructor(private changeDetectionRef: ChangeDetectorRef) { }

  onItemChange(event): void {
    this.activeItems = event;
    this.changeDetectionRef.detectChanges();
  }
}
`;
export const codigoHtml2 = `<desy-listbox classes="c-listbox--transparent">
  Por defecto
  <desy-listbox-label>Label</desy-listbox-label>
  <desy-listbox-item>Item1</desy-listbox-item>
  <desy-listbox-item>Item2</desy-listbox-item>
  <desy-listbox-item>Item3</desy-listbox-item>
</desy-listbox>`;
export const codigoTs2 = `import * as Code from './code-demo-listbox';
import { ChangeDetectorRef, Component } from '@angular/core';
import { ListboxItemData, ListboxLabelData } from 'desy-angular';

@Component({
  selector: 'app-demo-listbox',
  templateUrl: './demo-listbox.component.html'
})
export class DemoListboxComponent {

  nameComponent = 'listbox';
  mostrarCodigo = Code;
  id = 'listbox-editable';
  item: ListboxItemData = {
    html: 'item editable'
  }
  label: ListboxLabelData = {
    html: 'Esto es un label',
    classes: ''
  };
  html = 'Ejemplo';
  isMultiselectable: boolean;
  doesChangeButtonText: boolean;
  classes: string;
  classesContainer: string;
  classesTooltip: string;
  idPrefix: string;
  disabled: boolean;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaErrorMessage: string;
  tabindex: string;

  activeItems = [];

  constructor(private changeDetectionRef: ChangeDetectorRef) { }

  onItemChange(event): void {
    this.activeItems = event;
    this.changeDetectionRef.detectChanges();
  }
}
`;
export const codigoHtml3 = `<desy-listbox>
  Párrafos en items
  <desy-listbox-label>Label</desy-listbox-label>
  <desy-listbox-item classes="flex-wrap">
    Actuaciones previas/preparatorias <p class="text-xs font-normal pointer-events-none">Consulta previa, actos preparatorios de expediente, bases reguladoras...</p>
  </desy-listbox-item>
  <desy-listbox-item classes="flex-wrap">
    Inicio de la tramitación <p class="text-xs font-normal pointer-events-none">Solicitud, subsanación, declaración responsable, aprobación expediente...</p>
  </desy-listbox-item>
  <desy-listbox-item classes="flex-wrap">
    Otros trámites en fase de inicio <p class="text-xs font-normal pointer-events-none">Documentos que no se puedan asociar a ningún otro tramite en fase de inicio</p>
  </desy-listbox-item>
  <desy-listbox-item classes="flex-wrap">
    Participación pública <p class="text-xs font-normal pointer-events-none">Información pública, alegaciones, audiencia...</p>
  </desy-listbox-item>
  <desy-listbox-item classes="flex-wrap">
    Informes sectoriales <p class="text-xs font-normal pointer-events-none">Informes sectoriales que se emiten en la tramitación por órganos distintos del instructor...</p>
  </desy-listbox-item>
  <desy-listbox-item classes="flex-wrap">
    Valoración/Prueba/Licitación <p class="text-xs font-normal pointer-events-none">Informe de valoración, documentos técnicos de evaluación, peticiones de prueba, documentos fase de licitación, informes propuesta...</p>
  </desy-listbox-item>
</desy-listbox>`;
export const codigoTs3 = `import * as Code from './code-demo-listbox';
import { ChangeDetectorRef, Component } from '@angular/core';
import { ListboxItemData, ListboxLabelData } from 'desy-angular';

@Component({
  selector: 'app-demo-listbox',
  templateUrl: './demo-listbox.component.html'
})
export class DemoListboxComponent {

  nameComponent = 'listbox';
  mostrarCodigo = Code;
  id = 'listbox-editable';
  item: ListboxItemData = {
    html: 'item editable'
  }
  label: ListboxLabelData = {
    html: 'Esto es un label',
    classes: ''
  };
  html = 'Ejemplo';
  isMultiselectable: boolean;
  doesChangeButtonText: boolean;
  classes: string;
  classesContainer: string;
  classesTooltip: string;
  idPrefix: string;
  disabled: boolean;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaErrorMessage: string;
  tabindex: string;

  activeItems = [];

  constructor(private changeDetectionRef: ChangeDetectorRef) { }

  onItemChange(event): void {
    this.activeItems = event;
    this.changeDetectionRef.detectChanges();
  }
}
`;
