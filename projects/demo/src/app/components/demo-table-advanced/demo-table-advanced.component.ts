import { Component, OnInit } from '@angular/core';
import { CellData, HeadCellData, OrderBy, RecalculateTableParams, RowData, SelectItemData, WrapperData } from 'desy-angular';
import { SearchUtils } from '../../shared/utils/search-utils';
import * as Code from './code-demo-table-advanced';

@Component({
  selector: 'app-demo-table-advanced',
  templateUrl: './demo-table-advanced.component.html'
})
export class DemoTableAdvancedComponent implements OnInit {

  nameComponent = 'Table-advanced';
  mostrarCodigo = Code;

  caption: string;
  captionClasses: string;
  firstCellIsHeader: boolean;
  hasCheckboxes: boolean;
  idPrefix: string;
  classes = 'min-w-full';
  checkboxClasses: string;
  id: string;
  wrapper: WrapperData = {};
  hasSelect: boolean;
  hasFilter: boolean = true;
  orderBy = OrderBy

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  rowsChecked: any;
  rowsChecked2: any;

  itemCell: CellData = {
    html: '125€',
    classes: 'text-right'
  };
  itemRow: RowData = {
    id: 'miFila',
    cellsList: [{ html: 'Third row' }, { html: 'March' }, { html: '165€', classes: 'text-right' }, this.itemCell]
  };
  itemHead: HeadCellData = {
    html: 'Rate for vehicles',
    orderBy: OrderBy.none,
    classes: 'text-right',
    hasFilter: true
  };
  itemFooterCell: CellData = {
    html: '275€',
    classes: 'text-right'
  };
  itemFoot: RowData = {
    id: 'miFoot',
    cellsList: [{ 'html': 'First footer row' }, { 'html': 'Suma' }, { 'html': '325€', 'classes': 'text-right' }, this.itemFooterCell]
  };
  companyList = [{ name: 'Bsh electrodomesticos españa sa', initialIndex: 0, randomText: '' }, { name: 'Sociedad anonima industrias celulosa aragonesa', initialIndex: 0, randomText: '' }, { name: 'Esprinet iberica sl', initialIndex: 0, randomText: '' }, { name: 'Fujikura automotive europe sau', initialIndex: 0, randomText: '' }, { name: 'Adidas españa sau', initialIndex: 0, randomText: '' }, { name: 'Schindler, sa', initialIndex: 0, randomText: '' }, { name: 'Trans sese sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Amplifon iberica sau', initialIndex: 0, randomText: '' }, { name: 'Saica pack sl', initialIndex: 0, randomText: '' }, { name: 'Novapet sa', initialIndex: 0, randomText: '' }, { name: 'Argal alimentacion sa.', initialIndex: 0, randomText: '' }, { name: 'Carreras grupo logistico sa.', initialIndex: 0, randomText: '' }, { name: 'Megasider zaragoza sa', initialIndex: 0, randomText: '' }, { name: 'Caladero sl', initialIndex: 0, randomText: '' }, { name: 'Lecitrailer sa', initialIndex: 0, randomText: '' }, { name: 'Saica natur sociedad limitada', initialIndex: 0, randomText: '' }, { name: 'Daniel aguilo panisello sociedad anonima', initialIndex: 0, randomText: '' }, { name: 'Bebinter sa', initialIndex: 0, randomText: '' }, { name: 'Cuarte sl', initialIndex: 0, randomText: '' }, { name: 'Mann-hummel iberica sa', initialIndex: 0, randomText: '' }, { name: 'Valeo termico sau', initialIndex: 0, randomText: '' }, { name: 'Tereos starch & sweeteners iberia s.a.u.', initialIndex: 0, randomText: '' }, { name: 'Proclinic sa', initialIndex: 0, randomText: '' }, { name: 'Lozano transportes sau', initialIndex: 0, randomText: '' }, { name: 'Saltoki logistica zaragoza sa.', initialIndex: 0, randomText: '' }, { name: 'Distribuciones agropecuarias de aragon sl', initialIndex: 0, randomText: '' }, { name: 'Agreda automovil, sa', initialIndex: 0, randomText: '' }, { name: 'La zaragozana sa', initialIndex: 0, randomText: '' }, { name: 'Punt roma sl', initialIndex: 0, randomText: '' }, { name: 'Multienergia verde sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Linamar light metals zaragoza sa.', initialIndex: 0, randomText: '' }, { name: 'Nurel sa', initialIndex: 0, randomText: '' }, { name: 'Industrias quimicas del ebro, sa', initialIndex: 0, randomText: '' }, { name: 'Aves nobles y derivados sl', initialIndex: 0, randomText: '' }, { name: 'Hiab cranes sl', initialIndex: 0, randomText: '' }, { name: 'Vian as automobile sl', initialIndex: 0, randomText: '' }, { name: 'Productos porcinos secundarios sociedad anonima', initialIndex: 0, randomText: '' }, { name: 'Industrias aragonesas del aluminio sa', initialIndex: 0, randomText: '' }, { name: 'Enterprise solutions outsourcing españa sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Garda servicios de seguridad sa', initialIndex: 0, randomText: '' }, { name: 'Bergner europe, sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Generos de punto victrix sl', initialIndex: 0, randomText: '' }, { name: 'Cables rct sa', initialIndex: 0, randomText: '' }, { name: 'Servisar servicios sociales sl', initialIndex: 0, randomText: '' }, { name: 'Politours sa (en liquidacion)', initialIndex: 0, randomText: '' }, { name: 'Electrical components international slu', initialIndex: 0, randomText: '' }, { name: 'Jorge pork meat slu', initialIndex: 0, randomText: '' }, { name: 'Minera de santa marta sa', initialIndex: 0, randomText: '' }, { name: 'Servigas s xxi sa', initialIndex: 0, randomText: '' }, { name: 'Automoviles sanchez, sa', initialIndex: 0, randomText: '' }, { name: 'Farmhispania s.a.', initialIndex: 0, randomText: '' }, { name: 'Cables de comunicaciones zaragoza sl', initialIndex: 0, randomText: '' }, { name: 'Valorista sl.', initialIndex: 0, randomText: '' }, { name: 'Ibercaja mediacion de seguros sa', initialIndex: 0, randomText: '' }, { name: 'Celulosa fabril sa', initialIndex: 0, randomText: '' }, { name: 'Frutaria agricultura sl.', initialIndex: 0, randomText: '' }, { name: 'Aragocias, sociedad anonima', initialIndex: 0, randomText: '' }, { name: 'Vitalia home sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Cerro murillo sa', initialIndex: 0, randomText: '' }, { name: 'Aragon wagen sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Saltoki suministros zaragoza, sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Zalux sa', initialIndex: 0, randomText: '' }, { name: 'Zarsol sociedad limitada', initialIndex: 0, randomText: '' }, { name: 'Ehisa construcciones y obras sa', initialIndex: 0, randomText: '' }, { name: 'Arquisocial sociedad limitada', initialIndex: 0, randomText: '' }, { name: 'Wittur elevator components sociedad anonima', initialIndex: 0, randomText: '' }, { name: 'Master distancia sa', initialIndex: 0, randomText: '' }, { name: 'Fertinagro agrovip sl.', initialIndex: 0, randomText: '' }, { name: 'Construcciones mariano lopez navarro sa', initialIndex: 0, randomText: '' }, { name: 'Distribuidora internacional carmen sa', initialIndex: 0, randomText: '' }, { name: 'Destilerias mg sl', initialIndex: 0, randomText: '' }, { name: 'Dexiberica soluciones industriales sau', initialIndex: 0, randomText: '' }, { name: 'Velpiri sau.', initialIndex: 0, randomText: '' }, { name: 'Airtex products sa', initialIndex: 0, randomText: '' }, { name: 'Oviaragon s.c.l.', initialIndex: 0, randomText: '' }, { name: 'Sociedad aragonesa de gestion agroambiental sl.', initialIndex: 0, randomText: '' }, { name: 'Vitalia suite, sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'International casing products slu', initialIndex: 0, randomText: '' }, { name: 'Scanfisk seafood sl', initialIndex: 0, randomText: '' }, { name: 'Molinos del ebro sa', initialIndex: 0, randomText: '' }, { name: 'Euroarce mineria s.a.', initialIndex: 0, randomText: '' }, { name: 'Solitium sl.', initialIndex: 0, randomText: '' }, { name: 'Comercial salgar sl', initialIndex: 0, randomText: '' }, { name: 'Insonorizantes pelzer sa', initialIndex: 0, randomText: '' }, { name: 'Maetel instalaciones y servicios industriales sa.', initialIndex: 0, randomText: '' }, { name: 'Ringo valvulas sl', initialIndex: 0, randomText: '' }, { name: 'Trox españa sa', initialIndex: 0, randomText: '' }, { name: 'Teltronic sa', initialIndex: 0, randomText: '' }, { name: 'Brilen sa', initialIndex: 0, randomText: '' }, { name: 'Keter iberia sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Business telecommunications services europe sa.', initialIndex: 0, randomText: '' }, { name: 'Bm sportech ib sociedad limitada.', initialIndex: 0, randomText: '' }, { name: 'Chemieuro sl', initialIndex: 0, randomText: '' }, { name: 'Deutschland autos slu (extinguida)', initialIndex: 0, randomText: '' }, { name: 'Laboratorios saphir sa', initialIndex: 0, randomText: '' }, { name: 'Avanza zaragoza sa.', initialIndex: 0, randomText: '' }, { name: 'Valfondo inmuebles sociedad limitada', initialIndex: 0, randomText: '' }, { name: 'Fersa bearings sa', initialIndex: 0, randomText: '' }, { name: 'Brass & fittings sl', initialIndex: 0, randomText: '' }, { name: 'Mas prevencion servicio de prevencion sociedad limitada.', initialIndex: 0, randomText: '' },];
  currentCompanyPage = 1;
  companyPageElems = 7;
  visibleCompanyList = this.companyList;
  pageCompanyList = this.companyList;

  selectItems: SelectItemData[] = [{ text: 'Selecciona una opción', selected: true, value: '' }, { text: 'January', value: 'January' }, { text: 'February', value: 'February' }, { text: 'March', value: 'March' }];
  selectItemsContent: SelectItemData[] = [{ text: 'Selecciona una opción', selected: true, value: '' }, { text: '85€', value: '85€' }, { text: '75€', value: '75€' }, { text: '165€', value: '165€' }];
  selectItemsCol3: SelectItemData[] = [{ text: 'Selecciona una opción', selected: true, value: '' }, { text: '95€', value: '95€' }, { text: '55€', value: '55€' }, { text: '125€', value: '125€' }];

  selectObject = { classes: '', disabled: false, formGroupClasses: '' }

  firstRowChecked = true;

  ngOnInit(): void {
    this.companyList.forEach((c, i) => c.initialIndex = i);
    this.companyList.forEach(c => c.randomText = this.generateRandomText(20));
    this.handleCompanyPagChange(this.currentCompanyPage);
  }

  private generateRandomText(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  handleCompanyPagChange(page: number): void {
    this.pageCompanyList = this.visibleCompanyList.slice((page - 1) * this.companyPageElems,
      Math.min(page * this.companyPageElems, this.companyList.length));
    this.currentCompanyPage = page;
  }

  handleRecalculateCompanyTable(recalculateParams: RecalculateTableParams): void {
    let rows = this.companyList;
    recalculateParams.filters.forEach(filter => {
      rows = rows.filter(row =>
        SearchUtils.containsAnyWordFrom(this.getCellString(row, filter.columnIndex) + '', filter.filterText));
    });

    if (recalculateParams.sort) {
      const columnIndex = recalculateParams.sort.columnIndex;
      const isAsc = recalculateParams.sort.order === OrderBy.asc;
      rows = rows.sort((a, b) =>
        this.compareCellContent(this.getCellString(a, columnIndex), this.getCellString(b, columnIndex), isAsc));
    }

    this.visibleCompanyList = rows;
    this.pageCompanyList = this.visibleCompanyList.slice((this.currentCompanyPage - 1) * this.companyPageElems,
      Math.min(this.currentCompanyPage * this.companyPageElems, this.companyList.length));
  }

  getCellString(row, columnIndex: number): string {
    if (columnIndex === 0) {
      return row.initialIndex + '';
    } else if (columnIndex === 1) {
      return row.name;
    } else if (columnIndex === 2) {
      return row.randomText;
    }
  }

  /**
   * Compara el contenido de dos celdas. Este se invierte si el orden especificado es descendente.
   */
  compareCellContent(a: string, b: string, isAsc: boolean): number {
    let result;
    const numberRegex = /^-?\d+(\.|,)?\d*$/;
    const dateRegex = /^(\d{2})(\/|-)(\d{2})(\/|-)(\d{4})( (\d{2}):(\d{2})(:(\d{2}))?)?$/;
    if (a && b) {
      if (numberRegex.test(a) && numberRegex.test(b)) { // Numeros
        result = (parseFloat(a) - parseFloat(b))
      }
      else if (dateRegex.test(a) && dateRegex.test(b)) { // Fechas
        result = this.parseDateTime(a) - this.parseDateTime(b);
      } else { // Texto
        result = a.localeCompare(b, 'es', { sensitivity: 'base', ignorePunctuation: true });
      }
    }
    return result * (isAsc ? 1 : -1);
  }

  private parseDateTime(input: string) { // 31/12/2023 12:12:12 or 31-12-2023 12:12:12 fechas que acepta, hh:mm:ss es opcional
    let [datePart, timePart] = input.split(' ');
    let [day, month, year] = datePart.split(/\/|-/).map(Number);
    let [hours = 0, minutes = 0, seconds = 0] = (timePart ? timePart.split(':') : []).map(Number);
    let date = new Date(year, month - 1, day, hours, minutes, seconds);
    return date.getTime();
  }

}
