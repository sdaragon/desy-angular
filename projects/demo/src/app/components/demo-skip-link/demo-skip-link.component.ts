import * as Code from './code-demo-skip-link';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-skip-link',
  templateUrl: './demo-skip-link.component.html'
})
export class DemoSkipLinkComponent {

  nameComponent = 'skip-link';
  mostrarCodigo = Code;

  html: string = "<strong>Soy skip-link</strong>";
  fragment: string;
  classes = 'ds-focus';
  id: string;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
