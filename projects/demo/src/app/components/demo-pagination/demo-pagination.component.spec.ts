import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoPaginationComponent } from './demo-pagination.component';

xdescribe('DemoPaginationComponent', () => {
  let component: DemoPaginationComponent;
  let fixture: ComponentFixture<DemoPaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemoPaginationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
