import * as Code from './code-demo-pagination';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-pagination',
  templateUrl: './demo-pagination.component.html'
})
export class DemoPaginationComponent {

  nameComponent = 'pagination';
  mostrarCodigo = Code;
  
  id = 'id-editable';
  idPrefix = 'id-prefix-edit';
  hasSelect: boolean;
  classesContainer: string;
  totalItems = 5;
  itemsPerPage = 1;
  hasPrevious = true;
  hasNext = true;
  hasFirst = true;
  hasLast = true;
  previousText: string;
  nextText: string;
  firstText: string = "Primera"
  lastText:string = "Última"
  showPrevious = true;
  showNext = true;
  showFirst = true;
  showLast = true;
  classes: string;
  labelListbox = {html: '', classes:''}
  hasSelectItemsPerPage = true;

  currentPage = 1;

  selectedPage = 2;
  selectedPageDefault: number;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
