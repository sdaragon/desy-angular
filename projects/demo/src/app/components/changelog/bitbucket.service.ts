import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BitbucketService {
  private baseUrl = 'https://api.bitbucket.org/2.0/repositories';
  private workspace = 'sdaragon';
  private repoSlug = 'desy-angular';

  constructor(private http: HttpClient) {}

  getTags() {
    return this.http
      .get<any>(
        `${this.baseUrl}/${this.workspace}/${this.repoSlug}/refs/tags?pagelen=100`
      )
      .pipe(map((data) => data.values.map((tag) => tag.name)));
  }

  getCommitsBetweenTags(tag_nuevo: string, tag_anterior: string) {
    return this.http
      .get<any>(
        `${this.baseUrl}/${this.workspace}/${this.repoSlug}/commits/${tag_nuevo}?exclude=${tag_anterior}&pagelen=100`
      )
      .pipe(
        map((data) =>
          data.values.map((commit) => ({
            hash: commit.hash,
            message: commit.message,
            date: commit.date,
            link: commit.links.html.href,
          }))
        )
      );
  }
}
