import { Component, OnInit } from '@angular/core';
import { BitbucketService } from './bitbucket.service';

@Component({
  selector: 'app-changelog',
  templateUrl: './changelog.component.html',
})
export class ChangeLogComponent implements OnInit {
  changeLog = [];
  constructor(private bitbucketService: BitbucketService) { }

  ngOnInit() {
    this.getChangelog();
  }

  async getChangelog() {
    let tags = await this.bitbucketService.getTags().toPromise();
    const storedTags = JSON.parse(localStorage.getItem('tags')) || [];

    // Comprobar si hay nuevos tags
    const newTags = tags.filter((tag) => !storedTags.includes(tag));

    // Si no hay nuevos tags, recuperar la información del almacenamiento local
    if (newTags.length === 0) {
      this.changeLog = JSON.parse(localStorage.getItem('changeLog')) || [];
      return;
    }
    // Si hay nuevos tags, hacer las solicitudes a la API
    tags.sort((a: string, b: string) => {
      let aNum = a.substring(1).split('.').map(Number); // quita la "v" y divide en segmentos
      let bNum = b.substring(1).split('.').map(Number); // quita la "v" y divide en segmentos
  
      // compara cada segmento en orden
      for (let i = 0; i < aNum.length; i++) {
          if (aNum[i] !== bNum[i]) {
              return bNum[i] - aNum[i]; // orden descendente
          }
      }
  
      return 0; // son iguales
  });
  
    this.changeLog = [];
    for (let i = 0; i < tags.length - 1; i++) {
      const tag_nuevo = tags[i];
      const tag_anterior = tags[i + 1];
      let commits = await this.bitbucketService
        .getCommitsBetweenTags(tag_nuevo, tag_anterior)
        .toPromise();
      commits = commits.filter(
        (c) =>
          !c.message.toLowerCase().includes('merge') &&
          !c.message.toLowerCase().includes('fusionado') &&
          !c.message.toLowerCase().includes('bitbucket') &&
          !c.message.toLowerCase().includes('version') &&
          !c.message.toLowerCase().includes('versión')
      );
      commits = commits.map((c) => {
        c.message = c.message.charAt(0).toUpperCase() + c.message.slice(1);
        return c;
      });
      this.changeLog.push({
        name: tag_nuevo,
        values: commits,
      });
    }

    // Guardar los tags y el changelog en el almacenamiento local
    localStorage.setItem('tags', JSON.stringify(tags));
    localStorage.setItem('changeLog', JSON.stringify(this.changeLog));
  }
}
