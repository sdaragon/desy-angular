import { Component } from '@angular/core';
import { ModalButtonData, ModalData, ModalIconData, ModalOptions } from 'desy-angular';
import * as Code from './code-demo-modal';

@Component({
  selector: 'app-demo-modal',
  templateUrl: './demo-modal.component.html'
})
export class DemoModalComponent implements ModalOptions {

  nameComponent = 'modal';
  mostrarCodigo = Code;

  titleModal: ModalData = {
    html: 'Aviso',
    text: null,
    classes: ''
  };
  description: ModalData = {
    html: 'Estamos realizando labores de mantenimiento en el sistema. Es posible que algunos procesos tarden más de lo esperado. Rogamos disculpas.',
    text: null,
    classes: null
  };
  icon: ModalIconData = {
    html: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="1em" height="1em" class="block w-16 h-16 text-primary-light" aria-label="Pregunta" focusable="false"><path d="M12,0A12,12,0,1,0,24,12,12,12,0,0,0,12,0Zm0,19a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,12,19Zm1.6-6.08a1,1,0,0,0-.6.92,1,1,0,0,1-2,0,3,3,0,0,1,1.8-2.75A2,2,0,1,0,10,9.25a1,1,0,0,1-2,0,4,4,0,1,1,5.6,3.67Z" fill="currentColor"></path></svg>',
    type: null
  };

  itemP: ModalButtonData = {
    html: 'texto itemPrimary',
    classes: 'c-button--primary'
  };
  itemsPrimary: ModalButtonData[] = [ this.itemP ];
  itemS: ModalButtonData = {
    html: 'texto itemsSecondary',
    classes: 'c-button--secondary'
  };
  itemsSecondary: ModalButtonData[] = [ this.itemS ];

  itemLoaderP: ModalButtonData = {
    html: 'texto itemLoader1',
    classes: 'c-button-loader--primary',
    state: 'is-loading'
  }
  itemsLoaderPrimary: ModalButtonData[] = [this.itemLoaderP];

  itemLoaderS: ModalButtonData = {
    html: 'texto itemLoader2',
    classes: 'c-button-loader--secondary',
    state: 'is-success'
  }
  itemsLoaderSecondary: ModalButtonData[] = [this.itemLoaderS];

  id =  'id contenedor';
  isDismissible: boolean;
  classes: string;
  headingLevel: number;
  activeItemsLoader: boolean = false;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  stateButton = 'is-success';

  callerContent: string;

  textEditableContent: string;
  setTextContent(value: string): void {
    console.log("event");
    this.textEditableContent = value;
    this.stateButton = 'is-loading';
    setTimeout(() => {
      this.stateButton = 'is-success';
    }, 2000);
  }
}
