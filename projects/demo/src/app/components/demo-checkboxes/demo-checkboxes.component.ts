import * as Code from './code-demo-checkboxes';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorMessageData, FieldsetData, HintData, ItemCheckboxData } from 'desy-angular';

@Component({
  selector: 'app-demo-checkboxes',
  templateUrl: './demo-checkboxes.component.html'
})
export class DemoCheckboxesComponent implements OnInit {

  nameComponent = 'Checkboxes';
  mostrarCodigo = Code;

  mainValue;
  valueContent;

  form: FormGroup;
  formGroupContactClasses: string;

  firstChecked = true;

  fieldsetData: FieldsetData = {
    legend: {
      text: 'Which part of the Housing Act was your licence issued under?'
    },
    headingLevel: 2
  };
  hintData: HintData = {
    text: 'Select one of the options below.'
  };
  errorMessageData: ErrorMessageData = {};


  itemCheckboxDataCustom: ItemCheckboxData = {
    text: 'opción que puedes modificar',
    labelData: {},
    hintData: {
      text: 'texto hint'
    },
    value: 'custom',
    checked: false,
    name: 'example'
  };
  itemsContact: ItemCheckboxData[] = [
    {
      value: 'phone',
      text: 'Phone',
      conditional: true,
      name: 'how-contacted'
    },
    {
      value: 'email',
      text: 'Email',
      conditional: true,
      name: 'how-contacted'
    },
    {
      value: 'text',
      text: 'Text message',
      conditional: true,
      name: 'how-contacted'
    }
  ];

  itemChecked = false;
  itemIndeterminateChecked = false;

  formGroupClasses: string;
  idPrefix: string;
  name = 'example';
  hasDividers = true;
  classes = 'flex';
  id: string;
  describedBy: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  constructor(private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {

    this.form = new FormGroup({
      valueFormControl: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.email]),
      phone: new FormControl(null, [Validators.minLength(9)]),
      phoneTxt: new FormControl(null, [Validators.minLength(9)]),
    });

    this.form.valueChanges.subscribe(() => {
      if (this.form.invalid) {
        this.formGroupContactClasses = 'c-form-group--error';
      }else {
        this.formGroupContactClasses = '';
      }
    });
  }

  get valueFormControl(): AbstractControl { return this.form.get('valueFormControl'); }
  get email(): AbstractControl { return this.form.get('email'); }
  get phone(): AbstractControl { return this.form.get('phone'); }
  get phoneTxt(): AbstractControl { return this.form.get('phoneTxt'); }

  /**
   * Mejor sincronizar con la variable itemChecked antes que compartir itemRadioDataCustom.checked, ya que este se puede modificar despues
   * de la detección de cambios.
   * @param newValue nuevo valor para checked
   */
  detectChanges(newValue: boolean): void {
    this.itemChecked = newValue;
    this.itemCheckboxDataCustom.checked = newValue;
    this.changeDetectorRef.detectChanges();
  }

  /**
   * Mejor sincronizar con la variable itemChecked antes que compartir itemRadioDataCustom.checked, ya que este se puede modificar despues
   * de la detección de cambios.
   * @param newValue nuevo valor para checked
   */
  detectIndeterminateChanges(newValue: boolean): void {
    this.itemIndeterminateChecked = newValue;
    this.itemCheckboxDataCustom.indeterminateChecked = newValue;
    this.changeDetectorRef.detectChanges();
  }

}
