import * as Code from './code-demo-menu-horizontal';
import { Component } from '@angular/core';
import { MenuHorizontalItemData } from 'desy-angular';

@Component({
  selector: 'app-demo-menu-horizontal',
  templateUrl: './demo-menu-horizontal.component.html'
})
export class DemoMenuHorizontalComponent {

  nameComponent = 'Menu-horizontal';
  mostrarCodigo = Code;

  id: string;
  idPrefix: string;
  classes: string;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  itemCustom: MenuHorizontalItemData = {
    html: 'Ítem configurable',
    href: 'google.es',
  };
  itemsFull: MenuHorizontalItemData[] = [
    {
      html: 'item1',
      href: 'google.es',
    },
    {
      html: 'item2',
      href: 'google.es',
    },
    this.itemCustom
  ];

}
