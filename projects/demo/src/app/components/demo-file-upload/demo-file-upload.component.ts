import * as Code from './code-demo-file-upload';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ErrorMessageData, HintData, LabelData } from 'desy-angular';
import { ErrorMessageModel } from '../../shared/models/error-message-model';
import { HintModel } from '../../shared/models/hint-model';
import { LabelModel } from '../../shared/models/label-model';

@Component({
  selector: 'app-demo-file-upload',
  templateUrl: './demo-file-upload.component.html'
})
export class DemoFileUploadComponent implements OnInit {

  nameComponent = 'file-upload';
  mostrarCodigo = Code;

  labelData: LabelData = new LabelModel();
  hintData: HintData = new HintModel();
  errorMessageData: ErrorMessageData = new ErrorMessageModel();
  name: string;
  describedBy: string;
  formGroupClasses: string;
  id: string;
  classes: string;
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;
  accept: string;

  value: File;
  form: FormGroup;

  ngOnInit(): void {
    this.id = 'file-upload';
    this.name = 'file-upload';
    this.labelData.html = 'Texto en label';
    this.hintData.html = 'Tu foto puede estar en tus Imágenes, Fotos, Descargas o el Escritorio. O en una aplicación como Fotos.    ';
    this.errorMessageData.html = 'Error: esto es un mensaje de error';
    this.form = new FormGroup({
      valueFormControl: new FormControl()
    });
  }

}
