import * as Code from './code-demo-card';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-card',
  templateUrl: './demo-card.component.html'
})
export class DemoCardComponent {

  nameComponent = 'card';
  mostrarCodigo = Code;

  classes: string = "lg:w-2/3";
  containterClasses: string = "p-base bg-neutral-lighter border-t-8 border-neutral-dark";
  text: string;
  html: string = `<h3 class="c-h3"><a href="#" class="c-link">Título card</a></h3>
  <div class="prose max-w-none">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit suscipit mollitia non deleniti illum iure pariatur
      vel eligendi praesentium in. Velit amet distinctio minima libero dolorem tempora non aliquid? Corporis.</p>
  </div>`
  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


  // Card sectors
  activateSub:boolean = false;
  activateSuper:boolean = false;
  activateRight:boolean = false;
  activateLeft:boolean = false;


  backgroundColorSub: string = "transparent"
  backgroundColorSuper: string = "transparent"
  backgroundColorRight: string = "transparent"
  backgroundColorLeft: string = "transparent"

  backgroundImageUrlSub: string = "https://dummyimage.com/320x240/92949b/fff.jpg&text=Imagen"
  backgroundImageUrlSuper: string = "https://dummyimage.com/320x240/92949b/fff.jpg&text=Imagen"
  backgroundImageUrlRight: string = "https://dummyimage.com/320x240/92949b/fff.jpg&text=Imagen"
  backgroundImageUrlLeft: string = "https://dummyimage.com/320x240/92949b/fff.jpg&text=Imagen"

  classesSub: string = "lg:hidden h-56 mb-base bg-cover bg-center bg-no-repeat overflow-hidden"
  classesSuper: string = "lg:hidden h-56 mb-base bg-cover bg-center bg-no-repeat overflow-hidden"
  classesRight: string = "hidden lg:block w-72 mr-lg bg-cover bg-center bg-no-repeat overflow-hidden"
  classesLeft: string = "hidden lg:block w-72 mr-lg bg-cover bg-center bg-no-repeat overflow-hidden"

  textSub: string;
  textSuper: string;
  textRight: string;
  textLeft: string;

  roleSub: string;
  ariaLabelSub: string;
  ariaDescribedBySub: string;
  ariaLabelledBySub: string;
  ariaHiddenSub: string;
  ariaDisabledSub: string;
  ariaControlsSub: string;
  ariaCurrentSub: string;
  ariaLiveSub: string;
  ariaExpandedSub: string;
  ariaErrorMessageSub: string;
  ariaHasPopupSub: string;
  tabindexSub: string;

  roleSuper: string;
  ariaLabelSuper: string;
  ariaDescribedBySuper: string;
  ariaLabelledBySuper: string;
  ariaHiddenSuper: string;
  ariaDisabledSuper: string;
  ariaControlsSuper: string;
  ariaCurrentSuper: string;
  ariaLiveSuper: string;
  ariaExpandedSuper: string;
  ariaErrorMessageSuper: string;
  ariaHasPopupSuper: string;
  tabindexSuper: string;

  roleRight: string;
  ariaLabelRight: string;
  ariaDescribedByRight: string;
  ariaLabelledByRight: string;
  ariaHiddenRight: string;
  ariaDisabledRight: string;
  ariaControlsRight: string;
  ariaCurrentRight: string;
  ariaLiveRight: string;
  ariaExpandedRight: string;
  ariaErrorMessageRight: string;
  ariaHasPopupRight: string;
  tabindexRight: string;

  roleLeft: string;
  ariaLabelLeft: string;
  ariaDescribedByLeft: string;
  ariaLabelledByLeft: string;
  ariaHiddenLeft: string;
  ariaDisabledLeft: string;
  ariaControlsLeft: string;
  ariaCurrentLeft: string;
  ariaLiveLeft: string;
  ariaExpandedLeft: string;
  ariaErrorMessageLeft: string;
  ariaHasPopupLeft: string;
  tabindexLeft: string;


}
