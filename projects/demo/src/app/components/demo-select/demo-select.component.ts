import * as Code from './code-demo-select';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ErrorMessageData, HintData, LabelData, SelectItemData } from 'desy-angular';

@Component({
  selector: 'app-demo-select',
  templateUrl: './demo-select.component.html'
})
export class DemoSelectComponent implements OnInit {

  nameComponent = 'select';
  mostrarCodigo = Code;

  labelData: LabelData = {};
  hintData: HintData = {};
  errorMessageData: ErrorMessageData = {};
  name: string;
  item: SelectItemData = {
    text: 'editable',
    value: 'valor-editable'
  };
  items: SelectItemData[] = [
    {
      text: 'sin value'
    },
    {
      text: 'selected!!', value: 'selected', selected: true,
      role: 'a', ariaLabel: 'a', ariaDescribedBy: 'a', ariaLabelledBy: 'a', ariaHidden: 'a', ariaDisabled: 'a',
      ariaControls: 'a', ariaCurrent: 'a', ariaLive: 'a', ariaExpanded: 'a', ariaErrorMessage: 'a', ariaHasPopup: 'a'
    },
    {
      text: 'disable', value: 'disable', selected: false, disabled: true
    },
    this.item
  ];
  optionGroup: any = {
    label: 'Grupo editable',
    disabled: false,
    role: 'a', ariaLabel: 'a', ariaDescribedBy: 'a', ariaLabelledBy: 'a', ariaHidden: 'a', ariaDisabled: 'a',
    ariaControls: 'a', ariaCurrent: 'a', ariaLive: 'a', ariaExpanded: 'a', ariaErrorMessage: 'a', ariaHasPopup: 'a'
  };

  errorId: string;
  describedBy: string;
  formGroupClasses: string;
  disabled: boolean;
  id: string;
  classes = 'lg:flex-1';
  role: string;
  ariaLabel: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  value = 'selected';
  formContent: FormGroup;

  ngOnInit(): void {
    this.id = 'select';
    this.name = 'select';
    this.labelData.html = 'Texto en label';
    this.hintData.html = 'Texto en hint';
    this.errorMessageData.html = 'Texto en errorMessage';
    this.formContent = new FormGroup({
      valueFormControlContent: new FormControl()
    });
  }

}
