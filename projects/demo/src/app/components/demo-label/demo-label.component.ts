import * as Code from './code-demo-label';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-label',
  templateUrl: './demo-label.component.html'
})
export class DemoLabelComponent {

  nameComponent = 'label';
  mostrarCodigo = Code;

  html = 'Esto es un label';
  for: string;
  isPageHeading = true;
  classes: string = "c-h3";
  id: string;
  headingLevel: number = 3;

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
