import * as Code from './code-demo-tooltip';
import { Component } from '@angular/core';
import { IconData, ItemCheckboxData } from 'desy-angular';

@Component({
  selector: 'app-demo-tooltip',
  templateUrl: './demo-tooltip.component.html'
})
export class DemoTooltipComponent {

  nameComponent = 'tooltip';
  mostrarCodigo = Code;
  id = 'tooltip-editable';
  icon: IconData = {
    html: '',
    type: ''
  };
  complex: boolean;
  classes: string;
  html = 'Tooltip editable';

  contentHtml = 'Contenido <strong>a mostrar</strong> en el tooltip';

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;


  includeIconVisible = [];
  includeIcon: ItemCheckboxData[] = [
    {
      value: 'icon',
      text: 'Incluir icon',
      conditional: true,
      name: 'itemElems'
    }
  ];
}

