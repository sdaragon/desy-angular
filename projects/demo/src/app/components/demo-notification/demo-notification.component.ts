import * as Code from './code-demo-notification';
import { Component } from '@angular/core';
import { ContentData, DescriptionData, IconData, NotificationItemsData, TitleData } from 'desy-angular';

@Component({
  selector: 'app-demo-notification',
  templateUrl: './demo-notification.component.html'
})
export class DemoNotificationComponent {

  nameComponent = 'notification';
  mostrarCodigo = Code;

  allowDesyIcon = true;
  
  id = 'with-custom-icon';
  classes = 'border-warning-base bg-warning-light';
  isOpen = true;

  title: TitleData = {
    html: 'Acaba de publicarse la lista de admitidos y excluidos de la convocatoria publicado en el BOA',
    text: '',
    classes: ''
  };
  description: DescriptionData = {
    text: '',
    html: '<a class=" c-link break-all" href="#">https://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=1119520063030&type=pdf</a>',
    classes: ''
  };
  content: ContentData = {};
  item: NotificationItemsData = {
    html: 'Item editable'
  };
  items: NotificationItemsData[] = [
    { html: 'Item 1' },
    { html: 'Item 2' },
    this.item
  ];

  icon: IconData = {
    html: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140" width="1em" height="1em" class="w-8 h-8" aria-label="Atención" focusable="false"><path d="M138.42 118.29l-55-110a15 15 0 00-26.84 0l-55 110A15 15 0 0015 140h110a15 15 0 0013.42-21.71zM62.5 50a7.5 7.5 0 0115 0v30a7.5 7.5 0 01-15 0zm7.5 70a10 10 0 1110-10 10 10 0 01-10 10z" fill="currentColor"/></svg>',
  };
  type = 'alert';
  isDismissible: boolean;
  headingLevel: number;

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
