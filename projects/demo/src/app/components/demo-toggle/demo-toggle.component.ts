import * as Code from './code-demo-toggle';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo-toggle',
  templateUrl: './demo-toggle.component.html'
})
export class DemoToggleComponent implements OnInit {

  nameComponent = 'toggle';
  mostrarCodigo = Code;

  id:string;
  pressed: boolean = false;
  isExpandible: boolean = false;
  classes: string = "";
  onStateClasses: string = ""; 
  offStateClasses: string = ""; 
  isSwitch: boolean = true;
  
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  constructor() { }

  ngOnInit(): void {
  }

  toggle(event: any) {
  }

}
