/** Autogenerated with npm run code, do not modify here, only in original files. Here the changes will be overridden*/
export const codigoHtml0 = `<desy-error-message [visuallyHiddenText]="visuallyHiddenText" [id]="id" [classes]="classes" [role]="role" [ariaLabel]="ariaLabel" [ariaDescribedBy]="ariaDescribedBy" [ariaLabelledBy]="ariaLabelledBy" [ariaHidden]="ariaHidden" [ariaDisabled]="ariaDisabled" [ariaControls]="ariaControls" [ariaCurrent]="ariaCurrent" [ariaLive]="ariaLive" [ariaExpanded]="ariaExpanded" [ariaErrorMessage]="ariaErrorMessage" [ariaHasPopup]="ariaHasPopup" [tabindex]="tabindex">
  <ng-container *appCustomInnerContent="{ html: html }"></ng-container>
</desy-error-message>`;
export const codigoTs0 = `import * as Code from './code-demo-error-message';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-error-message',
  templateUrl: './demo-error-message.component.html'
})
export class DemoErrorMessageComponent {

  nameComponent = 'error-message';
  mostrarCodigo = Code;

  html = 'Error message about full name goes here';
  visuallyHiddenText: string;
  id: string;
  classes: string;
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
`;
