import * as Code from './code-demo-error-message';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-error-message',
  templateUrl: './demo-error-message.component.html'
})
export class DemoErrorMessageComponent {

  nameComponent = 'error-message';
  mostrarCodigo = Code;

  html = 'Error message about full name goes here';
  visuallyHiddenText: string;
  id: string;
  classes: string;
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
