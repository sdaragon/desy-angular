import * as Code from './code-demo-date-input';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorMessageData, FieldsetData, HintData, ItemDateInputData } from 'desy-angular';

@Component({
  selector: 'app-demo-date-input',
  templateUrl: './demo-date-input.component.html'
})
export class DemoDateInputComponent {

  nameComponent = 'Date-input';
  mostrarCodigo = Code;

  // value = { day: 4, month: 5, year: 6 };
  value;

  form = new FormGroup({
    date: new FormGroup({
      day: new FormControl(1, Validators.required),
      month: new FormControl(1, Validators.required),
      year: new FormControl(2000, Validators.required)
    })
  });

  dateDay: ItemDateInputData = {};

  dateMonth: ItemDateInputData = {};

  dateYear: ItemDateInputData = {
    name: 'year',
    classes: 'w-20',
    maxlength: 4,
    labelData: {}
  };

  dateYearHtml = "año"

  fieldsetData: FieldsetData = {
    legend: {
      html: 'Fecha de nacimiento'
    }
  };

  hintData: HintData = {
    html: 'Por ejemplo, 31 3 1980'
  };

  errorMessageData: ErrorMessageData = {};

  namePrefix = 'prefix';
  classes: string;
  formGroupClasses: string;
  id =  'identificador';

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;
}
