import * as Code from './code-demo-table';
import { Component, OnInit } from '@angular/core';
import { CellData, WrapperData } from 'desy-angular';

@Component({
  selector: 'app-demo-table',
  templateUrl: './demo-table.component.html'
})
export class DemoTableComponent implements OnInit {

  nameComponent = 'table';
  mostrarCodigo = Code;

  itemHead: CellData = {
    html: 'celda header editable',
    id: 'id para editar'
  };
  itemFoot: CellData = {
    html: 'celda footer editable',
    id: 'id para editar'
  };
  itemRow: CellData = {
    html: 'celda row editable',
    id: 'id para editar'
  };
  head: CellData[];
  foot: CellData[];
  rows: Array<CellData[]>;

  caption: string;
  captionClasses: string;
  firstCellIsHeader: boolean;
  classes: string;

  id: string;
  wrapper: WrapperData = {};

  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  hasHeader = true;
  hasFooter = true;
  numColumns = 3;
  numRows = 3;

  ngOnInit(): void {
    this.refreshTable();
  }

  refreshTable(): void {
    if (this.hasHeader) {
      this.head = [];
      for (let i = 0; i < this.numColumns; i++) {
        if (i === this.numColumns - 1) {
          this.head.push(this.itemHead);
        } else {
          this.head.push({
            id: `item-header-${i + 1}`,
            html: `item-header-${i + 1}`
          });
        }
      }
    } else {
      this.head = null;
    }
    
    if (this.hasFooter) {
      this.foot = [];
      for (let i = 0; i < this.numColumns; i++) {
        if (i === this.numColumns - 1) {
          this.foot.push(this.itemFoot);
        } else {
          this.foot.push({
            id: `item-footer-${i + 1}`,
            html: `item-footer-${i + 1}`
          });
        }
      }
    } else {
      this.foot = null;
    }

    this.rows = [];
    for (let i = 0; i < this.numRows; i++) {
      const row = [];
      for (let j = 0; j < this.numColumns; j++) {
        if (i === this.numRows - 1 && j === this.numColumns - 1) {
          row.push(this.itemRow);
        } else if (i === 0 && j === 0 && this.numRows >= 3 && this.numColumns >= 3) {
          row.push({
            id: `item-${i + 1}-${j + 1}`,
            html: `item-${i + 1}-${j + 1}, span: 2x2`,
            rowspan: 2,
            colspan: 2
          });
        } else if (i >= 2 || j >= 2 || this.numRows < 3 || this.numColumns < 3) {
          row.push({
            id: `item-${i + 1}-${j + 1}`,
            html: `item-${i + 1}-${j + 1}`
          });
        }
      }
      this.rows.push(row);
    }
  }

}
