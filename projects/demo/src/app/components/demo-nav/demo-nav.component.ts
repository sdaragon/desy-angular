import * as Code from './code-demo-nav';
import { Component } from '@angular/core';
import { NavItemData } from 'desy-angular';

@Component({
  selector: 'app-demo-nav',
  templateUrl: './demo-nav.component.html'
})
export class DemoNavComponent {

  nameComponent = 'nav';
  mostrarCodigo = Code;

  classes: string;
  hasNav: boolean;
  idPrefix = 'demo-nav';

  selectedItem: NavItemData;

  itemBasic: NavItemData = {};
  itemDisabled: NavItemData = {};
  itemActive: NavItemData = {};
  itemHtml: NavItemData = {};
  itemClasses: NavItemData = {};
  itemCustom: NavItemData = {};

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  constructor() {
    this.itemDisabled.html = 'disabled';
    this.itemDisabled.href = 'www.google.es';
    this.itemDisabled.id = 'item-nav-disabled';
    this.itemDisabled.disabled = true;
    this.itemDisabled.target = '';

    this.itemActive.html = 'active con divider';
    this.itemActive.href = 'www.google.es';
    this.itemActive.id = 'item-nav-active';
    this.itemActive.active = true;
    this.itemActive.target = '';
    this.itemActive.divider = true;

    this.itemHtml.html = '<strong>html</strong>';
    this.itemHtml.routerLink = '/componentes';
    this.itemHtml.id = 'item-nav-html';
    this.itemHtml.target = '';

    this.itemClasses.html = 'bg-warning-base';
    this.itemClasses.classes = 'bg-warning-base';
    this.itemClasses.routerLink = './';
    this.itemClasses.fragment = 'item-nav-warning';
    this.itemClasses.id = 'item-nav-classes';

    this.itemBasic.html = 'Opción básica';
    this.itemBasic.href = 'www.google.es';
    this.itemBasic.target = '_blank';

    this.itemCustom.html = 'opción modificable';
    this.itemCustom.href = 'www.google.es';
    this.itemCustom.target = '_blank';
  }

  getItems(): NavItemData[] {
    return [
      this.itemDisabled,
      this.itemActive,
      this.itemHtml,
      this.itemClasses,
      this.itemBasic,
      this.itemCustom
    ];
  }
}
