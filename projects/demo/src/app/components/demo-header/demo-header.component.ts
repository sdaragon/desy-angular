import * as Code from './code-demo-header';
import { ChangeDetectorRef, Component } from '@angular/core';
import {
  HeaderDropdownData,
  HeaderMobileTextData,
  HeaderNavigationData,
  HeaderNavigationItemData,
  HeaderOffcanvasData,
  HeaderSkipLinkData,
  HeaderSubnavData,
  ItemCheckboxData,
  NavItemData
} from 'desy-angular';

@Component({
  selector: 'app-demo-header',
  templateUrl: './demo-header.component.html',
  styles: [
  ]
})
export class DemoHeaderComponent {

  nameComponent = 'Header';
  mostrarCodigo = Code;

  // Header
  id: string;
  classes: string;
  containerClasses: string;
  homepageUrl: string;
  homepageRouterLink: string;
  homepageFragment: string;
  expandedLogo: boolean;
  noLogo: boolean;
  customLogoHtml: string;
  customNavigationHtml: string;


  mobileTextData: HeaderMobileTextData = {
    html: '<div>soy html de mobileText :d</div>'
  };

  skipLinkData: HeaderSkipLinkData = {
    html: 'Saltar al contenido principal',
    fragment: null,
    classes: null,
    id: null
  };

  subnavItemData: NavItemData = {
    active: true,
    html: 'Item Subnav personalizable',
    href: 'http://google.es',
  };
  subnavData: HeaderSubnavData = {
    html: 'subNav',
    items: [
      {
        html: 'Item Subnav 1',
      },
      this.subnavItemData
    ]
  };
  subnavDataWithoutItems: HeaderSubnavData = {
    text: 'Desy-angular'
  };

  navigationItemData: HeaderNavigationItemData = {
    html: 'Item Nav personalizable'
  };
  navigationData: HeaderNavigationData = {
    items: [
      {
        html: 'Item Nav 1'
      },
      this.navigationItemData
    ]
  };
  dropdownItemData: NavItemData = {
    active: true,
    html: 'Item Dropdown personalizable',
    href: 'http://google.es',
  };
  dropdownData: HeaderDropdownData = {
    html: 'Dropdown',
    classes: 'c-dropdown--header',
    items: [
      {
        html: 'Item Dropdown 1',
        href: 'http://google.es'
      },
      this.dropdownItemData
    ]
  };
  offsetcanvasData: HeaderOffcanvasData = {
    html: 'Offsetcanvas',
    textClose: 'Cerrar',
    contentHtml: null
  };

  // Accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  // Gestión de elementos
  headerElements: ItemCheckboxData[] = [
    {
      value: 'subnav',
      html: 'Subnav',
      conditional: true, 
      name: 'headerElems'
    },
    {
      value: 'navigation',
      html: 'Navigation',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'dropdown',
      html: 'DropDown',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'offcanvas',
      html: 'Offcanvas',
      hintText: 'Sólo es visible en pantallas pequeñas',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'skipLink',
      html: 'SkipLink personalizado',
      hintText: 'Sólo mediante contenido',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'customNavigation',
      html: 'CustomNavigation',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'mobileText',
      html: 'MobileText',
      conditional: true,
      name: 'headerElems'
    },
  ];
  headerVisibleElems = [];
  headerSubnavItems = true;
  headerDropdownCustom = false;
  headerDropdownCustomHtml = '<div class="p-base"><dl><dt class="text-base">Marta Pérez</dt><dd class="text-sm text-neutral-dark">Departamento de Ciencia, Universidad y Sociedad del Conocimiento</dd></dl></div>';
  contentCustomNavigation = 'nav-menu';

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  isHeaderElemVisible(elem: string): boolean {
    return this.headerVisibleElems.findIndex(e => e === elem) >= 0;
  }

  detectChanges(): void {
    // Se realiza para detectar los cambios correctamente cuando se muestra el desy-skip-link.
    this.changeDetector.detectChanges();
  }
}

