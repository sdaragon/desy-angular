/** Autogenerated with npm run code, do not modify here, only in original files. Here the changes will be overridden*/
export const codigoHtml0 = `<desy-header [id]="id" [classes]="classes" [containerClasses]="containerClasses" [homepageUrl]="homepageUrl" [homepageRouterLink]="homepageRouterLink" [homepageFragment]="homepageFragment" [expandedLogo]="expandedLogo" [noLogo]="noLogo" [customLogoHtml]="customLogoHtml" [role]="role" [ariaLabel]="ariaLabel" [ariaDescribedBy]="ariaDescribedBy" [ariaLabelledBy]="ariaLabelledBy" [ariaHidden]="ariaHidden" [ariaDisabled]="ariaDisabled" [ariaControls]="ariaControls" [ariaCurrent]="ariaCurrent" [ariaLive]="ariaLive" [ariaExpanded]="ariaExpanded" [ariaErrorMessage]="ariaErrorMessage" [ariaHasPopup]="ariaHasPopup" [tabindex]="tabindex">
  <desy-header-custom-navigation>
    <ng-container *ngIf="isHeaderElemVisible('customNavigation')">
      <desy-menu-navigation *ngIf="contentCustomNavigation === 'nav-menu'" id="with-all-parent-items-1" idPrefix="parent-example" ariaLabel="Menubar description" classes="bg-neutral-lighter c-menu-navigation--last-right w-full">
        <desy-menu-navigation-item classes="c-menu-navigation__button--header -mr-base" [sub]="{}">
          Menuitem
          <desy-menu-navigation-subitem>Subitem 1</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 2</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 3</desy-menu-navigation-subitem>
        </desy-menu-navigation-item>
        <desy-menu-navigation-item classes="c-menu-navigation__button--header -mr-base" [sub]="{}" [active]="true">
          Item activo 2
          <desy-menu-navigation-subitem>Subitem 1</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 2</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 3</desy-menu-navigation-subitem>
        </desy-menu-navigation-item>
        <desy-menu-navigation-item classes="c-menu-navigation__button--header -mr-base" [sub]="{}" [disabled]="true">
          Item deshabilitado 3
          <desy-menu-navigation-subitem>Subitem 1</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 2</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 3</desy-menu-navigation-subitem>
        </desy-menu-navigation-item>
        <desy-menu-navigation-item classes="c-menu-navigation__button--header -mr-base" [sub]="{}" [disabled]="true">
          Item 4 right
          <desy-menu-navigation-subitem>Subitem 1</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 2</desy-menu-navigation-subitem>
          <desy-menu-navigation-subitem>Subitem 3</desy-menu-navigation-subitem>
        </desy-menu-navigation-item>
      </desy-menu-navigation>
      <ng-container *ngIf="contentCustomNavigation === 'nav-custom'">
        <ng-container *appCustomInnerContent="{ html: customNavigationHtml }"></ng-container>
      </ng-container>
    </ng-container>
  </desy-header-custom-navigation>
  <desy-skip-link *ngIf="isHeaderElemVisible('skipLink')" [fragment]="skipLinkData.fragment" [classes]="skipLinkData.classes" [id]="skipLinkData.id">
    <ng-container *appCustomInnerContent="{ html: skipLinkData.html }"></ng-container>
  </desy-skip-link>
  <desy-header-subnav *ngIf="isHeaderElemVisible('subnav') && headerSubnavItems" [classes]="subnavData.classes" [classesContainer]="subnavData.classesContainer" [hiddenText]="subnavData.hiddenText" [classesTooltip]="subnavData.classesTooltip" [role]="subnavData.role" [ariaLabel]="subnavData.ariaLabel" [ariaDescribedBy]="subnavData.ariaDescribedBy" [ariaLabelledBy]="subnavData.ariaLabelledBy" [ariaHidden]="subnavData.ariaHidden" [ariaDisabled]="subnavData.ariaDisabled" [ariaControls]="subnavData.ariaControls" [ariaCurrent]="subnavData.ariaCurrent" [ariaLive]="subnavData.ariaLive" [ariaExpanded]="subnavData.ariaExpanded" [ariaErrorMessage]="subnavData.ariaErrorMessage" [ariaHasPopup]="subnavData.ariaHasPopup" [tabindex]="subnavData.tabindex">
    <ng-container *appCustomInnerContent="{ html: subnavData.html }"></ng-container>
    <desy-nav>
      <ng-container *ngFor="let item of subnavData.items; index as i">
        <desy-nav-item [id]="item.id ? item.id + '-content' : null" [classes]="item.classes" [title]="item.title" [(active)]="item.active" [href]="item.href" [routerLink]="item.routerLink" [fragment]="item.fragment" [target]="item.target" [disabled]="item.disabled" [divider]="item.divider" [role]="item.role" [ariaLabel]="item.ariaLabel" [ariaDescribedBy]="item.ariaDescribedBy" [ariaLabelledBy]="item.ariaLabelledBy" [ariaHidden]="item.ariaHidden" [ariaDisabled]="item.ariaDisabled" [ariaControls]="item.ariaControls" [ariaCurrent]="item.ariaCurrent" [ariaLive]="item.ariaLive" [ariaErrorMessage]="item.ariaErrorMessage" [tabindex]="item.tabindex">
          <ng-container *appCustomInnerContent="{ html: item.html }"></ng-container>
        </desy-nav-item>
      </ng-container>
    </desy-nav>
  </desy-header-subnav>

  <desy-header-subnav *ngIf="isHeaderElemVisible('subnav') && !headerSubnavItems" [classes]="subnavDataWithoutItems.classes" [classesContainer]="subnavDataWithoutItems.classesContainer" [hiddenText]="subnavDataWithoutItems.hiddenText" [classesTooltip]="subnavDataWithoutItems.classesTooltip" [role]="subnavDataWithoutItems.role" [ariaLabel]="subnavDataWithoutItems.ariaLabel" [ariaDescribedBy]="subnavDataWithoutItems.ariaDescribedBy" [ariaLabelledBy]="subnavDataWithoutItems.ariaLabelledBy" [ariaHidden]="subnavDataWithoutItems.ariaHidden" [ariaDisabled]="subnavDataWithoutItems.ariaDisabled" [ariaControls]="subnavDataWithoutItems.ariaControls" [ariaCurrent]="subnavDataWithoutItems.ariaCurrent" [ariaLive]="subnavDataWithoutItems.ariaLive" [ariaExpanded]="subnavDataWithoutItems.ariaExpanded" [ariaErrorMessage]="subnavDataWithoutItems.ariaErrorMessage" [ariaHasPopup]="subnavDataWithoutItems.ariaHasPopup" [tabindex]="subnavDataWithoutItems.tabindex">
    <ng-container *appCustomInnerContent="{ text: subnavDataWithoutItems.text }"></ng-container>
  </desy-header-subnav>

  <desy-header-dropdown *ngIf="isHeaderElemVisible('dropdown')" [classes]="dropdownData.classes" [classesContainer]="dropdownData.classesContainer" [hiddenText]="dropdownData.hiddenText" [classesTooltip]="dropdownData.classesTooltip" [role]="dropdownData.role" [ariaLabel]="dropdownData.ariaLabel" [ariaDescribedBy]="dropdownData.ariaDescribedBy" [ariaLabelledBy]="dropdownData.ariaLabelledBy" [ariaHidden]="dropdownData.ariaHidden" [ariaDisabled]="dropdownData.ariaDisabled" [ariaControls]="dropdownData.ariaControls" [ariaCurrent]="dropdownData.ariaCurrent" [ariaLive]="dropdownData.ariaLive" [ariaExpanded]="dropdownData.ariaExpanded" [ariaErrorMessage]="dropdownData.ariaErrorMessage" [ariaHasPopup]="dropdownData.ariaHasPopup" [tabindex]="dropdownData.tabindex">
    <ng-container *appCustomInnerContent="{ html: dropdownData.html }"></ng-container>
    <desy-content *ngIf="headerDropdownCustom">
      <ng-container *appCustomInnerContent="{ html: headerDropdownCustomHtml }"></ng-container>
    </desy-content>
    <desy-nav>
      <ng-container *ngFor="let item of dropdownData.items; index as i">
        <desy-nav-item [id]="item.id ? item.id + '-content-dropdown' : null" [classes]="item.classes" [title]="item.title" [(active)]="item.active" [href]="item.href" [routerLink]="item.routerLink" [fragment]="item.fragment" [target]="item.target" [disabled]="item.disabled" [divider]="item.divider" [role]="item.role" [ariaLabel]="item.ariaLabel" [ariaDescribedBy]="item.ariaDescribedBy" [ariaLabelledBy]="item.ariaLabelledBy" [ariaHidden]="item.ariaHidden" [ariaDisabled]="item.ariaDisabled" [ariaControls]="item.ariaControls" [ariaCurrent]="item.ariaCurrent" [ariaLive]="item.ariaLive" [ariaErrorMessage]="item.ariaErrorMessage" [tabindex]="item.tabindex">
          <ng-container *appCustomInnerContent="{ html: item.html }"></ng-container>
        </desy-nav-item>
      </ng-container>
    </desy-nav>
  </desy-header-dropdown>

  <desy-mobile-text *ngIf="isHeaderElemVisible('mobileText')" [classes]="mobileTextData.classes" [role]="mobileTextData.role" [ariaLabel]="mobileTextData.ariaLabel" [ariaDescribedBy]="mobileTextData.ariaDescribedBy" [ariaLabelledBy]="mobileTextData.ariaLabelledBy" [ariaHidden]="mobileTextData.ariaHidden" [ariaDisabled]="mobileTextData.ariaDisabled" [ariaControls]="mobileTextData.ariaControls" [ariaCurrent]="mobileTextData.ariaCurrent" [ariaLive]="mobileTextData.ariaLive" [ariaExpanded]="mobileTextData.ariaExpanded" [ariaErrorMessage]="mobileTextData.ariaErrorMessage" [ariaHasPopup]="mobileTextData.ariaHasPopup" [tabindex]="mobileTextData.tabindex">
    <ng-container *appCustomInnerContent="{ html: mobileTextData.html }"></ng-container>
  </desy-mobile-text>

  <desy-header-navigation *ngIf="isHeaderElemVisible('navigation')" [classes]="navigationData.classes">
    <ng-container *ngFor="let item of navigationData.items">
      <desy-header-navigation-item [href]="item.href" [routerLink]="item.routerLink" [fragment]="item.fragment" [id]="item.id" [active]="item.active" [disabled]="item.disabled">
        <ng-container *appCustomInnerContent="{ html: item.html }"></ng-container>
      </desy-header-navigation-item>
    </ng-container>
  </desy-header-navigation>

  <desy-header-offcanvas *ngIf="isHeaderElemVisible('offcanvas')" [classes]="offsetcanvasData.classes">
    <desy-header-offcanvas-button>
      <ng-container *appCustomInnerContent="{ html: offsetcanvasData.html }"></ng-container>
    </desy-header-offcanvas-button>
    <desy-header-offcanvas-close-button>
      <ng-container *appCustomInnerContent="{ html: offsetcanvasData.textClose }"></ng-container>
    </desy-header-offcanvas-close-button>
    <desy-content>
      <ul>
        <li>
          <h3 class="p-base text-base font-bold" id="caller-id">Páginas</h3>
          <desy-nav idPrefix="pages">
            <desy-nav-item [routerLink]="[]">
              Opción 1 (sin url)
            </desy-nav-item>
            <desy-nav-item routerLink="/">
              Opción 2 (componentes)
            </desy-nav-item>
            <desy-nav-item href="http://google.es">
              Opción 3 (externa)
            </desy-nav-item>
          </desy-nav>
        </li>
        <li>
          <h3 class="p-base pt-xl text-base font-bold">Información adicional</h3>
          <desy-nav classes="bg-warning-light" idPrefix="aditionalinfo">
            <desy-nav-item routerLink="/">
              Opción 1
            </desy-nav-item>
            <desy-nav-item routerLink="/">
              Opción 2
            </desy-nav-item>
            <desy-nav-item routerLink="/">
              Opción 3
            </desy-nav-item>
          </desy-nav>
        </li>
      </ul>
    </desy-content>
  </desy-header-offcanvas>
</desy-header>`;
export const codigoTs0 = `import * as Code from './code-demo-header';
import { ChangeDetectorRef, Component } from '@angular/core';
import {
  HeaderDropdownData,
  HeaderMobileTextData,
  HeaderNavigationData,
  HeaderNavigationItemData,
  HeaderOffcanvasData,
  HeaderSkipLinkData,
  HeaderSubnavData,
  ItemCheckboxData,
  NavItemData
} from 'desy-angular';

@Component({
  selector: 'app-demo-header',
  templateUrl: './demo-header.component.html',
  styles: [
  ]
})
export class DemoHeaderComponent {

  nameComponent = 'Header';
  mostrarCodigo = Code;

  // Header
  id: string;
  classes: string;
  containerClasses: string;
  homepageUrl: string;
  homepageRouterLink: string;
  homepageFragment: string;
  expandedLogo: boolean;
  noLogo: boolean;
  customLogoHtml: string;
  customNavigationHtml: string;


  mobileTextData: HeaderMobileTextData = {
    html: '<div>soy html de mobileText :d</div>'
  };

  skipLinkData: HeaderSkipLinkData = {
    html: 'Saltar al contenido principal',
    fragment: null,
    classes: null,
    id: null
  };

  subnavItemData: NavItemData = {
    active: true,
    html: 'Item Subnav personalizable',
    href: 'http://google.es',
  };
  subnavData: HeaderSubnavData = {
    html: 'subNav',
    items: [
      {
        html: 'Item Subnav 1',
      },
      this.subnavItemData
    ]
  };
  subnavDataWithoutItems: HeaderSubnavData = {
    text: 'Desy-angular'
  };

  navigationItemData: HeaderNavigationItemData = {
    html: 'Item Nav personalizable'
  };
  navigationData: HeaderNavigationData = {
    items: [
      {
        html: 'Item Nav 1'
      },
      this.navigationItemData
    ]
  };
  dropdownItemData: NavItemData = {
    active: true,
    html: 'Item Dropdown personalizable',
    href: 'http://google.es',
  };
  dropdownData: HeaderDropdownData = {
    html: 'Dropdown',
    classes: 'c-dropdown--header',
    items: [
      {
        html: 'Item Dropdown 1',
        href: 'http://google.es'
      },
      this.dropdownItemData
    ]
  };
  offsetcanvasData: HeaderOffcanvasData = {
    html: 'Offsetcanvas',
    textClose: 'Cerrar',
    contentHtml: null
  };

  // Accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  // Gestión de elementos
  headerElements: ItemCheckboxData[] = [
    {
      value: 'subnav',
      html: 'Subnav',
      conditional: true, 
      name: 'headerElems'
    },
    {
      value: 'navigation',
      html: 'Navigation',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'dropdown',
      html: 'DropDown',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'offcanvas',
      html: 'Offcanvas',
      hintText: 'Sólo es visible en pantallas pequeñas',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'skipLink',
      html: 'SkipLink personalizado',
      hintText: 'Sólo mediante contenido',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'customNavigation',
      html: 'CustomNavigation',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'mobileText',
      html: 'MobileText',
      conditional: true,
      name: 'headerElems'
    },
  ];
  headerVisibleElems = [];
  headerSubnavItems = true;
  headerDropdownCustom = false;
  headerDropdownCustomHtml = '<div class="p-base"><dl><dt class="text-base">Marta Pérez</dt><dd class="text-sm text-neutral-dark">Departamento de Ciencia, Universidad y Sociedad del Conocimiento</dd></dl></div>';
  contentCustomNavigation = 'nav-menu';

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  isHeaderElemVisible(elem: string): boolean {
    return this.headerVisibleElems.findIndex(e => e === elem) >= 0;
  }

  detectChanges(): void {
    // Se realiza para detectar los cambios correctamente cuando se muestra el desy-skip-link.
    this.changeDetector.detectChanges();
  }
}

`;
export const codigoHtml1 = `<desy-header homepageUrl="/">
  <desy-header-subnav>
    Gestor de expedientes
  </desy-header-subnav>
  <desy-mobile-text>
    Gestor de expedientes
  </desy-mobile-text>
  <desy-header-navigation>
    <desy-header-navigation-item href="#1">
      Navigation item 1
    </desy-header-navigation-item>
    <desy-header-navigation-item href="#2" [active]="true">
      Navigation item 2
    </desy-header-navigation-item>
    <desy-header-navigation-item href="#3">
      Navigation item 3
    </desy-header-navigation-item>
    <desy-header-navigation-item href="#4">
      Navigation item 4
    </desy-header-navigation-item>
  </desy-header-navigation>
</desy-header>`;
export const codigoTs1 = `import * as Code from './code-demo-header';
import { ChangeDetectorRef, Component } from '@angular/core';
import {
  HeaderDropdownData,
  HeaderMobileTextData,
  HeaderNavigationData,
  HeaderNavigationItemData,
  HeaderOffcanvasData,
  HeaderSkipLinkData,
  HeaderSubnavData,
  ItemCheckboxData,
  NavItemData
} from 'desy-angular';

@Component({
  selector: 'app-demo-header',
  templateUrl: './demo-header.component.html',
  styles: [
  ]
})
export class DemoHeaderComponent {

  nameComponent = 'Header';
  mostrarCodigo = Code;

  // Header
  id: string;
  classes: string;
  containerClasses: string;
  homepageUrl: string;
  homepageRouterLink: string;
  homepageFragment: string;
  expandedLogo: boolean;
  noLogo: boolean;
  customLogoHtml: string;
  customNavigationHtml: string;


  mobileTextData: HeaderMobileTextData = {
    html: '<div>soy html de mobileText :d</div>'
  };

  skipLinkData: HeaderSkipLinkData = {
    html: 'Saltar al contenido principal',
    fragment: null,
    classes: null,
    id: null
  };

  subnavItemData: NavItemData = {
    active: true,
    html: 'Item Subnav personalizable',
    href: 'http://google.es',
  };
  subnavData: HeaderSubnavData = {
    html: 'subNav',
    items: [
      {
        html: 'Item Subnav 1',
      },
      this.subnavItemData
    ]
  };
  subnavDataWithoutItems: HeaderSubnavData = {
    text: 'Desy-angular'
  };

  navigationItemData: HeaderNavigationItemData = {
    html: 'Item Nav personalizable'
  };
  navigationData: HeaderNavigationData = {
    items: [
      {
        html: 'Item Nav 1'
      },
      this.navigationItemData
    ]
  };
  dropdownItemData: NavItemData = {
    active: true,
    html: 'Item Dropdown personalizable',
    href: 'http://google.es',
  };
  dropdownData: HeaderDropdownData = {
    html: 'Dropdown',
    classes: 'c-dropdown--header',
    items: [
      {
        html: 'Item Dropdown 1',
        href: 'http://google.es'
      },
      this.dropdownItemData
    ]
  };
  offsetcanvasData: HeaderOffcanvasData = {
    html: 'Offsetcanvas',
    textClose: 'Cerrar',
    contentHtml: null
  };

  // Accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  // Gestión de elementos
  headerElements: ItemCheckboxData[] = [
    {
      value: 'subnav',
      html: 'Subnav',
      conditional: true, 
      name: 'headerElems'
    },
    {
      value: 'navigation',
      html: 'Navigation',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'dropdown',
      html: 'DropDown',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'offcanvas',
      html: 'Offcanvas',
      hintText: 'Sólo es visible en pantallas pequeñas',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'skipLink',
      html: 'SkipLink personalizado',
      hintText: 'Sólo mediante contenido',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'customNavigation',
      html: 'CustomNavigation',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'mobileText',
      html: 'MobileText',
      conditional: true,
      name: 'headerElems'
    },
  ];
  headerVisibleElems = [];
  headerSubnavItems = true;
  headerDropdownCustom = false;
  headerDropdownCustomHtml = '<div class="p-base"><dl><dt class="text-base">Marta Pérez</dt><dd class="text-sm text-neutral-dark">Departamento de Ciencia, Universidad y Sociedad del Conocimiento</dd></dl></div>';
  contentCustomNavigation = 'nav-menu';

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  isHeaderElemVisible(elem: string): boolean {
    return this.headerVisibleElems.findIndex(e => e === elem) >= 0;
  }

  detectChanges(): void {
    // Se realiza para detectar los cambios correctamente cuando se muestra el desy-skip-link.
    this.changeDetector.detectChanges();
  }
}

`;
export const codigoHtml2 = `<desy-header homepageUrl="/" [expandedLogo]="true">
</desy-header>`;
export const codigoTs2 = `import * as Code from './code-demo-header';
import { ChangeDetectorRef, Component } from '@angular/core';
import {
  HeaderDropdownData,
  HeaderMobileTextData,
  HeaderNavigationData,
  HeaderNavigationItemData,
  HeaderOffcanvasData,
  HeaderSkipLinkData,
  HeaderSubnavData,
  ItemCheckboxData,
  NavItemData
} from 'desy-angular';

@Component({
  selector: 'app-demo-header',
  templateUrl: './demo-header.component.html',
  styles: [
  ]
})
export class DemoHeaderComponent {

  nameComponent = 'Header';
  mostrarCodigo = Code;

  // Header
  id: string;
  classes: string;
  containerClasses: string;
  homepageUrl: string;
  homepageRouterLink: string;
  homepageFragment: string;
  expandedLogo: boolean;
  noLogo: boolean;
  customLogoHtml: string;
  customNavigationHtml: string;


  mobileTextData: HeaderMobileTextData = {
    html: '<div>soy html de mobileText :d</div>'
  };

  skipLinkData: HeaderSkipLinkData = {
    html: 'Saltar al contenido principal',
    fragment: null,
    classes: null,
    id: null
  };

  subnavItemData: NavItemData = {
    active: true,
    html: 'Item Subnav personalizable',
    href: 'http://google.es',
  };
  subnavData: HeaderSubnavData = {
    html: 'subNav',
    items: [
      {
        html: 'Item Subnav 1',
      },
      this.subnavItemData
    ]
  };
  subnavDataWithoutItems: HeaderSubnavData = {
    text: 'Desy-angular'
  };

  navigationItemData: HeaderNavigationItemData = {
    html: 'Item Nav personalizable'
  };
  navigationData: HeaderNavigationData = {
    items: [
      {
        html: 'Item Nav 1'
      },
      this.navigationItemData
    ]
  };
  dropdownItemData: NavItemData = {
    active: true,
    html: 'Item Dropdown personalizable',
    href: 'http://google.es',
  };
  dropdownData: HeaderDropdownData = {
    html: 'Dropdown',
    classes: 'c-dropdown--header',
    items: [
      {
        html: 'Item Dropdown 1',
        href: 'http://google.es'
      },
      this.dropdownItemData
    ]
  };
  offsetcanvasData: HeaderOffcanvasData = {
    html: 'Offsetcanvas',
    textClose: 'Cerrar',
    contentHtml: null
  };

  // Accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  // Gestión de elementos
  headerElements: ItemCheckboxData[] = [
    {
      value: 'subnav',
      html: 'Subnav',
      conditional: true, 
      name: 'headerElems'
    },
    {
      value: 'navigation',
      html: 'Navigation',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'dropdown',
      html: 'DropDown',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'offcanvas',
      html: 'Offcanvas',
      hintText: 'Sólo es visible en pantallas pequeñas',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'skipLink',
      html: 'SkipLink personalizado',
      hintText: 'Sólo mediante contenido',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'customNavigation',
      html: 'CustomNavigation',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'mobileText',
      html: 'MobileText',
      conditional: true,
      name: 'headerElems'
    },
  ];
  headerVisibleElems = [];
  headerSubnavItems = true;
  headerDropdownCustom = false;
  headerDropdownCustomHtml = '<div class="p-base"><dl><dt class="text-base">Marta Pérez</dt><dd class="text-sm text-neutral-dark">Departamento de Ciencia, Universidad y Sociedad del Conocimiento</dd></dl></div>';
  contentCustomNavigation = 'nav-menu';

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  isHeaderElemVisible(elem: string): boolean {
    return this.headerVisibleElems.findIndex(e => e === elem) >= 0;
  }

  detectChanges(): void {
    // Se realiza para detectar los cambios correctamente cuando se muestra el desy-skip-link.
    this.changeDetector.detectChanges();
  }
}

`;
export const codigoHtml3 = `<desy-header homepageUrl="/">
  <desy-mobile-text>
    Gestor de expedientes
  </desy-mobile-text>
  <desy-header-dropdown classesTooltip="w-64 max-h-48 overflow-y-auto">
   <span class="block text-right">Marta Pérez <br>(Administración)</span>
    <desy-content>
      <div class="p-base">
        <dl>
          <dt class="text-base">Marta Pérez</dt>
          <dd class="text-sm text-neutral-dark">Departamento de Ciencia, Universidad y Sociedad del Conocimiento</dd>
        </dl>
      </div>
    </desy-content>
  </desy-header-dropdown>
</desy-header>`;
export const codigoTs3 = `import * as Code from './code-demo-header';
import { ChangeDetectorRef, Component } from '@angular/core';
import {
  HeaderDropdownData,
  HeaderMobileTextData,
  HeaderNavigationData,
  HeaderNavigationItemData,
  HeaderOffcanvasData,
  HeaderSkipLinkData,
  HeaderSubnavData,
  ItemCheckboxData,
  NavItemData
} from 'desy-angular';

@Component({
  selector: 'app-demo-header',
  templateUrl: './demo-header.component.html',
  styles: [
  ]
})
export class DemoHeaderComponent {

  nameComponent = 'Header';
  mostrarCodigo = Code;

  // Header
  id: string;
  classes: string;
  containerClasses: string;
  homepageUrl: string;
  homepageRouterLink: string;
  homepageFragment: string;
  expandedLogo: boolean;
  noLogo: boolean;
  customLogoHtml: string;
  customNavigationHtml: string;


  mobileTextData: HeaderMobileTextData = {
    html: '<div>soy html de mobileText :d</div>'
  };

  skipLinkData: HeaderSkipLinkData = {
    html: 'Saltar al contenido principal',
    fragment: null,
    classes: null,
    id: null
  };

  subnavItemData: NavItemData = {
    active: true,
    html: 'Item Subnav personalizable',
    href: 'http://google.es',
  };
  subnavData: HeaderSubnavData = {
    html: 'subNav',
    items: [
      {
        html: 'Item Subnav 1',
      },
      this.subnavItemData
    ]
  };
  subnavDataWithoutItems: HeaderSubnavData = {
    text: 'Desy-angular'
  };

  navigationItemData: HeaderNavigationItemData = {
    html: 'Item Nav personalizable'
  };
  navigationData: HeaderNavigationData = {
    items: [
      {
        html: 'Item Nav 1'
      },
      this.navigationItemData
    ]
  };
  dropdownItemData: NavItemData = {
    active: true,
    html: 'Item Dropdown personalizable',
    href: 'http://google.es',
  };
  dropdownData: HeaderDropdownData = {
    html: 'Dropdown',
    classes: 'c-dropdown--header',
    items: [
      {
        html: 'Item Dropdown 1',
        href: 'http://google.es'
      },
      this.dropdownItemData
    ]
  };
  offsetcanvasData: HeaderOffcanvasData = {
    html: 'Offsetcanvas',
    textClose: 'Cerrar',
    contentHtml: null
  };

  // Accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

  // Gestión de elementos
  headerElements: ItemCheckboxData[] = [
    {
      value: 'subnav',
      html: 'Subnav',
      conditional: true, 
      name: 'headerElems'
    },
    {
      value: 'navigation',
      html: 'Navigation',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'dropdown',
      html: 'DropDown',
      hintText: 'Sólo es visible en pantallas grandes',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'offcanvas',
      html: 'Offcanvas',
      hintText: 'Sólo es visible en pantallas pequeñas',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'skipLink',
      html: 'SkipLink personalizado',
      hintText: 'Sólo mediante contenido',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'customNavigation',
      html: 'CustomNavigation',
      conditional: true,
      name: 'headerElems'
    },
    {
      value: 'mobileText',
      html: 'MobileText',
      conditional: true,
      name: 'headerElems'
    },
  ];
  headerVisibleElems = [];
  headerSubnavItems = true;
  headerDropdownCustom = false;
  headerDropdownCustomHtml = '<div class="p-base"><dl><dt class="text-base">Marta Pérez</dt><dd class="text-sm text-neutral-dark">Departamento de Ciencia, Universidad y Sociedad del Conocimiento</dd></dl></div>';
  contentCustomNavigation = 'nav-menu';

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  isHeaderElemVisible(elem: string): boolean {
    return this.headerVisibleElems.findIndex(e => e === elem) >= 0;
  }

  detectChanges(): void {
    // Se realiza para detectar los cambios correctamente cuando se muestra el desy-skip-link.
    this.changeDetector.detectChanges();
  }
}

`;
