import * as Code from './code-demo-datepicker';
import { Component } from '@angular/core';
import { ErrorMessageData, HintData, LabelData } from 'desy-angular';

@Component({
  selector: 'app-demo-datepicker',
  templateUrl: './demo-datepicker.component.html'
})
export class DemoDatepickerComponent {

  nameComponent = 'Datepicker';
  mostrarCodigo = Code;

  value;
  valueMultiple = "2024-01-10 2024-01-20";
  valueRange = "2024-01-10/2024-01-20";
  valueRangeSelector = "2024-01-10/2024-01-20";

  calendar = `<calendar-date min="2024-01-01"
                 max="2024-12-31"
                 locale="es-ES">
    <div slot="previous">
      <svg xmlns="http://www.w3.org/2000/svg"
           viewBox="0 0 16 16"
           height="1em"
           width="1em"
           class="self-center my-1"
           focusable="false"
           role="img"
           aria-label="Mes anterior">
        <path d="M4.00001 7.97945C4.00001 7.63023 4.14967 7.3309 4.39911 7.13135L9.93669 2.19243C10.3358 1.8931 10.9345 1.94298 11.2338 2.34209C11.5331 2.74119 11.5331 3.28996 11.134 3.58929L6.24497 7.87967C6.19508 7.92956 6.19508 7.97945 6.24497 8.07922L11.134 12.3696C11.5331 12.7188 11.583 13.2676 11.2338 13.6667C10.8846 14.0658 10.3358 14.1157 9.93669 13.7665C9.93669 13.7665 9.93669 13.7665 9.8868 13.7166L4.34922 8.82754C4.14967 8.62799 4.00001 8.27877 4.00001 7.97945Z"
              fill="currentColor"></path>
      </svg>
    </div>

    <div slot="next">
      <svg xmlns="http://www.w3.org/2000/svg"
           viewBox="0 0 16 16"
           height="1em"
           width="1em"
           class="self-center my-1"
           focusable="false"
           role="img"
           aria-label="Mes siguiente">
        <path d="M11.4673 8.02055C11.4673 8.36977 11.3176 8.6691 11.0682 8.86865L5.53062 13.8076C5.13152 14.1069 4.53286 14.057 4.23353 13.6579C3.9342 13.2588 3.9342 12.71 4.33331 12.4107L9.22234 8.12033C9.27223 8.07044 9.27223 8.02055 9.22234 7.92078L4.33331 3.6304C3.9342 3.28118 3.88432 2.73241 4.23353 2.33331C4.58275 1.9342 5.13152 1.88432 5.53062 2.23353C5.53062 2.23353 5.53062 2.23353 5.58051 2.28342L11.1181 7.17246C11.3176 7.37201 11.4673 7.72123 11.4673 8.02055Z"
              fill="currentColor"></path>
      </svg>
    </div>

    <div class="flex flex-wrap justify-center gap-lg">
      <calendar-month></calendar-month>
    </div>
  </calendar-date>`

  hintData: HintData = {
    html: 'Usa el formato: AAAA-MM-DD (año-mes-día)'
  };

  labelData: LabelData = {
    html: 'Elige fecha de inicio'
  };

  errorMessageData: ErrorMessageData = {
    html: 'Error: Esto es un mensaje de error'
  };

  standalone: boolean = false;
  classes: string;
  containerClasses: string;
  formGroupClasses: string;
  id = 'identificador';
  name: string;
  type: string;
  inputmode: string;
  describedBy: string;
  errorId: string;
  autocomplete: string;
  pattern: string;
  placeholder: string = "12/12/2022";
  attributes: string;
  dropdownClasses: string;


  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;
}
