import { Component, ComponentRef, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { AlertComponent, AlertOptions, AlertService, NotificationOptions } from 'desy-angular';
import * as Code from './code-demo-alert';

@Component({
  selector: 'app-demo-alert',
  templateUrl: './demo-alert.component.html'
})
export class DemoAlertComponent {

  @ViewChild('alertPlaceTemplate', {read: ViewContainerRef}) alertPlaceTemplate: ViewContainerRef;
  @ViewChild('alertPlace', {read: ViewContainerRef}) alertPlace: ViewContainerRef;

  nameComponent = 'alert';
  mostrarCodigo = Code;
  alertConfigurableActive = false;
  alertDangerActive = false;
  dynamicAlertCount = 0;

  alertOptions: AlertOptions = {
    id: 'alert',
    place: undefined,
    role: 'alert',
    ariaLive: 'assertive'
  };

  constructor(private alertService: AlertService) { }

  openServiceAlertWithTemplate(caller: TemplateRef<any>): void {

    this.dynamicAlertCount++;
    this.alertOptions.place = this.alertPlaceTemplate;

    this.alertService.openAlert(caller, this.alertOptions).then(result => {
      result.alert.instance.callerContext = {
        alertRef: result.alert,
        alertCount: this.dynamicAlertCount
      };
    });
  }

  openServiceAlert(): void {

    this.dynamicAlertCount++;
    this.alertOptions.place = this.alertPlace;

    const notificationOptions: NotificationOptions = {
      id: 'alert-service',
      titleNotification: {
        text: 'El documento ' + this.dynamicAlertCount + ' se ha cargado correctamente'
      },
      type: 'success',
      isDismissible: true
    };

    this.alertService.openAlert(notificationOptions, this.alertOptions).then();
  }

  closeServiceAlert(alertRef: ComponentRef<AlertComponent>): void {
    this.alertService.closeAlert(alertRef);
  }

}
