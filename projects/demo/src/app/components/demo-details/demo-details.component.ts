import * as Code from './code-demo-details';
import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-details',
  templateUrl: './demo-details.component.html'
})
export class DemoDetailsComponent {

  nameComponent = 'details';
  mostrarCodigo = Code;

  summaryHtml: string = "Más información <em>actualizada</em>";
  summaryClasses: string = "hover:underline";
  containerClasses: string = "p-base";
  content = "<p>Lorem ipsum dolor, sit amet <strong>consectetur</strong>, adipisicing elit. Quae omnis ipsa eius dolorum, maiores! Labore quaerat nam pariatur minima consectetur, tempora magnam. At sequi quidem exercitationem velit id, pariatur, animi.</p>";
  id: string;
  open: boolean;
  classes: string = "p-base bg-primary-light text-primary-base";

  // Atributos de accesibilidad
  role: string;
  ariaLabel: string;
  ariaDescribedBy: string;
  ariaLabelledBy: string;
  ariaHidden: string;
  ariaDisabled: string;
  ariaControls: string;
  ariaCurrent: string;
  ariaLive: string;
  ariaExpanded: string;
  ariaErrorMessage: string;
  ariaHasPopup: string;
  tabindex: string;

}
