import * as Code from './code-demo-search-bar';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorMessageData, LabelData } from 'desy-angular';
import { ErrorMessageModel } from '../../shared/models/error-message-model';
import { LabelModel } from '../../shared/models/label-model';

@Component({
  selector: 'app-demo-search-bar',
  templateUrl: './demo-search-bar.component.html'
})
export class DemoSearchBarComponent implements OnInit {

  nameComponent = 'SearchBar';
  mostrarCodigo = Code;

  value: string;
  valueError:string;
  form: FormGroup;

  id: string;
  describedBy: string;
  labelData: LabelData = new LabelModel();
  errorMessageData: ErrorMessageData = new ErrorMessageModel();
  buttonClasses: string;
  classes: string;
  placeholder: string;
  disabled: boolean;

  ariaControls: string;
  ariaCurrent: string;
  ariaDescribedBy: string;
  ariaDisabled: string;
  ariaErrorMessage: string;
  ariaExpanded: string;
  ariaHasPopup: string;
  ariaHidden: string;
  ariaLabel: string;
  ariaLabelledBy: string;
  ariaLive: string;
  role: string;
  tabindex: string;

  clickCount = 0;
  clickText: string;
  clickCountForm = 0;
  clickTextForm: string;

  showExternalButton = false;

  ngOnInit(): void {
    this.value = 'Valor por defecto en ngModel';
    this.id = 'search-bar-id';
    this.labelData.html = 'Texto en label';
    this.labelData.classes = 'sr-only';
    this.labelData.for = 'searchbar';
    this.errorMessageData.html = 'Texto en errorMessage';
    this.placeholder = 'Texto en placeholder';
    this.classes = 'flex-1';
    this.form = new FormGroup({
      valueFormControl: new FormControl('Valor por defecto en form', [Validators.required])
    });
  }

  updateClickCountText(): void {
    this.clickCount++;
    this.clickText = 'Detectado evento click emitido nº ' + this.clickCount + (this.showExternalButton ? ' (externo)' : ' (interno)');
  }

  updateClickCountTextForm(): void {
    this.clickCountForm++;
    this.clickTextForm = 'Detectado evento click emitido nº ' + this.clickCountForm
      + (this.showExternalButton ? ' (externo)' : ' (interno)');
  }

}
