import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { MenuComponentsComponent } from './components/menu-components/menu-components.component';

// desy-buttons
import { DemoButtonLoaderComponent } from './components/demo-button-loader/demo-button-loader.component';
import { DemoButtonComponent } from './components/demo-button/demo-button.component';
import { DemoDropdownComponent } from './components/demo-dropdown/demo-dropdown.component';
import { DemoListboxComponent } from './components/demo-listbox/demo-listbox.component';
import { DemoPillComponent } from './components/demo-pill/demo-pill.component';
import { DemoToggleComponent } from './components/demo-toggle/demo-toggle.component';
//  desy-forms
import { DemoCharacterCountComponent } from './components/demo-character-count/demo-character-count.component';
import { DemoCheckboxesComponent } from './components/demo-checkboxes/demo-checkboxes.component';
import { DemoDateInputComponent } from './components/demo-date-input/demo-date-input.component';
import { DemoErrorMessageComponent } from './components/demo-error-message/demo-error-message.component';
import { DemoFieldsetComponent } from './components/demo-fieldset/demo-fieldset.component';
import { DemoFileUploadComponent } from './components/demo-file-upload/demo-file-upload.component';
import { DemoHintComponent } from './components/demo-hint/demo-hint.component';
import { DemoInputGroupComponent } from './components/demo-input-group/demo-input-group.component';
import { DemoInputComponent } from './components/demo-input/demo-input.component';
import { DemoLabelComponent } from './components/demo-label/demo-label.component';
import { DemoRadiosComponent } from './components/demo-radios/demo-radios.component';
import { DemoSearchBarComponent } from './components/demo-search-bar/demo-search-bar.component';
import { DemoSelectComponent } from './components/demo-select/demo-select.component';
import { DemoTextareaComponent } from './components/demo-textarea/demo-textarea.component';
import { DemoTreeComponent } from './components/demo-tree/demo-tree.component';
// desy-modals
import { DemoDialogComponent } from './components/demo-dialog/demo-dialog.component';
import { DemoModalComponent } from './components/demo-modal/demo-modal.component';
// desy-views
import { DemoAccordionHistoryComponent } from './components/demo-accordion-history/demo-accordion-history.component';
import { DemoAccordionComponent } from './components/demo-accordion/demo-accordion.component';
import { DemoAlertComponent } from './components/demo-alert/demo-alert.component';
import { DemoCollapsibleComponent } from './components/demo-collapsible/demo-collapsible.component';
import { DemoDescriptionListComponent } from './components/demo-description-list/demo-description-list.component';
import { DemoDetailsComponent } from './components/demo-details/demo-details.component';
import { DemoItemComponent } from './components/demo-item/demo-item.component';
import { DemoMediaObjectComponent } from './components/demo-media-object/demo-media-object.component';
import { DemoPaginationComponent } from './components/demo-pagination/demo-pagination.component';
import { DemoSpinnerComponent } from './components/demo-spinner/demo-spinner.component';
import { DemoStatusItemComponent } from './components/demo-status-item/demo-status-item.component';
import { DemoStatusComponent } from './components/demo-status/demo-status.component';
import { DemoTabsComponent } from './components/demo-tabs/demo-tabs.component';
// desy-tables
import { DemoTableAdvancedComponent } from './components/demo-table-advanced/demo-table-advanced.component';
import { DemoTableComponent } from './components/demo-table/demo-table.component';
// desy-nav
import { DemoBreadcrumbsComponent } from './components/demo-breadcrumbs/demo-breadcrumbs.component';
import { DemoCardComponent } from './components/demo-card/demo-card.component';
import { DemoDatepickerComponent } from './components/demo-datepicker/demo-datepicker.component';
import { DemoErrorSummaryComponent } from './components/demo-error-summary/demo-error-summary.component';
import { DemoFooterComponent } from './components/demo-footer/demo-footer.component';
import { DemoHeaderAdvancedComponent } from './components/demo-header-advanced/demo-header-advanced.component';
import { DemoHeaderMiniComponent } from './components/demo-header-mini/demo-header-mini.component';
import { DemoHeaderComponent } from './components/demo-header/demo-header.component';
import { DemoLinksListComponent } from './components/demo-links-list/demo-links-list.component';
import { DemoMenuHorizontalComponent } from './components/demo-menu-horizontal/demo-menu-horizontal.component';
import { DemoMenuNavigationComponent } from './components/demo-menu-navigation/demo-menu-navigation.component';
import { DemoMenuVerticalComponent } from './components/demo-menu-vertical/demo-menu-vertical.component';
import { DemoMenubarComponent } from './components/demo-menubar/demo-menubar.component';
import { DemoNavComponent } from './components/demo-nav/demo-nav.component';
import { DemoNotificationComponent } from './components/demo-notification/demo-notification.component';
import { DemoSkipLinkComponent } from './components/demo-skip-link/demo-skip-link.component';
import { DemoTooltipComponent } from './components/demo-tooltip/demo-tooltip.component';



const routes: Routes = [
  { path: 'componentes', component: MenuComponentsComponent },

  // desy-buttons
  { path: 'button', component: DemoButtonComponent },
  { path: 'button-loader', component: DemoButtonLoaderComponent },
  { path: 'pill', component: DemoPillComponent },
  { path: 'listbox', component: DemoListboxComponent },
  { path: 'dropdown', component: DemoDropdownComponent },
  { path: 'toggle', component: DemoToggleComponent},

  //  desy-forms
  { path: 'hint', component: DemoHintComponent },
  { path: 'label', component: DemoLabelComponent },
  { path: 'error-message', component: DemoErrorMessageComponent },
  { path: 'fieldset', component: DemoFieldsetComponent },
  { path: 'textarea', component: DemoTextareaComponent },
  { path: 'character-count', component: DemoCharacterCountComponent },
  { path: 'input', component: DemoInputComponent },
  { path: 'select', component: DemoSelectComponent },
  { path: 'file-upload', component: DemoFileUploadComponent },
  { path: 'input-group', component: DemoInputGroupComponent },
  { path: 'radios', component: DemoRadiosComponent },
  { path: 'checkboxes', component: DemoCheckboxesComponent },
  { path: 'date-input', component: DemoDateInputComponent },
  { path: 'search-bar', component: DemoSearchBarComponent },
  { path: 'tree', component: DemoTreeComponent },
  { path: 'datepicker', component: DemoDatepickerComponent },

  // desy-modals
  { path: 'modal', component: DemoModalComponent },
  { path: 'dialog', component: DemoDialogComponent },

   // desy-views
  { path: 'alert', component: DemoAlertComponent },
  { path: 'accordion', component: DemoAccordionComponent },
  { path: 'accordion-history', component: DemoAccordionHistoryComponent },
  { path: 'spinner', component: DemoSpinnerComponent },
  { path: 'details', component: DemoDetailsComponent },
  { path: 'description-list', component: DemoDescriptionListComponent },
  { path: 'collapsible', component: DemoCollapsibleComponent },
  { path: 'item', component: DemoItemComponent },
  { path: 'media-object', component: DemoMediaObjectComponent },
  { path: 'pagination', component: DemoPaginationComponent },
  { path: 'status', component: DemoStatusComponent },
  { path: 'status-item', component: DemoStatusItemComponent },
  { path: 'tabs', component: DemoTabsComponent },
  { path: 'tooltip', component: DemoTooltipComponent},
  { path: 'card', component: DemoCardComponent},

  // desy-tables
  { path: 'table', component: DemoTableComponent },
  { path: 'table-advanced', component: DemoTableAdvancedComponent },

  // desy-nav
  { path: 'breadcrumbs', component: DemoBreadcrumbsComponent },
  { path: 'error-summary', component: DemoErrorSummaryComponent },
  { path: 'footer', component: DemoFooterComponent },
  { path: 'header', component: DemoHeaderComponent },
  { path: 'header-mini', component: DemoHeaderMiniComponent },
  { path: 'header-advanced', component: DemoHeaderAdvancedComponent },
  { path: 'links-list', component: DemoLinksListComponent },
  { path: 'menu-horizontal', component: DemoMenuHorizontalComponent },
  { path: 'menu-vertical/:frag', component: DemoMenuVerticalComponent },
  { path: 'menu-vertical', component: DemoMenuVerticalComponent },
  { path: 'menubar', component: DemoMenubarComponent },
  { path: 'menu-navigation', component: DemoMenuNavigationComponent },
  { path: 'nav', component: DemoNavComponent },
  { path: 'notification', component: DemoNotificationComponent },
  { path: 'skip-link', component: DemoSkipLinkComponent },

  { path: '', redirectTo: 'componentes', pathMatch: 'full' },
  { path: '**', redirectTo: 'componentes', pathMatch: 'full' }
];

const routerOptions: ExtraOptions = {
    useHash: false,
    anchorScrolling: 'enabled',
    scrollPositionRestoration: 'top'
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
