import { Component, OnInit } from '@angular/core';
import { HeaderDropdownData } from 'desy-angular';
import { DESY_COMPONENTS } from './shared/utils/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  componentList = [];

  dropdownData: HeaderDropdownData = {
    text: 'Componentes',
    items: this.componentList
  };

  ngOnInit(): void {
    DESY_COMPONENTS.forEach(component => this.componentList.push({
      text: component.title,
      routerLink: `/${component.alias}`
    }));
    this.componentList.sort((a, b) => a.text > b.text ? 1 : a.text === b.text ? 0 : -1);
  }

}
