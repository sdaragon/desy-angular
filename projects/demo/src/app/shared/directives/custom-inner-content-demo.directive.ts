import {
  ComponentFactory,
  ComponentFactoryResolver,
  Directive,
  Injector,
  Input, OnChanges,
  SimpleChanges, TemplateRef,
  ViewContainerRef
} from '@angular/core';
import {ContentBaseDemoComponent} from '../components/content-base-demo/content-base-demo.component';
import {ContentBaseComponent} from '../../../../../desy-angular/src/lib/shared/components';

/**
 * Copia de desyCustomInnerContent.
 * Permite introducir contenido de forma dinámica en una posición determinada desde:
 * - component: Componente existente de tipo ContentBase
 * - html: texto html en formato string
 * - text: texto plano en formato string
 */
@Directive({
  selector: '[appCustomInnerContent]'
})
export class CustomInnerContentDemoDirective implements OnChanges {

  @Input() appCustomInnerContent: { component?: any|ContentBaseComponent, template?: TemplateRef<any>, html?: string, text?: string };

  private factory: ComponentFactory<ContentBaseDemoComponent>;

  constructor(private viewContainerRef: ViewContainerRef, private resolver: ComponentFactoryResolver, private injector: Injector) {
    this.factory = this.resolver.resolveComponentFactory(ContentBaseDemoComponent);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.viewContainerRef.clear();
    if (this.appCustomInnerContent.component && this.appCustomInnerContent.component instanceof ContentBaseDemoComponent) {

      // Se crea la vista embebida basándose en el contenido del ContentBase indicado.
      this.viewContainerRef.createEmbeddedView(this.appCustomInnerContent.component.getContent());
    } else if (this.appCustomInnerContent.template) {

      // Se crea la vista embebida utilizando directamente el template.
      this.viewContainerRef.createEmbeddedView(this.appCustomInnerContent.template);
    } else if (this.appCustomInnerContent.html) {

      // Se carga el contenido en un div temporal.
      const tmpWrapper = document.createElement('div');
      tmpWrapper.innerHTML = this.appCustomInnerContent.html;

      // Los nodos hijos del div se proyectan dentro de un componente ContentBase.
      const nodeList = [];
      tmpWrapper.childNodes.forEach(node => nodeList.push(node));
      const componentRef = this.factory.create(this.injector, [ nodeList ]);

      // Finalmente se crea la vista embebida basándose en el contenido del ContentBase creado.
      // Se podría hacer con un this.viewContainerRef.createComponent(), pero así se evita el tag envolvente del componente en el DOM
      this.viewContainerRef.createEmbeddedView(componentRef.instance.getContent());
    } else if (this.appCustomInnerContent.text) {

      // Se carga el contenido en un nodo de texto
      const textNode = document.createTextNode(this.appCustomInnerContent.text);

      // El nodo de texto se proyecta dentro de un componente ContentBase
      const componentRef = this.factory.create(this.injector, [[ textNode ]]);

      // Finalmente se crea la vista embebida basándose en el contenido del ContentBase creado.
      // Se podría hacer con un this.viewContainerRef.createComponent(), pero así se evita el tag envolvente del componente en el DOM
      this.viewContainerRef.createEmbeddedView(componentRef.instance.getContent());
    }
  }

}
