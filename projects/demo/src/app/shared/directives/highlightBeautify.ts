import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';
import hljs from 'highlight.js';
import beautify from 'js-beautify';

@Directive({
  selector: 'code[highlightBeautify]'
})
export class HighlightBeautifyDirective implements AfterViewInit {

  @Input('language') language: string;

  constructor(private elRef: ElementRef) { }

  ngAfterViewInit() {
    let code = this.elRef.nativeElement.textContent;
    let beautifiedCode = this.beautifyCode(code);
    this.elRef.nativeElement.textContent = beautifiedCode;
    hljs.highlightElement(this.elRef.nativeElement);
  }

  private beautifyCode(code: string): string {
    if (this.language === 'html') {
      return beautify.html(code, {
        indent_size: 2,
        max_preserve_newlines: 0,
        wrap_attributes: 'preserve'
      });
    } else if (this.language === 'typescript') {
      return beautify(code, {
        language: 'typescript',
        indent_size: 2
      });
    } else {
      return code;
    }
  }

}
