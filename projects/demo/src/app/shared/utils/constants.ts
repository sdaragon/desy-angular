export const DESY_COMPONENTS = [
  // desy-buttons
  { title: 'Button' , alias: 'button'},
  { title: 'Button loader' , alias: 'button-loader'},
  { title: 'Dropdown' , alias: 'dropdown'},
  { title: 'Listbox' , alias: 'listbox'},
  { title: 'Pill' , alias: 'pill'},
  { title: 'Toggle', alias: 'toggle'},
  // desy-forms
  { title: 'Hint' , alias: 'hint'},
  { title: 'Label' , alias: 'label'},
  { title: 'Error-message' , alias: 'error-message'},
  { title: 'Fieldset' , alias: 'fieldset'},
  { title: 'Textarea' , alias: 'textarea'},
  { title: 'Character count' , alias: 'character-count'},
  { title: 'Input' , alias: 'input'},
  { title: 'Select' , alias: 'select'},
  { title: 'File-upload' , alias: 'file-upload'},
  { title: 'Input-group' , alias: 'input-group'},
  { title: 'Radios' , alias: 'radios'},
  { title: 'Checkboxes' , alias: 'checkboxes'},
  { title: 'Date-input' , alias: 'date-input'},
  { title: 'SearchBar' , alias: 'search-bar'},
  { title: 'Tree' , alias: 'tree'},
  { title: 'Datepicker' , alias: 'datepicker'},
  // desy-views
  { title: 'Alert' , alias: 'alert'},
  { title: 'Accordion' , alias: 'accordion'},
  { title: 'Accordion-History' , alias: 'accordion-history'},
  { title: 'Spinner' , alias: 'spinner'},
  { title: 'Details' , alias: 'details'},
  { title: 'Description-list' , alias: 'description-list'},
  { title: 'Collapsible' , alias: 'collapsible'},
  { title: 'Item' , alias: 'item'},
  { title: 'Media-object' , alias: 'media-object'},
  { title: 'Pagination' , alias: 'pagination'},
  { title: 'Status' , alias: 'status'},
  { title: 'Status-item' , alias: 'status-item'},
  { title: 'Tabs' , alias: 'tabs'},
  { title: 'Tooltip' , alias: 'tooltip'},
  { title: 'Card' , alias: 'card'},

  // desy-modals
  { title: 'Modal' , alias: 'modal'},
  { title: 'Dialog' , alias: 'dialog'},

  // desy-tables
  { title: 'Table' , alias: 'table'},
  { title: 'Table-advanced' , alias: 'table-advanced'},

  // desy-nav
  { title: 'Breadcrumbs' , alias: 'breadcrumbs'},
  { title: 'Error-summary' , alias: 'error-summary'},
  { title: 'Footer' , alias: 'footer'},
  { title: 'Links-list' , alias: 'links-list'},
  { title: 'Header' , alias: 'header'},
  { title: 'Header-mini' , alias: 'header-mini'},
  { title: 'Header-advanced' , alias: 'header-advanced'},
  { title: 'Menu-horizontal' , alias: 'menu-horizontal'},
  { title: 'Menu-vertical' , alias: 'menu-vertical'},
  { title: 'Menu-navigation' , alias: 'menu-navigation'},
  { title: 'Menubar' , alias: 'menubar'},
  { title: 'Nav' , alias: 'nav'},
  { title: 'Skip-link' , alias: 'skip-link'},
  { title: 'Notification' , alias: 'notification'},
];
