import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DesyAngularModule } from 'desy-angular';
import { AccessibilityInputsComponent } from './components/accessibility-inputs/accessibility-inputs.component';
import { ContentBaseDemoComponent } from './components/content-base-demo/content-base-demo.component';
import { TitleExampleComponent } from './components/examples/title-example/title-example.component';
import { ImplementationInfoComponent } from './components/implementation-info/implementation-info.component';
import { InputBooleanComponent } from './components/input-boolean/input-boolean.component';
import { InputNumberComponent } from './components/input-number/input-number.component';
import { InputTextComponent } from './components/input-text/input-text.component';
import { ShowCodeComponent } from './components/show-code/show-code.component';
import { SpacedComponent } from './components/spaced/spaced.component';
import { TitleDemoComponent } from './components/title-demo/title-demo.component';
import { TitleParamsComponent } from './components/title-params/title-params.component';
import { CustomInnerContentDemoDirective } from './directives/custom-inner-content-demo.directive';
import { HighlightBeautifyDirective } from './directives/highlightBeautify';

@NgModule({
  declarations: [
    AccessibilityInputsComponent,
    ContentBaseDemoComponent,
    InputTextComponent,
    InputBooleanComponent,
    TitleDemoComponent,
    TitleParamsComponent,
    SpacedComponent,
    ImplementationInfoComponent,
    InputNumberComponent,
    ShowCodeComponent,

    CustomInnerContentDemoDirective,

    // Examples new components
    TitleExampleComponent,
    
    HighlightBeautifyDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DesyAngularModule,
  ],
  exports: [
    AccessibilityInputsComponent,
    ContentBaseDemoComponent,
    InputTextComponent,
    InputBooleanComponent,
    TitleDemoComponent,
    TitleParamsComponent,
    SpacedComponent,
    ImplementationInfoComponent,
    InputNumberComponent,
    ShowCodeComponent,

    CustomInnerContentDemoDirective,
    
    // Examples new components
    TitleExampleComponent,
    
    HighlightBeautifyDirective
  ]
})
export class SharedModule { }
