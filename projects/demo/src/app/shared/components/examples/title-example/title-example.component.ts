import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title-example',
  template: `<h4 *ngIf="title" class="mt-lg mb-2xl text-sm text-neutral-dark uppercase group">{{title}}</h4>
             <p *ngIf="subtitle" class="-mt-2xl mb-2xl text-sm text-neutral-dark">{{ subtitle }}</p>`
})
export class TitleExampleComponent {

  @Input() title: string
  @Input() subtitle: string

}
