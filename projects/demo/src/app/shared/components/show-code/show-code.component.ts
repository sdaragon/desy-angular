import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-show-code',
  templateUrl: './show-code.component.html'
})
export class ShowCodeComponent {

  @Input() codeHtml: string;
  @Input() codeTs: string;

  copyHtmlButtonText = 'Copiar código'
  copyHtmlCode(code) {
    this.copyHtmlButtonText = 'Copiado'
    navigator.clipboard.writeText(code)
    setTimeout(() => this.copyHtmlButtonText = 'Copiar código', 2000)
  }

  copyTsButtonText = 'Copiar código'
  copyTsCode(code) {
    this.copyTsButtonText = 'Copiado'
    navigator.clipboard.writeText(code)
    setTimeout(() => this.copyTsButtonText = 'Copiar código', 2000)
  }

}
