import {Component, TemplateRef, ViewChild} from '@angular/core';

/**
 * Copia del componente de la librería para proyectar contenido directamente desde texto o html
 */
@Component({
  selector: 'app-content-base',
  template: '<ng-template #contentTemplate><ng-content></ng-content></ng-template>'
})
export class ContentBaseDemoComponent {

  @ViewChild('contentTemplate', { static: true }) private content: TemplateRef<any>;

  public getContent(): TemplateRef<any> {
    return this.content;
  }
}
