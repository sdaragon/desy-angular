# README #

This README would normally document whatever steps are necessary to get your application up and running.

### ¿Que aplicación representa este repositorio? ###
* Aplicación para el desarrollo de la librería Angular con los componentes definidos en [desy-html](https://desy.aragon.es/)
* Este proyecto consta de la librería y una aplicación demo para testearla.
* [Información del proyecto](https://paega2.atlassian.net/wiki/spaces/AreaUsuariosIntegradores/pages/2996797605/3.-+DESY+-+Sistema+de+dise+o+del+Gobierno+de+Arag+n)

### ¿Como comenzar con la aplicación? ###
* Para descargar el proyecto haz un git clone https://user@bitbucket.org/sdaragon/desy-angular.git
* Es necesario disponer de Angular Cli y NodeJS para poder desplegar el proyecto
* Recomendamos el uso de Visual Studio Code para el desarrollo.
* Primero, se instala las dependencias del proyecto usando `npm install`
* Luego, se generar la librería desy-angular `npm run build:lib`
* Para ejecutar la aplicación demo, usa el comando `ng serve`. Ir a `http://localhost:4200/`. La aplicación se recargará automáticamente si cambia alguno de los archivos de su proyecto.


# DesyAngular lib
This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18.

## Code scaffolding
Run `ng generate component component-name --project desy-angular` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project desy-angular`.
> Note: Don't forget to add `--project desy-angular` or else it will be added to the default project in your `angular.json` file. 

## Build
Run `npm run build:lib` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing
After building your library with `npm run build:lib`, go to the dist folder `cd dist/desy-angular` and run `npm publish`.

## Running unit tests
Run `ng test desy-angular` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
